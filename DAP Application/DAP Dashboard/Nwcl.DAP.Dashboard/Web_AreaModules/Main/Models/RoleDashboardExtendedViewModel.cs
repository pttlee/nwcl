﻿using Nwcl.DAP.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.Areas.Main.Models
{
    public class RoleDashboardExtendedViewModel : RoleDashboardViewModel
    {
        public string RoleName { get; set; }
        public string DashboardName { get; set; }

        [UIHint("DashboardDropDownGrid")]
        public new int DashboardId { get; set; }

        public List<DAL.Model.SystemMessage> SystemMessage { get; set; }
    }
}