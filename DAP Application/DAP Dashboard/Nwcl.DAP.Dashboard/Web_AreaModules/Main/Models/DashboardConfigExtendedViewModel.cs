﻿using Microsoft.PowerBI.Api.V2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.Areas.Main.Models
{
    public class DashboardConfigExtendedViewModel : Nwcl.DAP.Web.Common.Models.DashboardConfigModel
    {
        public EmbedToken EmbedToken { get; set; }

        public long MinutesToExpiration
        {
            get
            {
                var minutesToExpiration = (EmbedToken != null && EmbedToken.Expiration.HasValue ? EmbedToken.Expiration.Value : DateTime.UtcNow) 
                    - DateTime.UtcNow;
                return (long)minutesToExpiration.TotalMinutes;
            }
        }

        public string strToken { get; set; }

        public string ErrorMessage { get; set; }
    }
}