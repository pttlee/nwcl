﻿using Nwcl.DAP.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.Areas.Main.Models
{
    public class CompanyExtendedViewModel : CompanyViewModel
    {
        [UIHint("CityDropDownGrid")]
        public new string CityCode { get; set; }
        public string CurrentCityCode { get; set; }
        public string CurrentCityNameEn { get; set; }
        public string CurrentCityNameCn { get; set; }

        public List<DAL.Model.SystemMessage> SystemMessage { get; set; }
    }
}