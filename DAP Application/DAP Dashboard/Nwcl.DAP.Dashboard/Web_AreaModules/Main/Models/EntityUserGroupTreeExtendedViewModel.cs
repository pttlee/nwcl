﻿using Nwcl.DAP.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.Areas.Main.Models
{
    public class EntityUserGroupTreeExtendedViewModel
    {
        public string NodeCode { get; set; }
        public string NodeName { get; set; }
        public int level { get; set; }
        public bool isChecked { get; set; }
        public bool isExpanded { get; set; }

        public List<EntityUserGroupTreeExtendedViewModel> child { get; set; }
        public EntityUserGroupTreeExtendedViewModel parent { get; set; }

        public List<DAL.Model.SystemMessage> SystemMessage { get; set; }

        public string TemplateItemListJson()
        {
            List<EntityUserGroupTreeExtendedViewModel> child = this.child;

            if (child == null || child.Count() < 1)
            {
                //return String.Format("{{\"text\": \"{0}\", \"id\": \"{1}\"}}", this.NodeName.Trim(), this.NodeCode.Trim());
                return "[]";
            }
            else
            {
                string json = "";

                //if (child.Count() == 1)
                //{
                //    json += "{text: \"" + child.ElementAt(i).NodeName + "\", items: [" + child.ElementAt(i).TemplateItemListJson() + "]}";
                //}

                for(int i = 0; i < child.Count(); i++)
                {
                    if (i == 0)
                    {
                        json += "[";
                    }
                    json += String.Format("{{\"text\": \"{0}\", \"items\": {1}, \"id\": \"{2}\"{3}}}"
                        , child.ElementAt(i).NodeName.Trim()
                        , child.ElementAt(i).TemplateItemListJson()
                        , child.ElementAt(i).NodeCode.Trim()
                        , child.Count() >= 1 ? ", \"expanded\": \"true\"" : "");
                    if (i != child.Count() - 1)
                    {
                        json += ",";
                    }
                    if (i == child.Count() - 1)
                    {
                        json += "]";
                    }
                }

                return json;
            }
        }
    }
}