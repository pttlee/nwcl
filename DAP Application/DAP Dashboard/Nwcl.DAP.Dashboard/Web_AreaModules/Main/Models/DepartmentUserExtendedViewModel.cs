﻿using Nwcl.DAP.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.Areas.Main.Models
{
    public class DepartmentUserExtendedViewModel : DepartmentUserViewModel
    {
        public new int deptID { get; set; }
    }
}