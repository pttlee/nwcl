﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Nwcl.DAP.Application.Service;
using Nwcl.DAP.DAL.Model;
using Nwcl.DAP.Web.Areas.Main.Models;
using Nwcl.DAP.Web.Common;
using Nwcl.DAP.Web.Common.Controllers;
using Nwcl.DAP.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Nwcl.DAP.Web.Common.Attributes;
using System.Configuration;
using Microsoft.PowerBI.Api.V2;
using Microsoft.PowerBI.Api.V2.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Threading.Tasks;
using Microsoft.Rest;
using log4net;

namespace Nwcl.DAP.Web.Areas.Main.Controllers
{
    public class HomeController : BaseController
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static string Username = ConfigurationManager.AppSettings["pbiUsername"];
        private static string Password = ConfigurationManager.AppSettings["pbiPassword"];
        private static string AuthorityUrl = ConfigurationManager.AppSettings["authorityUrl"];
        private static string ResourceUrl = ConfigurationManager.AppSettings["resourceUrl"];
        //private static string ApplicationId = ConfigurationManager.AppSettings["ApplicationId"];
        private static string ApiUrl = ConfigurationManager.AppSettings["apiUrl"];
        //private static string WorkspaceId = ConfigurationManager.AppSettings["workspaceId"];
        //private static string ReportId = ConfigurationManager.AppSettings["reportId"];

        [CheckAccessActionFilter("Home", "Viewer")]
        public ActionResult Index(string id)
        {
            ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.Dashboard);
            ViewData["SystemMessageCode"] = "Dashboard";
            string adLogonName = System.Web.HttpContext.Current.User.Identity.Name;

            //adLogonName = "nwcl\\kentyeung"; // FOR UAT Prearation Only

            int did = 0;
            string username = adLogonName;
            string WorkspaceId = "";
            string ReportId = "";
            //Tempoaray use the hardcoded object
            DashboardConfigExtendedViewModel result = new DashboardConfigExtendedViewModel();

            #region Get the Dashboard Object from database, Check Permission for the current user ...
            try
            {
                did = Convert.ToInt32(id);
                if (did > 0)
                {

                    var dashboardObj = ServiceProvider.GetDashboardByAdLogonNameAndId(did, adLogonName);
                    //When dashboard ready use the dashboard Object to pass the report id and workspace id to power bi
                    if (dashboardObj == null)
                    {
                        throw new Exception("Not enough permission.");
                    }
                    ReportId = dashboardObj.ReportId == null? "" : dashboardObj.ReportId.ToString();
                    WorkspaceId = dashboardObj.WorkspaceId == null? "" :dashboardObj.WorkspaceId.ToString();
                    string rlsRole = dashboardObj.RLSRoleName;

                    Tuple<EmbedToken, Dataset, Report> tokenResult = GenerateEmbedToken(ReportId, WorkspaceId, rlsRole);

                    EmbedToken tokenResponse = tokenResult.Item1;
                    Dataset datasets = tokenResult.Item2;
                    Report report = tokenResult.Item3;

                    // Generate Embed Configuration.
                    result.IsEffectiveIdentityRequired = datasets.IsEffectiveIdentityRequired;
                    result.IsEffectiveIdentityRolesRequired = datasets.IsEffectiveIdentityRolesRequired;
                    result.EmbedToken = tokenResponse;
                    result.EmbedUrl = report.EmbedUrl;
                    result.strToken = tokenResponse.Token;
                    result.WorkspaceId = WorkspaceId;
                    result.Roles = rlsRole;
                    result.Id = report.Id;
                }
            }
            catch (Exception ex)
            {
                result.ErrorMessage = ex.Message;
                GetHyperLink(did, adLogonName);
                return View(result);
            }
            #endregion

            GetHyperLink(did, adLogonName);
            return View(result);
        }

        private void GetHyperLink(int id, string adLogonName)
        {
            
            var dashboardList = ServiceProvider.GetDashboardListByADLogonName(adLogonName);
            string hyperLists = "";
            for (int i = 0; i < dashboardList.Count; i++)
            {
                string name = DAP.Common.CultureHelper.GetCurrentCultureFieldLanguage().ToUpper().Equals(Nwcl.DAP.Common.Constants.FIELD_LANGUAGE_EN) ? dashboardList[i].DashboardNameEN : dashboardList[i].DashboardNameCN;
                hyperLists += "<a href='#' onclick=\"ShowDashboard('" + dashboardList[i].Id.ToString() + "')\" " + (dashboardList[i].Id == id ? "class=\"active\"" : "") + ">" + name + "</a>";
            }
            ViewData["DashboardURL"] = hyperLists;
        }

        private CommonServiceProvider _commonServiceProvider = null;
        private CommonServiceProvider ServiceProvider
        {
            get
            {
                if (this._commonServiceProvider == null)
                {
                    this._commonServiceProvider = (CommonServiceProvider)ServiceProviderFactory.GetServiceProvider(typeof(CommonServiceProvider));
                }
                return this._commonServiceProvider;
            }
        }

        private AuthenticationResult GetAuthenticationResult()
        {
            string passPhrase = ServiceProvider.GetParameter("Cryptography", "Config", "PassPhase").First().Value;

            Username = CryptographyServiceProvider.DecryptString(ServiceProvider.GetParameter("DashboardUserInfo", "Config", "Username").First().Value, passPhrase);
            Password = CryptographyServiceProvider.DecryptString(ServiceProvider.GetParameter("DashboardUserInfo", "Config", "Password").First().Value, passPhrase);
            var credential = new UserPasswordCredential(Username, Password);

            string ApplicationId = ServiceProvider.GetParameterSingle("PowerBIEmbed", "Config", "applicationId").Value;

            // Authenticate using created credentials
            var authenticationContext = new AuthenticationContext(AuthorityUrl);
            var authenticationResultTask = authenticationContext.AcquireTokenAsync(ResourceUrl, ApplicationId, credential);

            authenticationResultTask.Wait();

            AuthenticationResult authenticationResult = authenticationResultTask.Result;

            if (authenticationResultTask == null)
            {
                log.Error(string.Format("Cannot get AuthenticationResult. ResourceUrl: {0}, ApplicationId: {1}, Username: {2}", ResourceUrl, ApplicationId, Username));
                throw new Exception("Cannot authenticate through master account");
            }
            return authenticationResultTask.Result;
        }

        private Tuple<EmbedToken, Dataset, Report> GenerateEmbedToken(string ReportId, string WorkspaceId, string rlsRole)
        {
            string adLogonName = System.Web.HttpContext.Current.User.Identity.Name;

            //adLogonName = "nwcl\\kentyeung"; // FOR UAT Prearation Only

            string username = adLogonName;
            //Tempoaray use the hardcoded object
            #region Create credential for Power BI ...
            // Get Encrypted Username and Password from Database

            #endregion

            string ApplicationId = ServiceProvider.GetParameterSingle("PowerBIEmbed", "Config", "applicationId").Value;

            AuthenticationResult authenticationResult = GetAuthenticationResult();

            var tokenCredentials = new TokenCredentials(authenticationResult.AccessToken, "Bearer");

            // Create a Power BI Client object. It will be used to call Power BI APIs.
            using (var client = new PowerBIClient(new Uri(ApiUrl), tokenCredentials))
            {
                // Get a list of reports.
                var reportsTask = client.Reports.GetReportsInGroupAsync(WorkspaceId);

                reportsTask.Wait();

                var reports = reportsTask.Result;

                // No reports retrieved for the given workspace.
                if (reports.Value.Count() == 0)
                {
                    log.Error(string.Format("Cannot get report from Power BI. ReportId: {0}, WorkspaceId: {1}", ReportId, WorkspaceId));
                    throw new Exception("No reports were found in the workspace");
                }

                Report report;
                if (string.IsNullOrWhiteSpace(ReportId))
                {
                    // Get the first report in the workspace.
                    report = reports.Value.FirstOrDefault();
                }
                else
                {
                    report = reports.Value.FirstOrDefault(r => r.Id == ReportId);
                }

                if (report == null)
                {
                    log.Error(string.Format("Cannot get report from Power BI. ReportId: {0}, WorkspaceId: {1}", ReportId, WorkspaceId));
                    throw new Exception("No report with the given ID was found in the workspace.Make sure ReportId is valid.");
                }

                var datasetsTask = client.Datasets.GetDatasetByIdInGroupAsync(WorkspaceId, report.DatasetId);

                var datasets = datasetsTask.Result;
                //result.IsEffectiveIdentityRequired = datasets.IsEffectiveIdentityRequired;
                //result.IsEffectiveIdentityRolesRequired = datasets.IsEffectiveIdentityRolesRequired;
                GenerateTokenRequest generateTokenRequestParameters;
                // This is how you create embed token with effective identities

                if (!string.IsNullOrWhiteSpace(username))
                {
                    var rls = new EffectiveIdentity(username, new List<string> { report.DatasetId });
                    if (!string.IsNullOrWhiteSpace(rlsRole))
                    {
                        var rolesList = new List<string>();
                        rolesList.AddRange(rlsRole.Split(','));
                        rls.Roles = rolesList;
                    }

                    // Generate Embed Token with effective identities.
                    generateTokenRequestParameters = null;
                    generateTokenRequestParameters = new GenerateTokenRequest(accessLevel: "view", identities: new List<EffectiveIdentity> { rls });
                }
                else
                {
                    // Generate Embed Token for reports without effective identities.
                    generateTokenRequestParameters = new GenerateTokenRequest(accessLevel: "view");
                }

                var tokenResponseTask = client.Reports.GenerateTokenInGroupAsync(WorkspaceId, report.Id, generateTokenRequestParameters);

                tokenResponseTask.Wait();

                return Tuple.Create(tokenResponseTask.Result, datasets, report);
            }
        }

        [HttpPost]
        public JsonResult RefreshEmbedToken(string reportId, string groupId, string rlsRole)
        {
            try { 
                EmbedToken token = GenerateEmbedToken(reportId, groupId, rlsRole).Item1;

                return Json(new
                {
                    IsSuccess = true,
                    Message = "",
                    AccessToken = token.Token,
                    MinutesToExpiration = (long)(token.Expiration.Value - DateTime.UtcNow).TotalMinutes
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                log.Error(ex);

                return Json(new
                {
                    IsSuccess = false,
                    Message = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}