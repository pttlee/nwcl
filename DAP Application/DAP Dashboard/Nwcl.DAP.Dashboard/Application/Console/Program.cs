﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using Nwcl.DAP.Application.Service;

namespace Nwcl.DAP.Application.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            System.Console.Out.WriteLine("Running DAP AppServer Console ...");

            // Apply the Remoting configuration defined in the App.config
            
            CommonServiceProvider sp = new CommonServiceProvider();
            string passPharse = sp.GetParameter("Cryptography", "Config", "PassPhase").First().Value;

            System.Console.WriteLine("UserName: " + Nwcl.DAP.Application.Service.CryptographyServiceProvider.EncryptString("pbi@esystest.onmicrosoft.com", passPharse));
            System.Console.WriteLine("Password: " + Nwcl.DAP.Application.Service.CryptographyServiceProvider.EncryptString("!Pass111", passPharse));

            System.Console.WriteLine("UserName: " + Nwcl.DAP.Application.Service.CryptographyServiceProvider.DecryptString(Nwcl.DAP.Application.Service.CryptographyServiceProvider.EncryptString("pbi@esystest.onmicrosoft.com", passPharse), passPharse));
            System.Console.WriteLine("Password: " + Nwcl.DAP.Application.Service.CryptographyServiceProvider.DecryptString(Nwcl.DAP.Application.Service.CryptographyServiceProvider.EncryptString("!Pass111", passPharse), passPharse));


            System.Console.Out.WriteLine("[Press any key to terminate]");
            System.Console.In.ReadLine();

            

        }
    }
}
