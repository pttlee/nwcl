﻿using log4net;
using Nwcl.DAP.Common;
using Nwcl.DAP.DAL;
using Nwcl.DAP.DAL.DALManager;
using Nwcl.DAP.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Application.Service
{
    /// <summary>
    /// CommonServiceProvider to define the common operations
    /// </summary>
    public class CommonServiceProvider : MarshalByRefObject
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private CommonDALManager _commonDALManager = null;

        public CommonServiceProvider()
        {

        }

        #region Method
        /*
        /// <summary>
        /// Gets the regional office list
        /// </summary>
        /// <returns></returns>
        public List<RegionalOffice> GetRegionalOfficeList()
        {
            return this.CommonDALManager.GetRegionalOfficeList();
        }

        /// <summary>
        /// Gets the city list
        /// </summary>
        /// <returns></returns>
        public List<City> GetCityList()
        {
            return this.CommonDALManager.GetCityList();
        }

        /// <summary>
        /// Get Project by date range, to get the project record by filtering lastupdated
        /// For API use.
        /// /// Call by SCM batch program
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<Project> GetProjectByDateRange(DateTime startDate, DateTime endDate)
        {
            return this.CommonDALManager.GetProjectByDateRange(startDate, endDate);
        }

        /// <summary>
        /// Insert SCM Interface Log for API use
        /// </summary>
        /// <param name="scmInterface"></param>
        /// <returns></returns>
        public SCMInterface InsertSCMInterfaceLog(SCMInterface scmInterface)
        {
            return this.CommonDALManager.InsertSCMInterfaceLog(scmInterface);
        }


        /// <summary>
        /// Gets the status list
        /// </summary>
        /// <returns></returns>
        public List<Status> GetStatusList(int? TypeID=null)
        {
            return this.CommonDALManager.GetStatusList(TypeID);
        }

        /// <summary>
        /// Gets the status name by the status id and target culture
        /// </summary>
        /// <param name="statusId"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public string GetStatusName(int statusId, string culture)
        {
            return this.CommonDALManager.GetStatusName(statusId, this.ConvertToDBFieldLanguage(culture));
        }

        public Status GetStatusByCode(string code)
        {
            return this.CommonDALManager.GetStatusByCode(code);
        }

        public List<Project> GetProjectNameList()
        {
            return this.CommonDALManager.GetProjectNameList();
        }

        public List<Project> GetBudgetPlanProjectNameList()
        {
            return this.CommonDALManager.GetBudgetPlanProjectNameList();
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }

        public List<TenderPackage> GetTenderPackageByID(int id)
        {
            return this.CommonDALManager.GetTenderPackageByID(id);
        }

        public Project GetProjectByID(int id)
        {
            return this.CommonDALManager.GetProjectByID(id);
        }
        
        public Location GetLocationByID(int id)
        {
            return this.CommonDALManager.GetLocationByID(id);
        }

        public Unit GetUnitByID(int id)
        {
            return this.CommonDALManager.GetUnitByID(id);
        }

        public PackageItem GetPackageItemByID(int? id)
        {
            return this.CommonDALManager.GetPackageItemByID(id);
        }

        public Element GetCostCodeElement(int costCodeid)
        {
            return this.CommonDALManager.GetCostCodeElement(costCodeid);
        }

        public Trade GetCostCodeTrade(int costCodeid)
        {
            return this.CommonDALManager.GetCostCodeTrade(costCodeid);
        }

        public Trade GetCostCodeSubTrade(int costCodeid)
        {
            return this.CommonDALManager.GetCostCodeSubTrade(costCodeid);
        }


        /// <summary>
        /// Gets the company list
        /// </summary>
        /// <returns></returns>
        public List<Company> GetCompanyList()
        {
            return this.CommonDALManager.GetCompanyList();
        }

        public List<sp_GetMenuByUser_Result> GetMenuList(string userName, string languageCode)
        {
            return this.CommonDALManager.GetMenuList(userName, languageCode);
        }

        /// <summary>
        /// Gets the general cagetory list
        /// </summary>
        /// <returns></returns>
        public List<GeneralCategory> GetGeneralCategoriesList()
        {
            return this.CommonDALManager.GetGeneralCategoriesList();
        }

        /// <summary>
        /// Gets the general type list
        /// </summary>
        /// <returns></returns>
        public List<GeneralType> GetGeneralTypeList()
        {
            return this.CommonDALManager.GetGeneralTypeList();
        }
        public GeneralType GetGeneralTypeByCode(string code)
        {
            return this.CommonDALManager.GetGeneralTypeByCode(code);
        }



        /// <summary>
        /// Gets the user list
        /// </summary>
        /// <returns></returns>
        public List<SystemUser> GetUserList()
        {
            return this.CommonDALManager.GetUserList();
        }
        public List<sp_GetBreadcrumbsByPageID_Result> GetBreadcrubmByPageId(string pageId, string languageCode)
        {
            return this.CommonDALManager.GetBreadcrubmByPageId(pageId, languageCode);
        }

        /// <summary>
        /// Gets the department list
        /// </summary>
        /// <returns></returns>
        public List<Department> GetDepartmentList()
        {
            return this.CommonDALManager.GetDepartmentList();
        }

        /// <summary>
        /// Gets the team list
        /// </summary>
        /// <returns></returns>
        public List<Team> GetTeamList()
        {
            return this.CommonDALManager.GetTeamList();
        }

        /// <summary>
        /// Gets the section list
        /// </summary>
        /// <returns></returns>
        public List<Section> GetSectionList()
        {
            return this.CommonDALManager.GetSectionList();
        }


        public string GetElementNameById(int? id, string languageCode)
        {
            var temp = CommonDALManager.GetElement(id);
            return temp == null ? id.ToString() : languageCode.Equals(Common.Constants.CULTURE_INFO_CN, StringComparison.CurrentCultureIgnoreCase) || languageCode.Equals(Common.Constants.FIELD_LANGUAGE_CN, StringComparison.CurrentCultureIgnoreCase) ? temp.NameCN : temp.NameEN;
        }
        public string GetElementNameWithCodeById(int? id, string languageCode)
        {
            var temp = CommonDALManager.GetElement(id);
            return (temp == null
                ? id.ToString()
                : string.Format("({0}) {1}", temp.Code, languageCode.Equals(Common.Constants.CULTURE_INFO_CN, StringComparison.CurrentCultureIgnoreCase) || languageCode.Equals(Common.Constants.FIELD_LANGUAGE_CN, StringComparison.CurrentCultureIgnoreCase)
                    ? temp.NameCN : temp.NameEN));
        }
        public string GetTradeNameById(int id, string languageCode)
        {
            var temp = CommonDALManager.GetTrade(id);
            return temp == null ? id.ToString() : languageCode.Equals(Common.Constants.CULTURE_INFO_CN, StringComparison.CurrentCultureIgnoreCase) || languageCode.Equals(Common.Constants.FIELD_LANGUAGE_CN, StringComparison.CurrentCultureIgnoreCase) ? temp.NameCN : temp.NameEN;
        }
        public string GetTradeNameWithCodeById(int id, string languageCode)
        {
            var temp = CommonDALManager.GetTrade(id);
            return temp == null ?
                id.ToString()
                : string.Format("({0}) {1}", temp.Code, languageCode.Equals(Common.Constants.CULTURE_INFO_CN, StringComparison.CurrentCultureIgnoreCase) || languageCode.Equals(Common.Constants.FIELD_LANGUAGE_CN, StringComparison.CurrentCultureIgnoreCase)
                    ? temp.NameCN : temp.NameEN);
        }

        public string GetDevelopmentPurposeJson(bool isChinese)
        {
            var list = CommonDALManager.GetDevelopmentPurpose();
            var returnList = new List<string>();
            foreach (var item in list)
            {
                returnList.Add(string.Format("{{ \"value\":\"{0}\", \"text\":\"{1}\" }}", item.ID, isChinese ? item.NameCN : item.NameEN));
            }
            return string.Join(",", returnList);
        }

        public string GetPropertyTypeJson(bool isChinese)
        {
            return GeneratePropertyTypeJson(isChinese, CommonDALManager.GetPropertyTypeList());
        }
        private string GeneratePropertyTypeJson(bool isChinese, List<PropertyType> list, bool isParent = true)
        {
            var returnList = new List<string>();
            foreach (var item in list.Where(o => !isParent || !o.ParentID.HasValue))
            {
                var str = new List<string>();
                str.Add(string.Format("\"id\":\"{0}\"", item.ID));
                str.Add(string.Format("\"text\":\"{0}\"", isChinese ? item.NameCN : item.NameEN));
                str.Add(string.Format("\"relate\":\"{0}\"", item.DevelopmentPurposeID));
                if (list.Any(o => o.ParentID == item.ID))
                    str.Add(string.Format("\"items\":[{0}]", GeneratePropertyTypeJson(isChinese, list.Where(o => o.ParentID == item.ID).ToList(), false)));
                returnList.Add(string.Format("{{{0}}}", string.Join(",", str)));
            }
            return string.Join(",", returnList);
        }
        */

        /// <summary>
        /// Get Message from database instead of resource.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetMessageListByType(string type, string languageCode)
        {
            return this.CommonDALManager.GetMessageListByType(type, languageCode);
        }

        /// <summary>
        /// Get Breadcrumb
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        public List<sp_GetBreadcrumbsByPageID_Result> GetBreadcrubmByPageId(string pageId, string languageCode)
        {
            return this.CommonDALManager.GetBreadcrubmByPageId(pageId, languageCode);
        }

        public List<sp_GetMenuByUser_Result> GetMenuList(string userName, string languageCode)
        {
            return this.CommonDALManager.GetMenuList(userName, languageCode);
        }

        #region Masters (Region, City, Projects)        
        public List<MST_PROJ_CITY> GetCityList(bool? enableProxyCreation)
        {
            //bool checkPermission = new CheckPermission().ChcekPermission(username, controller, target);
            //if (checkPermission == true)
            return this.CommonDALManager.GetCityList(enableProxyCreation);
            //else
            //    throw new System.ArgumentException("AccessDenied");
        }

        public List<MST_PROJ_REGION> GetAllRegionCity(bool? enableProxyCreation)
        {
            return this.CommonDALManager.GetAllRegionCity(enableProxyCreation);
        }

        public List<MST_PROJ_PROJECT> GetProjectsByCity(bool? enableProxyCreation, string cityCode)
        {
            return this.CommonDALManager.GetProjectsByCity(enableProxyCreation, cityCode);
        }
        #endregion



        #region Entity Group

        public List<PRV_ENT_ENTGROUP> GetEntityGroupList(string username = "", string controller = "", string target = "")
        {
            //bool checkPermission = new CheckPermission().ChcekPermission(username, controller, target);
            //if (checkPermission == true)
            return this.CommonDALManager.GetEntityGroupList();
            //else
            //    throw new System.ArgumentException("AccessDenied");
        }

        public PRV_ENT_ENTGROUP AddEntityGroup(PRV_ENT_ENTGROUP model)
        {
            return this.CommonDALManager.InsertEntityGroup(model);
        }

        public PRV_ENT_ENTGROUP EditEntityGroup(PRV_ENT_ENTGROUP model)
        {
            return this.CommonDALManager.UpdateEntityGroup(model);
        }

        public PRV_ENT_ENTGROUP RemoveEntityGroup(PRV_ENT_ENTGROUP model)
        {
            return this.CommonDALManager.DeleteEntityGroup(model);
        }

        public int ValidateEntityGroupDelete(int id)
        {
            return this.CommonDALManager.ValidateEntityGroupDelete(id);
        }

        public int ValidateEntityGroupDuplicateName(string name, int id)
        {
            return this.CommonDALManager.ValidateEntityGroupDuplicateName(name, id);
        }

        public List<sp_GetEntityGroupTreeView_Result> GetEntityGroupTreeView()
        {
            return this.CommonDALManager.GetEntityGroupTreeView();
        }

        public List<sp_GetAllEntityTreeView_Result> GetAllEntityTreeView()
        {
            return this.CommonDALManager.GetAllEntityTreeView();
        }

        public List<string> GetParentEntityByEntityGroupId(int entityGroupId)
        {
            return this.CommonDALManager.GetParentEntityByEntityGroupId(entityGroupId);
        }

        public List<PRV_ENT_ENTGROUP_PROJ_RELN> GetEntityGroupProject()
        {
            return this.CommonDALManager.GetEntityGroupProject();
        }

        public PRV_ENT_ENTGROUP GetEntityGroupByEntityGroupId(int entityGroupId, bool? enableProxyCreation)
        {
            return this.CommonDALManager.GetEntityGroupByEntityGroupId(entityGroupId, enableProxyCreation);
        }

        public int CreateEntityGroupProject(int entityGroupId, string selectedCodes, string currentUserName)
        {
            return this.CommonDALManager.CreateEntityGroupProject(entityGroupId, selectedCodes, currentUserName);
        }

        public List<sp_GetDashboardByAdLogonName_Result> GetDashboardListByADLogonName(string logonName)
        {
            return this.CommonDALManager.GetDashboardListByADLogonName(logonName);
        }

        #region Entity Group User Group Role...
        public List<PRV_ENT_URG_ROL_RELN> GetEntityGroupUserGroupRoleList(string username = "", string controller = "", string target = "")
        {
            //bool checkPermission = new CheckPermission().ChcekPermission(username, controller, target);
            //if (checkPermission == true)
            return this.CommonDALManager.GetEntityGroupUserGroupRoleList();
            //else
            //    throw new System.ArgumentException("AccessDenied");
        }

        // avoid using this function if possible. too many joins
        public List<PRV_ENT_URG_ROL_RELN> GetPrivilegeDetailList(string username = "", string controller = "", string target = "")
        {
            //bool checkPermission = new CheckPermission().ChcekPermission(username, controller, target);
            //if (checkPermission == true)
            return this.CommonDALManager.GetPrivilegeDetailList();
            //else
            //    throw new System.ArgumentException("AccessDenied");
        }

        public List<PRV_ENT_URG_ROL_RELN> GetEntityGroupUserGroupRoleListByEntityGroupId(int entityGroupId, bool? enableProxyCreation)
        {
            //bool checkPermission = new CheckPermission().ChcekPermission(username, controller, target);
            //if (checkPermission == true)
            return this.CommonDALManager.GetEntityGroupUserGroupRoleListByEntityGroupId(entityGroupId, enableProxyCreation);
            //else
            //    throw new System.ArgumentException("AccessDenied");
        }

        public List<PRV_ENT_URG_ROL_RELN> GetEntityGroupUserGroupRoleListByUserGroupId(int userGroupId, bool? enableProxyCreation)
        {
            //bool checkPermission = new CheckPermission().ChcekPermission(username, controller, target);
            //if (checkPermission == true)
            return this.CommonDALManager.GetEntityGroupUserGroupRoleListByUserGroupId(userGroupId, enableProxyCreation);
            //else
            //    throw new System.ArgumentException("AccessDenied");
        }

        public List<PRV_ENT_URG_ROL_RELN> GetEntityGroupUserGroupRoleListByRoleId(int roleId, bool? enableProxyCreation)
        {
            //bool checkPermission = new CheckPermission().ChcekPermission(username, controller, target);
            //if (checkPermission == true)
            return this.CommonDALManager.GetEntityGroupUserGroupRoleListByRoleId(roleId, enableProxyCreation);
            //else
            //    throw new System.ArgumentException("AccessDenied");
        }

        public PRV_ENT_URG_ROL_RELN AddEntityGroupUserGroupRole(PRV_ENT_URG_ROL_RELN model)
        {
            return this.CommonDALManager.InsertEntityGroupGroupUserGroupRole(model);
        }

        public PRV_ENT_URG_ROL_RELN EditEntityGroupUserGroupRole(PRV_ENT_URG_ROL_RELN model)
        {
            return this.CommonDALManager.UpdateEntityGroupUserGroupRole(model);
        }

        public PRV_ENT_URG_ROL_RELN RemoveEntityGroupUserGroupRole(PRV_ENT_URG_ROL_RELN model)
        {
            return this.CommonDALManager.DeleteEntityGroupUserGroupRole(model);
        }

        public int ValidateEntityGroupUserGroupRole(int id)
        {
            return this.CommonDALManager.ValidateEntityGroupDelete(id);
        }
        #endregion

        #region UserGroup...        
        public List<PRV_URG_USRGROUP> GetUserGroupList(string username = "", string controller = "", string target = "")
        {
            //bool checkPermission = new CheckPermission().ChcekPermission(username, controller, target);
            //if (checkPermission == true)
            return this.CommonDALManager.GetUserGroupList();
            //else
            //    throw new System.ArgumentException("AccessDenied");
        }

        

        public PRV_URG_USRGROUP AddUserGroup(PRV_URG_USRGROUP model)
        {
            return this.CommonDALManager.InsertUserGroup(model);
        }

        public PRV_URG_USRGROUP EditUserGroup(PRV_URG_USRGROUP model)
        {
            return this.CommonDALManager.UpdateUserGroup(model);
        }

        public PRV_URG_USRGROUP RemoveUserGroup(PRV_URG_USRGROUP model)
        {
            return this.CommonDALManager.DeleteUserGroup(model);
        }

        public int ValidateUserGroup(int id)
        {
            return this.CommonDALManager.ValidateUserGroup(id);
        }

        public List<MST_PROJ_REGION> GetUserGroupTreeList()
        {
            return this.CommonDALManager.GetUserGroupTreeList();
        }

        

        public List<MST_PROJ_REGION> GetRegionTreeViewForUserGroup()
        {
            return this.CommonDALManager.GetRegionTreeViewForUserGroup();
        }

        public PRV_URG_USRGROUP GetUserGroupByUserGroupId(int userGroupId, bool? enableProxyCreation)
        {
            return this.CommonDALManager.GetUserGroupByUserGroupId(userGroupId, enableProxyCreation);
        }

        public List<PRV_URG_USER_GROUP_RELN> GetUserGroupUser()
        {
            return this.CommonDALManager.GetUserGroupUser();
        }
                

        public int CreateUserGroupUser(int userGroupId, string selectedCodes, string currentUserName)
        {
            return this.CommonDALManager.CreateUserGroupUser(userGroupId, selectedCodes, currentUserName);
        }

        
        #region Company        
        public List<PRV_URG_COMPANY> GetCompanyList(string username = "", string controller = "", string target = "")
        {
            //bool checkPermission = new CheckPermission().ChcekPermission(username, controller, target);
            //if (checkPermission == true)
            return this.CommonDALManager.GetCompanyList();
            //else
            //    throw new System.ArgumentException("AccessDenied");
        }

        public PRV_URG_COMPANY AddCompany(PRV_URG_COMPANY model)
        {
            return this.CommonDALManager.InsertCompany(model);
        }

        public PRV_URG_COMPANY EditCompany(PRV_URG_COMPANY model)
        {
            return this.CommonDALManager.UpdateCompany(model);
        }

        public PRV_URG_COMPANY RemoveCompany(PRV_URG_COMPANY model)
        {
            return this.CommonDALManager.DeleteCompany(model);
        }

        public int ValidateCompany(int id)
        {
            return this.CommonDALManager.ValidateCompany(id);
        }
        #endregion
        #endregion

        #region Role...
        public List<PRV_ROL_ROLE> GetRoleList(string username = "", string controller = "", string target = "")
        {
            //bool checkPermission = new CheckPermission().ChcekPermission(username, controller, target);
            //if (checkPermission == true)
            return this.CommonDALManager.GetRoleList();
            //else
            //    throw new System.ArgumentException("AccessDenied");
        }

        public PRV_ROL_ROLE GetRoleByRoleId(int roleId, bool? enableProxyCreation)
        {
            return this.CommonDALManager.GetRoleByRoleId(roleId, enableProxyCreation);
        }

        public PRV_ROL_ROLE AddRole(PRV_ROL_ROLE model)
        {
            return this.CommonDALManager.InsertRole(model);
        }

        public PRV_ROL_ROLE EditRole(PRV_ROL_ROLE model)
        {
            return this.CommonDALManager.UpdateRole(model);
        }

        public PRV_ROL_ROLE RemoveRole(PRV_ROL_ROLE model)
        {
            return this.CommonDALManager.DeleteRole(model);
        }

        public int ValidateRole(int id)
        {
            return this.CommonDALManager.ValidateRole(id);
        }

        public List<PRV_ROL_ROLE_DASHBOARD_RELN> GetRoleDashboardListByRoleId(int roleId, bool? enableProxyCreation)
        {
            //bool checkPermission = new CheckPermission().ChcekPermission(username, controller, target);
            //if (checkPermission == true)
            return this.CommonDALManager.GetRoleDashboardListByRoleId(roleId, enableProxyCreation);
            //else
            //    throw new System.ArgumentException("AccessDenied");
        }

        public PRV_ROL_ROLE_DASHBOARD_RELN AddRoleDashboard(PRV_ROL_ROLE_DASHBOARD_RELN model)
        {
            return this.CommonDALManager.InsertRoleDashboard(model);
        }

        public PRV_ROL_ROLE_DASHBOARD_RELN EditRoleDashboard(PRV_ROL_ROLE_DASHBOARD_RELN model)
        {
            return this.CommonDALManager.UpdateRoleDashboard(model);
        }

        public PRV_ROL_ROLE_DASHBOARD_RELN RemoveRoleDashboard(PRV_ROL_ROLE_DASHBOARD_RELN model)
        {
            return this.CommonDALManager.DeleteRoleDashboard(model);
        }

        public int ValidateRoleNameDuplicate(int roleId, string roleName)
        {
            return this.CommonDALManager.ValidateRoleNameDuplicate(roleId, roleName);
        }


        #region Dashboard
        public List<SYS_CFG_DASHBOARD> GetDashboardList(bool? enableProxyCreation)
        {
            //bool checkPermission = new CheckPermission().ChcekPermission(username, controller, target);
            //if (checkPermission == true)
            return this.CommonDALManager.GetDashboardList(enableProxyCreation);
            //else
            //    throw new System.ArgumentException("AccessDenied");
        }

        public sp_GetDashboardByAdLogonName_Result GetDashboardByAdLogonNameAndId(int id, string adLogonName)
        {
            return this.CommonDALManager.GetDashboardListByADLogonNameAndId(adLogonName, id);
        }

        public int ValidateRoleDashboardDuplicate(int dashboardId, int roleId)
        {
            return this.CommonDALManager.ValidateRoleDashboardDuplicate(dashboardId, roleId);
        }

        public int ValidateRoleEmpty(int roleId)
        {
            return this.CommonDALManager.ValidateRoleEmpty(roleId);
        }
        #endregion
        #endregion
        #endregion

        #region Upload Excel...
        public MST_PROJ_CITY AddCity(MST_PROJ_CITY model)
        {
            return this.CommonDALManager.InsertCity(model);
        }
        public MST_PROJ_REGION AddRegion(MST_PROJ_REGION model)
        {
            return this.CommonDALManager.InsertRegion(model);
        }
        public void AddProject(string cityCode, string projectCode, string projectPhase1, string projectPhase2,
                    string projectPhase3, string projectFullCode, string projectNameEN, string projectNameCN,
                    string remark, bool isFinancial, bool isSale, bool isPM, string keywords, string user)
        {
            CommonDALManager.InsertProject(cityCode, projectCode, projectPhase1, projectPhase2, projectPhase3,
                projectFullCode, projectNameEN, projectNameCN, remark, isFinancial, isSale, isPM, keywords, user);
        }
        #endregion

        #region Department...
        public List<PRV_URG_DEPARTMENT> GetDepartmentList(string username = "", string controller = "", string target = "")
        {
            return this.CommonDALManager.GetDepartmentList();            
        }

        public PRV_URG_DEPARTMENT GetDepartmentById(int Id, bool? enableProxyCreation)
        {
            return this.CommonDALManager.GetDepartmentById(Id, enableProxyCreation);
        }

        public PRV_URG_DEPARTMENT AddDepartment(PRV_URG_DEPARTMENT model)
        {
            return this.CommonDALManager.InsertDepartment(model);
        }

        public PRV_URG_DEPARTMENT EditDepartment(PRV_URG_DEPARTMENT model)
        {
            return this.CommonDALManager.UpdateDepartment(model);
        }

        public PRV_URG_DEPARTMENT RemoveDepartment(PRV_URG_DEPARTMENT model)
        {
            return this.CommonDALManager.DeleteDepartment(model);
        }

        public int ValidateDepartment(int id)
        {
            return this.CommonDALManager.ValidateDepartment(id);
        }

        public List<sp_GetUserByDepartmentId_Result> GetDepartmentUserByDepartmentId(int id)
        {
            return this.CommonDALManager.GetDepartmentUserByDepartmentId(id);
        }

        public void DeleteDepartmentUserByAdLogonNameAndDepartmentId(string adlogonName, int departmentId)
        {
            this.CommonDALManager.DeleteDepartmentUserByADLogonNameAndDepartmentId(adlogonName, departmentId);
        }

        public string AddUserToDepartment(string adlogonName, int departmentId, string currentUserName)
        {
            
            return this.CommonDALManager.AddUserToDepartment(adlogonName, departmentId, CultureHelper.GetCurrentCulture(), currentUserName);
        }

        #endregion
        #region Privilege
        public int ValidatePrivilegeDuplicate(int id, int entityGroupId, int userGroupId, int roleId)
        {
            return this.CommonDALManager.ValidatePrivilegeDuplicate(id, entityGroupId, userGroupId, roleId);
        }
        #endregion

        #endregion

        public List<SYS_CFG_PARAM> GetParameter(string funcGroup, string subFuncGroup, string key)
        {
            return this.CommonDALManager.GetParameter(funcGroup, subFuncGroup, key);

        }

        public List<sp_SearchUserByCriteria_Result> SearchUser(string domains, string criteria)
        {
            return this.CommonDALManager.SearchUser(domains, criteria);

        }

        #region Helper Method

        /// <summary>
        /// Converts the specified culture to the corresponding db field language
        /// </summary>
        /// <param name="culture"></param>
        /// <returns></returns>
        public DBFieldLanguage ConvertToDBFieldLanguage(string culture)
        {
            DBFieldLanguage dBFieldLanguage = DBFieldLanguage.EN;
            if (string.IsNullOrEmpty(culture))
            {
                culture = CultureHelper.GetCurrentCulture();
            }
            Enum.TryParse(CultureHelper.GetFieldLanguage(culture), true, out dBFieldLanguage);
            return dBFieldLanguage;
        }



        #endregion

        #region Property

        /// <summary>
        /// Gets the CommonDALManager object instance
        /// </summary>
        private CommonDALManager CommonDALManager
        {
            get
            {
                if (this._commonDALManager == null)
                {
                    this._commonDALManager = (CommonDALManager)DALManagerFactory.GetDALManager(typeof(CommonDALManager));
                }
                return this._commonDALManager;
            }
        }





        #endregion
    }
}
