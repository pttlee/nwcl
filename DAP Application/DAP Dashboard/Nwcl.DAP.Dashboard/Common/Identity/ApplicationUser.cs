﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Common.Identity
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            #region Claims

            userIdentity.AddClaim(new Claim("CompanyID", this.CompanyID == null ?this.CompanyID.ToString(): "0"));
            userIdentity.AddClaim(new Claim("RegionID", this.RegionID == null ? this.RegionID.ToString() : "0"));
            userIdentity.AddClaim(new Claim("CityID", this.CityID == null ? this.CityID.ToString() : "0"));
            userIdentity.AddClaim(new Claim("DepartmentID", this.DepartmentID == null ? this.DepartmentID.ToString() : "0"));
            userIdentity.AddClaim(new Claim("SectionID", this.SectionID == null ? this.SectionID.ToString() : "0"));
            userIdentity.AddClaim(new Claim("TeamID", this.TeamID == null ? this.TeamID.ToString() : "0"));
            userIdentity.AddClaim(new Claim("FirstNameCN", this.FirstNameCN ?? string.Empty));
            userIdentity.AddClaim(new Claim("LastNameCN", this.LastNameCN ?? string.Empty));
            userIdentity.AddClaim(new Claim("FirstNameEN", this.FirstNameEN ?? string.Empty));
            userIdentity.AddClaim(new Claim("LastNameEN", this.LastNameEN ?? string.Empty));
            userIdentity.AddClaim(new Claim("DisplayNameCN", this.DisplayNameCN ?? string.Empty));
            userIdentity.AddClaim(new Claim("DisplayNameEN", this.DisplayNameEN ?? string.Empty));
            #endregion

            return userIdentity;
        }

        #region Property



        public Nullable<int> CompanyID { get; set; }

        public Nullable<int> RegionID { get; set; }

        public Nullable<int> CityID { get; set; }

        public Nullable<int> DepartmentID { get; set; }

        public Nullable<int> SectionID { get; set; }

        public Nullable<int> TeamID { get; set; }

        public string DisplayNameCN { get; set; }

        public string DisplayNameEN { get; set; }

        public string FirstNameCN { get; set; }

        public string LastNameCN { get; set; }

        public string FirstNameEN { get; set; }

        public string LastNameEN { get; set; }

        public Nullable<bool> IsSSOEnabled { get; set; }

        public string ClientID { get; set; }

        public string ClientSecret { get; set; }

        #endregion
    }
}
