﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Common.Identity
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DAPIdentity", throwIfV1Schema: false)
        {
            // Disable Entity Framework Proxy Creation for Entity for Cross-Domain Serialization handling
            // Ref: https://msdn.microsoft.com/en-us/library/jj592886(v=vs.113).aspx
            Configuration.ProxyCreationEnabled = false;
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        /// <summary>
        /// Overrides this OnModelCreating method to make ASP.NET Identity Framework 2 adopts Database First Approach
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>().ToTable("SystemUsers");
            modelBuilder.Entity<IdentityRole>().ToTable("SystemRoles");
            modelBuilder.Entity<IdentityUserRole>().ToTable("SystemUserRoles");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("SystemUserClaims");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("SystemUserLogins");
        }
    }
}
