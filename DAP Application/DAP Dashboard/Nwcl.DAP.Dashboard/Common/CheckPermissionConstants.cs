﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Nwcl.DAP.Common
{
    public class CheckPermissionConstants
    {
        public enum MasterMaintenanceTarget
        {
            [Description("NewAdd")]
            btnAdd,
            [Description("OpenSearch")]
            btnSearch,
            [Description("btnEdit")]
            btnEdit,
            [Description("btnDelete")]
            btnDelete,
            [Description("btnExport")]
            btnExport,
        }
    }
}
