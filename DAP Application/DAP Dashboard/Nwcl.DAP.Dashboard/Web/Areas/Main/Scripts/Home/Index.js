﻿$(document).ready(function () {
    
    SetSplitter();

})

function SetSplitter() {
    $("#horizontal").kendoSplitter({
        panes: [
            { collapsible: true, size: "18%", },
            { collapsible: false, min: "950px" },
        ]
    });
}

function ShowDashboard(id) {
    location.href = contextRoot + '/Main/Home/index?id=' + id;
}

function generateEmbedToken(groupId, reportId, rlsRole) {
    var tokenResp = { MinutesToExpiration: 0, token: ""};
    $.ajax({
        url: contextRoot + "/Main/Home/RefreshEmbedToken",
        type: "post",
        data: { "groupId": groupId, "reportId": reportId, "rlsRole": rlsRole},
        async: false,
        success: function (dat) {
            if (dat.IsSuccess) {
                tokenResp.MinutesToExpiration = dat.MinutesToExpiration;
                tokenResp.token = dat.AccessToken;
            }
        },
        error: function (err) {
            //swal("error", err.Message, "error");
            //UnBlockUI();
            ;
        }
    });
    return tokenResp;
}


function setTokenExpirationListener(tokenExpiration,
    minutesToRefresh,
    groupId,
    reportId,
    rlsRole) {
    // get current time
    var currentTime = Date.now();
    var expiration = tokenExpiration * 60 * 1000;
    var safetyInterval = minutesToRefresh * 60 * 1000;

    // time until token refresh in milliseconds
    var timeout = expiration - safetyInterval;

    // if token already expired, generate new token and set the access token
    if (timeout <= 0) {
        console.log(printCurrentTime() + " - Updating Report Embed Token");
        updateToken(groupId, reportId, rlsRole);
    }
    // set timeout so minutesToRefresh minutes before token expires, token will be updated
    else {
        console.log(printCurrentTime() + " - Report Embed Token will be updated in " + timeout + " milliseconds.");
        setTimeout(function () {
            updateToken(groupId, reportId, rlsRole);
        }, timeout);
    }
}

function updateToken(groupId, reportId, rlsRole) {
    // Generate new EmbedToken
    var Token = generateEmbedToken(groupId, reportId, rlsRole);
            // Get a reference to the embedded report HTML element

    if (Token.token == "") {
        var retryCount = 5;
        for (var i = 0; i < retryCount; i++) {
            Token = generateEmbedToken(groupId, reportId, rlsRole); //retry due to session timeout?!...
            if (Token.token != "") {
                console.log("End retry. Retry Count = " + i + ", token = " + Token.token);
                i = retryCount;
            }
            else {
                console.log("retrying. Retry Count = " + i + ", token = " + Token.token);
            }
        }
    }

    if (Token.token == "") {
        swal({
            title: "",
            type: "warning",
            text: HomeResx.RefreshReportMsg,
            confirmButtonText: CommonResx.text_confirm,
        });
        UnBlockUI();
        return;
    }
    var embedContainer = $('#reportContainer')[0];

    // Get a reference to the embedded report.
    var report = powerbi.get(embedContainer);

    // Set AccessToken
    //alert(Token.token);
    console.log(printCurrentTime() + " - before token: " + report.getAccessToken());
    console.log(printCurrentTime() + " - New token: " + Token.token);
    report.setAccessToken(Token.token)
        .then(function (fulfill) {
            // Set token expiration listener
            // result.expiration is in ISO format
            //report.config.accessToken = Token.token;
            console.log(printCurrentTime() + " - after token: " + report.getAccessToken());

            setTokenExpirationListener(Token.MinutesToExpiration, 5 /*minutes before expiration*/,
                groupId, reportId, rlsRole);
        }, function (reason) {
            console.error("Rejected: " + reason);
        })
        .catch(function (response) {
            console.error(printCurrentTime() + " - setAccessTokenError: " + response.body);
        });
}

function printCurrentTime() {
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
        + (currentdate.getMonth() + 1) + "/"
        + currentdate.getFullYear() + " @ "
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds();
    return datetime;
}