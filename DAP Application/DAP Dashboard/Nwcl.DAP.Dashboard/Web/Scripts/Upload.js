﻿$(function () {

    $("#files").kendoUpload({
        async: {
            saveUrl: "/Upload/Save",
            removeUrl: "/Upload/Remove",
            autoUpload: true, //自动止传
            maxAutoRetries: 4,
        },
        multiple: false,
        validation: {
            allowedExtensions: [".xls", ".xlsx", ".doc", ".docx", ".ppt", ".pdf", ".txt", ".pptx"],
            maxFileSize: 31457280
        },
        localization: {
            invalidMaxFileSize: InvalidMaxFileSizeMessge,
            invalidFileExtension: InvalidFileExtension,
            select: SelectFileText,
        },
        complete: onUploadComplete,
        error: onUploadError,
        success: onUploadSuccess,
        upload: onUpload,
        select: selectFile
    });
});

function selectFile(e) {
    var name = e.files[0].name
    if (name.indexOf("@@") >= 0 || name.indexOf("#") >= 0 || name.indexOf("$") >= 0 || name.indexOf("%") >= 0 || name.indexOf("^") >= 0 || name.indexOf("&") >= 0) {
        UnBlockUI();
        //swal("@Resources.Resource_WarningMessage.FileNameVerification1 @@,#,$,%,^,& @Resources.Resource_WarningMessage.FileNameVerification2");

        var Message = FileNameVerification1 + ' @@,#,$,%,^,& ' + FileNameVerification2;

        swal(Message);

        e.preventDefault();
    }
}

function EmptyFunction() {

}

function onUploadSuccess(e) {

    UnBlockUI();

    if (e.operation == "upload") {

        var FileDescription = '';

        FileDescription = $("#FileDescription").val();
        $("#FileDescription").val('');

        $.ajax({
            type: "post",
            url: "/Upload/FileSave",
            data: {
                FileDescription: FileDescription,
                UploadFileType: '',
                gid: $("#GID").val()
            },
            cache: false,
            async: false,
            success: function (data) {

                if (data == "1") {
                    $(".k-upload-files.k-reset").find("li").remove();
                        $('#UploadFilesGrid').data('kendoGrid').dataSource.read();
                        $('#UploadFilesGrid').data('kendoGrid').refresh();

                } else {
                    $(".k-upload-files.k-reset").find("li").remove();
                    swal(data);
                }
            },
            error: function (msg) {
                alert("ERROR " + msg);

            }
        })
    }
}

function onUpload(e) {

    $.ajax({
        type: "post",
        url: "/ProjectCreation/Deletefies",
        data: {
            gnid: $("#GID").val()
        },
        cache: false,
        async: false,
        success: function (data) {
        },
        error: function (msg) {
            alert("ERROR " + msg);

        }
    })
}


function onUploadError(e) {

    UnBlockUI();
}

function onUploadComplete(e) {

    UnBlockUI();
}



function deleteFile(upbid) {

    $.ajax({
        type: "post",
        url: "/Upload/UploadFilesGridRemove",
        data: {
            fgid: upbid,
            GID: $("#GID").val(),
            UploadFileBy: ''
        },
        cache: false,
        async: false,
        success: function (data) {
            BindUploadFilesGrid();
        },
        error: function (msg) {
            alert("ERROR " + msg);
        }
    })
}

function deleteFileRequester(upbid) {

    $.ajax({
        type: "post",
        url: "/Upload/UploadFilesGridRemove",
        data: {
            fgid: upbid,
            GID: $("#GID").val(),
            UploadFileBy: 'Requester'
        },
        cache: false,
        async: false,
        success: function (data) {
            BindRequesterUploadFilesGrid();
        },
        error: function (msg) {
            alert("ERROR " + msg);
        }
    })
}


function deleteFileApprover(upbid) {

    $.ajax({
        type: "post",
        url: "/Upload/UploadFilesGridRemove",
        data: {
            fgid: upbid,
            GID: $("#GID").val(),
            UploadFileBy: 'Approver'
        },
        cache: false,
        async: false,
        success: function (data) {
            BindApproverUploadFilesGrid();
        },
        error: function (msg) {
            alert("ERROR " + msg);
        }
    })
}



//function BindFilesGrid() {


//    var UploadFileBy = $("#UploadFileBy").val();

//    //if ($('#UploadFilesGrid')) {
//    //    $('#UploadFilesGrid').data('kendoGrid').dataSource.read();
//    //    $('#UploadFilesGrid').data('kendoGrid').refresh();
//    //}

//    //if ($('#ApproverUploadFilesGrid')) {
//    //    $('#ApproverUploadFilesGrid').data('kendoGrid').dataSource.read();
//    //    $('#ApproverUploadFilesGrid').data('kendoGrid').refresh();
//    //}


//    //if (UploadFileBy === 'Requester')
//    //{
//    //    console.log('BindUploadFilesGrid');
//    //BindUploadFilesGrid();
//    //}
//    //else if (UploadFileBy === 'Approver')
//    //{
//    //    console.log('BindApproverUploadFilesGrid');
//    //BindApproverUploadFilesGrid();
//    //}
//}

function BindUploadFilesGrid() {

    var EnableUpload = $("#EnableUpload").val();

    var GID = $("#GID").val();

    var ColumnList = [{
        field: "FileName",
        filterable: false,
        title: FileNameText,
        width: 180
    }, {
        field: "FileDescription",
        filterable: false,
        title: FileDescriptionText,
        width: 200
    }, {
        field: "dCreatedDate",
        filterable: false,
        title: FileUploadTimeText,
        width: 180
    }];

    if (EnableUpload.toLowerCase() == "true") {
        ColumnList.push({
            title: FileOperationText,
            template: "<a href='/Upload/Download?Lfile= #: ResultPath#&nid=0'>" + FileDownloadText + "</a> # if (EnableDelete) { # <a onclick=deleteFile('#: UPbID #')>" + FileDeleteText + "</a> # } #",
            width: 82
        });
    }
    else {
        ColumnList.push({
            title: FileOperationText,
            template: "<a href='/Upload/Download?Lfile= #: ResultPath#&nid=0'>" + FileDownloadText + "</a>",
            width: 82
        });
    }

    //, UploadFileBy: "Requester"

    $("#UploadFilesGrid").kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: "/Upload/UploadedFilesRead",
                    dataType: "json",
                    data: function () { return { GID: GID , UploadFileBy: "" } },
                    cache: false
                }
            },
            schema: {
                model: {
                    fields: {
                        UPbID: { type: "string" },
                        FileName: { type: "string" },
                        FileDescription: { type: "string" },
                        //UploadFileType: { type: "string" },
                        //UploadFileTypeName: { type: "string" },
                        dCreatedDate: { type: "string" },
                        ResultPath: { type: "string" }
                    }
                }
            },
            //pageSize: 20,

        },
        scrollable: false,
        columns: ColumnList
    });
}

function BindRequesterUploadFilesGrid() {

    var EnableForm = $("#EnableForm").val();

    var GID = $("#GID").val();

    var ColumnList = [{
        field: "FileName",
        filterable: false,
        title: FileNameText,
        width: 180
    }, {
        field: "FileDescription",
        filterable: false,
        title: FileDescriptionText,
        width: 200
    }, {
        field: "dCreatedDate",
        filterable: false,
        title: FileUploadTimeText,
        width: 180
    }];

    ColumnList.push({
        title: FileOperationText,
        template: "<a href='/Upload/Download?Lfile= #: ResultPath#&nid=0'>" + FileDownloadText + "</a> # if (EnableDelete) { # <a onclick=deleteFileRequester('#: UPbID #')>" + FileDeleteText + "</a> # } #",
        width: 82
    });

    //if ($("#UploadFileBy").val() == "Approver") {
    //    ColumnList.push({
    //        title: FileOperationText,
    //        template: "<a href='/Upload/Download?Lfile= #: ResultPath#&nid=0'>" + FileDownloadText + "</a> # if (EnableDelete) { # <a onclick=deleteFileApprover('#: UPbID #')>" + FileDeleteText + "</a> # } #",
    //        width: 82
    //    });
    //}
    //else {
    //    ColumnList.push({
    //        title: FileOperationText,
    //        template: "<a href='/Upload/Download?Lfile= #: ResultPath#&nid=0'>" + FileDownloadText + "</a>",
    //        width: 82
    //    });
    //}

    $("#RequesterUploadFilesGrid").kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: "/Upload/UploadedFilesRead",
                    dataType: "json",
                    data: function () { return { GID: GID, UploadFileBy: "Requester" } },
                    cache: false
                }
            },
            schema: {
                model: {
                    fields: {
                        UPbID: { type: "string" },
                        FileName: { type: "string" },
                        FileDescription: { type: "string" },
                        //UploadFileType: { type: "string" },
                        //UploadFileTypeName: { type: "string" },
                        dCreatedDate: { type: "string" },
                        ResultPath: { type: "string" }
                    }
                }
            },
        },
        scrollable: false,
        columns: ColumnList
    });
}


function BindApproverUploadFilesGrid() {

    var EnableForm = $("#EnableForm").val();

    var GID = $("#GID").val();

    var ColumnList = [{
        field: "FileName",
        filterable: false,
        title: FileNameText,
        width: 180
    }, {
        field: "FileDescription",
        filterable: false,
        title: FileDescriptionText,
        width: 200
    }, {
        field: "dCreatedDate",
        filterable: false,
        title: FileUploadTimeText,
        width: 180
    }, {
        field: "SCompany",
        filterable: false,
        title: "公司",
        width: 80
    }, {
        field: "sDepartment",
        filterable: false,
        title: "部门",
        width: 80
    }, {
        field: "sPost",
        filterable: false,
        title: "职位",
        width: 80
    }, {
        field: "CreatedBy",
        filterable: false,
        title: "上传者",
        width: 80
    }, ];

    if ($("#UploadFileBy").val() == "Approver") {
        ColumnList.push({
            title: FileOperationText,
            template: "<a href='/Upload/Download?Lfile= #: ResultPath#&nid=0'>" + FileDownloadText + "</a> # if (EnableDelete) { # <a onclick=deleteFileApprover('#: UPbID #')>" + FileDeleteText + "</a> # } #",
            width: 82
        });
    }
    else {
        ColumnList.push({
            title: FileOperationText,
            template: "<a href='/Upload/Download?Lfile= #: ResultPath#&nid=0'>" + FileDownloadText + "</a>",
            width: 82
        });
    }




    $("#ApproverUploadFilesGrid").kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: "/Upload/UploadedFilesRead",
                    dataType: "json",
                    data: function () { return { GID: GID, UploadFileBy: "Approver" } },
                    cache: false
                }
            },
            schema: {
                model: {
                    fields: {
                        UPbID: { type: "string" },
                        FileName: { type: "string" },
                        FileDescription: { type: "string" },
                        //UploadFileType: { type: "string" },
                        //UploadFileTypeName: { type: "string" },
                        dCreatedDate: { type: "string" },
                        ResultPath: { type: "string" }
                    }
                }
            },
        },
        scrollable: false,
        columns: ColumnList
    });
}