﻿function ShowMessage(str) {
    $.blockUI({
        message: str, css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
    setTimeout($.unblockUI, 2000);
}
function ShowMessageNOTime(str) {
      $.blockUI({
        //message: str, css: {
        //    border: 'none',
        //    padding: '15px',
        //    backgroundColor: '#000',
        //    '-webkit-border-radius': '10px',
        //    '-moz-border-radius': '10px',
        //    opacity: .5,
        //    color: '#fff'
          //}
          message: '<img src="../../Images/progress.gif" />' + str, css: {
              border: 'none',
              padding: '15px',
              backgroundColor: '#000',
              '-webkit-border-radius': '10px',
              '-moz-border-radius': '10px',
              opacity: .5,
              color: '#fff'
          }
    });
  
}
