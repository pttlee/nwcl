﻿$.validator.addMethod('dategreaterthan',
    function (value, element, parameters) {

        var id = '#' + parameters['dependentproperty'];

        var control = $(id).data("kendoDatePicker");

        var startdate = control.value();

        var enddate = $(element).data("kendoDatePicker").value();

        if (startdate > enddate) {
            return false;
        }

        return true;
    }
);

$.validator.unobtrusive.adapters.add(
    'dategreaterthan',
    ['dependentproperty'],
    function (options) {
        options.rules['dategreaterthan'] = {
            dependentproperty: options.params['dependentproperty']
        };
        options.messages['dategreaterthan'] = options.message;
    });
