﻿//$(function () {
//    $("#SubmitBtn").click(function () {
//        submitRequest();

//    });
//});


var mode = "";


function onBudgetRequestEnd() {
    if (mode == "View") {
        DisableAll();
    }
}

function DisableAll() {

    mode = "View";
    // Disable all datepicker
    $(".k-calendar-container").each(function () {
        var elementId = this.id.split("_")[0];
        var tempDP = $("#" + elementId).data("kendoDatePicker");
        tempDP.enable(false);
    });

    // Now disable all DropDowns
    $(".k-list-container").each(function () {
        var elementId = this.id.split("-")[0];
        //if (elementId != "NatureID") {
        if (elementId != "") {
            var tempDL = $("#" + elementId).data("kendoDropDownList");
            tempDL.enable(false);
        }
        // }
        //if (elementId == "NatureID") {
        //    var tempDL = $("#" + elementId).data("kendoDropDownList");
        //    tempDL.readonly(true);
        //}
    });

    $(".k-textbox").each(function () {
        $(this).prop("disabled", true).addClass("k-state-disabled DisableControl");
    });

    $(".k-formatted-value").each(function () {
        $(this).addClass("DisableControl");
    });


    $(".k-numerictextbox").each(function () {

        var numerictextbox = $(this).find("[data-role='numerictextbox']").data("kendoNumericTextBox");
        //console.log(this);
        //console.log(numerictextbox);
        //var elementId = this.id.split("-")[0];
        //var numerictextbox = $("#" + elementId).data("kendoNumericTextBox");

        numerictextbox.enable();

        numerictextbox.enable(false);

    });

    $(".calBtn").each(function () {
        $(this).hide();
    });



    //var numerictextbox = $(".numerictextbox").data("kendoNumericTextBox");

    //numerictextbox.enable();

    //numerictextbox.enable(false);
}

function EnableAll() {
    mode = "Edit";

    // Disable all datepicker
    $(".k-calendar-container").each(function () {
        var elementId = this.id.split("_")[0];
        var tempDP = $("#" + elementId).data("kendoDatePicker");
        tempDP.enable(true);
    });

    // Now disable all DropDowns
    $(".k-list-container").each(function () {
        var elementId = this.id.split("-")[0];
        if (elementId != "") {
            var tempDL = $("#" + elementId).data("kendoDropDownList");
            tempDL.enable(true);
        }
    });

    $(".k-textbox").each(function () {
        $(this).prop("disabled", false).removeClass("k-state-disabled DisableControl");
    });

    $(".k-formatted-value").each(function () {
        $(this).removeClass("DisableControl");
    });


    $(".k-numerictextbox").each(function () {

        var numerictextbox = $(this).find("[data-role='numerictextbox']").data("kendoNumericTextBox");
        //console.log(this);
        //console.log(numerictextbox);
        //var elementId = this.id.split("-")[0];
        //var numerictextbox = $("#" + elementId).data("kendoNumericTextBox");

        numerictextbox.enable();

        numerictextbox.enable(true);

    });


    $(".calBtn").each(function () {
        $(this).show();
    });

    //var numerictextbox = $(".numerictextbox").data("kendoNumericTextBox");

    //numerictextbox.enable();

    //numerictextbox.enable(false);
}


function ChangeNewLine(text) {
    var regexp = new RegExp('\n', 'g');
    return text.replace(regexp, '<br>');
}

function SetMenuActive(target) {
    console.log("1");
    $(target).parent().addClass("menuActive");
    console.log("2");
}



function ParseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[0], mdy[1] - 1, mdy[2]);
}

function DayDiff(first, second) {
    return Math.round((second - first) / (1000 * 60 * 60 * 24)) + 1;
}








function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}


function AmountLtoU(num) {
    ///<summery>小写金额转化大写金额</summery>
    ///<param name=num type=number>金额</param>
    if (isNaN(num)) return "";
    var strPrefix = "";
    if (num < 0) strPrefix = "(负)";
    num = Math.abs(num);
    if (num >= 1000000000000) return "无效数值！";
    var strOutput = "";
    var strUnit = '仟佰拾亿仟佰拾万仟佰拾元角分';
    var strCapDgt = '零壹贰叁肆伍陆柒捌玖';
    num += "00";
    var intPos = num.indexOf('.');
    if (intPos >= 0) {
        num = num.substring(0, intPos) + num.substr(intPos + 1, 2);
    }
    strUnit = strUnit.substr(strUnit.length - num.length);
    for (var i = 0; i < num.length; i++) {
        strOutput += strCapDgt.substr(num.substr(i, 1), 1) + strUnit.substr(i, 1);
    }
    return strPrefix + strOutput.replace(/零角零分$/, '整').replace(/零[仟佰拾]/g, '零').replace(/零{2,}/g, '零').replace(/零([亿|万])/g, '$1').replace(/零+元/, '元').replace(/亿零{0,3}万/, '亿').replace(/^元/, "零元");
};


function SetBlockUI(Timespan) {
    App.blockUI({
        boxed: true
    });

    window.setTimeout(function () {
        App.unblockUI();
    }, Timespan);
}

function BlockUI() {
    App.blockUI({
        boxed: true
    });
}
function UnBlockUI() {
    App.unblockUI();
}
function fmoney(s, n) {
    n = n > 0 && n <= 20 ? n : 2;
    s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
    var l = s.split(".")[0].split("").reverse(), r = s.split(".")[1];
    t = "";
    for (i = 0; i < l.length; i++) {
        t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
    }
    return t.split("").reverse().join("") + "." + r;
}
var openmenu = "false";
var menuclicknumber = 0;


$(window).load(function (e) {
    //var openmenu = $("#openmenu").val()

    // How come first time need double click language bar to show drop down?
    $("#dropdown-flag").click();
    $("#dropdown-user").click();
    // Delete when it is no longer needed

    var l_varCookie = document.cookie;
    openmenu = get_cookie("openmenu");
    if (openmenu == "false" || openmenu == null || openmenu == "") {
        openmenu = "false";
        $.app.menu.collapse();
        $("body").attr("class", "vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-collapsed");
    } else {
        menuclicknumber = 1;
    }

}
)
function mainclick() {

    var date = new Date();
    var expiresDays = 1;
    date.setTime(date.getTime() + expiresDays * 24 * 3600 * 1000);

    if (openmenu == "false" || openmenu == "") {
        if (menuclicknumber == 0) {
            menuclicknumber = 1;
        } else {
            document.cookie = "openmenu=true; path=/;expires=" + date.toGMTString();
            openmenu = "true";
        }

    } else {
        document.cookie = "openmenu=false; path=/;expires=" + date.toGMTString();
        openmenu = "false";
    }


}
function get_cookie(Name) {
    var search = Name + "="
    var returnvalue = "";
    if (document.cookie.length > 0) {
        sd = document.cookie.indexOf(search);
        if (sd != -1) {
            sd += search.length;
            end = document.cookie.indexOf(";", sd);
            if (end == -1)
                end = document.cookie.length;
            returnvalue = unescape(document.cookie.substring(sd, end))
        }
    }
    return returnvalue;
}

function isChinese(s) {
    var patrn = /^[\u4E00-\u9FA5]+$/;
    if (!patrn.test(s)) {
        return false;
    }
    return true;
}

function isEnglish(s) {
    var patrn = new RegExp("^[a-zA-Z ]+$");
    if (!patrn.test(s)) {
        return false;
    } else {
        return true;
    }
} 

function getMsgTypeID(s)
{
    if (s == 'REQUIRED')
        return '#01 ';
    else if (s == 'FORMAT')
        return '#02 ';
    else if (s == 'SYSTEM')
        return '#03 ';
    else
        return '';
}



function openFullscreen() {
    var elem = document.documentElement;
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
}

function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    }
}


function isIE() {

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
    {
        return true;
    }
    else  // If another browser, return 0
    {
        return false;
    }
}