﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Web.Common
{
    /// <summary>
    /// Class the define the execution result of the controller action
    /// </summary>
    public class ExecutionResult
    {
        public ExecutionResult(bool success)
        {
            this.IsSuccess = success;
        }

        public ExecutionResult(bool success, string message)
        {
            this.IsSuccess = success;
            this.Message = message; 
        }

        /// <summary>
        /// Gets or sets whether the execution is success or not
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Gets or sets the additional message for the execution result
        /// </summary>
        public string Message { get; set; }
    }
}
