﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Web.Common.Bo
{
    public class GridHeaderAttributes
    {
        [DefaultValue("")]
        public string Class { get; set; }
        [DefaultValue("")]
        public string style { get; set; }
    }
}
