﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Web.Common.Bo
{
    public class GridFooterTemplate
    {
        public string Template { get; set; }
        public bool Isfunction { get; set; }
    }
}
