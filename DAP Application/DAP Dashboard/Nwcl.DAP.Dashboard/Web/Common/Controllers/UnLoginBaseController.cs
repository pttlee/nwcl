﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Nwcl.DAP.Common;
using System.Configuration;
using Nwcl.DAP.Application.Service;
using Microsoft.AspNet.Identity;
using System.ComponentModel;
using System.Reflection;
using System.Web.Routing;

namespace Nwcl.DAP.Web.Common.Controllers
{
    /// <summary>
    /// Class to define the Base MVC Controller with some common operations or handling required for the other controllers
    /// </summary>
    [Authorize]
    public class UnLoginBaseController : Controller
    {
        private ViewModelHelper _viewModelHelper = null;
        private CommonServiceProvider _commonServiceProvider = null;

        protected string GetCurrentController()
        {
            return (string)ControllerContext.RouteData.Values["controller"];
        }
        protected string GetCurrentArea()
        {
            return "Main";// (string)ControllerContext.RouteData.Values["area"];
        }

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string cultureName = null;

            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                cultureName = cultureCookie.Value;
            }
            else
            {
                cultureName = "CN";
                //    cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ?
                //            Request.UserLanguages[0] :  // obtain it from HTTP header AcceptLanguages
                //            null;

            }

            // Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe

            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            return base.BeginExecuteCore(callback, state);
        }

        /// <summary>
        /// Sets the specified culture and store the value in cookies
        /// </summary>
        /// <param name="culture"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        public ActionResult SetCulture(string culture, string returnUrl)
        {
            // Validate input
            culture = CultureHelper.GetImplementedCulture(culture);
            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
            {
                cookie.Value = culture;   // update cookie value
            }
            else
            {
                cookie = new HttpCookie("_culture");
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
            //return Redirect(returnUrl);

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }


        /// <summary>
        /// Helper methods to pass the action arguments in array of KeyValuePair<string, object> format 
        /// as dynamic data defined in ViewBag for CheckAccessActionFilter use
        /// </summary>
        /// <param name="args"></param>
        protected void PassArgumentsToCheckAccessActionFilter(params KeyValuePair<string, object>[] args)
        {
            if (args != null)
            {
                ViewBag.ActionArguments = args;
            }
        }

        /// <summary>
        /// Wrapper method to execute the specified action and return the execution result
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        protected ExecutionResult Execute(Action action)
        {
            ExecutionResult executionResult = null;

            if (action == null)
            {
                throw new ArgumentNullException("action", "Action to execute is not defined!");
            }

            try
            {
                action.Invoke();
                executionResult = new ExecutionResult(true);
            }
            catch (Exception ex)
            {
                executionResult = new ExecutionResult(false, ex.Message);
            }

            return executionResult;
        }

        protected string GetDescription(Enum value)
        {
            return value.GetDescription();
            //value
            //    .GetType()
            //    .GetMember(value.ToString())
            //    .FirstOrDefault()
            //    .GetCustomAttribute<DescriptionAttribute>()
            //    .Description;
        }

        #region Property

        /// <summary>
        /// Gets the ViewModelHelper for the Controller to do Entity Model and View Model Conversion
        /// </summary>
        protected ViewModelHelper ViewModelHelper
        {
            get
            {
                if (this._viewModelHelper == null)
                {
                    this._viewModelHelper = new ViewModelHelper();
                }
                return this._viewModelHelper;
            }
        }

        /// <summary>
        /// Gets the CommonServiceProvider for the controller
        /// </summary>
        protected CommonServiceProvider CommonServiceProvider
        {
            get
            {
                if (this._commonServiceProvider == null)
                {
                    this._commonServiceProvider = (CommonServiceProvider)ServiceProviderFactory.GetServiceProvider(typeof(CommonServiceProvider));
                }
                return this._commonServiceProvider;
            }
        }

        /// <summary>
        /// Gets the current user name of the authenticated user
        /// </summary>
        protected string CurrentUserName
        {
            get
            {
                return User.Identity.GetUserName();
            }
        }
        #endregion

        protected void SetFromID(DAP.Common.Constants.ScreenID screenID)
        {
            ViewData["FormID"] = screenID.GetDescription();
        }

        protected void SetSession(string guid, string name, object value)
        {
            Session[string.Format("{0}{1}", name, guid)] = value;
        }

        protected object GetSession(string guid, string name, bool delete = true)
        {
            var value = Session[string.Format("{0}{1}", name, guid)];
            if (delete)
                Session[string.Format("{0}{1}", name, guid)] = null;
            return value;
        }

        

        public bool IsChinese
        {
            get
            {
                var temp = CultureHelper.GetCurrentCulture().ToUpper();
                return !string.IsNullOrEmpty(temp) && (temp == DAP.Common.Constants.CULTURE_INFO_CN || temp == DAP.Common.Constants.FIELD_LANGUAGE_CN);
            }
        }

    }
}
