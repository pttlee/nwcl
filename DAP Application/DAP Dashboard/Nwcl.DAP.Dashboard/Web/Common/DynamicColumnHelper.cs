﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Nwcl.DAP.Web.Common.Bo;
using Nwcl.DAP.Common;

namespace Nwcl.DAP.Web.Common
{
    public class DynamicColumnHelper
    {
        public static string DateFormat = "dd/MM/yyyy";
        public static string DateTimeFormat = "dd/MM/yyyy HH:mm:ss";

        public string GenerateColumnJson(List<DataGridColumnAttributes> colAttr)
        {
            var dataStr = new StringBuilder("[");

            foreach (var col in colAttr)
            {
                List<string> dataArr = new List<string>();

                dataStr.Append("{");
                if (!string.IsNullOrEmpty(col.field))
                {
                    dataArr.Add(string.Format("\"{0}\":\"{1}\"", nameof(col.field), col.field));
                }
                if (!string.IsNullOrEmpty(col.format))
                {
                    dataArr.Add(string.Format("\"{0}\":\"{1}\"", nameof(col.format), col.format));
                }
                if (!string.IsNullOrEmpty(col.title))
                {
                    dataArr.Add(string.Format("\"{0}\":\"{1}\"", nameof(col.title), col.title));
                }
                if (!string.IsNullOrEmpty(col.width))
                {
                    dataArr.Add(string.Format("\"{0}\":\"{1}\"", nameof(col.width), col.width));
                }
                if (col.hidden != null)
                {
                    dataArr.Add(string.Format("\"{0}\":{1}", nameof(col.hidden), col.hidden.Value.ToString().ToLower()));
                }
                if (col.template != null)
                {
                    if (col.template.Isfunction)
                    {
                        dataArr.Add(string.Format("\"{0}\":{1}", nameof(col.template), col.template.Template));
                    }
                    else
                    {
                        dataArr.Add(string.Format("\"{0}\":\"{1}\"", nameof(col.template), col.template.Template));
                    }
                }
                if (col.headerTemplate != null)
                {
                    if (col.headerTemplate.Isfunction)
                    {
                        dataArr.Add(string.Format("\"{0}\":{1}", nameof(col.headerTemplate), col.headerTemplate.Template));
                    }
                    else
                    {
                        dataArr.Add(string.Format("\"{0}\":\"{1}\"", nameof(col.headerTemplate), col.headerTemplate.Template));

                    }
                }
                if (col.headerAttributes != null)
                {
                    dataArr.Add(string.Format("\"{0}\": {{ \"{1}\":\"{2}\", \"{3}\":\"{4}\" }}"
                        , nameof(col.headerAttributes)
                        , nameof(col.headerAttributes.Class)
                        , col.headerAttributes.Class
                        , nameof(col.headerAttributes.style)
                        , col.headerAttributes.style));
                }
                if (col.attributes != null)
                {
                    dataArr.Add(string.Format("\"{0}\": {{ \"{1}\":\"{2}\", \"{3}\":\"{4}\" }}"
                        , nameof(col.attributes)
                        , nameof(col.attributes.Class)
                        , col.attributes.Class
                        , nameof(col.attributes.style)
                        , col.attributes.style));
                }
                if (col.footerTemplate != null)
                {
                    if (col.footerTemplate.Isfunction)
                    {
                        dataArr.Add(string.Format("\"{0}\":{1}", nameof(col.footerTemplate), col.footerTemplate.Template));
                    }
                    else
                    {
                        dataArr.Add(string.Format("\"{0}\":\"{1}\"", nameof(col.footerTemplate), col.footerTemplate.Template));

                    }
                }
                dataStr.Append(string.Join(",", dataArr.ToArray()));
                dataStr.Append("},");
            }
            dataStr.Append("]");
            return dataStr.ToString();
        }

        public string GenerateDataJson(List<DataGridDataAttributes> dataAttr)
        {
            SortedDictionary<int, Dictionary<string, ValueAttributes>> dataDict = new SortedDictionary<int, Dictionary<string, ValueAttributes>>();

            foreach (var data in dataAttr)
            {
                Dictionary<string, ValueAttributes> valuePair = new Dictionary<string, ValueAttributes>();
                valuePair.Add(data.ColumnName, data.Value);

                if (dataDict.ContainsKey(data.RowNo))
                {
                    dataDict[data.RowNo].Add(data.ColumnName, data.Value);
                }
                else
                {
                    dataDict.Add(data.RowNo, valuePair);
                }
            }

            var dataStr = new StringBuilder("[");
            foreach (var data in dataDict)
            {
                dataStr.Append("{");
                dataStr.AppendFormat("\"id\":\"{0}\",", data.Key);

                List<string> dataArr = new List<string>();

                foreach (var item in data.Value)
                {
                    if (item.Value.Data != null)
                    {
                        switch (item.Value.DataType)
                        {
                            case DataType.Text:
                                dataArr.Add(string.Format("\"{0}\":\"{1}\"", item.Key, item.Value.Data.ToString()));
                                break;
                            case DataType.Integer:
                                dataArr.Add(string.Format("\"{0}\":{1}", item.Key, item.Value.Data.ToString().ToNullableInteger()));
                                break;
                            case DataType.Decimal:
                                dataArr.Add(string.Format("\"{0}\":{1}", item.Key, item.Value.Data.ToString().ToNullableDecimal()));
                                break;
                            case DataType.Boolean:
                                dataArr.Add(string.Format("\"{0}\":{1}", item.Key, Convert.ToBoolean(item.Value.Data) ? "true" : "false"));
                                break;
                            case DataType.Date:
                                dataArr.Add(string.Format("\"{0}\":\"{1}\"", item.Key, Convert.ToDateTime(item.Value.Data).ToString(DateFormat)));
                                break;
                            case DataType.DateTime:
                                dataArr.Add(string.Format("\"{0}\":\"{1}\"", item.Key, Convert.ToDateTime(item.Value.Data).ToString(DateTimeFormat)));
                                break;
                        }
                    }
                    else
                    {
                        dataArr.Add(string.Format("\"{0}\":null", item.Key));
                    }
                }
                dataStr.Append(string.Join(",", dataArr.ToArray()));
                dataStr.Append("},");
            }
            dataStr.Remove(dataStr.Length - 1, 1); //remove last ','
            dataStr.Append("]");
            return dataStr.ToString();
        }
    }
}
