﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Nwcl.DAP.Web.Common.Models
{
    /// <summary>
    /// Class to define the model required for the ResxJS partial view
    /// </summary>
    public class ResxJSModel
    {
        /// <summary>
        /// Gets and sets the ResxJS Javascript Object Name
        /// </summary>
        public string ResxJSObjName { get; set; }

        /// <summary>
        /// Gets and sets the fully qualified name of the .NET Resource
        /// </summary>
        public string ResxFullyQualifiedName { get; set; }

        /// <summary>
        /// Gets and sets the assembly of the .NET Resource
        /// </summary>
        public Assembly ResxAssembly { get; set; }
    }
}