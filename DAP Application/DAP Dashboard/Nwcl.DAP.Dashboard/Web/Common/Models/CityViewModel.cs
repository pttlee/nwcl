﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Web.Common.Models
{
    public  class CityViewModel
    {
        public int Id { get; set; }
        public string RegionCode { get; set; }
        public string CityCode { get; set; }
        public string CityNameEn { get; set; }
        public string CityNameCn { get; set; }
        public DateTime ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
    }
}
