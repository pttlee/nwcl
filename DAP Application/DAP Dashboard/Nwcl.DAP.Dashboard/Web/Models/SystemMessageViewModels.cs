﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.Models
{
    public class SystemMessageViewModels
    {
        public Dictionary<string,string> Message { get; set; }
    }
}