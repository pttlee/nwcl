﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.Models
{
    public class LeftMenuViewModels
    {
        public LeftMenuViewModels(){
            SubMenu = new List<LeftMenuViewModels>();
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> ParentID { get; set; }
        public Nullable<int> OrderSeq { get; set; }
        public string Path { get; set; }
        public string BeforeTitleIcon { get; set; }
        public string AfterTitleIcon { get; set; }
        public Nullable<bool> Status { get; set; }
        public List<LeftMenuViewModels> SubMenu { get; set; }
    }
}