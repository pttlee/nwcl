﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nwcl.DAP.Application.Service;
using Nwcl.DAP.Common;
using Nwcl.DAP.DAL.Model;
using Nwcl.DAP.Web.Common;
using Nwcl.DAP.Web.Common.Controllers;
using Nwcl.DAP.Web.Common.Models;
using Nwcl.DAP.Web.Models;
using Nwcl.DAP.Web.Common.Attributes;

namespace Nwcl.DAP.Web.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        [CheckAccessActionFilter("Home", "Viewer")]
        public ActionResult Index()
        {
            // Default page is Main/Home index Page
            return RedirectToAction("Index", "Home", new { Area = "Main" });
        }

        [CheckAccessActionFilter("Home", "Viewer")]
        public ActionResult _LeftMenu()
        {
            var culture = CultureHelper.GetCurrentCulture();
            var list = CommonServiceProvider.GetMenuList(CurrentUserName, culture);
            var returnList = new List<LeftMenuViewModels>();
            foreach (var item in list.Where(o => !o.ParentID.HasValue))
            {
                var newItem = new LeftMenuViewModels();
                newItem.AfterTitleIcon = item.AfterTitleIcon;
                newItem.BeforeTitleIcon = item.BeforeTitleIcon;
                newItem.ID = item.ID.Value;
                newItem.Name = item.Name;
                newItem.OrderSeq = item.OrderSeq;
                newItem.ParentID = item.ParentID;
                newItem.Path = item.Path;
                newItem.Status = item.Status;
                newItem.SubMenu.AddRange(GenerateLeftMenuItems(list, newItem.ID));
                returnList.Add(newItem);
            }
            return PartialView(returnList);
        }

        private List<LeftMenuViewModels> GenerateLeftMenuItems(List<sp_GetMenuByUser_Result> list, int id)
        {
            var returnList = new List<LeftMenuViewModels>();
            foreach (var item in list.Where(o => o.ParentID.HasValue && o.ParentID.Value == id))
            {
                var newItem = new LeftMenuViewModels();
                newItem.AfterTitleIcon = item.AfterTitleIcon;
                newItem.BeforeTitleIcon = item.BeforeTitleIcon;
                newItem.ID = item.ID.Value;
                newItem.Name = item.Name;
                newItem.OrderSeq = item.OrderSeq;
                newItem.ParentID = item.ParentID;
                newItem.Path = item.Path;
                newItem.Status = item.Status;
                newItem.SubMenu.AddRange(GenerateLeftMenuItems(list, newItem.ID));
                returnList.Add(newItem);
            }
            return returnList;
        }

        [CheckAccessActionFilter("Home", "Viewer")]
        public ActionResult _Breadcrumb(string pageId, string title, string path)
        {
            var culture = CultureHelper.GetCurrentCulture();
            var list = CommonServiceProvider.GetBreadcrubmByPageId(pageId, culture);
            var returnList = new List<BreadcrumbViewModels>();
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    var newItem = new BreadcrumbViewModels();
                    newItem.AfterTitleIcon = item.AfterTitleIcon;
                    newItem.BeforeTitleIcon = item.BeforeTitleIcon;
                    newItem.Name = item.Name;
                    newItem.Path = item.Path;
                    newItem.Seq = item.Seq.Value;
                    returnList.Add(newItem);
                }
            }

            return PartialView(returnList.OrderByDescending(o => o.Seq).ToList());
        }

        [CheckAccessActionFilter("Home", "Viewer")]
        public ActionResult _SystemMessage(string function)
        {
            var model = new SystemMessageViewModels();
            model.Message = CommonServiceProvider.GetMessageListByType(function, Nwcl.DAP.Common.CultureHelper.GetCurrentCulture().ToLower());
            return PartialView(model);
        }

        [CheckAccessActionFilter("Home", "Viewer")]
        public JsonResult GetUserGroupList()
        {
            List<UserGroupViewModel> list =
                 ViewModelHelper.ConvertUserGroupListToViewModel(this.CommonServiceProvider.GetUserGroupList());
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [CheckAccessActionFilter("Home", "Viewer")]
        public JsonResult GetRoleList()
        {
            List<RoleViewModel> list =
                 ViewModelHelper.ConvertRoleListToViewModel(this.CommonServiceProvider.GetRoleList());
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //[CheckAccessActionFilter("Home", "Viewer")]
        //public JsonResult GetRegionalOfficeList()
        //{
        //    List<RegionalOfficeViewModel> regionalOfficeViewModelList =
        //         ViewModelHelper.ConvertRegionalOfficeListToRegionalOfficeViewModelList(this.CommonServiceProvider.GetRegionalOfficeList());
        //    return Json(regionalOfficeViewModelList, JsonRequestBehavior.AllowGet);
        //}

        //[CheckAccessActionFilter("Home", "Viewer")]
        //public JsonResult GetCityList(int regionalOfficeID)
        //{
        //    List<CityViewModel> cityViewModelList =
        //        ViewModelHelper.ConvertCityListToCityViewModelList(this.CommonServiceProvider.GetCityList().Where(e => e.RegionalOfficeID == regionalOfficeID).ToList());
        //    return Json(cityViewModelList, JsonRequestBehavior.AllowGet);
        //}

        //[CheckAccessActionFilter("Home", "Viewer")]
        //public JsonResult GetStatusList(int? TypeID)
        //{
        //    List<StatusViewModel> statusViewModelList =
        //        ViewModelHelper.ConvertStatusListToStatusViewModelList(this.CommonServiceProvider.GetStatusList(TypeID));
        //    return Json(statusViewModelList, JsonRequestBehavior.AllowGet);
        //}

        //[CheckAccessActionFilter("Home", "Viewer")]
        //public JsonResult GetCompanyList(int regionalOfficeID, int cityID)
        //{
        //    List<CompanyViewModel> companyViewModelList =
        //        ViewModelHelper.ConvertCompanyListToCompanyViewModelList(this.CommonServiceProvider.GetCompanyList().Where(e => e.RegionalOfficeID == regionalOfficeID && e.CityID == cityID).ToList());
        //    return Json(companyViewModelList, JsonRequestBehavior.AllowGet);
        //}

        //[CheckAccessActionFilter("Home", "Viewer")]
        //public JsonResult GetDepartmentList(int regionalOfficeID, int cityID, int companyID)
        //{
        //    List<DepartmentViewModel> departmentViewModelList =
        //        ViewModelHelper.ConvertDepartmentListToDepartmentViewModelList(this.CommonServiceProvider.GetDepartmentList().Where(e => e.RegionalOfficeID == regionalOfficeID && e.CityID == cityID && e.CompanyID == companyID).ToList());
        //    return Json(departmentViewModelList, JsonRequestBehavior.AllowGet);
        //}

        //[CheckAccessActionFilter("Home", "Viewer")]
        //public JsonResult GetTeamList(int sectionID)
        //{
        //    List<TeamViewModel> teamViewModelList =
        //        ViewModelHelper.ConvertTeamListToTeamViewModelList(this.CommonServiceProvider.GetTeamList().Where(e => e.SectionID == sectionID).ToList());
        //    return Json(teamViewModelList, JsonRequestBehavior.AllowGet);
        //}

        //[CheckAccessActionFilter("Home", "Viewer")]
        //public JsonResult GetSectionList(int departmentID)
        //{
        //    List<SectionViewModel> sectionViewModelList =
        //        ViewModelHelper.ConvertSectionListToSectionViewModelList(this.CommonServiceProvider.GetSectionList().Where(e => e.DepartmentID == departmentID).ToList());

        //    return Json(sectionViewModelList, JsonRequestBehavior.AllowGet);
        //}

        //[CheckAccessActionFilter("Home", "Viewer")]
        //public JsonResult GetAllCompanyList()
        //{
        //    List<CompanyViewModel> companyViewModelList =
        //        ViewModelHelper.ConvertCompanyListToCompanyViewModelList(this.CommonServiceProvider.GetCompanyList().ToList());
        //    return Json(companyViewModelList, JsonRequestBehavior.AllowGet);
        //}

        //[CheckAccessActionFilter("Home", "Viewer")]
        //public JsonResult GetAllDepartmentList()
        //{
        //    List<DepartmentViewModel> departmentViewModelList =
        //        ViewModelHelper.ConvertDepartmentListToDepartmentViewModelList(this.CommonServiceProvider.GetDepartmentList().ToList());
        //    return Json(departmentViewModelList, JsonRequestBehavior.AllowGet);
        //}

        //[CheckAccessActionFilter("Home", "Viewer")]
        //public JsonResult GetAllTeamList()
        //{
        //    List<TeamViewModel> teamViewModelList =
        //        ViewModelHelper.ConvertTeamListToTeamViewModelList(this.CommonServiceProvider.GetTeamList().ToList());
        //    return Json(teamViewModelList, JsonRequestBehavior.AllowGet);
        //}

        //[CheckAccessActionFilter("Home", "Viewer")]
        //public JsonResult GetAllSectionList()
        //{
        //    List<SectionViewModel> sectionViewModelList =
        //        ViewModelHelper.ConvertSectionListToSectionViewModelList(this.CommonServiceProvider.GetSectionList().ToList());
        //    return Json(sectionViewModelList, JsonRequestBehavior.AllowGet);
        //}

        [CheckAccessActionFilter("Home", "Viewer")]
        public JsonResult GetNumberLevelList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            SelectListItem s = new SelectListItem { Value = "0", Text = "" };
            list.Add(s);
            for (int i = 1; i <= 50; i++)
            {
                s = new SelectListItem { Value = i.ToString(), Text = i.ToString() };
                list.Add(s);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [CheckAccessActionFilter("Home", "Viewer")]
        public JsonResult GetLetterLevelList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            SelectListItem s = new SelectListItem { Value = "0", Text = "" };
            list.Add(s);
            for (int i = 97; i < 123; i++)
            {
                string word = char.ConvertFromUtf32(i);
                s = new SelectListItem { Value = word.ToUpper(), Text = word.ToUpper() };
                list.Add(s);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}