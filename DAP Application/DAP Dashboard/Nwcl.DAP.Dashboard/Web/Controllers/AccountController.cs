﻿using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Nwcl.DAP.Common.Identity;
using Nwcl.DAP.Web.Common.Controllers;
using Nwcl.DAP.Web.Models;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Xml;

namespace Nwcl.DAP.Web.Controllers
{
    public class AccountController : UnLoginBaseController
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            return ProcessLogin(returnUrl, "");

        }

        [AllowAnonymous]
        public ActionResult LoginForm(int? errId)
        {
            //ViewBag.Msg_cn = "请登入";
            //ViewBag.Msg_en = "Please Login";

            ViewBag.Msg_cn = ErrMessage.getErrMsgCn(errId == null ? 0 : errId.Value);
            ViewBag.Msg_en = ErrMessage.getErrMsgEn(errId == null ? 0 : errId.Value);

            return View();
        }

        //
        // POST: /Account/LoginForm
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LoginForm(LoginViewModel model, int? errId)
        {
            //returnUrl = (string)Session["ContextRoot"];

            bool isUserAuthenticated = false;
            try
            {
                string[] parts = model.UserName.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                string domain = parts[0] != null ? parts[0].ToUpper() : "";
                string username = parts[1];
                string password = model.Password;
                isUserAuthenticated = IsUserAuthenticated(domain, username, password);
            }
            catch (Exception ex)
            {
                ViewBag.Msg_cn = "Incorrect user name or password";
                ViewBag.Msg_en = "用户或密码不正确";
                return View();
            }
            if (isUserAuthenticated)
            {
                return ProcessLogin(null, model.UserName);
            }
            else
            {
                ViewBag.Msg_cn = "Incorrect user name or password";
                ViewBag.Msg_en = "用户或密码不正确";
            }
            return View();
        }

        private bool IsUserAuthenticated(string domain, string username, string password)
        {
            bool isValid = false;
            // create a "principal context" - e.g. your domain (could be machine, too)
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, domain))
            {
                // validate the credentials
                isValid = pc.ValidateCredentials(username, password);
            }
            return isValid;
        }

        /// <summary>
        /// Endpoint to allow login to the system by windows SSO
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ProcessLogin(string returnUrl, string loginAsUserName)
        {
            string AD_UserName = "";
            string AD_NamePart = "";
            string AAS_COMPANY_CODE = "";
            string AAS_SYSTEM_ID = "";
            string AAS_UserRole = "";
            string errMessage = "";
            string dapPAPRequiredAASRole = "";
            bool hasAASUserRole = false;

            int errId = 0;

            try
            {
                if (loginAsUserName != null && loginAsUserName.Trim().Length > 0)
                {
                    AD_UserName = loginAsUserName;
                }
                else
                {
                    //AD_UserName = System.Web.HttpContext.Current.User.Identity.Name;
                    AD_UserName = Request.LogonUserIdentity.Name;
                }

                //AD_UserName = "nwcl\\kentyeung"; //For UAT Preparation Only

                //AD_UserName = Request.LogonUserIdentity.Name;
                string[] parts = AD_UserName.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts != null && parts.Length >= 2)
                {
                    //AAS_COMPANY_CODE = parts[0];
                    AD_NamePart = parts[1];
                }
                else
                {
                    AD_NamePart = AD_UserName;
                }


                //Getting DisplayName of Login User and Store in Session
                string userDisplayName = AD_UserName;
                try
                {
                    //userDisplayName = ActiveDirectoryHelper.GetUserDisplayNameFromAD(Request.LogonUserIdentity.Name);
                    userDisplayName = ActiveDirectoryHelper.GetUserDisplayNameFromAD(AD_UserName);

                    //userDisplayName = ActiveDirectoryHelper.GetUserDisplayNameFromAD(AD_UserName); //For UAT Preparation Only

                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
                Session["userDisplayName"] = userDisplayName;

                //System.Security.Principal.WindowsIdentity.GetCurrent().GetUserName();
                ViewData["adUserName"] = AD_UserName;

                try
                {
                    AAS_SYSTEM_ID = this.CommonServiceProvider.GetParameterSingle("Authen", "PAPDP", "AASSystemName").Value;
                    AAS_COMPANY_CODE = this.CommonServiceProvider.GetParameterSingle("Authen", "PAPDP", "AASCompanyCode").Value;
                    dapPAPRequiredAASRole = this.CommonServiceProvider.GetParameterSingle("Authen", "PAPDP", "AASRoleName").Value;
                }
                catch (Exception ex)
                {
                    errMessage = "Error getting AAS parameters from Configuration";
                    errId = 2;
                    log.Error(string.Format("Error getting AAS parameters from Configuration, AAS_SYSTEM_ID {0}, dapPAPRequiredAASRole {1}"
                            , AAS_SYSTEM_ID, dapPAPRequiredAASRole));
                    log.Error(ex);
                    throw ex;
                }

                try
                {
                    Application.Service.AASServiceProvider aassp = new Application.Service.AASServiceProvider("BasicHttpBinding_IAuthorization");
                    hasAASUserRole = aassp.CheckHasAASUserRoles(AD_UserName, AAS_COMPANY_CODE, AAS_SYSTEM_ID, dapPAPRequiredAASRole);
                }
                catch (Exception ex)
                {
                    errMessage = string.Format("AAS Execution failed for user name: {0}, company code: {1}, system id: {2} ",
                        AD_NamePart, AAS_COMPANY_CODE, AAS_SYSTEM_ID);
                    errId = 3;
                    log.Error(errMessage);
                    log.Error(ex);
                    throw ex;
                }

#if DEBUG
                hasAASUserRole = true; // Bypass AAS
#endif

                if (!hasAASUserRole)
                {
                    //return to fail page
                    //ViewBag.ReturnUrl = returnUrl;
                    errId = 4;
                    ViewBag.ReturnUrl = null;
                    RouteValueDictionary rvd = new RouteValueDictionary();
                    rvd.Add("errId", errId);
                    return RedirectToAction("LoginForm", "Account", rvd);
                }
                else
                {
                    //Login
                    Session["Logined"] = AAS_UserRole;
                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        returnUrl = Url.Action("Index", "Home");
                    }
                    return Redirect(returnUrl);
                    //return RedirectToAction("Index", "EntityGroup", new { Area = "Main" });
                    //return AutoLogin(Nwcl.DAP.Common.Constants.WINDOWS_LOGIN_PROVIDER_KEY, returnUrl, AD_UserName);
                }
            }
            catch (Exception e)
            {
                log.Error(e);
                ViewBag.ReturnUrl = returnUrl;
                RouteValueDictionary rvd = new RouteValueDictionary();
                rvd.Add("errId", errId);
                return RedirectToAction("LoginForm", "Account", rvd);
            }
        }

#region for Anthony Debug use...
            // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult LoginAnt(string returnUrl)
        {
            string AD_UserName = "";
            string AAS_COMPANY_CODE = "nwcl";       //hard code
            string AAS_SYSTEM_ID = "DAP";           //hard code
            string AAS_UserRole = "";
            string env = System.Configuration.ConfigurationManager.AppSettings["Environment"];
            try
            {
                AD_UserName = System.Web.HttpContext.Current.User.Identity.Name;
                //Request.LogonUserIdentity.Name;
                //System.Security.Principal.WindowsIdentity.GetCurrent().GetUserName();
                ViewData["adUserName"] = AD_UserName;
                if (AD_UserName.ToLower() == "dev\\administrator")
                {
                    AAS_UserRole = "Logined";

                }
                else
                    AAS_UserRole = "";

                if (AAS_UserRole == null || AAS_UserRole == "")
                {
                    //return to fail page
                    ViewBag.ReturnUrl = returnUrl;
                    return View();
                }
                else
                {
                    //Login
                    Session["Logined"] = AAS_UserRole;
                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        returnUrl = Url.Action("Index", "Home");
                    }
                    return RedirectToAction("Index", "Home", new { Area = "Main" });
                    //return AutoLogin(Nwcl.DAP.Common.Constants.WINDOWS_LOGIN_PROVIDER_KEY, returnUrl, AD_UserName);
                }
            }
            catch (Exception e)
            {
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }

        }
#endregion

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                // TODO: To be supported later
                //case SignInStatus.LockedOut:
                //    return View("Lockout");
                //case SignInStatus.RequiresVerification:
                //    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        /// <summary>
        /// Endpoint to allow login to the system by windows SSO
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult SSO(string returnUrl)
        {

            if (string.IsNullOrEmpty(returnUrl))
            {
                returnUrl = Url.Action("Index", "Home");
            }
            return ExternalLogin(Nwcl.DAP.Common.Constants.WINDOWS_LOGIN_PROVIDER_KEY, returnUrl);
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

#region Reserved Actions

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult AutoLogin(string provider, string returnUrl, string userId)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }), userId);
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    // special checking to see if the user signed in by external login provider ("Windows") has enabled its required property
                    switch (loginInfo.Login.LoginProvider)
                    {
                        case Nwcl.DAP.Common.Constants.WINDOWS_LOGIN_PROVIDER_KEY:
                            var user = await UserManager.FindByNameAsync(loginInfo.ExternalIdentity.Name);
                            if (!user.IsSSOEnabled.HasValue || !user.IsSSOEnabled.Value)
                            {
                                log.Warn("SSO is not enabled for this user! ");
                                return RedirectToAction("Login");
                            }
                            break;
                    }
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    //ViewBag.ReturnUrl = returnUrl;
                    //ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    //return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });

                    // NOTE: Create the user account directly if this does not exist currently
                    string email = string.IsNullOrEmpty(loginInfo.Email) ? Guid.NewGuid() + "@nwcl.com.hk" : loginInfo.Email;
                    var newUser = new ApplicationUser { UserName = loginInfo.ExternalIdentity.Name, Email = email, IsSSOEnabled = true };
                    var identityResult = await UserManager.CreateAsync(newUser);
                    if (identityResult.Succeeded)
                    {
                        identityResult = await UserManager.AddLoginAsync(newUser.Id, loginInfo.Login);
                        if (identityResult.Succeeded)
                        {
                            await SignInManager.SignInAsync(newUser, isPersistent: false, rememberBrowser: false);
                            return RedirectToLocal(returnUrl);
                        }
                    }
                    return RedirectToAction("Login");
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

#endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }


        internal class ErrMessage
        {
            private static Dictionary<int, string> errMsgCnDic = new Dictionary<int, string>(){
            {0, "请登入" },
            {1, "用户或密码不正确" },
            {2, "AAS配置不正确" },
            {3, "AAS服务错误" },
            {4, "用户AAS角色不正确" }
            };
            private static Dictionary<int, string> errMsgEnDic = new Dictionary<int, string>(){
            {0, "Please Login" },
            {1, "Incorrect user name or password" },
            {2, "Incorrect AAS configuration" },
            {3, "AAS Web Service Error" },
            {4, "Incorrect AAS Role for user" }
            };

            public static string getErrMsgEn(int key)
            {
                if (errMsgEnDic.ContainsKey(key))
                {
                    return errMsgEnDic[key];
                }
                else
                {
                    return errMsgEnDic[0];
                }
            }

            public static string getErrMsgCn(int key)
            {
                if (errMsgCnDic.ContainsKey(key))
                {
                    return errMsgCnDic[key];
                }
                else
                {
                    return errMsgCnDic[0];
                }
            }
        }
        #region Property

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

#endregion

#region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
#endregion
    }
}