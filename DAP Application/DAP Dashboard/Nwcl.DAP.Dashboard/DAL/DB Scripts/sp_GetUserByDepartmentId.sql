﻿USE [DAP_PAP]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetUserByDepartmentId]    Script Date: 11/28/2018 4:33:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[sp_GetUserByDepartmentId] 
@DepartmentId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT u.ADLogonName, u.ADDisplayName, u.ADDomain, u.ADRegion,u.ADCompany,u.ADDepartment,u.ADEmail, u.ADTitle FROM
	[dbo].[MST_URG_USR] u INNER JOIN [dbo].[PRV_URG_DEPARTMENT_USER_RELN] d ON u.ADLogonName = d.ADLogonName
	WHERE d.DepartmentId = @DepartmentId
	
END
GO


