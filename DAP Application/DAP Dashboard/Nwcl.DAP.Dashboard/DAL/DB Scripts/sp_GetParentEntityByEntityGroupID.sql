﻿
/****** Object:  StoredProcedure [dbo].[sp_GetParentEntityByEntityGroupID]    Script Date: 11/13/2018 10:54:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetParentEntityByEntityGroupID]
	-- Add the parameters for the stored procedure here
	@EntityGroupId int
AS
BEGIN
with projparent as (
select Isnull(prj.citycode, Isnull(prj1.citycode, Isnull(prj2.citycode, prj3.citycode))) as ParentCityCode
,  Isnull(prj.projectfullcode, Isnull(prj1.projectfullcode, Isnull(prj2.projectfullcode, prj3.projectfullcode))) as parentprojectfullcode0
,  Isnull(prj1.projectfullcode, Isnull(prj2.projectfullcode, prj3.projectfullcode)) as parentprojectfullcode1
,  Isnull(prj2.projectfullcode, prj3.projectfullcode) as parentprojectfullcode2
,  prj3.projectfullcode as parentprojectfullcode3
from [dbo].[PRV_ENT_ENTGROUP_PROJ_RELN] reln
left join [dbo].[MST_PROJ_PROJECT] prj3
on prj3.[ProjectFullCode] = reln.[ProjectFullCode]
left join [dbo].[MST_PROJ_PROJECT] prj2
on prj2.[ProjectFullCode] = prj3.parentprojectfullcode
left join [dbo].[MST_PROJ_PROJECT] prj1
on prj1.[ProjectFullCode] = prj2.parentprojectfullcode
left join [dbo].[MST_PROJ_PROJECT] prj
on prj.[ProjectFullCode] = prj1.parentprojectfullcode
where reln.EntityGroupId = @EntityGroupId
)
select distinct city.regioncode as parentcode
from projparent
left join [dbo].[MST_PROJ_CITY] city
on projparent.ParentCityCode = city.citycode
union
select distinct ParentCityCode
from projparent
union
select distinct p.parentcode from 
(
select distinct parentprojectfullcode0 as parentcode
from projparent
union
select distinct parentprojectfullcode1 as parentcode
from projparent
union
select distinct parentprojectfullcode2 as parentcode
from projparent
union
select distinct parentprojectfullcode3 as parentcode
from projparent
) p

END
GO