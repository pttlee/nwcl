﻿-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- exec [dbo].[sp_GetSelectedParentByUserGroupID] 1
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetSelectedParentByUserGroupID]
	-- Add the parameters for the stored procedure here
	@UserGroupId int
AS
BEGIN
with userparent as (
select Isnull(c.citycode, i.citycode) as ParentCityCode
,  'c'+Isnull(d.CompanyId, c.Id) as ParentCompany --0
,  'd'+Isnull(u.DepartmentId, d.Id) as ParentDepartment --1
,  'u'+u.Id as userCode --2
from [dbo].[PRV_URG_USER_GROUP_RELN] reln
left join [dbo].[PRV_URG_USER] u
on u.Id = reln.UserId
left join [dbo].[PRV_URG_DEPARTMENT] d
on d.[Id] = u.DepartmentId
left join [dbo].[PRV_URG_COMPANY] c
on c.Id = d.CompanyId
left join [dbo].[MST_PROJ_CITY] i
on i.CityCode = c.CityCode
where reln.UserGroupId = @UserGroupId
)
select distinct city.regioncode as parentcode
from userparent
left join [dbo].[MST_PROJ_CITY] city
on userparent.ParentCityCode = city.citycode
union
select distinct ParentCityCode
from userparent
union
select distinct p.parentcode from 
(
select distinct ParentCompany as parentcode
from userparent
union
select distinct ParentDepartment as parentcode
from userparent
union
select distinct userCode as parentcode
from userparent
) p
END
GO
