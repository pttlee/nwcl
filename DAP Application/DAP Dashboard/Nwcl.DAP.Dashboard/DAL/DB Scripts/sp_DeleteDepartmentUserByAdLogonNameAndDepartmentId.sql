﻿USE [DAP_PAP]
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteDepartmentUserByAdLogonNameAndDepartmentId]    Script Date: 11/27/2018 10:08:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_DeleteDepartmentUserByAdLogonNameAndDepartmentId]
	@DepartmentId INT,
	@ADLogonName NVARCHAR(200)
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM
	PRV_URG_DEPARTMENT_USER_RELN WHERE DepartmentId = @DepartmentId AND ADLogonName = @ADLogonName

END
