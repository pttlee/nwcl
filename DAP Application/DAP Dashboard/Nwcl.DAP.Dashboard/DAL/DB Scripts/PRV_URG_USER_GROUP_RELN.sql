﻿USE [DAP_PAP]
GO

/****** Object:  Table [dbo].[PRV_URG_USER_GROUP_RELN]    Script Date: 11/28/2018 3:48:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PRV_URG_USER_GROUP_RELN](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserGroupId] [int] NULL,
	[ModifiedDt] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ADLogonName] [nvarchar](200) NULL,
 CONSTRAINT [PK_PRV_URG_USER_GROUP_RELN_01] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PRV_URG_USER_GROUP_RELN]  WITH NOCHECK ADD  CONSTRAINT [Relationship23] FOREIGN KEY([ADLogonName])
REFERENCES [dbo].[MST_URG_USR] ([ADLogonName])
GO

ALTER TABLE [dbo].[PRV_URG_USER_GROUP_RELN] NOCHECK CONSTRAINT [Relationship23]
GO

ALTER TABLE [dbo].[PRV_URG_USER_GROUP_RELN]  WITH CHECK ADD  CONSTRAINT [Relationship24] FOREIGN KEY([UserGroupId])
REFERENCES [dbo].[PRV_URG_USRGROUP] ([Id])
GO

ALTER TABLE [dbo].[PRV_URG_USER_GROUP_RELN] CHECK CONSTRAINT [Relationship24]
GO


