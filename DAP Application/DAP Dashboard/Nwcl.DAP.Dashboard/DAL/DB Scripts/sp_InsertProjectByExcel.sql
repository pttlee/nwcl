﻿USE [DAP_PAP]
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertProjectByExcel]    Script Date: 11/14/2018 9:49:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_InsertProjectByExcel]
	@CityCode VARCHAR(10),
	@ProjectCode VARCHAR(10),
	@ProjectPhase1 VARCHAR(10),
	@ProjectPhase2 VARCHAR(10),
	@ProjectPhase3 VARCHAR(10),
	@ProjectFullCode VARCHAR(100),
	@ProjectNameEN NVARCHAR(100),
	@ProjectNameCN NVARCHAR(50),
	@Remark NVARCHAR(500),
	@IsFinancial BIT,
	@IsSale BIT,
	@IsPM BIT,
	@Keywords NVARCHAR(1000) = '',
	@User VARCHAR(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @cnt INT
	SELECT @cnt = COUNT(*) FROM MST_PROJ_PROJECT
	WHERE ProjectFullCode = @ProjectFullCode

	IF @cnt > 0
	BEGIN
		UPDATE MST_PROJ_PROJECT SET 
		CityCode = @CityCode,
		CityId = (SELECT Id From MST_PROJ_CITY WHERE CityCode = @CityCode),
		ProjectNameEN = @ProjectNameEN,
		ProjectNameCN = @ProjectNameCN,
		Remark = @Remark,
		IsFinancial = @IsFinancial,
		IsSale = @IsSale,
		IsPM = @IsPM,
		Keywords = @Keywords ,
		ModifiedBy = @User,
		ModifiedDt = GetDATE()
		WHERE
		ProjectFullCode = @ProjectFullCode

		
	END
	ELSE
	BEGIN

	INSERT INTO MST_PROJ_PROJECT
	(
		CityCode,
		CityId,
		ProjectCode,
		ProjectPhase1,
		ProjectPhase2,
		ProjectPhase3,
		ProjectFullCode,
		ProjectNameEN,
		ProjectNameCN,
		ProjectParentNameEN,
		ProjectParentNameCN,
		Remark,
		IsFinancial,
		IsSale,
		IsPM,
		Keywords,
		ParentID,
		ParentProjectFullCode,
		ModifiedDt,
		ModifiedBy
	)
	VALUES
	(
		@citycode,
		(SELECT Id From MST_PROJ_CITY WHERE CityCode = @CityCode),
		@ProjectCode,
		@ProjectPhase1,
		@ProjectPhase2,
		@ProjectPhase3,
		@ProjectFullCode,
		@ProjectNameEN,
		@ProjectNameCN,
		CASE 
			WHEN ISNULL(@ProjectPhase3,'') != ''
				THEN (SELECT x.ProjectNameEN FROM MST_PROJ_PROJECT x WHERE x.ProjectFullCode = @CityCode + '-' + @ProjectCode +@projectphase1+@ProjectPhase2) --Parent Level 2
			WHEN ISNULL(@ProjectPhase2,'') != '' AND ISNULL(@ProjectPhase3,'') = ''
				THEN (SELECT x.ProjectNameEN FROM MST_PROJ_PROJECT x WHERE x.ProjectFullCode = @CityCode + '-' + @ProjectCode +@projectphase1) --Parent Level 1
			WHEN ISNULL(@ProjectPhase1,'') != '' AND ISNULL(@ProjectPhase2,'') = '' AND ISNULL(@ProjectPhase3,'') =''
				THEN (SELECT x.ProjectNameEN FROM MST_PROJ_PROJECT x WHERE x.ProjectFullCode = @CityCode + '-' + @ProjectCode ) --Parent Level 0
		ELSE 
			NULL
		END
		,
		CASE 
			WHEN ISNULL(@ProjectPhase3,'') != ''
				THEN (SELECT x.ProjectNameCN FROM MST_PROJ_PROJECT x WHERE x.ProjectFullCode = @CityCode + '-' + @ProjectCode +@projectphase1+@ProjectPhase2) --Parent Level 2
			WHEN ISNULL(@ProjectPhase2,'') != '' AND ISNULL(@ProjectPhase3,'') = ''
				THEN (SELECT x.ProjectNameCN FROM MST_PROJ_PROJECT x WHERE x.ProjectFullCode = @CityCode + '-' + @ProjectCode +@projectphase1) --Parent Level 1
			WHEN ISNULL(@ProjectPhase1,'') != '' AND ISNULL(@ProjectPhase2,'') = '' AND ISNULL(@ProjectPhase3,'') =''
				THEN (SELECT x.ProjectNameCN FROM MST_PROJ_PROJECT x WHERE x.ProjectFullCode = @CityCode + '-' + @ProjectCode ) --Parent Level 0
		ELSE 
			NULL
		END
		,
		@Remark,
		@IsFinancial,
		@IsSale,
		@IsPM,
		@Keywords,
		CASE 
			WHEN ISNULL(@ProjectPhase3,'') != ''
				THEN (SELECT x.Id FROM MST_PROJ_PROJECT x WHERE x.ProjectFullCode = @CityCode + '-' + @ProjectCode +@projectphase1+@ProjectPhase2) --Parent Level 2
			WHEN ISNULL(@ProjectPhase2,'') != '' AND ISNULL(@ProjectPhase3,'') = ''
				THEN (SELECT x.Id FROM MST_PROJ_PROJECT x WHERE x.ProjectFullCode = @CityCode + '-' + @ProjectCode +@projectphase1) --Parent Level 1
			WHEN ISNULL(@ProjectPhase1,'') != '' AND ISNULL(@ProjectPhase2,'') = '' AND ISNULL(@ProjectPhase3,'') =''
				THEN (SELECT x.Id FROM MST_PROJ_PROJECT x WHERE x.ProjectFullCode = @CityCode + '-' + @ProjectCode ) --Parent Level 0
		ELSE 
			NULL
		END,
		CASE 
			WHEN ISNULL(@ProjectPhase3,'') != ''
				THEN (@CityCode + '-' + @ProjectCode +@projectphase1+@ProjectPhase2) --Parent Level 2
			WHEN ISNULL(@ProjectPhase2,'') != '' AND ISNULL(@ProjectPhase3,'') = ''
				THEN (@CityCode + '-' + @ProjectCode +@projectphase1) --Parent Level 1
			WHEN ISNULL(@ProjectPhase1,'') != '' AND ISNULL(@ProjectPhase2,'') = '' AND ISNULL(@ProjectPhase3,'') =''
				THEN (@CityCode + '-' + @ProjectCode ) --Parent Level 0
		ELSE 
			NULL
		END
		,
		GETDATE(),
		@User
	)
		
	END
	SELECT TOP 1 Id From MST_PROJ_PROJECT WHERE ProjectFullCode = @ProjectFullCode
END
