﻿USE [DAP_PAP]
GO
/****** Object:  StoredProcedure [dbo].[sp_SearchUserByCriteria]    Script Date: 11/27/2018 10:07:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [dbo].[sp_SearchUserByCriteria] 'NWCL;NWD;CC;EC;' , ''
ALTER PROCEDURE [dbo].[sp_SearchUserByCriteria] 
@DomainList VARCHAR(200),
@Criteria NVARCHAR(200) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	--DECLARE @DomainList VARCHAR(MAX)
	--SET @DomainList = 'SYG;WHN'
	
	--DECLARE @Criteria NVARCHAR(200) = 'Manager'

	SELECT u.ADLogonName, u.ADDisplayName, u.ADDomain, u.ADRegion,u.ADCompany,u.ADDepartment,u.ADEmail, u.ADTitle FROM
	[dbo].[MST_URG_USR] u 
	WHERE 
	(
		ADLogonName LIKE '%'+ISNULL(@Criteria, ADLogonName)+'%'
		OR ADDisplayName LIKE '%'+ISNULL(@Criteria, ADDisplayName)+'%'
		OR ADCompany LIKE '%'+ISNULL(@Criteria, ADCompany)+'%'
		OR ADDepartment LIKE '%'+ISNULL(@Criteria, ADDepartment)+'%'
		OR ADEmail LIKE '%'+ISNULL(@Criteria, ADEmail)+'%'
		OR ADTitle LIKE '%'+ISNULL(@Criteria, ADTitle)+'%'
	)
	AND ADDomain IN (SELECT [Value] from string_split (@DomainList, ';'))
	




END
