﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.DAL
{
    /// <summary>
    /// Enums to define the available DB Field Language
    /// </summary>
    public enum DBFieldLanguage
    {
        /// <summary>
        /// English db field language
        /// </summary>
        EN,

        /// <summary>
        /// Simplified Chinese db field language
        /// </summary>
        CN
    }
}
