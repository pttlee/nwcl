﻿using log4net;
using Nwcl.DAP.DAL.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.DAL.DALManager
{
    /// <summary>
    /// CommonDALManager to define the operations to manipulate DB objects in common module
    /// </summary>
    public class CommonDALManager
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CommonDALManager()
        {

        }

        #region Method



        public Dictionary<string, string> GetMessageListByType(string type, string languageCode)
        {
            //List<SystemMessage> messageList = new List<SystemMessage>();
            using (var DAPDB = this.DAPDB)
            {
                return DAPDB.SystemMessages.Where(e => e.ResourceType == type || e.ResourceType == "Common").ToDictionary(o => o.Name, k => languageCode == "en" || languageCode == "en-us" ? k.EN : k.CN);
            }
            //return messageList;
        }

        public List<sp_GetBreadcrumbsByPageID_Result> GetBreadcrubmByPageId(string pageId, string languageCode)
        {
            using (var DAPDB = this.DAPDB)
            {
                return DAPDB.sp_GetBreadcrumbsByPageID(pageId, languageCode).ToList();
            }
        }
        public List<sp_GetMenuByUser_Result> GetMenuList(string userName, string languageCode)
        {
            using (var DAPDB = this.DAPDB)
            {
                return DAPDB.sp_GetMenuByUser(userName, languageCode).ToList();
            }
        }

        #region Masters (Region, City, Projects)
        public List<MST_PROJ_CITY> GetCityList(bool? enableProxyCreation)
        {
            List<MST_PROJ_CITY> cityList = new List<MST_PROJ_CITY>();
            using (var DAPDB = this.DAPDB)
            {
                DAPDB.Configuration.ProxyCreationEnabled = enableProxyCreation == null ? true : enableProxyCreation.Value;
                cityList.AddRange(DAPDB.MST_PROJ_CITY.ToArray());
            }
            return cityList;
        }

        public List<MST_PROJ_REGION> GetAllRegionCity(bool? enableProxyCreation)
        {
            List<MST_PROJ_REGION> treeView = new List<MST_PROJ_REGION>();
            using (var DAPDB = this.DAPDB)
            {
                DAPDB.Configuration.ProxyCreationEnabled = enableProxyCreation == null ? true : enableProxyCreation.Value;
                treeView = DAPDB.MST_PROJ_REGION.Include(o => o.MST_PROJ_CITY)
                    .OrderBy(o => o.RegionCode).ToList();
            }
            return treeView;
        }

        public List<MST_PROJ_PROJECT> GetProjectsByCity(bool? enableProxyCreation, string cityCode)
        {
            List<MST_PROJ_PROJECT> treeView = new List<MST_PROJ_PROJECT>();
            using (var DAPDB = this.DAPDB)
            {
                DAPDB.Configuration.ProxyCreationEnabled = enableProxyCreation == null ? true : enableProxyCreation.Value;
                treeView = DAPDB.MST_PROJ_PROJECT.Where(o => o.CityCode.Equals(cityCode)).OrderBy(o => o.ProjectFullCode).ToList();
            }
            return treeView;
        }
        #endregion

        #region Entity Group...
        private const int existReturnCode = 1;
        private const int notExistReturnCode = 0;
        private const int notExistEntityGroupCode = 0;

        public List<PRV_ENT_ENTGROUP> GetEntityGroupList()
        {
            List<PRV_ENT_ENTGROUP> entityGroupList = new List<PRV_ENT_ENTGROUP>();
            using (var DAPDB = this.DAPDB)
            {
                entityGroupList.AddRange(DAPDB.PRV_ENT_ENTGROUP.ToArray());
            }
            return entityGroupList;
        }

        public PRV_ENT_ENTGROUP InsertEntityGroup(PRV_ENT_ENTGROUP model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("City", "Entity Group is not well defined!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    model = DAPDB.PRV_ENT_ENTGROUP.Add(model);
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to add Entity Group to DB!", ex);
                    throw;
                }
            }

            return model;
        }

        public PRV_ENT_ENTGROUP UpdateEntityGroup(PRV_ENT_ENTGROUP model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Entity Group", "Entity Group is not specified!");
            }
            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Modified;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to edit Entity Group!", ex);
                    throw;
                }
            }
            return model;
        }

        public PRV_ENT_ENTGROUP DeleteEntityGroup(PRV_ENT_ENTGROUP model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Entity Group", "Entity Group is not specified!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Deleted;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to delete Entity Group!", ex);
                    throw;
                }
            }

            return model;
        }

        public int ValidateEntityGroupDelete(int id)
        {
            if (id <= notExistReturnCode)
            {
                throw new ArgumentNullException("Entity Group", "Entity Group is not specified!");
            }
            int result = existReturnCode;
            using (var dapDB = this.DAPDB)
            {
                try
                {
                    List<PRV_ENT_ENTGROUP_PROJ_RELN> list = dapDB.PRV_ENT_ENTGROUP_PROJ_RELN.Where(e => e.EntityGroupId == id).ToList();
                    List<PRV_ENT_URG_ROL_RELN> list2 = dapDB.PRV_ENT_URG_ROL_RELN.Where(e => e.EntityGroupId == id).ToList();
                    if (list.Count == 0 && list2.Count == 0)
                    {
                        result = notExistReturnCode;
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    result = notExistReturnCode;
                    log.Error("Unable to validate!", ex);
                    throw;
                }
            }
            return result;
            return 1;
        }

        public int ValidateEntityGroupDuplicateName(string name, int id)
        {
            if (name == null)
            {
                throw new ArgumentNullException("Company", "Company is not specified!");
            }
            int result = notExistReturnCode;
            using (var dapDB = this.DAPDB)
            {
                try
                {
                    if (id > 0)
                    {
                        List<PRV_ENT_ENTGROUP> list = dapDB.PRV_ENT_ENTGROUP.Where(e => e.EntityGroupName == name && e.Id != id).ToList();
                        if (list.Count > 0)
                        {
                            result = existReturnCode;
                        }
                    }
                    else
                    {

                        List<PRV_ENT_ENTGROUP> list = dapDB.PRV_ENT_ENTGROUP.Where(e => e.EntityGroupName == name).ToList();
                        if (list.Count > 0)
                        {
                            result = existReturnCode;
                        }
                    }

                }
                catch (DbEntityValidationException ex)
                {
                    result = notExistReturnCode;
                    log.Error("Unable to validate Company code!", ex);
                    throw;
                }
            }
            return result;

        }

        #region Entity Group User Group Role...

        // avoid using this function if possible. too many joins
        public List<PRV_ENT_URG_ROL_RELN> GetPrivilegeDetailList()
        {
            List<PRV_ENT_URG_ROL_RELN> list = new List<PRV_ENT_URG_ROL_RELN>();
            using (var DAPDB = this.DAPDB)
            {
                list.AddRange(DAPDB.PRV_ENT_URG_ROL_RELN
                    .Include("PRV_ENT_ENTGROUP.PRV_ENT_ENTGROUP_PROJ_RELN.MST_PROJ_PROJECT")
                    .Include("PRV_URG_USRGROUP.PRV_URG_USER_GROUP_RELN.MST_URG_USR")
                    .Include("PRV_ROL_ROLE.PRV_ROL_ROLE_DASHBOARD_RELN.SYS_CFG_DASHBOARD")
                    .ToArray());
            }
            return list;
        }

        public List<PRV_ENT_URG_ROL_RELN> GetEntityGroupUserGroupRoleList()
        {
            List<PRV_ENT_URG_ROL_RELN> list = new List<PRV_ENT_URG_ROL_RELN>();
            using (var DAPDB = this.DAPDB)
            {
                list.AddRange(DAPDB.PRV_ENT_URG_ROL_RELN
                    .Include(o => o.PRV_ENT_ENTGROUP)
                    .Include(o => o.PRV_URG_USRGROUP)
                    .Include(o => o.PRV_ROL_ROLE)
                    .ToArray());
            }
            return list;
        }

        public List<PRV_ENT_URG_ROL_RELN> GetEntityGroupUserGroupRoleListByEntityGroupId(int entityGroupId, bool? enableProxyCreation)
        {
            List<PRV_ENT_URG_ROL_RELN> list = new List<PRV_ENT_URG_ROL_RELN>();
            using (var DAPDB = this.DAPDB)
            {
                DAPDB.Configuration.ProxyCreationEnabled = enableProxyCreation == null ? true : enableProxyCreation.Value;
                list.AddRange(DAPDB.PRV_ENT_URG_ROL_RELN.Where(o => o.EntityGroupId == entityGroupId)
                    .Include(o => o.PRV_ENT_ENTGROUP)
                    .Include(o => o.PRV_URG_USRGROUP)
                    .Include(o => o.PRV_ROL_ROLE).ToArray());
            }
            return list;
        }

        public List<PRV_ENT_URG_ROL_RELN> GetEntityGroupUserGroupRoleListByUserGroupId(int userGroupId, bool? enableProxyCreation)
        {
            List<PRV_ENT_URG_ROL_RELN> list = new List<PRV_ENT_URG_ROL_RELN>();
            using (var DAPDB = this.DAPDB)
            {
                DAPDB.Configuration.ProxyCreationEnabled = enableProxyCreation == null ? true : enableProxyCreation.Value;
                list.AddRange(DAPDB.PRV_ENT_URG_ROL_RELN.Where(o => o.UserGroupId == userGroupId)
                    .Include(o => o.PRV_ENT_ENTGROUP)
                    .Include(o => o.PRV_URG_USRGROUP)
                    .Include(o => o.PRV_ROL_ROLE).ToArray());
            }
            return list;
        }

        public List<PRV_ENT_URG_ROL_RELN> GetEntityGroupUserGroupRoleListByRoleId(int roleId, bool? enableProxyCreation)
        {
            List<PRV_ENT_URG_ROL_RELN> list = new List<PRV_ENT_URG_ROL_RELN>();
            using (var DAPDB = this.DAPDB)
            {
                DAPDB.Configuration.ProxyCreationEnabled = enableProxyCreation == null ? true : enableProxyCreation.Value;
                list.AddRange(DAPDB.PRV_ENT_URG_ROL_RELN.Where(o => o.RoleId == roleId)
                    .Include(o => o.PRV_ENT_ENTGROUP)
                    .Include(o => o.PRV_URG_USRGROUP)
                    .Include(o => o.PRV_ROL_ROLE).ToArray());
            }
            return list;
        }

        public PRV_ENT_URG_ROL_RELN InsertEntityGroupGroupUserGroupRole(PRV_ENT_URG_ROL_RELN model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("PRV_ENT_URG_ROL_RELN", "Entity Group User Group Role model is not well defined!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    model = DAPDB.PRV_ENT_URG_ROL_RELN.Add(model);
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to add Entity Group User Group Role record to DB!", ex);
                    throw;
                }
            }

            return model;
        }

        public PRV_ENT_URG_ROL_RELN UpdateEntityGroupUserGroupRole(PRV_ENT_URG_ROL_RELN model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("PRV_ENT_URG_ROL_RELN", "Entity Group User Group Role model is not well defined!");
            }
            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Modified;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to update Entity Group User Group Role record!", ex);
                    throw;
                }
            }
            return model;
        }

        public PRV_ENT_URG_ROL_RELN DeleteEntityGroupUserGroupRole(PRV_ENT_URG_ROL_RELN model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("PRV_ENT_URG_ROL_RELN", "Entity Group User Group Role is not specified!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Deleted;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to delete Entity Group User Group Role!", ex);
                    throw;
                }
            }

            return model;
        }

        public int ValidateEntityGroupUserGroupRole(int id)
        {
            //if (id <= notExistEntityGroup)
            //{
            //    throw new ArgumentNullException("Entity Group", "Entity Group is not specified!");
            //}
            //int result = existEntityGroup;
            //using (var DAPDB = this.DAPDB)
            //{
            //    try
            //    {
            //        List<Project> projectList = PCMSDB.Projects.Where(e => e.CityID == id).ToList();
            //        if (projectList.Count > notExistCity)
            //        {
            //            result = notExistCity;
            //        }
            //    }
            //    catch (DbEntityValidationException ex)
            //    {
            //        result = notExistCity;
            //        log.Error("Unable to validate City!", ex);
            //        throw;
            //    }
            //}
            //return result;
            return 1;
        }
        #endregion

        #region User Group...
        public List<MST_PROJ_REGION> GetUserGroupTreeList()
        {
            List<MST_PROJ_REGION> regionList = new List<MST_PROJ_REGION>();
            using (var DAPDB = this.DAPDB)
            {
                DAPDB.Configuration.ProxyCreationEnabled = false;
                regionList = DAPDB.MST_PROJ_REGION
                    .Include("MST_PROJ_CITY.PRV_URG_COMPANY.PRV_URG_DEPARTMENT.PRV_URG_USER.MST_URG_USR")
                    .ToList();
            }
            return regionList;
        }

        public List<PRV_URG_USRGROUP> GetUserGroupList()
        {
            List<PRV_URG_USRGROUP> UserGroupList = new List<PRV_URG_USRGROUP>();
            using (var DAPDB = this.DAPDB)
            {
                UserGroupList.AddRange(DAPDB.PRV_URG_USRGROUP.ToArray());
            }
            return UserGroupList;
        }

        public PRV_URG_USRGROUP InsertUserGroup(PRV_URG_USRGROUP model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("User Group", "User Group is not well defined!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    model = DAPDB.PRV_URG_USRGROUP.Add(model);
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to add User Group to DB!", ex);
                    throw;
                }
            }

            return model;
        }

        public PRV_URG_USRGROUP UpdateUserGroup(PRV_URG_USRGROUP model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("User Group", "User Group is not specified!");
            }
            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Modified;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to edit User Group!", ex);
                    throw;
                }
            }
            return model;
        }

        public PRV_URG_USRGROUP DeleteUserGroup(PRV_URG_USRGROUP model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("User Group", "User Group is not specified!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Deleted;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to delete User Group!", ex);
                    throw;
                }
            }

            return model;
        }

        public int ValidateUserGroup(int id)
        {
            //if (id <= notExistUserGroup)
            //{
            //    throw new ArgumentNullException("User Group", "User Group is not specified!");
            //}
            //int result = existUserGroup;
            //using (var DAPDB = this.DAPDB)
            //{
            //    try
            //    {
            //        List<Project> projectList = PCMSDB.Projects.Where(e => e.CityID == id).ToList();
            //        if (projectList.Count > notExistCity)
            //        {
            //            result = notExistCity;
            //        }
            //    }
            //    catch (DbEntityValidationException ex)
            //    {
            //        result = notExistCity;
            //        log.Error("Unable to validate City!", ex);
            //        throw;
            //    }
            //}
            //return result;
            return 1;
        }

        

        public List<MST_PROJ_REGION> GetRegionTreeViewForUserGroup()
        {
            List<MST_PROJ_REGION> treeView = new List<MST_PROJ_REGION>();
            using (var DAPDB = this.DAPDB)
            {
                treeView = DAPDB.MST_PROJ_REGION
                    .Include("MST_PROJ_CITY.PRV_URG_COMPANY.PRV_URG_DEPARTMENT.PRV_URG_DEPARTMENT_USER_RELN.MST_URG_USR")
                    .ToList();
            }
            return treeView;
        }

        public PRV_URG_USRGROUP GetUserGroupByUserGroupId(int userGroupId, bool? enableProxyCreation)
        {
            PRV_URG_USRGROUP ug = new PRV_URG_USRGROUP();
            using (var DAPDB = this.DAPDB)
            {
                DAPDB.Configuration.ProxyCreationEnabled = enableProxyCreation == null ? true : enableProxyCreation.Value;
                ug = DAPDB.PRV_URG_USRGROUP.Include(u => u.PRV_URG_USER_GROUP_RELN).Where(u => u.Id == userGroupId).FirstOrDefault();
            }
            return ug;
        }

        public List<PRV_URG_USER_GROUP_RELN> GetUserGroupUser()
        {
            var returnObjList = new List<PRV_URG_USER_GROUP_RELN>();
            using (var DAPDB = this.DAPDB)
            {
                returnObjList = DAPDB.PRV_URG_USER_GROUP_RELN.ToList();
            }
            return returnObjList;
        }                

        public int CreateUserGroupUser(int userGroupId, string selectedCodes, string currentUserName)
        {

            if (userGroupId < 1)
            {
                throw new ArgumentNullException("Create User Group User", "User Group ID is not specified!");
            }

            int result = 0;

            using (var db = DAPDB)
            {
                DbContextTransaction trans = db.Database.BeginTransaction();
                try
                {
                    //delete all user in the group first
                    db.PRV_URG_USER_GROUP_RELN.RemoveRange(db.PRV_URG_USER_GROUP_RELN.Where(o => o.UserGroupId == userGroupId));

                    if (!string.IsNullOrEmpty(selectedCodes))
                    {
                        string[] selectedCode = selectedCodes.Split(',').Distinct().ToArray();

                        for (int i = 0; i < selectedCode.Length; i++)
                        {
                            //if (selectedCode[i].ToString().Length > 1 && selectedCode[i].ToString().Substring(0, 1) == "u")
                            //{
                            //    userId = Convert.ToInt32(selectedCode[i].ToString().Substring(1));
                            //    u = db.PRV_URG_DEPARTMENT_USER_RELN.ToList().FirstOrDefault(o => o.Id == userId);
                            //}

                            //if (u != null)
                            //{
                                PRV_URG_USER_GROUP_RELN model = new PRV_URG_USER_GROUP_RELN();
                                model.UserGroupId = userGroupId;
                                model.ADLogonName = selectedCode[i];
                                model.ModifiedDt = DateTime.Now;
                                model.ModifiedBy = currentUserName;
                                model = db.PRV_URG_USER_GROUP_RELN.Add(model);

                            //}
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                    result = 1;
                }
                catch (Exception ex)
                {
                    log.Error("Unable to create UserGroup!", ex);
                    trans.Rollback();
                }
            }
            return result;
        }

        #endregion
        #region Company

        public List<PRV_URG_COMPANY> GetCompanyList()
        {
            List<PRV_URG_COMPANY> CompanyList = new List<PRV_URG_COMPANY>();
            using (var DAPDB = this.DAPDB)
            {
                CompanyList.AddRange(DAPDB.PRV_URG_COMPANY.Include("MST_PROJ_CITY").ToArray());
            }
            return CompanyList;
        }

        public PRV_URG_COMPANY InsertCompany(PRV_URG_COMPANY model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Company", "Company is not well defined!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    model = DAPDB.PRV_URG_COMPANY.Add(model);
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to add Company to DB!", ex);
                    throw;
                }
            }

            return model;
        }

        public PRV_URG_COMPANY UpdateCompany(PRV_URG_COMPANY model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Company", "Company is not specified!");
            }
            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Modified;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to edit Company!", ex);
                    throw;
                }
            }
            return model;
        }

        public PRV_URG_COMPANY DeleteCompany(PRV_URG_COMPANY model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Company", "Company is not specified!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Deleted;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to delete Company!", ex);
                    throw;
                }
            }

            return model;
        }

        public int ValidateCompany(int id)
        {
            //if (id <= notExistUserGroup)
            //{
            //    throw new ArgumentNullException("User Group", "User Group is not specified!");
            //}
            //int result = existUserGroup;
            //using (var DAPDB = this.DAPDB)
            //{
            //    try
            //    {
            //        List<Project> projectList = PCMSDB.Projects.Where(e => e.CityID == id).ToList();
            //        if (projectList.Count > notExistCity)
            //        {
            //            result = notExistCity;
            //        }
            //    }
            //    catch (DbEntityValidationException ex)
            //    {
            //        result = notExistCity;
            //        log.Error("Unable to validate City!", ex);
            //        throw;
            //    }
            //}
            //return result;
            return 1;
        }


        #endregion

        #region Department

        public List<PRV_URG_DEPARTMENT> GetDepartmentList()
        {
            List<PRV_URG_DEPARTMENT> DepartmentList = new List<PRV_URG_DEPARTMENT>();
            using (var DAPDB = this.DAPDB)
            {
                DepartmentList.AddRange(DAPDB.PRV_URG_DEPARTMENT.Include("PRV_URG_COMPANY").ToArray());
            }
            return DepartmentList;
        }
        public PRV_URG_DEPARTMENT GetDepartmentById(int Id, bool? enableProxyCreation)
        {
            PRV_URG_DEPARTMENT obj = new PRV_URG_DEPARTMENT();
            using (var DAPDB = this.DAPDB)
            {
                DAPDB.Configuration.ProxyCreationEnabled = enableProxyCreation == null ? true : enableProxyCreation.Value;
                obj = DAPDB.PRV_URG_DEPARTMENT.Include("PRV_URG_COMPANY")
                    .Where(o => o.Id == Id).First();
            }
            return obj;
        }
        public PRV_URG_DEPARTMENT InsertDepartment(PRV_URG_DEPARTMENT model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Department", "Department is not well defined!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    model = DAPDB.PRV_URG_DEPARTMENT.Add(model);
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to add Department to DB!", ex);
                    throw;
                }
            }

            return model;
        }
        public PRV_URG_DEPARTMENT UpdateDepartment(PRV_URG_DEPARTMENT model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Department", "Department is not specified!");
            }
            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Modified;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to edit Department!", ex);
                    throw;
                }
            }
            return model;
        }
        public PRV_URG_DEPARTMENT DeleteDepartment(PRV_URG_DEPARTMENT model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Department", "Department is not specified!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Deleted;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to delete Department!", ex);
                    throw;
                }
            }

            return model;
        }
        public int ValidateDepartment(int id)
        {
            //if (id <= notExistUserGroup)
            //{
            //    throw new ArgumentNullException("User Group", "User Group is not specified!");
            //}
            //int result = existUserGroup;
            //using (var DAPDB = this.DAPDB)
            //{
            //    try
            //    {
            //        List<Project> projectList = PCMSDB.Projects.Where(e => e.CityID == id).ToList();
            //        if (projectList.Count > notExistCity)
            //        {
            //            result = notExistCity;
            //        }
            //    }
            //    catch (DbEntityValidationException ex)
            //    {
            //        result = notExistCity;
            //        log.Error("Unable to validate City!", ex);
            //        throw;
            //    }
            //}
            //return result;
            return 1;
        }
        public List<sp_GetUserByDepartmentId_Result> GetDepartmentUserByDepartmentId(int id)
        {
            List<sp_GetUserByDepartmentId_Result> list = null;

            using (var db = this.DAPDB)
            {
                list = db.sp_GetUserByDepartmentId(id).ToList();
            }
            return list;
        }
        public void DeleteDepartmentUserByADLogonNameAndDepartmentId(string adLogonName, int departmentId)
        {
            using (var db = this.DAPDB)
            {
                db.sp_DeleteDepartmentUserByAdLogonNameAndDepartmentId(departmentId, adLogonName);
            }
        }

        public string AddUserToDepartment(string adLogonName, int departmentId, string language, string currentUser)
        {
            string result = "";
            using (var db = this.DAPDB)
            {
                var duList = db.PRV_URG_DEPARTMENT_USER_RELN.Where(o => o.ADLogonName == adLogonName && o.DepartmentId == departmentId).ToList();
                if (duList.Count > 0)
                {
                    if (language.ToLower() == "en-us")
                        result = db.SystemMessages.Where(o => o.ResourceType == "Department" && o.Name == "text_userExistInDepartment").FirstOrDefault().EN;
                    else
                        result = db.SystemMessages.Where(o => o.ResourceType == "Department" && o.Name == "text_userExistInDepartment").FirstOrDefault().CN;
                }

                if(result.Length == 0)
                {
                    MST_URG_USR u = db.MST_URG_USR.Where(o => o.ADLogonName == adLogonName).FirstOrDefault();
                    PRV_URG_DEPARTMENT_USER_RELN deptUser = new PRV_URG_DEPARTMENT_USER_RELN();
                    deptUser.ADLogonName = u.ADLogonName;
                    deptUser.ADDomain = u.ADDomain;
                    deptUser.DepartmentId = departmentId;
                    deptUser.ModifiedBy = currentUser;
                    deptUser.ModifiedDt = DateTime.Now;
                    db.PRV_URG_DEPARTMENT_USER_RELN.Add(deptUser);
                    db.SaveChanges();
                }

            }
            return result;

        }
        #endregion

        #endregion

        #region Role...
        public List<PRV_ROL_ROLE> GetRoleList()
        {
            List<PRV_ROL_ROLE> RoleList = new List<PRV_ROL_ROLE>();
            using (var DAPDB = this.DAPDB)
            {
                RoleList.AddRange(DAPDB.PRV_ROL_ROLE.Include("PRV_ROL_ROLE_DASHBOARD_RELN.SYS_CFG_DASHBOARD").ToArray());
            }
            return RoleList;
        }

        public PRV_ROL_ROLE GetRoleByRoleId(int roleId, bool? enableProxyCreation)
        {
            PRV_ROL_ROLE role = new PRV_ROL_ROLE();
            using (var DAPDB = this.DAPDB)
            {
                DAPDB.Configuration.ProxyCreationEnabled = enableProxyCreation == null ? true : enableProxyCreation.Value;
                role = DAPDB.PRV_ROL_ROLE
                    .Where(rol => rol.Id == roleId).First();
            }
            return role;
        }

        public PRV_ROL_ROLE InsertRole(PRV_ROL_ROLE model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Role", "Role is not well defined!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    model = DAPDB.PRV_ROL_ROLE.Add(model);
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to add Role to DB!", ex);
                    throw;
                }
            }

            return model;
        }

        public PRV_ROL_ROLE UpdateRole(PRV_ROL_ROLE model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Role", "Role is not specified!");
            }
            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Modified;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to edit Role!", ex);
                    throw;
                }
            }
            return model;
        }

        public PRV_ROL_ROLE DeleteRole(PRV_ROL_ROLE model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Role", "Role is not specified!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Deleted;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to delete Role!", ex);
                    throw;
                }
            }

            return model;
        }

        public int ValidateRole(int id)
        {
            //if (id <= notExistRole)
            //{
            //    throw new ArgumentNullException("Role", "Role is not specified!");
            //}
            //int result = existRole;
            //using (var DAPDB = this.DAPDB)
            //{
            //    try
            //    {
            //        List<Project> projectList = PCMSDB.Projects.Where(e => e.CityID == id).ToList();
            //        if (projectList.Count > notExistCity)
            //        {
            //            result = notExistCity;
            //        }
            //    }
            //    catch (DbEntityValidationException ex)
            //    {
            //        result = notExistCity;
            //        log.Error("Unable to validate City!", ex);
            //        throw;
            //    }
            //}
            //return result;
            return 1;
        }

        public List<PRV_ROL_ROLE_DASHBOARD_RELN> GetRoleDashboardListByRoleId(int roleId, bool? enableProxyCreation)
        {
            List<PRV_ROL_ROLE_DASHBOARD_RELN> RoleDashboardList = new List<PRV_ROL_ROLE_DASHBOARD_RELN>();
            using (var DAPDB = this.DAPDB)
            {
                DAPDB.Configuration.ProxyCreationEnabled = enableProxyCreation == null ? true : enableProxyCreation.Value;
                RoleDashboardList.AddRange(DAPDB.PRV_ROL_ROLE_DASHBOARD_RELN
                    .Where(r => r.RoleId == roleId)
                    .Include(role => role.PRV_ROL_ROLE).Include(db => db.SYS_CFG_DASHBOARD)
                    .ToArray());
            }
            return RoleDashboardList;
        }

        public PRV_ROL_ROLE_DASHBOARD_RELN InsertRoleDashboard(PRV_ROL_ROLE_DASHBOARD_RELN model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Role Dashboard Relationship", "Role Dashboard Relationship is not well defined!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    model = DAPDB.PRV_ROL_ROLE_DASHBOARD_RELN.Add(model);
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to add Role Dashboard Relationship to DB!", ex);
                    throw;
                }
            }

            return model;
        }

        public PRV_ROL_ROLE_DASHBOARD_RELN UpdateRoleDashboard(PRV_ROL_ROLE_DASHBOARD_RELN model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Role Dashboard Relationship", "Role Dashboard Relationship is not specified!");
            }
            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Modified;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to edit Role Dashboard Relationship!", ex);
                    throw;
                }
            }
            return model;
        }

        public PRV_ROL_ROLE_DASHBOARD_RELN DeleteRoleDashboard(PRV_ROL_ROLE_DASHBOARD_RELN model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Role Dashboard Relationship", "Role Dashboard Relationship is not specified!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    DAPDB.Entry(model).State = EntityState.Deleted;
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to delete Role Dashboard Relationship!", ex);
                    throw;
                }
            }

            return model;
        }

        public int ValidateRoleNameDuplicate(int roleId, string roleName)
        {
            if (roleName == null || roleName.Length == 0)
            {
                throw new ArgumentNullException("Role Name", "Role Name is not specified!");
            }

            int result = existReturnCode;
            using (var dapDB = this.DAPDB)
            {
                try
                {
                    List<PRV_ROL_ROLE> list = dapDB.PRV_ROL_ROLE.Where(e =>
                        e.Id != roleId && e.RoleName == roleName.Trim()).ToList();
                    if (list.Count == 0)
                    {
                        result = notExistReturnCode;
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    result = existReturnCode;
                    log.Error("Unable to validate!", ex);
                    throw;
                }
            }
            return result;
        }

        public int ValidateRoleEmpty(int roleId)
        {
            if (roleId <= notExistReturnCode)
            {
                throw new ArgumentNullException("Role ID", "Role ID is not specified!");
            }

            int result = existReturnCode;
            using (var dapDB = this.DAPDB)
            {
                try
                {
                    PRV_ROL_ROLE role = dapDB.PRV_ROL_ROLE.Where(e =>
                        e.Id == roleId)
                        .Include(o => o.PRV_ENT_URG_ROL_RELN)
                        .Include(o => o.PRV_ROL_ROLE_DASHBOARD_RELN).First();
                    if (role.PRV_ROL_ROLE_DASHBOARD_RELN.Count() == 0 && role.PRV_ENT_URG_ROL_RELN.Count() == 0)
                    {
                        result = notExistReturnCode;
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    result = existReturnCode;
                    log.Error("Unable to validate!", ex);
                    throw;
                }
            }
            return result;
        }

        #region Dashboard
        public List<SYS_CFG_DASHBOARD> GetDashboardList(bool? enableProxyCreation)
        {
            List<SYS_CFG_DASHBOARD> dashboardList = new List<SYS_CFG_DASHBOARD>();
            using (var DAPDB = this.DAPDB)
            {
                DAPDB.Configuration.ProxyCreationEnabled = enableProxyCreation == null ? true : enableProxyCreation.Value;
                dashboardList.AddRange(DAPDB.SYS_CFG_DASHBOARD.ToArray());
            }
            return dashboardList;
        }

        public SYS_CFG_DASHBOARD GetDashboardById(int id)
        {
            SYS_CFG_DASHBOARD retObj = new SYS_CFG_DASHBOARD();
            using (var db = this.DAPDB)
            {
                retObj = db.SYS_CFG_DASHBOARD.Where(o => o.Id == id).FirstOrDefault();
            }
            return retObj;
        }

        public int ValidateRoleDashboardDuplicate(int dashboardId, int roleId)
        {
            if (dashboardId <= notExistReturnCode)
            {
                throw new ArgumentNullException("Dashboard", "Dashboard is not specified!");
            }

            if (roleId <= notExistReturnCode)
            {
                throw new ArgumentNullException("Role", "Role is not specified!");
            }

            int result = existReturnCode;
            using (var dapDB = this.DAPDB)
            {
                try
                {
                    List<PRV_ROL_ROLE_DASHBOARD_RELN> list = dapDB.PRV_ROL_ROLE_DASHBOARD_RELN.Where(e =>
                        e.DashboardId == dashboardId && e.RoleId == roleId).ToList();
                    if (list.Count == 0)
                    {
                        result = notExistReturnCode;
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    result = existReturnCode;
                    log.Error("Unable to validate!", ex);
                    throw;
                }
            }
            return result;
        }
        #endregion
        #endregion

        #region Privilege
        public int ValidatePrivilegeDuplicate(int id, int entityGroupId, int userGroupId, int roleId)
        {
            if (entityGroupId <= notExistReturnCode)
            {
                throw new ArgumentNullException("Entity Group", "Entity Group is not specified!");
            }

            if (userGroupId <= notExistReturnCode)
            {
                throw new ArgumentNullException("User Group", "User Group is not specified!");
            }

            if (roleId <= notExistReturnCode)
            {
                throw new ArgumentNullException("Role", "Role is not specified!");
            }

            int result = existReturnCode;
            using (var dapDB = this.DAPDB)
            {
                try
                {
                    List<PRV_ENT_URG_ROL_RELN> list = dapDB.PRV_ENT_URG_ROL_RELN.Where(e =>
                        e.Id != id &&
                        e.EntityGroupId == entityGroupId && e.UserGroupId == userGroupId && e.RoleId == roleId).ToList();
                    if (list.Count == 0)
                    {
                        result = notExistReturnCode;
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    result = existReturnCode;
                    log.Error("Unable to validate!", ex);
                    throw;
                }
            }
            return result;
        }
        #endregion


        public List<SYS_CFG_PARAM> GetParameter(string funcGroup, string subFuncGroup, string key)
        {
            List<SYS_CFG_PARAM> list = new List<SYS_CFG_PARAM>();
            using (var db = this.DAPDB)
            {
                list = db.SYS_CFG_PARAM.Where(o => o.FuncGroup == funcGroup && o.SubFuncGroup == subFuncGroup && o.Key == key).ToList();
            }
            return list;
        }

        public List<sp_GetEntityGroupTreeView_Result> GetEntityGroupTreeView()
        {
            List<sp_GetEntityGroupTreeView_Result> treeView = new List<sp_GetEntityGroupTreeView_Result>();
            using (var DAPDB = this.DAPDB)
            {
                treeView = DAPDB.sp_GetEntityGroupTreeView().ToList();
            }
            return treeView;
        }

        public List<sp_GetAllEntityTreeView_Result> GetAllEntityTreeView()
        {
            List<sp_GetAllEntityTreeView_Result> treeView = new List<sp_GetAllEntityTreeView_Result>();
            using (var DAPDB = this.DAPDB)
            {
                treeView = DAPDB.sp_GetAllEntityTreeView().ToList();
            }
            return treeView;
        }

        public List<sp_GetDashboardByAdLogonName_Result> GetDashboardListByADLogonName(string adLogonName)
        {
            List<sp_GetDashboardByAdLogonName_Result> list = new List<sp_GetDashboardByAdLogonName_Result>();
            using (var db = this.DAPDB)
            {
                list = db.sp_GetDashboardByAdLogonName(adLogonName).ToList();
            }
            return list;
        }

        public sp_GetDashboardByAdLogonName_Result GetDashboardListByADLogonNameAndId(string adLogonName, int dashboardID)
        {
            sp_GetDashboardByAdLogonName_Result retObj = new sp_GetDashboardByAdLogonName_Result();
            using (var db = this.DAPDB)
            {
                retObj = db.sp_GetDashboardByAdLogonName(adLogonName).Where(o => o.Id == dashboardID).FirstOrDefault();
            }
            return retObj;
        }

        public List<PRV_ENT_ENTGROUP_PROJ_RELN> GetEntityGroupProject()
        {
            var returnObjList = new List<PRV_ENT_ENTGROUP_PROJ_RELN>();
            using (var DAPDB = this.DAPDB)
            {
                returnObjList = DAPDB.PRV_ENT_ENTGROUP_PROJ_RELN.ToList();
            }
            return returnObjList;
        }

        public PRV_ENT_ENTGROUP GetEntityGroupByEntityGroupId(int entityGroupId, bool? enableProxyCreation)
        {
            var returnObjList = new PRV_ENT_ENTGROUP();
            using (var DAPDB = this.DAPDB)
            {
                DAPDB.Configuration.ProxyCreationEnabled = enableProxyCreation == null ? true : enableProxyCreation.Value;
                returnObjList = DAPDB.PRV_ENT_ENTGROUP
                    .Where(o => o.Id == entityGroupId)
                    .Include(o => o.PRV_ENT_ENTGROUP_PROJ_RELN).FirstOrDefault();
            }
            return returnObjList;
        }

        public List<string> GetParentEntityByEntityGroupId(Nullable<int> entityGroupId)
        {
            List<string> treeView = new List<string>();
            using (var DAPDB = this.DAPDB)
            {
                treeView = DAPDB.sp_GetParentEntityByEntityGroupID(entityGroupId).ToList();
            }
            return treeView;
        }

        /// <summary>
        /// Create Entity Group Project Record from Tree View, 
        /// Delete the records by searching the rel record by entity group id, then add the rel records.
        /// </summary>
        /// <param name="entityGroupId"></param>
        /// <param name="selectedCodes"></param>
        /// <param name="currentUserName"></param>
        /// <returns></returns>
        public int CreateEntityGroupProject(int entityGroupId, string selectedCodes, string currentUserName)
        {

            if (entityGroupId < 1)
            {
                throw new ArgumentNullException("Create Entity Group Relation record", "EntityGroupID is not specified!");
            }

            int result = 0;

            using (var db = DAPDB)
            {
                DbContextTransaction trans = db.Database.BeginTransaction();
                try
                {
                    db.sp_DeleteEntityGroupProjectByEntityGroupId(entityGroupId);

                    if (!string.IsNullOrEmpty(selectedCodes))
                    {
                        string[] selectedCode = selectedCodes.Split(',');

                        for (int i = 0; i < selectedCode.Length; i++)
                        {
                            try
                            {
                                if (selectedCode[i] != "0")
                                {
                                    MST_PROJ_PROJECT p = new MST_PROJ_PROJECT();
                                    p = GetProjectByCode(selectedCode[i]);
                                    if (p != null)
                                    {
                                        PRV_ENT_ENTGROUP_PROJ_RELN model = new PRV_ENT_ENTGROUP_PROJ_RELN();
                                        model.EntityGroupId = entityGroupId;
                                        model.ProjectFullCode = selectedCode[i];
                                        model.ModifiedDt = DateTime.Now;
                                        model.ModifiedBy = currentUserName;
                                        model = db.PRV_ENT_ENTGROUP_PROJ_RELN.Add(model);
                                            
                                    }
                                }
                            }
                            catch (DbEntityValidationException ex)
                            {
                                log.Error("Unable to add PRV_ENT_ENTGROUP_PROJ_RELN to DB!", ex);
                                throw;
                            }

                        }
                    }
                    db.SaveChanges();
                    result = 1;
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    log.Error("Unable to create PRV_ENT_ENTGROUP_PROJ_RELN!", ex);
                    trans.Rollback();
                }
            }

            return result;
        }

        public MST_PROJ_PROJECT GetProjectByCode(string fullCode)
        {
            MST_PROJ_PROJECT projectItem = null;
            using (var db = DAPDB)
            {
                projectItem = db.MST_PROJ_PROJECT.FirstOrDefault(e => e.ProjectFullCode == fullCode);
            }
            return projectItem;
        }
        #endregion

        #region City...
        public MST_PROJ_CITY InsertCity(MST_PROJ_CITY model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("City", "City is not well defined!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    //int count =;
                    if (!DAPDB.MST_PROJ_CITY.Any(e => e.CityCode == model.CityCode))
                    {
                        //Insert
                        model = DAPDB.MST_PROJ_CITY.Add(model);
                    }
                    else
                    {
                        //Update
                        DAPDB.Entry(model).State = EntityState.Modified;
                    }
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to add Entity Group to DB!", ex);
                    throw;
                }
            }
            return model;
        }
        #endregion
        #region Region...
        public MST_PROJ_REGION InsertRegion(MST_PROJ_REGION model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("Region", "Region is not well defined!");
            }

            using (var DAPDB = this.DAPDB)
            {
                try
                {
                    int count = DAPDB.MST_PROJ_REGION.ToArray().Where(e => e.RegionCode == model.RegionCode).Count();
                    if (count == 0)
                    {
                        //Insert
                        model = DAPDB.MST_PROJ_REGION.Add(model);
                    }
                    else
                    {
                        //Update
                        DAPDB.Entry(model).State = EntityState.Modified;
                        DAPDB.SaveChanges();
                    }
                    DAPDB.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    log.Error("Unable to add Entity Group to DB!", ex);
                    throw;
                }
            }
            return model;
        }
        #endregion
        #region Project...
        public void InsertProject(string cityCode, string projectCode, string projectPhase1, string projectPhase2,
                    string projectPhase3, string projectFullCode, string projectNameEN, string projectNameCN,
                    string remark, bool isFinancial, bool isSale, bool isPM, string keywords, string user)
        {
            using (var db = this.DAPDB)
            {
                try
                {
                    db.sp_InsertProjectByExcel(cityCode, projectCode, projectPhase1, projectPhase2, projectPhase3,
                    projectFullCode, projectNameEN, projectNameCN, remark, isFinancial, isSale, isPM, keywords, user);
                    db.Dispose();
                }
                catch (Exception e)
                { }
            }
        }
        #endregion

        public List<sp_SearchUserByCriteria_Result> SearchUser(string domains, string criteria)
        {
            List<sp_SearchUserByCriteria_Result> list = null;
            using (var db = this.DAPDB)
            {
                list = db.sp_SearchUserByCriteria(domains, criteria).ToList();
            }
            return list;
        }

        #region Property

        /// <summary>
        /// Gets the DB container for DAP
        /// </summary>
        public DAPEntities DAPDB
        {
            get
            {
                return new DAPEntities();
            }
        }



        #endregion


    }
}
