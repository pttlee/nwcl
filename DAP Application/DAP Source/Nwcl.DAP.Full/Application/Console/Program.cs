﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Application.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            System.Console.Out.WriteLine("Running DAP AppServer Console ...");

            // Apply the Remoting configuration defined in the App.config
            RemotingConfiguration.Configure("Nwcl.DAP.Application.Console.exe.config", false);

            System.Console.Out.WriteLine("[Press any key to terminate]");
            System.Console.In.ReadLine();
        }
    }
}
