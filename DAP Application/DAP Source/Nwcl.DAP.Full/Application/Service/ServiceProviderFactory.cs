﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Application.Service
{
    /// <summary>
    /// Factory class to create ServiceProvider object instance
    /// </summary>
    public static class ServiceProviderFactory
    {
        #region Method

        /// <summary>
        /// Gets the instance of the specified service provider type
        /// </summary>
        /// <param name="serviceProviderType"></param>
        /// <returns></returns>
        public static object GetServiceProvider(Type serviceProviderType)
        {
            object serviceProviderObj = null;
            if (serviceProviderType != null)
            {
                if (IsRemoteService)
                {
                    #region Remoting Call

                    string trimmedServiceProviderEndPointUrl = !string.IsNullOrEmpty(ServiceProviderEndPointUrl) ? ServiceProviderEndPointUrl.TrimEnd('/') : null;
                    
                    // Create an instance of the remote object
                    if (serviceProviderType == typeof(CommonServiceProvider))
                    {
                        serviceProviderObj = Activator.GetObject(typeof(CommonServiceProvider),
                            trimmedServiceProviderEndPointUrl + "/Nwcl.DAP.Application.Service.CommonServiceProvider.svc");
                    }
                    else
                    {
                        switch (serviceProviderType.FullName)
                        {
                            case "Nwcl.DAP.Application.Service.ProjectServiceProvider":
                                serviceProviderObj = Activator.GetObject(GetType("Nwcl.DAP.Application.Service.Project.dll", "Nwcl.DAP.Application.Service.ProjectServiceProvider"),
                                    trimmedServiceProviderEndPointUrl + "/Nwcl.DAP.Application.Service.ProjectServiceProvider.svc");
                                break;
                            case "Nwcl.DAP.Application.Service.MasterMaintenanceServiceProvider":
                                serviceProviderObj = Activator.GetObject(GetType("Nwcl.DAP.Application.Service.MasterMaintenance.dll", "Nwcl.DAP.Application.Service.MasterMaintenanceServiceProvider"),
                                    trimmedServiceProviderEndPointUrl + "/Nwcl.DAP.Application.Service.MasterMaintenanceServiceProvider.svc");
                                break;

                            case "Nwcl.DAP.Application.Service.AdministrationServiceProvider":
                                serviceProviderObj = Activator.GetObject(GetType("Nwcl.DAP.Application.Service.Administration.dll", "Nwcl.DAP.Application.Service.AdministrationServiceProvider"),
                                    trimmedServiceProviderEndPointUrl + "/Nwcl.DAP.Application.Service.AdministrationServiceProvider.svc");
                                break;
                            case "Nwcl.DAP.Application.Service.TradePackageServiceProvider":
                                serviceProviderObj = Activator.GetObject(GetType("Nwcl.DAP.Application.Service.Project.dll", "Nwcl.DAP.Application.Service.TradePackageServiceProvider"),
                                    trimmedServiceProviderEndPointUrl + "/Nwcl.DAP.Application.Service.TradePackageServiceProvider.svc");
                                break;


                            case "Nwcl.DAP.Application.Service.TradeBudgetServiceProvider":
                                serviceProviderObj = Activator.GetObject(GetType("Nwcl.DAP.Application.Service.MasterMaintenance.dll", "Nwcl.DAP.Application.Service.TradeBudgetServiceProvider"),
                                    trimmedServiceProviderEndPointUrl + "/Nwcl.DAP.Application.Service.TradeBudgetServiceProvider.svc");
                                break;
                            case "Nwcl.DAP.Application.Service.ProgramManagementServiceProvider":
                                serviceProviderObj = Activator.GetObject(GetType("Nwcl.DAP.Application.Service.Project.dll", "Nwcl.DAP.Application.Service.ProgramManagementServiceProvider"),
                                    trimmedServiceProviderEndPointUrl + "/Nwcl.DAP.Application.Service.ProgramManagementServiceProvider.svc");
                                break;    
                        }
                    }

                    #endregion
                }
                else
                {
                    #region Same Domain Call

                    if (serviceProviderType == typeof(CommonServiceProvider))
                    {
                        serviceProviderObj = new CommonServiceProvider();
                    }
                    else
                    {
                        switch (serviceProviderType.FullName)
                        {
                            case "Nwcl.DAP.Application.Service.ProjectServiceProvider":
                                serviceProviderObj = CreateTypeInstance("Nwcl.DAP.Application.Service.Project.dll", "Nwcl.DAP.Application.Service.ProjectServiceProvider");
                                break;
                            case "Nwcl.DAP.Application.Service.CostPlanServiceProvider":
                                serviceProviderObj = CreateTypeInstance("Nwcl.DAP.Application.Service.Project.dll", "Nwcl.DAP.Application.Service.CostPlanServiceProvider");
                                break;
                            case "Nwcl.DAP.Application.Service.BudgetPlanServiceProvider":
                                serviceProviderObj = CreateTypeInstance("Nwcl.DAP.Application.Service.Project.dll", "Nwcl.DAP.Application.Service.BudgetPlanServiceProvider");
                                break;
                            case "Nwcl.DAP.Application.Service.MasterMaintenanceServiceProvider":
                                serviceProviderObj = CreateTypeInstance("Nwcl.DAP.Application.Service.MasterMaintenance.dll", "Nwcl.DAP.Application.Service.MasterMaintenanceServiceProvider");
                                break;
                            case "Nwcl.DAP.Application.Service.AdministrationServiceProvider":
                                serviceProviderObj = CreateTypeInstance("Nwcl.DAP.Application.Service.Administration.dll", "Nwcl.DAP.Application.Service.AdministrationServiceProvider");
                                break;
                            case "Nwcl.DAP.Application.Service.TradeBudgetServiceProvider":
                                serviceProviderObj = CreateTypeInstance("Nwcl.DAP.Application.Service.MasterMaintenance.dll", "Nwcl.DAP.Application.Service.TradeBudgetServiceProvider");
                                break;
                            case "Nwcl.DAP.Application.Service.TradePackageServiceProvider":
                                serviceProviderObj = CreateTypeInstance("Nwcl.DAP.Application.Service.Project.dll", "Nwcl.DAP.Application.Service.TradePackageServiceProvider");
                                break;

                            case "Nwcl.DAP.Application.Service.ProgramManagementServiceProvider":
                                serviceProviderObj = CreateTypeInstance("Nwcl.DAP.Application.Service.Project.dll", "Nwcl.DAP.Application.Service.ProgramManagementServiceProvider");
                                break;
                        }
                    }

                    #endregion
                }

            }
            return serviceProviderObj;
        }

        /// <summary>
        /// Gets the type by specifying the assembly and type
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        private static Type GetType(string assemblyName, string typeName)
        {
            Type type = Type.GetType(typeName);
            if (type == null)
            {
                string assemblyFolderPath = null;
                if (ConfigurationManager.AppSettings["AssemblyFolder"] != null)
                {
                    assemblyFolderPath = Path.Combine(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase, ConfigurationManager.AppSettings["AssemblyFolder"]);
                }
                else
                {
                    assemblyFolderPath = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
                }
                Assembly assembly = Assembly.LoadFrom(Path.Combine(assemblyFolderPath, assemblyName));
                type = assembly.GetType(typeName, true);
            }
            return type;
        }

        /// <summary>
        /// Creates the type instance by specifying the assembly and type
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        private static object CreateTypeInstance(string assemblyName, string typeName)
        {
            return System.Activator.CreateInstance(GetType(assemblyName, typeName));
        }

        #endregion

        #region Property

        /// <summary>
        /// Gets whether the service provider should be instantiated remotely
        /// </summary>
        public static bool IsRemoteService
        {
            get
            {
                return !string.IsNullOrEmpty(ServiceProviderEndPointUrl);
            }
        }

        /// <summary>
        /// Gets the end point url for the service provider
        /// </summary>
        public static string ServiceProviderEndPointUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["ServiceProviderEndPointUrl"];
            }
        }

        #endregion
    }
}
