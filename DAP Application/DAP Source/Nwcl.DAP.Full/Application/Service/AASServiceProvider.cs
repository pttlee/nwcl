﻿using Nwcl.DAP.Application.Service.AASAuthorization;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Xml;

namespace Nwcl.DAP.Application.Service
{
    public class AASServiceProvider : MarshalByRefObject
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private AuthorizationClient _authorizationClient;

        public AASServiceProvider(string endpointConfigName)
        {
            _authorizationClient = new AuthorizationClient(endpointConfigName);
        }

        public bool CheckHasAASUserRoles(string AD_UserName, string AAS_COMPANY_CODE, string AAS_SYSTEM_ID, string roleId)
        {
            string AAS_UserRoles = "";
            bool hasUserRole = false;
            log.Info(string.Format("processing user {0} with company {1}", AD_UserName, AAS_COMPANY_CODE));
            try { 
                AAS_UserRoles = _authorizationClient.GetUserRole(AD_UserName, AAS_COMPANY_CODE, AAS_SYSTEM_ID, roleId);
                if (AAS_UserRoles.Trim().Length == 0)
                {
                    log.Debug(string.Format("User {0} with company {1} returned role with AAS: {2}", 
                        AD_UserName, AAS_COMPANY_CODE, AAS_UserRoles));
                    return hasUserRole;
                }
                else
                {
                    log.Debug(string.Format("User {0} with company {1} has AAS {2} role. Returning true",
                        AD_UserName, AAS_COMPANY_CODE, roleId));
                    hasUserRole = true;
                }
            }
            catch(Exception ex)
            {
                log.Error(string.Format("AAS Execution failed for user name: {0}, company code: {1}, system id: {2}, role Id: {3} ",
                    AD_UserName, AAS_COMPANY_CODE, AAS_SYSTEM_ID, roleId));
                log.Error(string.Format("AAS Execution endPoint address: ", _authorizationClient.Endpoint.Address));
                log.Error(ex);
                throw ex;
            }
            return hasUserRole;
        }

        public List<string> GetAASUserRoles(string AD_UserName, string AAS_COMPANY_CODE, string AAS_SYSTEM_ID)
        {
            List<string> AAS_UserRolesList = new List<string>();
            string AAS_UserRoles = "";
            try
            {
                AAS_UserRoles = _authorizationClient.GetUserRole(AD_UserName, AAS_COMPANY_CODE, AAS_SYSTEM_ID, "");
                if (AAS_UserRoles.Equals(""))
                {
                    return AAS_UserRolesList;
                }
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(AAS_UserRoles);
                    XmlNodeList roleList = doc.DocumentElement.SelectNodes("/user_role/system/role");
                    foreach (XmlNode roleNode in roleList)
                    {
                        string AAS_UserRoleExtracted = roleNode == null ? "" : roleNode.Value;
                        AAS_UserRolesList.Add(AAS_UserRoleExtracted);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("AAS Execution failed for user name: {0}, company code: {1}, system id: {2} ",
                    AD_UserName, AAS_COMPANY_CODE, AAS_SYSTEM_ID));
                log.Error(string.Format("AAS Execution endPoint address: ", _authorizationClient.Endpoint.Address));
                log.Error(ex);
                throw ex;
            }
            return AAS_UserRolesList;
        }
    }
}
