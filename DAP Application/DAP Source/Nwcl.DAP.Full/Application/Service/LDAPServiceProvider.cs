﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;

namespace Nwcl.DAP.Application.Service
{
    public class LDAPServiceProvider : MarshalByRefObject
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SearchResultCollection SearchObjectsFromBaseDN(string username, string password
            , string basedn, string ldapQuery)
        {
            try { 
                log.Info(string.Format("Processing {0} with query: {1}", basedn, ldapQuery));
                DirectoryEntry rootEntry = new DirectoryEntry(basedn);
                //rootEntry.AuthenticationType = AuthenticationTypes.None; //Or whatever it need be
                rootEntry.Username = username;
                rootEntry.Password = password;
                DirectorySearcher searcher = new DirectorySearcher(rootEntry);
                searcher.Filter = ldapQuery;
                searcher.PageSize = 999999;

                searcher.PropertiesToLoad.Add("samaccountname");
                searcher.PropertiesToLoad.Add("displayname");
                searcher.PropertiesToLoad.Add("co");
                searcher.PropertiesToLoad.Add("company");
                searcher.PropertiesToLoad.Add("department");
                searcher.PropertiesToLoad.Add("title");
                searcher.PropertiesToLoad.Add("mail");

                SearchResultCollection result = searcher.FindAll();
                // log.Info(string.Format("Search returns from {0} with count {1}", basedn, result.Count));
                searcher.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
        }

        public List<DAL.Model.MST_URG_USR> ConvertResultToDALUsers(SearchResultCollection result
            , string domainCode, string modifier)
        {
            try {
                log.Debug(string.Format("entering function for domain {0} with result count {1}", domainCode, result.Count));
                List<DAL.Model.MST_URG_USR> userList = new List<DAL.Model.MST_URG_USR>();

                if (result.Count > 0)
                {
                    foreach (SearchResult r in result)
                    {
                        if (r.Properties["samaccountname"].Count == 0)
                        {
                            log.Debug(string.Format("samaccountname is missing, skipping record: {0}", r.Path));
                            continue;
                        }
                        log.Debug(string.Format("Processing user with samaccountname: {0}", r.Properties["samaccountname"][0]));
                        DAL.Model.MST_URG_USR user = new DAL.Model.MST_URG_USR()
                        {
                            ADLogonName = string.Format("{0}\\{1}", domainCode.ToLower(), r.Properties["samaccountname"][0]),
                            ADDisplayName = r.Properties["displayName"].Count > 0 ? (string)r.Properties["displayName"][0] : "",
                            ADDomain = domainCode.ToLower(),
                            ADRegion = r.Properties["co"].Count > 0 ? (string)r.Properties["co"][0] : "",
                            ADCompany = r.Properties["company"].Count > 0 ? (string)r.Properties["company"][0] : "",
                            ADDepartment = r.Properties["department"].Count > 0 ? (string)r.Properties["department"][0] : "",
                            ADEmail = r.Properties["mail"].Count > 0 ? (string)r.Properties["mail"][0] : "",
                            ADTitle = r.Properties["title"].Count > 0 ? (string)r.Properties["title"][0] : "",
                            ModifiedDt = DateTime.Now,
                            ModifiedBy = modifier
                        };
                        userList.Add(user);
                    }
                }
                log.Debug(string.Format("leaving function for domain {0} with userList count {1}", domainCode, userList.Count));
                return userList;
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
        }
    }
}
