﻿USE [DAP_PAP]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetEntityGroupTreeView]    Script Date: 11/15/2018 11:10:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [dbo].[sp_GetEntityGroupTreeView]
ALTER PROCEDURE [dbo].[sp_GetEntityGroupTreeView]
	
AS
BEGIN
	DECLARE @SplunkProjectCode TABLE
	(
		ProjectCode NVARCHAR(200)
	)
	
	INSERT INTO @SplunkProjectCode SELECT ProjectFullCode as ProjectCode from INT_SPLUNK_PROJECT

	INSERT INTO @SplunkProjectCode
	SELECT SUBSTRING(ProjectFullCode,1,11) FROM INT_SPLUNK_PROJECT
	WHERE LEN(ProjectFullCode) = 13

	INSERT INTO @SplunkProjectCode
	SELECT SUBSTRING(ProjectFullCode,1,9) FROM INT_SPLUNK_PROJECT
	WHERE LEN(ProjectFullCode) >= 11
	
	INSERT INTO @SplunkProjectCode
	SELECT SUBSTRING(ProjectFullCode,1,7) FROM INT_SPLUNK_PROJECT
	WHERE LEN(ProjectFullCode) >= 9

	DECLARE @DistinctSplunkProjectCode TABLE
	(
		ProjectCode NVARCHAR(200)
	)

	INSERT INTO @DistinctSplunkProjectCode SELECT Distinct ProjectCode  From @SplunkProjectCode ORDER BY ProjectCode
	
	DECLARE @EntityGroupHierarchy TABLE 
	(
		Code NVARCHAR(100),
		NameCN NVARCHAR(200),
		NameEN NVARCHAR(200),
		ParentCode NVARCHAR(200),
		[Type] VARCHAR(100),
		Id INT
	)

	-- Region
	INSERT INTO @EntityGroupHierarchy
	SELECT 
		RegionCode AS 'Code',
		RegionNameCN AS 'NameCN',
		RegionNameEN AS 'NameEN',
		'Root' AS 'ParentCode',
		'1 Region' AS 'Type',
		Id AS 'Id'
		FROM
		MST_PROJ_REGION
	
	-- City
	INSERT INTO @EntityGroupHierarchy
		SELECT 
		CityCode AS 'Code',
		CityNameCN AS 'NameCN',
		CityNameEN AS 'NameEN',
		RegionCode AS 'ParentCode',
		'2 City' AS 'Type',
		Id AS 'Id'
		FROM MST_PROJ_CITY
	
	INSERT INTO @EntityGroupHierarchy
	SELECT
	-- Project Root Level		
		ProjectFullCode AS 'Code',
		ProjectNameCN AS 'NameCN',
		ProjectNameEN AS 'NameEN',
		CityCode AS 'ParentCode',
		'3 Project' AS 'Type',
		Id
		FROM
		MST_PROJ_PROJECT p INNER JOIN @DistinctSplunkProjectCode a ON a.ProjectCode = p.ProjectFullCode
		WHERE ISNULL(ProjectPhase1, '') = ''
		AND ISNULL(ProjectPhase2, '') = ''
		AND ISNULL(ProjectPhase3, '') = ''
	
	INSERT INTO @EntityGroupHierarchy
	SELECT
	-- Project Level 1, Level 2, Level 3		
		ProjectFullCode AS 'Code',
		ProjectNameCN AS 'NameCN',
		ProjectNameEN AS 'NameEN',
		ParentProjectFullCode AS 'ParentCode',
		'4 Project' AS 'Type',
		Id
		FROM
		MST_PROJ_PROJECT p INNER JOIN @DistinctSplunkProjectCode a ON a.ProjectCode = p.ProjectFullCode
		WHERE ISNULL(ProjectPhase1,'') != ''
		OR ISNULL(ProjectPhase2, '') != ''
		OR ISNULL(ProjectPhase3, '') != ''
			
	SELECT * FROM @EntityGroupHierarchy 


END
GO
