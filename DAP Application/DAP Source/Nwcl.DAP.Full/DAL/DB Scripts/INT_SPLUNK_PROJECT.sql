﻿USE [DAP_PAP]
GO

/****** Object:  Table [dbo].[INT_SPLUNK_PROJECT]    Script Date: 11/27/2018 2:30:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[INT_SPLUNK_PROJECT](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProjectFullCode] [nvarchar](30) NOT NULL,
	[ModifiedDt] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL
) ON [PRIMARY]
GO


