﻿USE [DAP_PAP]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetBreadcrumbsByPageID]    Script Date: 11/14/2018 9:47:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_GetBreadcrumbsByPageID]
	@PageID varchar(50),
	@LanguageCode varchar(10)
AS
BEGIN

	SET NOCOUNT ON;

With tbl
	As
	(
		Select 1 As Seq, ID, Case When UPPER(@LanguageCode) In ('CN','ZH-CN')  Then NameCN Else NameEN End As Name, ParentID, OrderSeq, Path, BeforeTitleIcon, AfterTitleIcon, Status from Menu 
		Where PageId = @PageId
		Union All
		Select b.Seq+1, a.ID, Case When UPPER(@LanguageCode) In ('CN','ZH-CN') Then a.NameCN Else a.NameEN End As Name, a.ParentID, a.OrderSeq, a.Path, a.BeforeTitleIcon, a.AfterTitleIcon, a.Status From Menu a join tbl b on b.ParentID = a.ID
	)

	Select Seq, Name, Case When Status = 1 Then Path Else '#' End As Path, BeforeTitleIcon, AfterTitleIcon, Status From tbl 
	Where ID <> 10000
	Union All
	Select 99, Case When UPPER(@LanguageCode) In ('CN','ZH-CN')  Then NameCN Else NameEN End, Path, BeforeTitleIcon, AfterTitleIcon, Status from Menu 
	Where ID = 10000
	Order By Seq Desc

END
