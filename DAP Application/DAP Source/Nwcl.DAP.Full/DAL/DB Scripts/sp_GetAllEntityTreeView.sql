﻿USE [DAP_PAP]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetEntityGroupTreeView]    Script Date: 11/15/2018 11:10:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [dbo].[sp_GetEntityGroupTreeView]
CREATE PROCEDURE [dbo].[sp_GetAllEntityTreeView]
	
AS
BEGIN
	
	-- Region
	SELECT 
		RegionCode AS 'Code',
		RegionNameCN AS 'NameCN',
		RegionNameEN AS 'NameEN',
		'Root' AS 'ParentCode',
		'1 Region' AS 'Type',
		Id AS 'Id'
		FROM
		MST_PROJ_REGION
	UNION
	-- City
		SELECT 
		CityCode AS 'Code',
		CityNameCN AS 'NameCN',
		CityNameEN AS 'NameEN',
		RegionCode AS 'ParentCode',
		'2 City' AS 'Type',
		Id AS 'Id'
		FROM MST_PROJ_CITY
	UNION
	-- Project Root Level
		SELECT 
		ProjectFullCode AS 'Code',
		ProjectNameCN AS 'NameCN',
		ProjectNameEN AS 'NameEN',
		CityCode AS 'ParentCode',
		'3 Project' AS 'Type',
		Id
		FROM
		MST_PROJ_PROJECT
		WHERE ISNULL(ProjectPhase1, '') = ''
		AND ISNULL(ProjectPhase2, '') = ''
		AND ISNULL(ProjectPhase3, '') = ''
	UNION
	-- Project Level 1, Level 2, Level 3
		SELECT 
		ProjectFullCode AS 'Code',
		ProjectNameCN AS 'NameCN',
		ProjectNameEN AS 'NameEN',
		ParentProjectFullCode AS 'ParentCode',
		'4 Project' AS 'Type',
		Id
		FROM
		MST_PROJ_PROJECT
		WHERE ISNULL(ProjectPhase1,'') != ''
		OR ISNULL(ProjectPhase2, '') != ''
		OR ISNULL(ProjectPhase3, '') != ''
		
		
END
GO
