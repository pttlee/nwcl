﻿USE [DAP_PAP]
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteEntityGroupProjectByEntityGroupId]    Script Date: 11/14/2018 9:45:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_DeleteEntityGroupProjectByEntityGroupId]
	@EntityGroupID INT
AS
BEGIN
	
	SET NOCOUNT ON;

	DELETE FROM PRV_ENT_ENTGROUP_PROJ_RELN WHERE EntityGroupId = @EntityGroupID
END
