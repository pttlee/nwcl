﻿USE [DAP_PAP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sp_GetProjectCodeFromSplunk
AS
BEGIN
	SELECT ProjectCode from openquery([SPLUNK ODBC],'select project_code_standardized as ProjectCode  from [SPLUNK ODBC].[All].[RootObject in DAP_DS_ALL_PROJECT_LIST]')
END
GO
