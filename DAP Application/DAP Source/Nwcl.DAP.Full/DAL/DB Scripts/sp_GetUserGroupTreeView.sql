﻿-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- exec [dbo].[sp_GetUserGroupTreeView]
ALTER PROCEDURE [dbo].[sp_GetUserGroupTreeView]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	
	DECLARE  @UserGroupTreeView TABLE
	(
		Code NVARCHAR(100),
		NameCN NVARCHAR(200),
		NameEN NVARCHAR(200),
		ParentCode NVARCHAR(200),
		[Type] VARCHAR(100),
		Id INT
	)

	-- Region
	INSERT INTO @UserGroupTreeView
	SELECT 
		RegionCode AS 'Code',
		RegionNameCN AS 'NameCN',
		RegionNameEN AS 'NameEN',
		'Root' AS 'ParentCode',
		'1 Region' AS 'Type',
		Id AS 'Id'
		FROM
		MST_PROJ_REGION
	
	-- City
	INSERT INTO @UserGroupTreeView
		SELECT 
		CityCode AS 'Code',
		CityNameCN AS 'NameCN',
		CityNameEN AS 'NameEN',
		RegionCode AS 'ParentCode',
		'2 City' AS 'Type',
		Id AS 'Id'
		FROM MST_PROJ_CITY
	
	-- Company	
	INSERT INTO @UserGroupTreeView
		SELECT
		'c' + CONVERT(NVARCHAR(100), Id) AS 'Code',
		CompanyName AS 'NameCN',
		CompanyName AS 'NameEN',
		CityCode AS 'ParentCode',
		'3 Company' AS 'Type',
		Id AS 'Id'
		FROM
		PRV_URG_COMPANY
		
	-- Department	
	INSERT INTO @UserGroupTreeView
		SELECT
		'd' + CONVERT(NVARCHAR(100), Id) AS 'Code',
		DepartmentName AS 'NameCN',
		DepartmentName AS 'NameEN',
		'c' + CONVERT(NVARCHAR(100), CompanyId) AS 'ParentCode',
		'4 Department' AS 'Type',
		Id AS 'Id'
		FROM
		PRV_URG_DEPARTMENT
	
	-- User	
	INSERT INTO @UserGroupTreeView
		SELECT
		'u' + CONVERT(NVARCHAR(100), Id) AS 'Code',
		a.ADDisplayName + ' ('+a.ADEmail+')' AS 'NameCN',
		a.ADDisplayName + ' ('+a.ADEmail+')' AS 'NameEN',
		'd' + CONVERT(NVARCHAR(100), DepartmentId) AS 'ParentCode',
		'5 User' AS 'Type',
		Id AS 'Id'
		FROM
		PRV_URG_USER a
	
	
			
	SELECT * FROM @UserGroupTreeView 


	
	--drop table @UserGroupTreeView
	
END
GO
