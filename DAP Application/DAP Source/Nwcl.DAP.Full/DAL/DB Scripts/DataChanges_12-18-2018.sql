﻿USE [DAP_PAP]
GO
/****** Object:  Table [dbo].[MST_PROJ_CITY_UPLOAD]    Script Date: 12/18/2018 7:02:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MST_PROJ_CITY_UPLOAD](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [varchar](50) NOT NULL,
	[RegionCode] [nvarchar](10) NULL,
	[CityCode] [nvarchar](10) NOT NULL,
	[CityNameEN] [nvarchar](50) NULL,
	[CityNameCN] [nvarchar](10) NULL,
	[ModifiedDt] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
 CONSTRAINT [PK_MST_PROJ_CITY_UPLOAD] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC,
	[CityCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_MST_PROJ_CITY_UPLOAD] UNIQUE NONCLUSTERED 
(
	[GUID] ASC,
	[CityCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MST_PROJ_PROJECT_UPLOAD]    Script Date: 12/18/2018 7:02:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MST_PROJ_PROJECT_UPLOAD](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [varchar](50) NOT NULL,
	[CityCode] [nvarchar](10) NULL,
	[CityId] [int] NULL,
	[ProjectCode] [char](3) NULL,
	[ProjectPhase1] [varchar](2) NULL,
	[ProjectPhase2] [varchar](2) NULL,
	[ProjectPhase3] [varchar](2) NULL,
	[ProjectFullCode] [nvarchar](30) NOT NULL,
	[ProjectNameEN] [nvarchar](100) NULL,
	[ProjectNameCN] [nvarchar](50) NULL,
	[ProjectParentNameEN] [nvarchar](100) NULL,
	[ProjectParentNameCN] [nvarchar](50) NULL,
	[Remark] [nvarchar](500) NULL,
	[IsFinancial] [bit] NULL,
	[IsSale] [bit] NULL,
	[IsPM] [bit] NULL,
	[Keywords] [nvarchar](1000) NULL,
	[ParentID] [int] NULL,
	[ParentProjectFullCode] [varchar](50) NULL,
	[ModifiedDt] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
 CONSTRAINT [PK_MST_PROJ_PROJECT_UPLOAD] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC,
	[ProjectFullCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MST_PROJ_REGION_UPLOAD]    Script Date: 12/18/2018 7:02:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MST_PROJ_REGION_UPLOAD](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [varchar](50) NOT NULL,
	[RegionCode] [nvarchar](10) NOT NULL,
	[RegionNameEN] [nvarchar](50) NULL,
	[RegionNameCN] [nvarchar](10) NULL,
	[ModifiedDt] [datetime] NULL,
	[ModifieBy] [varchar](50) NULL,
 CONSTRAINT [PK_MST_PROJ_REGION_UPLOAD] PRIMARY KEY NONCLUSTERED 
(
	[GUID] ASC,
	[RegionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_MST_PROJ_REGION_UPLOAD] UNIQUE CLUSTERED 
(
	[GUID] ASC,
	[RegionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MST_PROJ_CITY_UPLOAD]  WITH NOCHECK ADD  CONSTRAINT [FK_CITY_UPLOAD_REGION_UPLOAD] FOREIGN KEY([GUID], [RegionCode])
REFERENCES [dbo].[MST_PROJ_REGION_UPLOAD] ([GUID], [RegionCode])
GO
ALTER TABLE [dbo].[MST_PROJ_CITY_UPLOAD] NOCHECK CONSTRAINT [FK_CITY_UPLOAD_REGION_UPLOAD]
GO
ALTER TABLE [dbo].[MST_PROJ_PROJECT_UPLOAD]  WITH NOCHECK ADD  CONSTRAINT [FK_PROJECT_UPLOAD_CITY_UPLOAD] FOREIGN KEY([GUID], [CityCode])
REFERENCES [dbo].[MST_PROJ_CITY_UPLOAD] ([GUID], [CityCode])
GO
ALTER TABLE [dbo].[MST_PROJ_PROJECT_UPLOAD] NOCHECK CONSTRAINT [FK_PROJECT_UPLOAD_CITY_UPLOAD]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllCityByRegion]    Script Date: 12/18/2018 7:02:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_GetAllCityByRegion] 
	-- Add the parameters for the stored procedure here
	@gid varchar(40)
	, @regionCode nvarchar(10)
AS
BEGIN
    IF (@gid IS NOT NULL) AND (LEN(@gid) > 0)
	BEGIN
		SELECT [Id]
		  ,[RegionCode]
		  ,[CityCode]
		  ,[CityNameEN]
		  ,[CityNameCN]
		  ,[ModifiedDt]
		  ,[ModifiedBy]
		FROM [DAP_PAP].[dbo].[MST_PROJ_CITY_UPLOAD]
	  WHERE [GUID] = @gid
	  AND [RegionCode] = @regionCode
	  END
	ELSE
	BEGIN
		SELECT [Id]
		  ,[RegionCode]
		  ,[CityCode]
		  ,[CityNameEN]
		  ,[CityNameCN]
		  ,[ModifiedDt]
		  ,[ModifiedBy]
		FROM [DAP_PAP].[dbo].[MST_PROJ_CITY]
	  WHERE [RegionCode] = @regionCode
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllProjectByCity]    Script Date: 12/18/2018 7:02:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetAllProjectByCity] 
	-- Add the parameters for the stored procedure here
	@gid varchar(40)
	, @cityCode nvarchar(10)
AS
BEGIN
    IF (@gid IS NOT NULL) AND (LEN(@gid) > 0)
	BEGIN
		SELECT [Id]
		  ,[CityCode]
		  ,[CityId]
		  ,[ProjectCode]
		  ,[ProjectPhase1]
		  ,[ProjectPhase2]
		  ,[ProjectPhase3]
		  ,[ProjectFullCode]
		  ,[ProjectNameEN]
		  ,[ProjectNameCN]
		  ,[ProjectParentNameEN]
		  ,[ProjectParentNameCN]
		  ,[Remark]
		  ,[IsFinancial]
		  ,[IsSale]
		  ,[IsPM]
		  ,[Keywords]
		  ,[ParentID]
		  ,[ParentProjectFullCode]
		  ,[ModifiedDt]
		  ,[ModifiedBy]
	  FROM [DAP_PAP].[dbo].[MST_PROJ_PROJECT_UPLOAD]
	  WHERE [GUID] = @gid
	  AND [CityCode] = @cityCode
	  END
	ELSE
	BEGIN
		SELECT [Id]
		  ,[CityCode]
		  ,[CityId]
		  ,[ProjectCode]
		  ,[ProjectPhase1]
		  ,[ProjectPhase2]
		  ,[ProjectPhase3]
		  ,[ProjectFullCode]
		  ,[ProjectNameEN]
		  ,[ProjectNameCN]
		  ,[ProjectParentNameEN]
		  ,[ProjectParentNameCN]
		  ,[Remark]
		  ,[IsFinancial]
		  ,[IsSale]
		  ,[IsPM]
		  ,[Keywords]
		  ,[ParentID]
		  ,[ParentProjectFullCode]
		  ,[ModifiedDt]
		  ,[ModifiedBy]
	  FROM [DAP_PAP].[dbo].[MST_PROJ_PROJECT]
	  WHERE [CityCode] = @cityCode
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllRegion]    Script Date: 12/18/2018 7:02:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetAllRegion] 
	-- Add the parameters for the stored procedure here
	@gid varchar(40)
AS
BEGIN
    IF (@gid IS NOT NULL) AND (LEN(@gid) > 0)
	BEGIN
		SELECT [Id]
      ,[RegionCode]
      ,[RegionNameEN]
      ,[RegionNameCN]
      ,[ModifiedDt]
      ,[ModifieBy]
	  FROM [DAP_PAP].[dbo].[MST_PROJ_REGION_UPLOAD]
	  WHERE [GUID] = @gid
	  END
	ELSE
	BEGIN
		SELECT [Id]
      ,[RegionCode]
      ,[RegionNameEN]
      ,[RegionNameCN]
      ,[ModifiedDt]
      ,[ModifieBy]
	  FROM [DAP_PAP].[dbo].[MST_PROJ_REGION]
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateMasterFromTemp]    Script Date: 12/18/2018 7:02:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_UpdateMasterFromTemp]
	@gid varchar(50)
AS
BEGIN
	IF (@gid IS NULL) OR (LEN(@gid) = 0)
	BEGIN
		return
	END

	BEGIN TRAN
	BEGIN TRY
	-- Delete Data From Master
	Delete from [dbo].[MST_PROJ_PROJECT]
	Delete from [dbo].[MST_PROJ_CITY]
	Delete from [dbo].[MST_PROJ_REGION]

	INSERT INTO [dbo].[MST_PROJ_REGION] 
	([RegionCode]
      ,[RegionNameEN]
      ,[RegionNameCN]
      ,[ModifiedDt]
      ,[ModifieBy])
	  Select [RegionCode]
      ,[RegionNameEN]
      ,[RegionNameCN]
      ,[ModifiedDt]
      ,[ModifieBy]
	  from [dbo].[MST_PROJ_REGION_UPLOAD] 
	  WHERE [GUID] = @gid

	  INSERT INTO [dbo].[MST_PROJ_CITY]
	  ([RegionCode]
      ,[CityCode]
      ,[CityNameEN]
      ,[CityNameCN]
      ,[ModifiedDt]
      ,[ModifiedBy])
	  Select [RegionCode]
      ,[CityCode]
      ,[CityNameEN]
      ,[CityNameCN]
      ,[ModifiedDt]
      ,[ModifiedBy]
	  from [dbo].[MST_PROJ_CITY_UPLOAD]
	  WHERE [GUID] = @gid

	  INSERT INTO [dbo].[MST_PROJ_PROJECT]
	  ([CityCode]
      ,[CityId]
      ,[ProjectCode]
      ,[ProjectPhase1]
      ,[ProjectPhase2]
      ,[ProjectPhase3]
      ,[ProjectFullCode]
      ,[ProjectNameEN]
      ,[ProjectNameCN]
      ,[ProjectParentNameEN]
      ,[ProjectParentNameCN]
      ,[Remark]
      ,[IsFinancial]
      ,[IsSale]
      ,[IsPM]
      ,[Keywords]
      ,[ParentID]
      ,[ParentProjectFullCode]
      ,[ModifiedDt]
      ,[ModifiedBy])
	  Select [CityCode]
      ,[CityId]
      ,[ProjectCode]
      ,[ProjectPhase1]
      ,[ProjectPhase2]
      ,[ProjectPhase3]
      ,[ProjectFullCode]
      ,[ProjectNameEN]
      ,[ProjectNameCN]
      ,[ProjectParentNameEN]
      ,[ProjectParentNameCN]
      ,[Remark]
      ,[IsFinancial]
      ,[IsSale]
      ,[IsPM]
      ,[Keywords]
      ,[ParentID]
      ,[ParentProjectFullCode]
      ,[ModifiedDt]
      ,[ModifiedBy]
	FROM [dbo].[MST_PROJ_PROJECT_UPLOAD]
	WHERE [GUID] = @gid
	COMMIT TRAN
	END TRY
	BEGIN CATCH
	IF @@TRANCOUNT > 0 ROLLBACK
	END CATCH
END
GO
