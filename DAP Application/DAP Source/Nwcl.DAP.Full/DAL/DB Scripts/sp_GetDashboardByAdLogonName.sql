﻿USE [DAP_PAP]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetDashboardByAdLogonName]    Script Date: 12/5/2018 11:05:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec [sp_GetDashboardByAdLogonName] 'dev\administrator'
ALTER PROCEDURE [dbo].[sp_GetDashboardByAdLogonName]
@AdLogonName NVARCHAR(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT distinct d.DashboardNameCN, d.DashboardNameEN, d.Id, d.ReportId, d.WorkspaceId FROM
	[dbo].[MST_URG_USR] u 
	INNER JOIN [dbo].[PRV_URG_USER_GROUP_RELN] ugr ON u.ADLogonName = ugr.ADLogonName
	INNER JOIN [dbo].[PRV_ENT_URG_ROL_RELN] eurr ON eurr.UserGroupId = ugr.UserGroupId
	INNER JOIN [dbo].[PRV_ROL_ROLE_DASHBOARD_RELN] dr ON dr.RoleId = eurr.RoleId
	INNER JOIN [dbo].[SYS_CFG_DASHBOARD] d ON d.Id = dr.DashboardId
	WHERE u.ADLogonName = @AdLogonName

END
