﻿USE [DAP_PAP]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetMenuByUser]    Script Date: 11/14/2018 9:48:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_GetMenuByUser]
	@UserName nvarchar(256),
	@LanguageCode varchar(10)
AS
BEGIN

	SET NOCOUNT ON;

	With tbl
	As
	(
		Select ID, Case When UPPER(@LanguageCode) In ('CN','ZH-CN')  Then NameCN Else NameEN End As Name, ParentID, OrderSeq, Path, BeforeTitleIcon, AfterTitleIcon, Status from Menu 
		Where ParentID Is Null And Display = 1 And Status = 1
		Union All
		Select a.ID, Case When UPPER(@LanguageCode) In ('CN','ZH-CN') Then a.NameCN Else a.NameEN End As Name, a.ParentID, a.OrderSeq, a.Path, a.BeforeTitleIcon, a.AfterTitleIcon, a.Status From Menu a join tbl b on a.ParentID = b.ID
		Where a.Display = 1 And a.Status = 1
	)

	Select * From tbl Order By OrderSeq, ID
END
