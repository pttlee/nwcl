﻿USE [DAP_PAP]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetUserByDepartmentId]    Script Date: 11/28/2018 4:33:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[sp_GetUserByDepartmentId] 
@DepartmentId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ADLogonName, ADDomain FROM
	[dbo].[PRV_URG_DEPARTMENT_USER_RELN] d 
	WHERE d.DepartmentId = @DepartmentId
	
END
GO


