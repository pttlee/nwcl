﻿using Nwcl.DAP.DAL.DALManager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.DAL
{
    /// <summary>
    /// Factory class to create DAL Manager object instance
    /// </summary>
    public static class DALManagerFactory
    {
        /// <summary>
        /// Gets (Creates if needed) the object instance of the specified type of DAL Manager
        /// </summary>
        /// <param name="dalManagerType"></param>
        /// <returns></returns>
        public static object GetDALManager(Type dalManagerType)
        {
            // TODO: Use .NET reflection to instantiate the DAL Manager object instance specified by the dalManagerType
            object dalManagerObj = null;
            if(dalManagerType != null)
            {
                if(dalManagerType == typeof(CommonDALManager))
                {
                    dalManagerObj = new CommonDALManager();
                }
                else
                {
                    switch (dalManagerType.FullName)
                    {
                        case "Nwcl.DAP.DAL.DALManager.ProjectDALManager":
                            dalManagerObj = CreateTypeInstance("Nwcl.DAP.DAL.Project.dll", "Nwcl.DAP.DAL.DALManager.ProjectDALManager");
                            break;
                        case "Nwcl.DAP.DAL.DALManager.CostPlanDALManager":
                            dalManagerObj = CreateTypeInstance("Nwcl.DAP.DAL.Project.dll", "Nwcl.DAP.DAL.DALManager.CostPlanDALManager");
                            break;
                        case "Nwcl.DAP.DAL.DALManager.BudgetPlanDALManager":
                            dalManagerObj = CreateTypeInstance("Nwcl.DAP.DAL.Project.dll", "Nwcl.DAP.DAL.DALManager.BudgetPlanDALManager");
                            break;
                        case "Nwcl.DAP.DAL.DALManager.MasterMaintenanceDALManager":
                            dalManagerObj = CreateTypeInstance("Nwcl.DAP.DAL.MasterMaintenance.dll", "Nwcl.DAP.DAL.DALManager.MasterMaintenanceDALManager");
                            break;
                        case "Nwcl.DAP.DAL.DALManager.AdministrationDALManager":
                            dalManagerObj = CreateTypeInstance("Nwcl.DAP.DAL.Administration.dll", "Nwcl.DAP.DAL.DALManager.AdministrationDALManager");
                            break;
                        case "Nwcl.DAP.DAL.DALManager.TradeBudgetDALManager":
                            dalManagerObj = CreateTypeInstance("Nwcl.DAP.DAL.MasterMaintenance.dll", "Nwcl.DAP.DAL.DALManager.TradeBudgetDALManager");
                            break;
                        case "Nwcl.DAP.DAL.DALManager.TradePackageDALManager":
                            dalManagerObj = CreateTypeInstance("Nwcl.DAP.DAL.Project.dll", "Nwcl.DAP.DAL.DALManager.TradePackageDALManager");
                            break;
                        case "Nwcl.DAP.DAL.DALManager.ProgramManagementDALManager":
                            dalManagerObj = CreateTypeInstance("Nwcl.DAP.DAL.Project.dll", "Nwcl.DAP.DAL.DALManager.ProgramManagementDALManager");
                            break;
                    }
                }
            }
            return dalManagerObj;
        }

        private static Type GetType(string assemblyName, string typeName)
        {
            Type type = Type.GetType(typeName);
            if (type == null)
            {
                string assemblyFolderPath = null;
                if(ConfigurationManager.AppSettings["AssemblyFolder"] != null)
                {
                    assemblyFolderPath = Path.Combine(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase, ConfigurationManager.AppSettings["AssemblyFolder"]);
                }
                else
                {
                    assemblyFolderPath = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
                }
                Assembly assembly = Assembly.LoadFrom(Path.Combine(assemblyFolderPath, assemblyName));
                type = assembly.GetType(typeName, true);
            }
            return type;
        }

        private static object CreateTypeInstance(string assemblyName, string typeName)
        {
            return System.Activator.CreateInstance(GetType(assemblyName, typeName));
        }
    }
}
