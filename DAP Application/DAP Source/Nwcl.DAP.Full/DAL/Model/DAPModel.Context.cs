﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nwcl.DAP.DAL.Model
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class DAPEntities : DbContext
    {
        public DAPEntities()
            : base("name=DAPEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Menu> Menus { get; set; }
        public virtual DbSet<MST_PROJ_CITY> MST_PROJ_CITY { get; set; }
        public virtual DbSet<MST_PROJ_PROJECT> MST_PROJ_PROJECT { get; set; }
        public virtual DbSet<MST_PROJ_REGION> MST_PROJ_REGION { get; set; }
        public virtual DbSet<PRV_ENT_ENTGROUP> PRV_ENT_ENTGROUP { get; set; }
        public virtual DbSet<PRV_ENT_ENTGROUP_PROJ_RELN> PRV_ENT_ENTGROUP_PROJ_RELN { get; set; }
        public virtual DbSet<PRV_ENT_URG_ROL_RELN> PRV_ENT_URG_ROL_RELN { get; set; }
        public virtual DbSet<PRV_ROL_ROLE> PRV_ROL_ROLE { get; set; }
        public virtual DbSet<PRV_ROL_ROLE_DASHBOARD_RELN> PRV_ROL_ROLE_DASHBOARD_RELN { get; set; }
        public virtual DbSet<PRV_URG_COMPANY> PRV_URG_COMPANY { get; set; }
        public virtual DbSet<PRV_URG_DEPARTMENT> PRV_URG_DEPARTMENT { get; set; }
        public virtual DbSet<PRV_URG_USRGROUP> PRV_URG_USRGROUP { get; set; }
        public virtual DbSet<SYS_CFG_DASHBOARD> SYS_CFG_DASHBOARD { get; set; }
        public virtual DbSet<SYS_CFG_PARAM> SYS_CFG_PARAM { get; set; }
        public virtual DbSet<SystemMessage> SystemMessages { get; set; }
        public virtual DbSet<SystemParameter> SystemParameters { get; set; }
        public virtual DbSet<MST_URG_USR> MST_URG_USR { get; set; }
        public virtual DbSet<PRV_URG_USER_GROUP_RELN> PRV_URG_USER_GROUP_RELN { get; set; }
        public virtual DbSet<PRV_URG_DEPARTMENT_USER_RELN> PRV_URG_DEPARTMENT_USER_RELN { get; set; }
        public virtual DbSet<INT_SPLUNK_PROJECT> INT_SPLUNK_PROJECT { get; set; }
        public virtual DbSet<MST_PROJ_CITY_UPLOAD> MST_PROJ_CITY_UPLOAD { get; set; }
        public virtual DbSet<MST_PROJ_PROJECT_UPLOAD> MST_PROJ_PROJECT_UPLOAD { get; set; }
        public virtual DbSet<MST_PROJ_REGION_UPLOAD> MST_PROJ_REGION_UPLOAD { get; set; }
    
        public virtual int sp_DeleteDepartmentUserByAdLogonNameAndDepartmentId(Nullable<int> departmentId, string aDLogonName)
        {
            var departmentIdParameter = departmentId.HasValue ?
                new ObjectParameter("DepartmentId", departmentId) :
                new ObjectParameter("DepartmentId", typeof(int));
    
            var aDLogonNameParameter = aDLogonName != null ?
                new ObjectParameter("ADLogonName", aDLogonName) :
                new ObjectParameter("ADLogonName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_DeleteDepartmentUserByAdLogonNameAndDepartmentId", departmentIdParameter, aDLogonNameParameter);
        }
    
        public virtual int sp_DeleteEntityGroupProjectByEntityGroupId(Nullable<int> entityGroupID)
        {
            var entityGroupIDParameter = entityGroupID.HasValue ?
                new ObjectParameter("EntityGroupID", entityGroupID) :
                new ObjectParameter("EntityGroupID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_DeleteEntityGroupProjectByEntityGroupId", entityGroupIDParameter);
        }
    
        public virtual int sp_DeleteUserGroupUserByUserGroupId(Nullable<int> userGroupId)
        {
            var userGroupIdParameter = userGroupId.HasValue ?
                new ObjectParameter("UserGroupId", userGroupId) :
                new ObjectParameter("UserGroupId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_DeleteUserGroupUserByUserGroupId", userGroupIdParameter);
        }
    
        public virtual ObjectResult<sp_GetAllEntityTreeView_Result> sp_GetAllEntityTreeView()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetAllEntityTreeView_Result>("sp_GetAllEntityTreeView");
        }
    
        public virtual ObjectResult<sp_GetBreadcrumbsByPageID_Result> sp_GetBreadcrumbsByPageID(string pageID, string languageCode)
        {
            var pageIDParameter = pageID != null ?
                new ObjectParameter("PageID", pageID) :
                new ObjectParameter("PageID", typeof(string));
    
            var languageCodeParameter = languageCode != null ?
                new ObjectParameter("LanguageCode", languageCode) :
                new ObjectParameter("LanguageCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetBreadcrumbsByPageID_Result>("sp_GetBreadcrumbsByPageID", pageIDParameter, languageCodeParameter);
        }
    
        public virtual ObjectResult<sp_GetDashboardByAdLogonName_Result> sp_GetDashboardByAdLogonName(string adLogonName)
        {
            var adLogonNameParameter = adLogonName != null ?
                new ObjectParameter("AdLogonName", adLogonName) :
                new ObjectParameter("AdLogonName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetDashboardByAdLogonName_Result>("sp_GetDashboardByAdLogonName", adLogonNameParameter);
        }
    
        public virtual ObjectResult<sp_GetEntityGroupTreeView_Result> sp_GetEntityGroupTreeView()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetEntityGroupTreeView_Result>("sp_GetEntityGroupTreeView");
        }
    
        public virtual ObjectResult<sp_GetMenuByUser_Result> sp_GetMenuByUser(string userName, string languageCode)
        {
            var userNameParameter = userName != null ?
                new ObjectParameter("UserName", userName) :
                new ObjectParameter("UserName", typeof(string));
    
            var languageCodeParameter = languageCode != null ?
                new ObjectParameter("LanguageCode", languageCode) :
                new ObjectParameter("LanguageCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetMenuByUser_Result>("sp_GetMenuByUser", userNameParameter, languageCodeParameter);
        }
    
        public virtual ObjectResult<string> sp_GetParentEntityByEntityGroupID(Nullable<int> entityGroupId)
        {
            var entityGroupIdParameter = entityGroupId.HasValue ?
                new ObjectParameter("EntityGroupId", entityGroupId) :
                new ObjectParameter("EntityGroupId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("sp_GetParentEntityByEntityGroupID", entityGroupIdParameter);
        }
    
        public virtual ObjectResult<string> sp_GetProjectCodeFromSplunk()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("sp_GetProjectCodeFromSplunk");
        }
    
        public virtual int sp_GetSelectedParentByUserGroupID(Nullable<int> userGroupId)
        {
            var userGroupIdParameter = userGroupId.HasValue ?
                new ObjectParameter("UserGroupId", userGroupId) :
                new ObjectParameter("UserGroupId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_GetSelectedParentByUserGroupID", userGroupIdParameter);
        }
    
        public virtual ObjectResult<sp_GetUserByDepartmentId_Result> sp_GetUserByDepartmentId(Nullable<int> departmentId)
        {
            var departmentIdParameter = departmentId.HasValue ?
                new ObjectParameter("DepartmentId", departmentId) :
                new ObjectParameter("DepartmentId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetUserByDepartmentId_Result>("sp_GetUserByDepartmentId", departmentIdParameter);
        }
    
        public virtual int sp_GetUserGroupTreeView()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_GetUserGroupTreeView");
        }
    
        public virtual ObjectResult<Nullable<int>> sp_InsertProjectByExcel(string cityCode, string projectCode, string projectPhase1, string projectPhase2, string projectPhase3, string projectFullCode, string projectNameEN, string projectNameCN, string remark, Nullable<bool> isFinancial, Nullable<bool> isSale, Nullable<bool> isPM, string keywords, string user)
        {
            var cityCodeParameter = cityCode != null ?
                new ObjectParameter("CityCode", cityCode) :
                new ObjectParameter("CityCode", typeof(string));
    
            var projectCodeParameter = projectCode != null ?
                new ObjectParameter("ProjectCode", projectCode) :
                new ObjectParameter("ProjectCode", typeof(string));
    
            var projectPhase1Parameter = projectPhase1 != null ?
                new ObjectParameter("ProjectPhase1", projectPhase1) :
                new ObjectParameter("ProjectPhase1", typeof(string));
    
            var projectPhase2Parameter = projectPhase2 != null ?
                new ObjectParameter("ProjectPhase2", projectPhase2) :
                new ObjectParameter("ProjectPhase2", typeof(string));
    
            var projectPhase3Parameter = projectPhase3 != null ?
                new ObjectParameter("ProjectPhase3", projectPhase3) :
                new ObjectParameter("ProjectPhase3", typeof(string));
    
            var projectFullCodeParameter = projectFullCode != null ?
                new ObjectParameter("ProjectFullCode", projectFullCode) :
                new ObjectParameter("ProjectFullCode", typeof(string));
    
            var projectNameENParameter = projectNameEN != null ?
                new ObjectParameter("ProjectNameEN", projectNameEN) :
                new ObjectParameter("ProjectNameEN", typeof(string));
    
            var projectNameCNParameter = projectNameCN != null ?
                new ObjectParameter("ProjectNameCN", projectNameCN) :
                new ObjectParameter("ProjectNameCN", typeof(string));
    
            var remarkParameter = remark != null ?
                new ObjectParameter("Remark", remark) :
                new ObjectParameter("Remark", typeof(string));
    
            var isFinancialParameter = isFinancial.HasValue ?
                new ObjectParameter("IsFinancial", isFinancial) :
                new ObjectParameter("IsFinancial", typeof(bool));
    
            var isSaleParameter = isSale.HasValue ?
                new ObjectParameter("IsSale", isSale) :
                new ObjectParameter("IsSale", typeof(bool));
    
            var isPMParameter = isPM.HasValue ?
                new ObjectParameter("IsPM", isPM) :
                new ObjectParameter("IsPM", typeof(bool));
    
            var keywordsParameter = keywords != null ?
                new ObjectParameter("Keywords", keywords) :
                new ObjectParameter("Keywords", typeof(string));
    
            var userParameter = user != null ?
                new ObjectParameter("User", user) :
                new ObjectParameter("User", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("sp_InsertProjectByExcel", cityCodeParameter, projectCodeParameter, projectPhase1Parameter, projectPhase2Parameter, projectPhase3Parameter, projectFullCodeParameter, projectNameENParameter, projectNameCNParameter, remarkParameter, isFinancialParameter, isSaleParameter, isPMParameter, keywordsParameter, userParameter);
        }
    
        public virtual ObjectResult<sp_SearchUserByCriteria_Result> sp_SearchUserByCriteria(string domainList, string criteria)
        {
            var domainListParameter = domainList != null ?
                new ObjectParameter("DomainList", domainList) :
                new ObjectParameter("DomainList", typeof(string));
    
            var criteriaParameter = criteria != null ?
                new ObjectParameter("Criteria", criteria) :
                new ObjectParameter("Criteria", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_SearchUserByCriteria_Result>("sp_SearchUserByCriteria", domainListParameter, criteriaParameter);
        }
    
        public virtual ObjectResult<MST_PROJ_CITY> sp_GetAllCityByRegion(string gid, string regionCode)
        {
            var gidParameter = gid != null ?
                new ObjectParameter("gid", gid) :
                new ObjectParameter("gid", typeof(string));
    
            var regionCodeParameter = regionCode != null ?
                new ObjectParameter("regionCode", regionCode) :
                new ObjectParameter("regionCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MST_PROJ_CITY>("sp_GetAllCityByRegion", gidParameter, regionCodeParameter);
        }
    
        public virtual ObjectResult<MST_PROJ_CITY> sp_GetAllCityByRegion(string gid, string regionCode, MergeOption mergeOption)
        {
            var gidParameter = gid != null ?
                new ObjectParameter("gid", gid) :
                new ObjectParameter("gid", typeof(string));
    
            var regionCodeParameter = regionCode != null ?
                new ObjectParameter("regionCode", regionCode) :
                new ObjectParameter("regionCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MST_PROJ_CITY>("sp_GetAllCityByRegion", mergeOption, gidParameter, regionCodeParameter);
        }
    
        public virtual ObjectResult<MST_PROJ_PROJECT> sp_GetAllProjectByCity(string gid, string cityCode)
        {
            var gidParameter = gid != null ?
                new ObjectParameter("gid", gid) :
                new ObjectParameter("gid", typeof(string));
    
            var cityCodeParameter = cityCode != null ?
                new ObjectParameter("cityCode", cityCode) :
                new ObjectParameter("cityCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MST_PROJ_PROJECT>("sp_GetAllProjectByCity", gidParameter, cityCodeParameter);
        }
    
        public virtual ObjectResult<MST_PROJ_PROJECT> sp_GetAllProjectByCity(string gid, string cityCode, MergeOption mergeOption)
        {
            var gidParameter = gid != null ?
                new ObjectParameter("gid", gid) :
                new ObjectParameter("gid", typeof(string));
    
            var cityCodeParameter = cityCode != null ?
                new ObjectParameter("cityCode", cityCode) :
                new ObjectParameter("cityCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MST_PROJ_PROJECT>("sp_GetAllProjectByCity", mergeOption, gidParameter, cityCodeParameter);
        }
    
        public virtual ObjectResult<MST_PROJ_REGION> sp_GetAllRegion(string gid)
        {
            var gidParameter = gid != null ?
                new ObjectParameter("gid", gid) :
                new ObjectParameter("gid", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MST_PROJ_REGION>("sp_GetAllRegion", gidParameter);
        }
    
        public virtual ObjectResult<MST_PROJ_REGION> sp_GetAllRegion(string gid, MergeOption mergeOption)
        {
            var gidParameter = gid != null ?
                new ObjectParameter("gid", gid) :
                new ObjectParameter("gid", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MST_PROJ_REGION>("sp_GetAllRegion", mergeOption, gidParameter);
        }
    
        public virtual int sp_UpdateMasterFromTemp(string gid)
        {
            var gidParameter = gid != null ?
                new ObjectParameter("gid", gid) :
                new ObjectParameter("gid", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_UpdateMasterFromTemp", gidParameter);
        }
    }
}
