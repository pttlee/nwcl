//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nwcl.DAP.DAL.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class MST_PROJ_REGION_UPLOAD
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MST_PROJ_REGION_UPLOAD()
        {
            this.MST_PROJ_CITY_UPLOAD = new HashSet<MST_PROJ_CITY_UPLOAD>();
        }
    
        public int Id { get; set; }
        public string GUID { get; set; }
        public string RegionCode { get; set; }
        public string RegionNameEN { get; set; }
        public string RegionNameCN { get; set; }
        public Nullable<System.DateTime> ModifiedDt { get; set; }
        public string ModifieBy { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MST_PROJ_CITY_UPLOAD> MST_PROJ_CITY_UPLOAD { get; set; }
    }
}
