//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nwcl.DAP.DAL.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class SYS_CFG_PARAM
    {
        public int Id { get; set; }
        public string FuncGroup { get; set; }
        public string SubFuncGroup { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string DisplayName { get; set; }
        public Nullable<int> DisplayOrder { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<bool> IsEnabled { get; set; }
    }
}
