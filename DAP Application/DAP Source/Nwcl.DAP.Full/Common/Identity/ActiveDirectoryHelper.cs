﻿using System.DirectoryServices.AccountManagement;

namespace Nwcl.DAP.Common.Identity
{
    public class ActiveDirectoryHelper
    {
        private string _domain = null;
        private string _ou = null;
        private string _serviceUser = null;
        private string _serviceUserPassword = null;

        public ActiveDirectoryHelper()
        {
        }

        public ActiveDirectoryHelper(string domain, string ou, string serviceUser, string serviceUserPassword)
        {
            this._domain = domain;
            this._ou = ou;
            this._serviceUser = serviceUser;
            this._serviceUserPassword = serviceUserPassword;
        }

        #region Validate Methods

        /// <summary>
        /// Validates the username and password of a given user
        /// </summary>
        /// <param name="userName">The username to validate</param>
        /// <param name="password">The password of the username to validate</param>
        /// <returns>Returns True of user is valid</returns>
        public bool ValidateCredentials(string userName, string password)
        {
            PrincipalContext oPrincipalContext = this.GetPrincipalContext();
            return oPrincipalContext.ValidateCredentials(userName, password);
        }

        /// <summary>
        /// Checks if the User Account is Expired
        /// </summary>
        /// <param name="userName">The username to check</param>
        /// <returns>Returns true if Expired</returns>
        public bool IsUserExpired(string userName)
        {
            UserPrincipal userPrincipal = this.GetUser(userName);
            if (userPrincipal.AccountExpirationDate != null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Checks if user exists on AD
        /// </summary>
        /// <param name="userName">The username to check</param>
        /// <returns>Returns true if username Exists</returns>
        public bool UserExists(string userName)
        {
            if (this.GetUser(userName) == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Checks if user account is locked
        /// </summary>
        /// <param name="userName">The username to check</param>
        /// <returns>Returns true of Account is locked</returns>
        public bool IsAccountLocked(string userName)
        {
            UserPrincipal userPrincipal = this.GetUser(userName);
            return userPrincipal.IsAccountLockedOut();
        }

        /// <summary>
        /// Gets a certain user on Active Directory
        /// </summary>
        /// <param name="userName">The username to get</param>
        /// <returns>Returns the UserPrincipal Object</returns>
        public UserPrincipal GetUser(string userName)
        {
            return UserPrincipal.FindByIdentity(this.GetPrincipalContext(), userName);
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Gets the base principal context
        /// </summary>
        /// <returns>Returns the PrincipalContext object</returns>
        public PrincipalContext GetPrincipalContext()
        {
            PrincipalContext oPrincipalContext = new PrincipalContext(ContextType.Domain, 
                this._domain, this._ou, ContextOptions.SimpleBind, this._serviceUser, this._serviceUserPassword);
            return oPrincipalContext;
        }

        /// <summary>
        /// Gets the principal context on specified OU
        /// </summary>
        /// <param name="ou">The OU you want your Principal Context to run on</param>
        /// <returns>Returns the PrincipalContext object</returns>
        public PrincipalContext GetPrincipalContext(string ou)
        {
            PrincipalContext oPrincipalContext = new PrincipalContext(ContextType.Domain, 
                this._domain, ou, ContextOptions.SimpleBind, _serviceUser, _serviceUserPassword);
            return oPrincipalContext;
        }

        #endregion

        public static string GetUserDisplayNameFromAD(string logonName)
        {
            using (var context = new PrincipalContext(ContextType.Domain))
            {
                var principal = UserPrincipal.FindByIdentity(context, logonName);
                var displayName = principal.DisplayName;
                return displayName;
            }
        }
    }
}
