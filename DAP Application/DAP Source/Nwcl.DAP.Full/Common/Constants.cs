﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Common
{
    /// <summary>
    /// Class to define the constants used in the system
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Defines the Date Time display format in the system
        /// </summary>
        public const string DATE_TIME_DISPLAY_FORMAT = "dd-MM-yyyy";

        /// <summary>
        /// Defines the Month display format in the system
        /// </summary>
        public const string MONTH_DISPLAY_FORMAT = "MM-yyyy";

        /// <summary>
        /// Defines the field language text for English
        /// </summary>
        public const string FIELD_LANGUAGE_EN = "EN";

        /// <summary>
        /// Defines the field language text for Simplified Chinese
        /// </summary>
        public const string FIELD_LANGUAGE_CN = "CN";

        /// <summary>
        /// Defines the culture info value for English
        /// </summary>
        public const string CULTURE_INFO_EN = "EN-US";

        /// <summary>
        /// Defines the culture info value for Simplified Chinese
        /// </summary>
        public const string CULTURE_INFO_CN = "ZH-CN";

        /// <summary>
        /// Will remove later. should come from db.
        /// </summary>
        public const string COST_PLAN_DRAFT_STATUS = "Draft";

        /// <summary>
        /// Defines the extension for the Excel file
        /// </summary>
        public const string EXCEL_EXTENSION = ".xlsx";

        /// <summary>
        /// Defines the name for the exported Excel file
        /// </summary>
        public const string EXPORTED_EXCEL_NAME = "Exported";

        /// <summary>
        /// Defines the extension for the xml file
        /// </summary>
        public const string XML_EXTENSION = ".xml";

        /// <summary>
        /// Defines the key for the Windows Login Provider
        /// </summary>
        public const string WINDOWS_LOGIN_PROVIDER_KEY = "Windows";

        /// <summary>
        /// Defines the general type code of project company
        /// </summary>
        public const string GENERAL_TYPE_CODE_CompanyType = "CompanyType";

        /// <summary>
        /// Defines the general type code of CostCodeType
        /// </summary>
        public const string GENERAL_TYPE_CODE_CostCodeType = "CostCodeType";

        public enum CostPlanStatus
        {
            [Description("C001")]
            Submitted,
            [Description("C002")]
            Historical,
            [Description("C003")]
            PendingApproval,
            [Description("C004")]
            Approved,
            [Description("C005")]
            Rejected,
            [Description("C006")]
            FinalCheck
        }

        public enum BudgetPlanStatus
        {
            [Description("B001")]
            Submitted,
            [Description("B002")]
            Historical,
            [Description("B003")]
            PendingApproval,
            [Description("B004")]
            Approved,
            [Description("B005")]
            Rejected,
            [Description("B006")]
            FinalCheck
        }

        public enum ProgramManagementStatus
        {
            [Description("PMT001")]
            Submitted,
            [Description("PMT002")]
            Historical,
            [Description("PMT003")]
            PendingApproval,
            [Description("PMT004")]
            Approved,
            [Description("PMT005")]
            Rejected,
            [Description("PMT006")]
            FinalCheck
        }

        public enum WorkflowStatus
        {
            [Description("WF001")]
            Submitted,
            [Description("WF002")]
            WaitingForApprvoal,
            [Description("WF003")]
            Approved,
            [Description("WF004")]
            Rejected,
            [Description("WF005")]
            Returned,
            [Description("WF006")]
            Query,
            [Description("WF007")]
            Expired
        }

        public enum ScreenID
        {
            [Description("ADM_ENT_001")]
            EntityGroupMaintenance,
            [Description("ADM_ENT_00101")]
            EditEntityGroup,
            [Description("ADM_USR_001")]
            UserGroupMaintenance,
            [Description("ADM_USR_00101")]
            EditUserGroup,
            [Description("ADM_MTD_001")]
            UploadExcel,
            [Description("ADM_USR_002")]
            CompanyMaintenance,
            [Description("ADM_ROL_001")]
            RoleMaintenance,
            [Description("ADM_ROL_00101")]
            EditRole,            
            [Description("ADM_USR_003")]
            DepartmentMaintenance,
            [Description("ADM_UPM_001")]
            PrivilegeMaintenance,
            [Description("ADM_UPM_002")]
            PrivilegeList,            
            [Description("ADM_USR_00301")]
            EditDepartment,
            [Description("DASHBOARD")]
            Dashboard
        }
        
        public enum StatusTypes
        {
            [Description("Project")]
            Project,
            [Description("CostPlan")]
            CostPlan,
            [Description("BudgetPlan")]
            BudgetPlan,
            [Description("TradeBudget")]
            TradeBudget
        }

    }
}
