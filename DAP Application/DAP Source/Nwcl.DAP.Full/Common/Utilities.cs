﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Common
{
    /// <summary>
    /// Utilities class to define some handy methods
    /// </summary>
    public static class Utilities
    {
        public static string ByteToString(byte[] bytes)
        {
            string binaryString = "";
            for (int i = 0; i < bytes.Length; i++)
            {
                binaryString += bytes[i].ToString("X2");
            }
            return (binaryString);
        }

        public static string HMACEncryption(string username, string password)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(username);

            HMACSHA512 hmacsha512 = new HMACSHA512(keyByte);
            byte[] messageBytes = encoding.GetBytes(password);
            byte[] hashmessage = hmacsha512.ComputeHash(messageBytes);
            return ByteToString(hashmessage);
        }

        public static decimal? ToNullableDecimal(this string str)
        {
            decimal temp = 0;
            if (!string.IsNullOrEmpty(str) && !string.IsNullOrEmpty(str.Trim()) && decimal.TryParse(str, out temp))
            {
                return temp;
            }
            else
            {
                return null;
            }
        }

        public static int ToInteger(this string str)
        {
            var value = ToNullableInteger(str);
            return value.HasValue ? value.Value : 0;
        }

        public static int? ToNullableInteger(this string str)
        {
            var temp = 0;
            if (!string.IsNullOrEmpty(str) && !string.IsNullOrEmpty(str.Trim()) && int.TryParse(str, out temp))
            {
                return temp;
            }
            else
            {
                return null;
            }
        }

        public static decimal ToDecimal(this string str)
        {
            var value = ToNullableDecimal(str);
            return value.HasValue ? value.Value : 0;
        }

        /// <summary>
        /// 1 -> A<br/>
        /// 2 -> B<br/>
        /// 3 -> C<br/>
        /// ...
        /// Add _ behind the columnString because TreeGrid bug
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static string ExcelColumnFromNumber(int column)
        {
            string columnString = "";
            decimal columnNumber = column;
            while (columnNumber > 0)
            {
                decimal currentLetterNumber = (columnNumber - 1) % 26;
                char currentLetter = (char)(currentLetterNumber + 65);
                columnString = currentLetter + columnString;
                columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;
            }
            return columnString+"_";
        }


        /// <summary>
        /// A -> 1<br/>
        /// B -> 2<br/>
        /// C -> 3<br/>
        /// ...
        /// Remove _ at the end of column name becuase route the TreeGridBug
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static int NumberFromExcelColumn(string column)
        {
            int retVal = 0;
            string col = column.ToUpper().TrimEnd('_');
            for (int iChar = col.Length - 1; iChar >= 0; iChar--)
            {
                char colPiece = col[iChar];
                int colNum = colPiece - 64;
                retVal = retVal + colNum * (int)Math.Pow(26, col.Length - (iChar + 1));
            }
            return retVal;
        }

        public static string GetDescription(this Enum value)
        {
            return
                value
                    .GetType()
                    .GetMember(value.ToString())
                    .FirstOrDefault()
                    .GetCustomAttribute<DescriptionAttribute>()
                    .Description;
        }
    }
}
