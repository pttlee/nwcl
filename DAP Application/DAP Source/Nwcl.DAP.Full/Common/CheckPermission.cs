﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Nwcl.DAP.Common;
using System.Configuration;

using System.ComponentModel;
using System.Reflection;
using System.Security.Principal;

namespace Nwcl.DAP.Common
{
    /// <summary>
    /// Class to define the Base MVC Controller with some common operations or handling required for the other controllers
    /// </summary>
    public class CheckPermission
    {
        //string debug = System.Configuration.ConfigurationManager.AppSettings["Debug"].ToString();

        public bool ChcekPermission(string username, string controllerName, string targetName)
        {
            string debug = System.Configuration.ConfigurationManager.AppSettings["Debug"];

             if (username.ToLower() == "atos" || !string.IsNullOrEmpty(debug) && debug == "true")
                return true;
            else { 
                return false;
            }
        }
    }
}
