﻿// show checked node IDs on datasource change
//Tab1
$(function () {
    $("#NewAddDashboard").click(function () {
        var grid = $("#RoleDashboardGrid").data("kendoGrid");
        if ($("#RoleDashboardGrid").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        grid.addRow();
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );
        } else {
            grid.addRow();
            //SetFormDirty(true);
        }
    });

    $("#RoleDashboardGrid").on("click", ".GridRowEdit", function () {
        var row = $(this).closest("tr");
        var control = $(this);

        if ($("#RoleDashboardGrid").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#RoleDashboardGrid").data("kendoGrid").editRow(row);
                        //control.closest("tr").find(".GridRowEdit").addClass('displayNone');
                        //control.closest("tr").find(".GridRowSave").removeClass('displayNone');
                        //control.closest("tr").find(".GridRowDelete").addClass('displayNone');
                        //control.closest("tr").find(".GridRowCancel").removeClass('displayNone');
                        $("#RoleDashboardGrid").find(".k-grid-edit-row .GridRowEdit").addClass('displayNone');
                        $("#RoleDashboardGrid").find(".k-grid-edit-row .GridRowDelete").addClass('displayNone');
                        $("#RoleDashboardGrid").find(".k-grid-edit-row .GridRowSave").removeClass('displayNone');
                        $("#RoleDashboardGrid").find(".k-grid-edit-row .GridRowCancel").removeClass('displayNone');
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );
            //$("#RoleDashboardGrid").find(".k-grid-edit-row").find("#RoleId").attr("value", $("#EditRole_RoleId").attr("value"));

        } else {
            $("#RoleDashboardGrid").data("kendoGrid").editRow(row);
            $(this).closest("tr").find(".GridRowEdit").addClass('displayNone');
            $(this).closest("tr").find(".GridRowSave").removeClass('displayNone');
            $(this).closest("tr").find(".GridRowDelete").addClass('displayNone');
            $(this).closest("tr").find(".GridRowCancel").removeClass('displayNone');

            $(this).closest("tr").find("input[name='DashboardId']").removeAttr('data-val-required');
            $(this).closest("tr").find("input[name='RoleId']").removeAttr('data-val-required');

            //SetFormDirty(true);
        }
    });

    $("#RoleDashboardGrid").on("click", ".GridRowSave", function () {
        var row = $(this).closest("tr");
        $('#RoleDashboardGrid').data('kendoGrid').dataSource.data()[
            $('#RoleDashboardGrid').data('kendoGrid').dataSource.data().length - 1].RoleId = $("#EditRole_RoleId").val();
        var prompt = "";

        prompt = verifyRoleDashboard($(this));

        if (prompt == "") {

            var result = $("#DuplicateDashboard").val();
            if (result == 1) {
                swal({
                    title: "",
                    type: "warning",
                    text: getMsgTypeID("SYSTEM") + CommonResx.text_dapDashboard + sysMsg.text_existCode,
                    confirmButtonText: CommonResx.text_confirm,
                });
            }
            else {
                $("#RoleDashboardGrid").data("kendoGrid").saveRow(row);
                //$("#editMode").val("0");
                //UpdateDirtyGrid();
            }
        } else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });

    $("#RoleDashboardGrid").on("click", ".GridRowDelete", function () {
        var row = $(this).closest("tr");
        var RoleId = $(this).closest("tr").find("input:eq(0)").val();
        swal({
            title: "",
            text: sysMsg.text_confirmDelete,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    $("#RoleDashboardGrid").data("kendoGrid").removeRow(row);
                    //UpdateDirtyGrid();
                    UnBlockUI();
                }
                else { }
            }
        );
    });

    $("#RoleDashboardGrid").on("click", ".GridRowCancel", function () {
        swal({
            title: "",
            text: sysMsg.text_confirmCancel,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        },

            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    var row = $(this).closest("tr");
                    $("#RoleDashboardGrid").data("kendoGrid").cancelRow(row);
                    $(this).closest("tr").find(".GridRowEdit").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowSave").addClass('displayNone');
                    $(this).closest("tr").find(".GridRowDelete").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowCancel").addClass('displayNone');
                    //UpdateDirtyGrid();
                    UnBlockUI();
                }
                else { }
            }
        );
    });





    //Tab2
    $("#NewAddPrivilege").click(function () {
        var grid = $("#EntityGroupUserGroupRole").data("kendoGrid");
        if ($("#EntityGroupUserGroupRole").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        grid.addRow();
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );
        } else {
            grid.addRow();
            //SetFormDirty(true);
        }
    });

    $("#EntityGroupUserGroupRole").on("click", ".GridRowEdit", function () {
        var row = $(this).closest("tr");
        var control = $(this);

        if ($("#EntityGroupUserGroupRole").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#EntityGroupUserGroupRole").data("kendoGrid").editRow(row);
                        //control.closest("tr").find(".GridRowEdit").addClass('displayNone');
                        //control.closest("tr").find(".GridRowSave").removeClass('displayNone');
                        //control.closest("tr").find(".GridRowDelete").addClass('displayNone');
                        //control.closest("tr").find(".GridRowCancel").removeClass('displayNone');
                        $("#EntityGroupUserGroupRole").find(".k-grid-edit-row .GridRowEdit").addClass('displayNone');
                        $("#EntityGroupUserGroupRole").find(".k-grid-edit-row .GridRowDelete").addClass('displayNone');
                        $("#EntityGroupUserGroupRole").find(".k-grid-edit-row .GridRowSave").removeClass('displayNone');
                        $("#EntityGroupUserGroupRole").find(".k-grid-edit-row .GridRowCancel").removeClass('displayNone');
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );

        } else {
            $("#EntityGroupUserGroupRole").data("kendoGrid").editRow(row);
            $(this).closest("tr").find(".GridRowEdit").addClass('displayNone');
            $(this).closest("tr").find(".GridRowSave").removeClass('displayNone');
            $(this).closest("tr").find(".GridRowDelete").addClass('displayNone');
            $(this).closest("tr").find(".GridRowCancel").removeClass('displayNone');

            $(this).closest("tr").find("input[name='UserGroupId']").removeAttr('data-val-required');
            $(this).closest("tr").find("input[name='RoleId']").removeAttr('data-val-required');

            //SetFormDirty(true);
        }
    });

    $("#EntityGroupUserGroupRole").on("click", ".GridRowSave", function () {
        var row = $(this).closest("tr");
        $('#EntityGroupUserGroupRole').data('kendoGrid').dataSource.data()[
            $('#EntityGroupUserGroupRole').data('kendoGrid').dataSource.data().length - 1].RoleId = $("#EditRole_RoleId").val();
        var prompt = "";

        prompt = verifyPrivilege($(this));

        if (prompt == "") {   
            var result = $("#DuplicatePrivilege").val();
            if (result == 1) {
                swal({
                    title: "",
                    type: "warning",
                    text: getMsgTypeID("SYSTEM") + CommonResx.text_dapPrivilege + sysMsg.text_existCode,
                    confirmButtonText: CommonResx.text_confirm,
                });
            }
            else {
                BlockUI();
                $("#EntityGroupUserGroupRole").data("kendoGrid").saveRow(row);
                //UpdateDirtyGrid();
                UnBlockUI();
            }
        } else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });

    $("#EntityGroupUserGroupRole").on("click", ".GridRowDelete", function () {
        var row = $(this).closest("tr");
        var EntityGroupId = $(this).closest("tr").find("input:eq(0)").val();
        swal({
            title: "",
            text: sysMsg.text_confirmDelete,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    $("#EntityGroupUserGroupRole").data("kendoGrid").removeRow(row);
                    //UpdateDirtyGrid();
                    UnBlockUI();
                }
                else { }
            }
        );
    });

    $("#EntityGroupUserGroupRole").on("click", ".GridRowCancel", function () {
        swal({
            title: "",
            text: sysMsg.text_confirmCancel,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        },

            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    var row = $(this).closest("tr");
                    $("#EntityGroupUserGroupRole").data("kendoGrid").cancelRow(row);
                    $(this).closest("tr").find(".GridRowEdit").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowSave").addClass('displayNone');
                    $(this).closest("tr").find(".GridRowDelete").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowCancel").addClass('displayNone');
                    //UpdateDirtyGrid();
                    UnBlockUI();
                }
                else { }
            }
        );
    });

})

function onEdit(e) {

    if (e.model.isNew()) {        
        e.container.find(".GridRowEdit").addClass('displayNone');
        e.container.find(".GridRowSave").removeClass('displayNone');
        e.container.find(".GridRowDelete").addClass('displayNone');
        e.container.find(".GridRowCancel").removeClass('displayNone');

        e.container.find("input[name='UserGroupId']").removeAttr('data-val-required');
        e.container.find("input[name='EntityGroupId']").removeAttr('data-val-required');

    }
}

function verifyPrivilege(e) {
    //var countPromptMessages = 0;
    //var countRequiredValidate = 0;
    var msgs = [];
    var promptMessage = "";
    var isCodeAlreadyInput = false;


    if (e.closest("tr").find("input[name='EntityGroupId']").val() == 0) {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CommonResx.text_dapEntityGroup);
        //countPromptMessages++;
        //countRequiredValidate++;
    }

    if (e.closest("tr").find("input[name='UserGroupId']").val() == 0) {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CommonResx.text_dapUserGroup);
        //countPromptMessages++;
        //countRequiredValidate++;
    }

    

    for (var j = 0; j < msgs.length; j++) {
        promptMessage += msgs[j] + '\n';
    }

    if (promptMessage == "") { checkDuplicatePrivilege(e); }

    //Generate Validation Message to sentence //20180610
    //for (var j = 0; j < msgs.length; j++)
    //{
    //    //start to add and or , from the second item
    //    if (j > 0) {
    //        if (j < countPromptMessages - 1) {
    //            if (msgs[j].trim().length > 0)
    //                promptMessage += " , "
    //        }
    //        else
    //            promptMessage += " " + sysMsg.text_and + " ";
    //    }
    //    promptMessage += msgs[j];
    //}

    return promptMessage;
}

function checkDuplicatePrivilege(e) {
    var entityGroupId = e.closest("tr").find("input[name='EntityGroupId']").val();
    var userGroupId = e.closest("tr").find("input[name='UserGroupId']").val();
    var roleId = $("#EditRole_RoleId").val();
    var id = e.closest("tr").find("input:eq(0)").val();
    $.ajax({
        url: contextRoot + "/Main/Role/ValidateEntityGroupUserGroupRoleDuplicate",
        type: "post",
        data: { "id": id, "entityGroupId": entityGroupId, "userGroupId": userGroupId, "roleId": roleId },
        async: false,
        success: function (dat) {
            if (dat.IsSuccess) {
                if (dat.Message == "1") {
                    $("#DuplicatePrivilege").val("1");
                }
                else {
                    $("#DuplicatePrivilege").val("0");
                }
            }
        },
        error: function (err) {
            swal("error", err.Message, "error");
            UnBlockUI();
        }
    })

}

function verifyRoleDashboard(e) {
    //var countPromptMessages = 0;
    //var countRequiredValidate = 0;
    var msgs = [];
    var promptMessage = "";
    
    if (e.closest("tr").find("input[name='DashboardId']").val() == 0) {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CommonResx.text_dapDashboard);
        //countPromptMessages++;
        //countRequiredValidate++;
    }


    for (var j = 0; j < msgs.length; j++) {
        promptMessage += msgs[j] + '\n';
    }

    if (promptMessage == "") { checkDuplicateDashboard(e); }

    return promptMessage;
}

function checkDuplicateDashboard(e) {
    var dashboardId = e.closest("tr").find("input[name='DashboardId']").val();
    var roleId = $("#EditRole_RoleId").val();
    var id = e.closest("tr").find("input:eq(0)").val();
    $.ajax({
        url: contextRoot + "/Main/Role/ValidateRoleDashboardDuplicate",
        type: "post",
        data: { "dashboardId": dashboardId, "roleId": roleId},
        async: false,
        success: function (dat) {
            if (dat.IsSuccess) {
                if (dat.Message == "1") {
                    $("#DuplicateDashboard").val("1");
                }
                else {
                    $("#DuplicateDashboard").val("0");
                }
            }
        },
        error: function (err) {
            swal("error", err.Message, "error");
            UnBlockUI();
        }
    })

}

function exportList() {
    var grid = $("#EntityGroupUserGroupRole").data("kendoGrid");
    grid.setOptions({
        excel: {
            allPages: true
        }
    });
    grid.saveAsExcel();
}


function ClickReturn() {
    if ($("#RoleDashboardGrid").find(".k-grid-edit-row").length > 0
        || $("#EntityGroupUserGroupRole").find(".k-grid-edit-row").length > 0) {
        swal({
            title: "",
            text: sysMsg.text_confirmLeave,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    location.href = contextRoot + "/Main/Role";
                    UnBlockUI();
                }
                else { }
            }
        );
    }
    else {
        //SetFormDirty(false);
        location.href = contextRoot + "/Main/Role";
    }
}

function checkDirtyPage() {
    return ($("#RoleDashboardGrid .k-grid-edit-row").length > 0 || $("#EntityGroupUserGroupRole .k-grid-edit-row").length > 0);
}