﻿$(function () {
    $("#search").hide();

    $("#NewAdd").click(function () {
        var grid = $("#RoleGrid").data("kendoGrid");
        if ($("#RoleGrid").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        grid.addRow();
                        $("#RoleGrid").find(".k-grid-edit-row").closest("tr").find(".GridRowView").addClass('displayNone');
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );
        } else {
            grid.addRow();
            //SetFormDirty(true);
            $("#RoleGrid").find(".k-grid-edit-row").closest("tr").find(".GridRowView").addClass('displayNone');
        }
    });

    $("#RoleGrid").on("click", ".GridRowEdit", function () {
        var row = $(this).closest("tr");
        var control = $(this);

        if ($("#RoleGrid").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#RoleGrid").data("kendoGrid").editRow(row);

                        //control.closest("tr").find(".GridRowView").addClass('displayNone');
                        //control.closest("tr").find(".GridRowEdit").addClass('displayNone');
                        //control.closest("tr").find(".GridRowSave").removeClass('displayNone');
                        //control.closest("tr").find(".GridRowDelete").addClass('displayNone');
                        //control.closest("tr").find(".GridRowCancel").removeClass('displayNone');
                        $("#RoleGrid").find(".k-grid-edit-row .GridRowView").addClass('displayNone');
                        $("#RoleGrid").find(".k-grid-edit-row .GridRowEdit").addClass('displayNone');
                        $("#RoleGrid").find(".k-grid-edit-row .GridRowDelete").addClass('displayNone');
                        $("#RoleGrid").find(".k-grid-edit-row .GridRowSave").removeClass('displayNone');
                        $("#RoleGrid").find(".k-grid-edit-row .GridRowCancel").removeClass('displayNone');
                        //SetFormDirty(true);
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );

        } else {
            $("#RoleGrid").data("kendoGrid").editRow(row);
            $(this).closest("tr").find(".GridRowView").addClass('displayNone');

            $(this).closest("tr").find(".GridRowEdit").addClass('displayNone');
            $(this).closest("tr").find(".GridRowSave").removeClass('displayNone');
            $(this).closest("tr").find(".GridRowDelete").addClass('displayNone');
            $(this).closest("tr").find(".GridRowCancel").removeClass('displayNone');
            //SetFormDirty(true);
        }
    });

    $("#RoleGrid").on("click", ".GridRowSave", function () {
        var row = $(this).closest("tr");
        var prompt = "";

        prompt = verifyRoleName($(this));

        if (prompt == "") {
            var result = $("#DuplicateRoleName").val();
            if (result == 1) {
                swal({
                    title: "",
                    type: "warning",
                    text: getMsgTypeID("SYSTEM") + CommonResx.text_dapRole + sysMsg.text_existCode,
                    confirmButtonText: CommonResx.text_confirm,
                });
            }
            else {
                BlockUI();
                $("#RoleGrid").data("kendoGrid").saveRow(row);
                $(this).closest("tr").find(".GridRowView").removeClass('displayNone');
                //SetFormDirty(false);
                UnBlockUI();
            }
        } else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });
    
    $("#RoleGrid").on("click", ".GridRowDelete", function () {
        var row = $(this).closest("tr");
        var roleId = $(this).closest("tr").find("input:eq(0)").val();
        var prompt = "";
        $.ajax({
            url: "/Role/ValidateRoleEmpty",
            type: "post",
            data: { "roleId": roleId },
            async: false,
            success: function (dat) {
                if (dat.IsSuccess) {
                    if (dat.Message != "0") {
                        prompt += getMsgTypeID("SYSTEM") + sysMsg.text_existPrivilege;
                    }
                }
            },
            error: function (err) {
                swal("error", err.Message, "error");
                UnBlockUI();
            }
        })
        if (prompt == "") {

            swal({
                title: "",
                text: sysMsg.text_confirmDelete,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#RoleGrid").data("kendoGrid").removeRow(row);
                        //SetFormDirty(false);
                        UnBlockUI();
                    }
                    else { }
                }
            );
        }
        else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });

    $("#RoleGrid").on("click", ".GridRowCancel", function () {
        swal({
            title: "",
            text: sysMsg.text_confirmCancel,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        },

            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    var row = $(this).closest("tr");
                    $("#RoleGrid").data("kendoGrid").cancelRow(row);
                    $(this).closest("tr").find(".GridRowEdit").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowSave").addClass('displayNone');
                    $(this).closest("tr").find(".GridRowDelete").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowCancel").addClass('displayNone');
                    //SetFormDirty(false);
                    UnBlockUI();
                }
                else { }
            }
        );
    });
})

function verifyRoleName(e) {
    //var countPromptMessages = 0;
    //var countRequiredValidate = 0;
    var msgs = [];
    var promptMessage = "";

    if (e.closest("tr").find("input[name='RoleName']").val() == "") {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CommonResx.text_dapRole);
        //countPromptMessages++;
        //countRequiredValidate++;
    }


    for (var j = 0; j < msgs.length; j++) {
        promptMessage += msgs[j] + '\n';
    }

    if (promptMessage == "") {
        checkDuplicateRoleName(e);
    }

    return promptMessage;
}

function checkDuplicateRoleName(e) {
    var roleName = e.closest("tr").find("input[name='RoleName']").val();
    var roleId = e.closest("tr").find("input:eq(0)").val();
    var id = e.closest("tr").find("input:eq(0)").val();
    $.ajax({
        url: contextRoot + "/Main/Role/ValidateRoleNameDuplicate",
        type: "post",
        data: { "roleId": roleId, "roleName": roleName},
        async: false,
        success: function (dat) {
            if (dat.IsSuccess) {
                if (dat.Message == "1") {
                    $("#DuplicateRoleName").val("1");
                }
                else {
                    $("#DuplicateRoleName").val("0");
                }
            }
        },
        error: function (err) {
            swal("error", err.Message, "error");
            UnBlockUI();
        }
    })

}

function onEdit(e) {
    
    if (e.model.isNew()) {
        //e.container.find(".GridRowEdit").hide();
        e.container.find(".GridRowEdit").addClass('displayNone');
        //e.container.find(".GridRowSave").show();
        e.container.find(".GridRowSave").removeClass('displayNone');
        //e.container.find(".GridRowDelete").hide();
        e.container.find(".GridRowDelete").addClass('displayNone');
        //e.container.find(".GridRowCancel").show();
        e.container.find(".GridRowCancel").removeClass('displayNone');
        
        e.container.find("input[name='RoleName']").removeAttr('data-val-required');

    }
}



function openSearchGrid() {
    $("#search").show();
    $("#openSearch").hide();

}

function hideOpenSearchGrid() {
    $("#search").hide();
    $("#openSearch").show();
    $("#RoleGrid").data("kendoGrid").dataSource.filter({});
}

function searchList() {
    var searchStr = $("#SearchText").val();
    if (searchStr != null && searchStr.length > 0) {
        var filters = $("#RoleGrid").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
                {
                    field: "RoleName",
                operator: "contains",
                value: searchStr
                },
                {
                    field: "DashboardName",
                    operator: "contains",
                    value: searchStr
                }]
        });
        $("#RoleGrid").data("kendoGrid").refresh();
        $("#RoleGrid").data("kendoGrid").dataSource.filter(filters);
    }
}

function exportList() {
    var grid = $("#RoleGrid").data("kendoGrid");
    grid.setOptions({
        excel: {
            allPages: true
        }
    });
    grid.saveAsExcel();
}

function EditRoleDetail(RoleId) {
    RedirectScreen(contextRoot + "/Main/Role/EditRole?RoleID=" + RoleId);
}

function checkDirtyPage() {
    if ($("#RoleGrid .k-grid-edit-row").length > 0) {
        return true;
    }
    else {
        return false;
    }
}