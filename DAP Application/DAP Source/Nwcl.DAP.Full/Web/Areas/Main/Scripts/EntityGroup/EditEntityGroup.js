﻿// show checked node IDs on datasource change
$(document).ajaxStart(function () { BlockUI(); }).ajaxStop(function () { UnBlockUI(); });
function onCheck(e) {

    var checkedNodes = [],
        treeView = $("#EntityGroupTreeView").data("kendoTreeView"),
        message;

    if (treeView.dataItem(e.node).id.length > 4) {
        $("#IsEditingTree").val("1");
        //SetFormDirty(true);
    }

    if (!treeView.dataItem(e.node).checked) return;

    BlockUI();
    treeView.expand(treeView.dataItem(e.node));

    checkAllChildNodes(treeView, e.node);

    if (treeView.dataItem(e.node).id.length <= 4 && treeView.dataItem(e.node).checked) {
        treeView.dataItem(e.node).set("checked", false);
    }
    UnBlockUI();
}

function checkAllChildNodes(treeView, node) {
    treeView.dataItem(node).set("expanded", true);
    if (treeView.dataItem(node).id.length > 4) {
        treeView.dataItem(node).set("checked", true);
        $("#IsEditingTree").val("1");
        //SetFormDirty(true);
    }
    var childNodes = node.childNodes;
    if (node.childNodes.length > 1) {
        for (var i = 0; i < childNodes[1].childNodes.length; i++) {
            var innerNode = childNodes[1].childNodes[i];
            checkAllChildNodes(treeView, innerNode)
        }
    }
}

function expandAllChildNodes(treeView, dataItem) {
    dataItem.set("expanded", true);
    var childNodes = dataItem.children.data();
    if (childNodes.length > 0) {
        for (var i = 0; i < childNodes.length; i++) {
            expandAllChildNodes(treeView, childNodes[i]);
        }
    }
}

$(window).on('load', function () {
    var treeView = $("#EntityGroupTreeView").data("kendoTreeView");
    treeView.items().each(function (i, el) {
        $(el).on("dblclick", function (e) {
            var dataItem = treeView.dataSource.get(treeView.select().data().id);
            expandAllChildNodes(treeView, dataItem);
        });
    });
});

function getAllCheckedCodeRegion(treeView, node) {
    var checkedCodes = [];

    var childNodes = node.childNodes;
    if (node.childNodes.length > 1) {
        for (var i = 0; i < childNodes.length; i++) {
            var innerNode = childNodes[i];
            checkedCodes = checkedCodes.concat(getAllCheckedProjectCode(treeView, innerNode));
        }
    }
    return checkedCodes;
}

function getAllCheckedProjectCode(treeView, node) {
    var checkedCodes = [];
    if (treeView.dataItem(node).checked && treeView.dataItem(node).id.length > 4) {
        checkedCodes.push(treeView.dataItem(node).id);
    }

    var childNodes = node.childNodes;
    if (node.childNodes.length > 1) {
        for (var i = 0; i < childNodes[1].childNodes.length; i++) {
            var innerNode = childNodes[1].childNodes[i];
            checkedCodes = checkedCodes.concat(getAllCheckedProjectCode(treeView, innerNode));
        }
    }
    return checkedCodes;
}

function clickSave() {
    //if ($("#selectedId").val() == "") {
    //    swal({
    //        title: "",
    //        type: "warning",
    //        text: getMsgTypeID("SYSTEM") + sysMsg.text_PlsSelectItem,
    //        confirmButtonText: CommonResx.text_confirm,
    //    })
    //} else {
        $.ajax({
            url: contextRoot + "/Main/EntityGroup/CreateEntityGroupProject",
            type: "post",
            data: {
                "entityGroupID": $("#EditEntityGroup_EntityGroupId").val(),
                "selectedCodes": getAllCheckedCodeRegion($("#EntityGroupTreeView").data("kendoTreeView"), $('.k-group')[0]).join(",")
            },
            async: false,
            success: function (dat) {
                if (dat.IsSuccess) {
                    if (dat.Message == "1") {
                        $("#IsEditingTree").val("0");
                        //UpdateDirty($("#EntityGroupUserGroupRole .k-grid-edit-row"));
                        swal({
                            title: "",
                            type: "success",
                            text: CommonResx.SaveSucess,
                            confirmButtonText: CommonResx.btnOk_Text
                        });
                    }
                }
            },
            error: function (err) {
                swal("error", dat.Message, "error");
                UnBlockUI();
            }
        })
    //}
}

function ClickReturn() {
    RedirectScreen(contextRoot + "/Main/EntityGroup");
}






//Tab2
$(function () {
    $("#search").hide();
    $("#NewAdd").click(function () {
        var grid = $("#EntityGroupUserGroupRole").data("kendoGrid");
        if ($("#EntityGroupUserGroupRole").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        grid.addRow();
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );
        } else {
            grid.addRow();
            //SetFormDirty(true);
        }
    });

    $("#EntityGroupUserGroupRole").on("click", ".GridRowEdit", function () {
        var row = $(this).closest("tr");
        var control = $(this);

        if ($("#EntityGroupUserGroupRole").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#EntityGroupUserGroupRole").data("kendoGrid").editRow(row);
                        //control.closest("tr").find(".GridRowEdit").addClass('displayNone');
                        //control.closest("tr").find(".GridRowSave").removeClass('displayNone');
                        //control.closest("tr").find(".GridRowDelete").addClass('displayNone');
                        //control.closest("tr").find(".GridRowCancel").removeClass('displayNone');
                        //$("#DepartmentGrid").find(".k-grid-edit-row .GridRowView").addClass('displayNone');
                        $("#EntityGroupUserGroupRole").find(".k-grid-edit-row .GridRowEdit").addClass('displayNone');
                        $("#EntityGroupUserGroupRole").find(".k-grid-edit-row .GridRowDelete").addClass('displayNone');
                        $("#EntityGroupUserGroupRole").find(".k-grid-edit-row .GridRowSave").removeClass('displayNone');
                        $("#EntityGroupUserGroupRole").find(".k-grid-edit-row .GridRowCancel").removeClass('displayNone');
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );
        } else {
            $("#EntityGroupUserGroupRole").data("kendoGrid").editRow(row);
            $(this).closest("tr").find(".GridRowEdit").addClass('displayNone');
            $(this).closest("tr").find(".GridRowSave").removeClass('displayNone');
            $(this).closest("tr").find(".GridRowDelete").addClass('displayNone');
            $(this).closest("tr").find(".GridRowCancel").removeClass('displayNone');

            $(this).closest("tr").find("input[name='UserGroupId']").removeAttr('data-val-required');
            $(this).closest("tr").find("input[name='RoleId']").removeAttr('data-val-required');

            //SetFormDirty(true);
        }
    });

    $("#EntityGroupUserGroupRole").on("click", ".GridRowSave", function () {
        var row = $(this).closest("tr");
        $('#EntityGroupUserGroupRole').data('kendoGrid').dataSource.data()[
            $('#EntityGroupUserGroupRole').data('kendoGrid').dataSource.data().length - 1].EntityGroupId = $("#EditEntityGroup_EntityGroupId").val();
        var prompt = "";

        prompt = verifyEntityGroup($(this));

        if (prompt == "") {   

            var result = $("#DuplicatePrivilege").val();
            if (result == 1) {
                swal({
                    title: "",
                    type: "warning",
                    text: getMsgTypeID("SYSTEM") + CommonResx.text_dapPrivilege + sysMsg.text_existCode,
                    confirmButtonText: CommonResx.text_confirm,
                });
            }
            else {
                BlockUI();
                $("#EntityGroupUserGroupRole").data("kendoGrid").saveRow(row);
                //UpdateDirty($("#EntityGroupUserGroupRole .k-grid-edit-row"));
                UnBlockUI();
            }

            //$("#editMode").val("0");
        } else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });

    $("#EntityGroupUserGroupRole").on("click", ".GridRowDelete", function () {
        var row = $(this).closest("tr");
        var EntityGroupId = $(this).closest("tr").find("input:eq(0)").val();
        var prompt = "";
        //$.ajax({
        //    url: contextRoot + "/Main/EntityGroup/ValidateEntityGroup",
        //    type: "post",
        //    data: { "Id": EntityGroupId },
        //    async: false,
        //    success: function (dat) {
        //        if (dat.IsSuccess) {
        //            if (dat.Message == "0") {
        //                prompt += getMsgTypeID("SYSTEM") + sysMsg.text_existEntityGroup;
        //            }
        //        }
        //    },
        //    error: function (err) {
        //        swal("error", err.Message, "error");
        //        UnBlockUI();
        //    }
        //})
        if (prompt == "") {

            swal({
                title: "",
                text: sysMsg.text_confirmDelete,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#EntityGroupUserGroupRole").data("kendoGrid").removeRow(row);
                        //UpdateDirty($("#EntityGroupUserGroupRole .k-grid-edit-row"));
                        UnBlockUI();
                    }
                    else { }
                }
            );
        }
        else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });

    $("#EntityGroupUserGroupRole").on("click", ".GridRowCancel", function () {
        swal({
            title: "",
            text: sysMsg.text_confirmCancel,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        },

            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    var row = $(this).closest("tr");
                    $("#EntityGroupUserGroupRole").data("kendoGrid").cancelRow(row);
                    $(this).closest("tr").find(".GridRowEdit").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowSave").addClass('displayNone');
                    $(this).closest("tr").find(".GridRowDelete").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowCancel").addClass('displayNone');
                    //SetFormDirty(false);
                    //UpdateDirty($("#EntityGroupUserGroupRole .k-grid-edit-row"));
                    UnBlockUI();
                }
                else { }
            }
        );
    });


    //Change tab event. Make sure items are saved before changing tab
    //$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    //    //e.target // newly activated tab
    //    //e.relatedTarget // previous active tab
    //    //alert('test');
    //    if (e.relatedTarget.id == "base-tab1" && $('#IsEditingTree').val() == "1") {
    //            swal({
    //                title: "",
    //                text: sysMsg.text_confirmLeave,
    //                type: "warning",
    //                showCancelButton: true,
    //                confirmButtonColor: "#DD6B55",
    //                confirmButtonText: CommonResx.text_confirm,
    //                cancelButtonText: CommonResx.text_cancel,
    //                closeOnConfirm: false,
    //                closeOnCancel: false
    //            },
    //                function (isConfirm) {
    //                    swal.close();
    //                    if (isConfirm) {
    //                        BlockUI();
    //                        $('#IsEditingTree').val("0");
    //                        UpdateDirty();
    //                        $('#EntityGroupTreeView').data("kendoTreeView").dataSource.read();
    //                        UnBlockUI();
    //                    }
    //                    else {
    //                        e.preventDefault();
    //                    }
    //                }
    //            );

    //    }
    //    else if (e.relatedTarget.id == "base-tab2" && $("#EntityGroupUserGroupRole .k-grid-edit-row").length > 0) {
    //        GridEditLeaveConfirmation($("#EntityGroupUserGroupRole"), $("#EntityGroupUserGroupRole .k-grid-edit-row"));
    //        if ($("#EntityGroupUserGroupRole .k-grid-edit-row").length > 0) {
    //            e.preventDefault();
    //        }
    //    }
    //    return;
    //});
})

function onEdit(e) {

    if (e.model.isNew()) {        
        e.container.find(".GridRowEdit").addClass('displayNone');
        e.container.find(".GridRowSave").removeClass('displayNone');
        e.container.find(".GridRowDelete").addClass('displayNone');
        e.container.find(".GridRowCancel").removeClass('displayNone');

        e.container.find("input[name='UserGroupId']").removeAttr('data-val-required');
        e.container.find("input[name='RoleId']").removeAttr('data-val-required');

    }
}

function verifyEntityGroup(e) {
    //var countPromptMessages = 0;
    //var countRequiredValidate = 0;
    var msgs = [];
    var promptMessage = "";
    var isCodeAlreadyInput = false;

    if (e.closest("tr").find("input[name='RoleId']").val() == 0) {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CommonResx.text_dapRole);
        //countPromptMessages++;
        //countRequiredValidate++;
    }

    if (e.closest("tr").find("input[name='UserGroupId']").val() == 0) {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CommonResx.text_dapUserGroup);
        //countPromptMessages++;
        //countRequiredValidate++;
    }

    for (var j = 0; j < msgs.length; j++) {
        promptMessage += msgs[j] + '\n';
    }


    //Generate Validation Message to sentence //20180610
    //for (var j = 0; j < msgs.length; j++)
    //{
    //    //start to add and or , from the second item
    //    if (j > 0) {
    //        if (j < countPromptMessages - 1) {
    //            if (msgs[j].trim().length > 0)
    //                promptMessage += " , "
    //        }
    //        else
    //            promptMessage += " " + sysMsg.text_and + " ";
    //    }
    //    promptMessage += msgs[j];
    //}

    if (promptMessage == "") { checkDuplicatePrivilege(e); }

    return promptMessage;
}

function checkDuplicatePrivilege(e) {
    var entityGroupId = $("#entityGroupId").val();
    var userGroupId = e.closest("tr").find("input[name='UserGroupId']").val();
    var roleId = e.closest("tr").find("input[name='RoleId']").val();
    var id = e.closest("tr").find("input:eq(0)").val();
    $.ajax({
        url: contextRoot +"/Main/Privilege/ValidatePrivilegeDuplicate",
        type: "post",
        data: { "id": id, "entityGroupId": entityGroupId, "userGroupId": userGroupId, "roleId": roleId },
        async: false,
        success: function (dat) {
            if (dat.IsSuccess) {
                if (dat.Message == "1") {
                    $("#DuplicatePrivilege").val("1");
                }
                else {
                    $("#DuplicatePrivilege").val("0");
                }
            }
        },
        error: function (err) {
            swal("error", err.Message, "error");
            UnBlockUI();
        }
    })

}

function exportList() {
    var grid = $("#EntityGroupUserGroupRole").data("kendoGrid");
    grid.saveAsExcel();
}

function checkDirtyPage() {
    return ($("#EntityGroupUserGroupRole .k-grid-edit-row").length > 0 || $("#IsEditingTree").val() == "1");
}