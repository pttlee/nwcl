﻿
$(function () {
    $("#search").hide();
    $("#NewAdd").click(function () {
        var grid = $("#CompanyGrid").data("kendoGrid");
        if ($("#CompanyGrid").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        grid.addRow();
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );
        } else {
            grid.addRow();
        }
    });

    $("#CompanyGrid").on("click", ".GridRowEdit", function () {
        var row = $(this).closest("tr");
        var control = $(this);

        if ($("#CompanyGrid").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#CompanyGrid").data("kendoGrid").editRow(row);                        
                        control.closest("tr").find(".GridRowEdit").addClass('displayNone');
                        control.closest("tr").find(".GridRowSave").removeClass('displayNone');
                        control.closest("tr").find(".GridRowDelete").addClass('displayNone');
                        control.closest("tr").find(".GridRowCancel").removeClass('displayNone');
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );

        } else {
            $("#CompanyGrid").data("kendoGrid").editRow(row);            
            $(this).closest("tr").find(".GridRowEdit").addClass('displayNone');
            $(this).closest("tr").find(".GridRowSave").removeClass('displayNone');
            $(this).closest("tr").find(".GridRowDelete").addClass('displayNone');
            $(this).closest("tr").find(".GridRowCancel").removeClass('displayNone');

            $(this).closest("tr").find("input[name='Code']").attr('maxlength', 10);
            $(this).closest("tr").find("input[name='Code']").css('text-transform', 'uppercase');
            $(this).closest("tr").find("input[name='NameCN']").attr('maxlength', 50);
            $(this).closest("tr").find("input[name='NameEN']").attr('maxlength', 50);
            $(this).closest("tr").find("input[name='RegionalOfficeID']").removeAttr('data-val-required');
            $(this).closest("tr").find("input[name='CityID']").removeAttr('data-val-required');
        }
    });

    $("#CompanyGrid").on("click", ".GridRowSave", function () {
        var row = $(this).closest("tr");
        var prompt = "";

        prompt = verifyCompany($(this));

        if (prompt == "") {
            checkCode($(this));
            if ($("#DuplicateCode").val() == "1") {

                swal({
                    title: "",
                    type: "warning",
                    text: getMsgTypeID("SYSTEM") + sysMsg.text_existCompanyCode,
                    confirmButtonText: CommonResx.text_confirm,
                });
            }
            else {
                $("#CompanyGrid").data("kendoGrid").saveRow(row);
                $("#editMode").val("0");
            }
        } else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });

    $("#CompanyGrid").on("click", ".GridRowDelete", function () {
        var row = $(this).closest("tr");
        var CompanyId = $(this).closest("tr").find("input:eq(0)").val();
        var prompt = "";
        $.ajax({
            url: "/Company/ValidateCompany",
            type: "post",
            data: { "Id": CompanyId },
            async: false,
            success: function (dat) {
                if (dat.IsSuccess) {
                    if (dat.Message == "0") {
                        prompt += getMsgTypeID("SYSTEM") + sysMsg.text_existCompany;
                    }
                }
            },
            error: function (err) {
                swal("error", err.Message, "error");
                UnBlockUI();
            }
        })
        if (prompt == "") {

            swal({
                title: "",
                text: sysMsg.text_confirmDelete,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#CompanyGrid").data("kendoGrid").removeRow(row);
                        UnBlockUI();
                    }
                    else { }
                }
            );
        }
        else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });

    $("#CompanyGrid").on("click", ".GridRowCancel", function () {
        swal({
            title: "",
            text: sysMsg.text_confirmCancel,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false,
        },

            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    var row = $(this).closest("tr");
                    $("#CompanyGrid").data("kendoGrid").cancelRow(row);
                    //$(this).closest("tr").find(".GridRowEdit").show();
                    $(this).closest("tr").find(".GridRowEdit").removeClass('displayNone');
                    //$(this).closest("tr").find(".GridRowSave").hide();
                    $(this).closest("tr").find(".GridRowSave").addClass('displayNone');
                    //$(this).closest("tr").find(".GridRowDelete").show();
                    $(this).closest("tr").find(".GridRowDelete").removeClass('displayNone');
                    //$(this).closest("tr").find(".GridRowCancel").hide();
                    $(this).closest("tr").find(".GridRowCancel").addClass('displayNone');
                    UnBlockUI();
                }
                else { }
            }
        );
    });

})

function onEdit(e) {
    if (e.model.isNew()) {
        //e.container.find(".GridRowEdit").hide();
        e.container.find(".GridRowEdit").addClass('displayNone');
        //e.container.find(".GridRowSave").show();
        e.container.find(".GridRowSave").removeClass('displayNone');
        //e.container.find(".GridRowDelete").hide();
        e.container.find(".GridRowDelete").addClass('displayNone');
        //e.container.find(".GridRowCancel").show();
        e.container.find(".GridRowCancel").removeClass('displayNone');
        e.container.find("input[name='Code']").attr('maxlength', 10);
        e.container.find("input[name='Code']").css('text-transform', 'uppercase');
        e.container.find("input[name='NameCN']").attr('maxlength', 50);
        e.container.find("input[name='NameEN']").attr('maxlength', 50);
        e.container.find("input[name='RegionalOfficeID']").removeAttr('data-val-required');
        e.container.find("input[name='CityID']").removeAttr('data-val-required');

    }
    e.container.find("input[name='RegionalOfficeID']").removeAttr('data-val-required');
    e.container.find("input[name='CityID']").removeAttr('data-val-required');
}

function verifyCompany(e) {
    //var countPromptMessages = 0;
    //var countRequiredValidate = 0;
    var msgs = [];
    var promptMessage = "";
    var isCodeAlreadyInput = false;
    //if (e.closest("tr").find("input[name='CurrentRegionID']").val() == "") {
    //    msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CompanyResx.lblRegionalOfficeNameCN_Text);
    //    //countPromptMessages++;
    //    //countRequiredValidate++;
    //}

    //if (e.closest("tr").find("input[name='CurrentCityID']").val() == "") {
    //    msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CompanyResx.lblCityNameCN_Text);
    //    //countPromptMessages++;
    //    //countRequiredValidate++;
    //}
    var regionalOfficeID = $("#RegionalOfficeID").data("kendoDropDownList").value();
    if (regionalOfficeID == 0) {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CompanyResx.lblRegionalOfficeNameCN_Text);
    }
    var cityID = $("#CityID").data("kendoDropDownList").value();
    if (cityID == 0) {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CompanyResx.lblCityNameCN_Text);
    }

    if (e.closest("tr").find("input[name='Code']").val() == "") {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CompanyResx.lblCode_Text);
        //countPromptMessages++;
        //countRequiredValidate++;
    }

    if (e.closest("tr").find("input[name='NameCN']").val() == "") {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CompanyResx.lblNameCN_Text);
        //countPromptMessages++;
        //countRequiredValidate++;
    }

    if (e.closest("tr").find("input[name='Code']").val().length > 10) {
        msgs.push(getMsgTypeID("FORMAT") + sysMsg.text_maxCodeLength);
        //countPromptMessages++;
    }

    //if (e.closest("tr").find("input[name='Code']").val().length > 0 && !isEnglish(e.closest("tr").find("input[name='Code']").val())) {
    //    msgs.push(getMsgTypeID("FORMAT") + sysMsg.text_codeFormatError);
    //    countPromptMessages++;
    //}

    //if (e.closest("tr").find("input[name='NameCN']").val() != "") {
    //    var s = e.closest("tr").find("input[name='NameCN']").val();
    //    if (!isChinese(s)) {
    //        msgs.push(getMsgTypeID("FORMAT") + CompanyResx.lblNameCN_Text + sysMsg.text_LangFormatError);
    //        //countPromptMessages++;
    //    }
    //}
    //if (e.closest("tr").find("input[name='NameEN']").val() != "") {
    //    var s = e.closest("tr").find("input[name='NameEN']").val();
    //    if (!isEnglish(s)) {            
    //        msgs.push(getMsgTypeID("FORMAT") + CompanyResx.lblNameEN_Text + sysMsg.text_LangFormatError);
    //        //countPromptMessages++;
    //    }
    //}

    for (var j = 0; j < msgs.length; j++) {
        promptMessage += msgs[j] + '\n';
    }


    //Generate Validation Message to sentence //20180610
    //for (var j = 0; j < msgs.length; j++)
    //{
    //    //start to add and or , from the second item
    //    if (j > 0) {
    //        if (j < countPromptMessages - 1) {
    //            if (msgs[j].trim().length > 0)
    //                promptMessage += " , "
    //        }
    //        else
    //            promptMessage += " " + sysMsg.text_and + " ";
    //    }
    //    promptMessage += msgs[j];
    //}

    return promptMessage;
}

function checkCode(e) {
    var code = e.closest("tr").find("input[name='Code']").val();
    var CompanyId = e.closest("tr").find("input:eq(0)").val();
    $.ajax({
        url: "/Company/ValidateCompanyCode",
        type: "post",
        data: { "Code": code, "CompanyId": CompanyId },
        async: false,
        success: function (dat) {
            if (dat.IsSuccess) {
                if (dat.Message == "1") {
                    $("#DuplicateCode").val("1");
                }
                else {
                    $("#DuplicateCode").val("0");
                }
            }
        },
        error: function (err) {
            swal("error", err.Message, "error");
            UnBlockUI();
        }
    })

}

function openSearchGrid() {
    $("#search").show();
    $("#openSearch").hide();

}

function hideOpenSearchGrid() {
    $("#search").hide();
    $("#openSearch").show();
    $("#CompanyGrid").data("kendoGrid").dataSource.filter({});
}

function searchList() {
    var searchStr = $("#SearchText").val();
    if (searchStr != null && searchStr.length > 0) {
        var filters = $("#CompanyGrid").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [{
                field: "Code",
                operator: "contains",
                value: searchStr
            },
            {
                field: "RegionalOfficeNameCN",
                operator: "contains",
                value: searchStr
            },
            {
                field: "CityNameCN",
                operator: "contains",
                value: searchStr
            },
            {
                field: "NameCN",
                operator: "contains",
                value: searchStr
            },
            {
                field: "NameEN",
                operator: "contains",
                value: searchStr
            }]
        });
        $("#CompanyGrid").data("kendoGrid").refresh();
        $("#CompanyGrid").data("kendoGrid").dataSource.filter(filters);
    }
}

function exportList() {
    var grid = $("#CompanyGrid").data("kendoGrid");
    grid.saveAsExcel();
}

function filterSearchCity() {
    return {
        RegionalOfficeID: $("#RegionalOfficeID").val()
    };
}
