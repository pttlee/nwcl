﻿$(function () {
    $("#search").hide();
    $("#NewAdd").click(function () {
        var grid = $("#DepartmentGrid").data("kendoGrid");
        if ($("#DepartmentGrid").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        grid.addRow();
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );
        } else {
            grid.addRow();
            //SetFormDirty(true);
        }
    });

    $("#DepartmentGrid").on("click", ".GridRowEdit", function () {
        var row = $(this).closest("tr");
        var control = $(this);

        if ($("#DepartmentGrid").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#DepartmentGrid").data("kendoGrid").editRow(row);
                        //control.closest("tr").find(".GridRowView").addClass('displayNone');
                        //control.closest("tr").find(".GridRowEdit").addClass('displayNone');
                        //control.closest("tr").find(".GridRowSave").removeClass('displayNone');
                        //control.closest("tr").find(".GridRowDelete").addClass('displayNone');
                        //control.closest("tr").find(".GridRowCancel").removeClass('displayNone');
                        $("#DepartmentGrid").find(".k-grid-edit-row .GridRowView").addClass('displayNone');
                        $("#DepartmentGrid").find(".k-grid-edit-row .GridRowEdit").addClass('displayNone');
                        $("#DepartmentGrid").find(".k-grid-edit-row .GridRowDelete").addClass('displayNone');
                        $("#DepartmentGrid").find(".k-grid-edit-row .GridRowSave").removeClass('displayNone');
                        $("#DepartmentGrid").find(".k-grid-edit-row .GridRowCancel").removeClass('displayNone');
                        //SetFormDirty(true);
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );

        } else {
            $("#DepartmentGrid").data("kendoGrid").editRow(row);
            $(this).closest("tr").find(".GridRowView").addClass('displayNone');
            $(this).closest("tr").find(".GridRowEdit").addClass('displayNone');
            $(this).closest("tr").find(".GridRowSave").removeClass('displayNone');
            $(this).closest("tr").find(".GridRowDelete").addClass('displayNone');
            $(this).closest("tr").find(".GridRowCancel").removeClass('displayNone');
            //SetFormDirty(true);
        }
    });

    $("#DepartmentGrid").on("click", ".GridRowSave", function () {
        var row = $(this).closest("tr");
        var prompt = "";

        prompt = verifyDepartment($(this));

        if (prompt == "") {
            checkName($(this));
            var result = $("#DuplicateName").val();
            if (result == 1) {
                swal({
                    title: "",
                    type: "warning",
                    text: getMsgTypeID("SYSTEM") + sysMsg.text_existName,
                    confirmButtonText: CommonResx.text_confirm,
                });
            }
            else {
                BlockUI();
                $("#DepartmentGrid").data("kendoGrid").saveRow(row);
                //SetFormDirty(false);
                UnBlockUI();
            }
        } else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });

    $("#DepartmentGrid").on("click", ".GridRowDelete", function () {
        var row = $(this).closest("tr");
        var DepartmentId = $(this).closest("tr").find("input:eq(0)").val();
        var prompt = "";
        //$.ajax({
        //    url: "/Department/ValidateDepartmentDelete",
        //    type: "post",
        //    data: { "Id": DepartmentId },
        //    async: false,
        //    success: function (dat) {
        //        if (dat.IsSuccess) {
        //            if (dat.Message == "1") {
        //                prompt += getMsgTypeID("SYSTEM") + sysMsg.text_existDepartment;
        //            }
        //        }
        //    },
        //    error: function (err) {
        //        swal("error", err.Message, "error");
        //        UnBlockUI();
        //    }
        //})
        if (prompt == "") {

            swal({
                title: "",
                text: sysMsg.text_confirmDelete,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#DepartmentGrid").data("kendoGrid").removeRow(row);
                        //SetFormDirty(false);
                        UnBlockUI();
                    }
                    else { }
                }
            );
        }
        else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });

    $("#DepartmentGrid").on("click", ".GridRowCancel", function () {
        swal({
            title: "",
            text: sysMsg.text_confirmCancel,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        },

            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    var row = $(this).closest("tr");
                    $("#DepartmentGrid").data("kendoGrid").cancelRow(row);
                    $(this).closest("tr").find(".GridRowView").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowEdit").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowSave").addClass('displayNone');
                    $(this).closest("tr").find(".GridRowDelete").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowCancel").addClass('displayNone');
                    //SetFormDirty(false);
                    UnBlockUI();
                }
                else { }
            }
        );
    });

})

function onEdit(e) {
    
    if (e.model.isNew()) {
        //e.container.find(".GridRowEdit").hide();
        e.container.find(".GridRowView").addClass('displayNone');
        e.container.find(".GridRowEdit").addClass('displayNone');
        //e.container.find(".GridRowSave").show();
        e.container.find(".GridRowSave").removeClass('displayNone');
        //e.container.find(".GridRowDelete").hide();
        e.container.find(".GridRowDelete").addClass('displayNone');
        //e.container.find(".GridRowCancel").show();
        e.container.find(".GridRowCancel").removeClass('displayNone');
        
        e.container.find("input[name='DepartmentName']").removeAttr('data-val-required');

    }
}

function verifyDepartment(e) {
    //var countPromptMessages = 0;
    //var countRequiredValidate = 0;
    var msgs = [];
    var promptMessage = "";
    var isCodeAlreadyInput = false;
    

    if (e.closest("tr").find("input[name='DepartmentName']").val() == "") {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + DepartmentResx.lblDepartmentName_Text);
        //countPromptMessages++;
        //countRequiredValidate++;
    }

    if (e.closest("tr").find("input[name='CityCode']").val() == "") {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + DepartmentResx.lblCityName_Text);
        //countPromptMessages++;
        //countRequiredValidate++;
    }

    

    for (var j = 0; j < msgs.length; j++) {
        promptMessage += msgs[j] + '\n';
    }

    return promptMessage;
}


function openSearchGrid() {
    $("#search").show();
    $("#openSearch").hide();

}

function hideOpenSearchGrid() {
    $("#search").hide();
    $("#openSearch").show();
    $("#DepartmentGrid").data("kendoGrid").dataSource.filter({});
}

function searchList() {
    var searchStr = $("#SearchText").val();
    if (searchStr != null && searchStr.length > 0) {
        var filters = $("#DepartmentGrid").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
                {
                    field: "DepartmentName",
                    operator: "contains",
                    value: searchStr
                },
                {
                    field: "CompanyName",
                    operator: "contains",
                    value: searchStr
                }
            ]
        });
        $("#DepartmentGrid").data("kendoGrid").refresh();
        $("#DepartmentGrid").data("kendoGrid").dataSource.filter(filters);
    }
}

function exportList() {
    var grid = $("#DepartmentGrid").data("kendoGrid");
    grid.setOptions({
        excel: {
            allPages: true
        }
    });
    grid.saveAsExcel();
}

function EditDepartmentDetail(DepartmentId) {
    RedirectScreen(contextRoot + "/Main/Department/EditDepartment?DepartmentID=" + DepartmentId);
}

function checkName(e) {
    var name = e.closest("tr").find("input[name='DepartmentName']").val();
    var id = e.closest("tr").find("input:eq(0)").val();
    $.ajax({
        url: contextRoot + "/Main/Department/ValidateDepartmentDuplicateName",
        type: "post",
        data: { "Name": name, "Id": id },
        async: false,
        success: function (dat) {
            if (dat.IsSuccess) {
                if (dat.Message == "1") {
                    $("#DuplicateName").val("1");
                }
                else {
                    $("#DuplicateName").val("0");
                }
            }
        },
        error: function (err) {
            swal("error", err.Message, "error");
            UnBlockUI();
        }
    })

}

function checkDirtyPage() {
    if ($("#DepartmentGrid .k-grid-edit-row").length > 0) {
        return true;
    }
    else {
        return false;
    }
}