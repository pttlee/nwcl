﻿
$(function () {

    $("#SelectedGrid").on("click", ".GridRowDelete", function () {
        var row = $(this).closest("tr");
        var deptId = $(this).closest("tr").find("input:eq(0)").val();
        swal({
            title: "",
            text: sysMsg.text_confirmDelete,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    //$("#SelectedGrid").data("kendoGrid").async = false;
                    $("#SelectedGrid").data("kendoGrid").removeRow(row);
                    //$("#SelectedGrid").data("kendoGrid").dataSource.sync();
                    //$("#SelectedGrid").data("kendoGrid").dataSource.read();
                    UnBlockUI();
                }
                else { }
            }
        );
    });





    $.ajax({
        type: "POST",
        url: contextRoot+"/Main/Department/GetDomains",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: OnSuccess,
        failure: function (response) {
            swal("error",response.d,"error");
        },
        error: function (response) {
            swal("error", response.d, "error");
        }
    });

    

});

function OnSuccess(r) {
    var domains = r;
    
    $.each(domains, function (i) {
        addCheckbox(this.Value);
    });
    
};

function addCheckbox(name) {
    var container = $('.domainCheckbox');
    var inputs = container.find('input');
    var id = inputs.length + 1;
    var div = $('<div />', { style: 'margin: 5px; width: 100px; float: left'});
    $('<input />', { type: 'checkbox', id: 'cb' + id, value: name, class:'chkDomain' }).appendTo(div);
    $('<label />', { 'for': 'cb' + id, text: name }).appendTo(div);
    div.appendTo(container);
};



function searchClicked()
{
    
    var criteria = $("#txtCriteria").val();
    var domainList = '';

    $(".chkDomain:checked").each(function (i) {
        domainList += $(this).val()+';';        
    });

    var gridObject = $("#UserGrid").data("kendoGrid");
    gridObject.dataSource.transport.options.read.url = contextRoot + "/Main/Department/SearchUser?domains=" + domainList + "&criteria=" + encodeURI(criteria);
    gridObject.dataSource.read();



    //var paramterJSON = { 'domains':  domainList , 'criteria': criteria };
    //$.ajax({
    //    type: "POST",
    //    url: "/Main/Department/SearchUser",
        
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    data: JSON.stringify(paramterJSON),
    //    success: OnSuccessSearch,
    //    failure: function (response) {
    //        swal("error", response.d, "error");
    //    },
    //    error: function (response) {
    //        swal("error", response.d, "error"); 
    //    }
    //});
}

//function OnSuccessSearch(r) {
       
//    $("#UserGrid").kendoGrid({
//        dataSource: {
//            data: r
//      }
//    });
//};

function AddUserToGroup(adLogonName) {
    var url = window.location.href;

    var departmentID = url.substring(url.lastIndexOf('=') + 1, url.length);
    var paramterJSON = { 'departmentId': departmentID, 'adLogonName': adLogonName };
    var isValid = false;
    $.ajax({
        type: "POST",
        url: contextRoot + "/Main/Department/AddUserToGroup",

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(paramterJSON),
        async: false,
        success: function (response) {
            if (response.Message.length > 0) {
                isValid = false;
                swal({
                    title: "",
                    type: "warning",
                    text: response.Message,
                    confirmButtonText: CommonResx.text_confirm,
                });
                
            }
            else
                isValid = true;
        },
        failure: function (response) {
            swal("error", response.d, "error");
        },
        error: function (response) {
            swal("error", response.d, "error"); 
        }
    });

    var gridObject = $("#SelectedGrid").data("kendoGrid");
    gridObject.dataSource.read();
    

}

function ClickReturn() {
    BlockUI();
    location.href = contextRoot + "/Main/Department";
    UnBlockUI();
}