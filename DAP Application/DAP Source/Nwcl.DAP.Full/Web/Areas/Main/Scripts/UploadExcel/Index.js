﻿$(document).ajaxStart(function () { BlockUI(); }).ajaxStop(function () { UnBlockUI(); });

function onSelect(e) {
    kendoConsole.log("Select :: " + getFileInfo(e));
}

function onUpload(e) {
    kendoConsole.log("Upload :: " + getFileInfo(e));
    //$.ajax({
    //    url: "/UploadExcel/ValidateUserGroup",
    //    type: "post",
    //    data: { "Id": UserGroupId },
    //    async: false,
    //    success: function (dat) {
    //        if (dat.IsSuccess) {
    //            if (dat.Message == "0") {
    //                prompt += getMsgTypeID("SYSTEM") + sysMsg.text_existUserGroup;
    //            }
    //        }
    //    },
    //    error: function (err) {
    //        swal("error", err.Message, "error");
    //        UnBlockUI();
    //    }
    //})
}

function onSuccess(e) {
    kendoConsole.log("Success (" + e.operation + ") :: " + getFileInfo(e));
    if (e.operation == 'remove') {
        $('#confirmUploadExcelDiv').hide();
        return;
    }
    //BlockUI();
    var gid = e.response.data;
    $('#uploadGuid').val(gid);
    //alert(gid);
    $.ajax({
        type: 'Get', async: 'false',
        data: {
        },
        url: contextRoot + '/Main/UploadExcel/_ProjectTreePartial?guid=' + gid,
        dataType: 'html',  // add this line
        success: function (result) {
            $('#entityTreeDiv').html(result);

            // display confirmation box
            if (e.operation == 'remove') {
                $('#confirmUploadExcelDiv').hide();
            } else if (e.operation == 'upload') {
                $('#confirmUploadExcelDiv').show();
            } else {
                $('#confirmUploadExcelDiv').hide();
            }
        }
    });
    //UnBlockUI();
}

function onError(e) {
    kendoConsole.log("Error (" + e.operation + ") :: " + getFileInfo(e));
}

function onComplete(e) {
    kendoConsole.log("Complete");
}

function onCancel(e) {
    kendoConsole.log("Cancel :: " + getFileInfo(e));
}

function onRemove(e) {
    $('#confirmUploadExcelDiv').hide();
    kendoConsole.log("Remove :: " + getFileInfo(e));
}

function onProgress(e) {
    kendoConsole.log("Upload progress :: " + e.percentComplete + "% :: " + getFileInfo(e));
}

function onClear(e) {
    kendoConsole.log("Clear");
}

function getFileInfo(e) {
    return $.map(e.files, function (file) {
        var info = file.name;

        // File size is not available in all browsers
        if (file.size > 0) {
            info += " (" + Math.ceil(file.size / 1024) + " KB)";
        }
        return info;
    }).join(", ");
}

function ClickReturn() {
    location.reload();
}

function clickSave() {
    $.ajax({
        type: 'Get', async: 'false',
        data: {
        },
        url: contextRoot + '/Main/UploadExcel/UpdateTempToMaster?guid=' + $('#uploadGuid').val(),
        dataType: 'html', 
        success: function (result) {
            $('#confirmUploadExcelDiv').hide();

            swal({ title: "", text: CommonResx.SaveSucess, type: "success" },
                function () {
                    location.reload();
                }
            );
        }
    });
}