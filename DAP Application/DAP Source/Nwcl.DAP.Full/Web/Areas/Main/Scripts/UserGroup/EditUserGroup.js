﻿// show checked node IDs on datasource change
function onCheck(e) {
    var checkedNodes = [],
        treeView = $("#UserGroupTreeView").data("kendoTreeView"),
        message;

    if (treeView.dataItem(e.node).id != "-1") {
        $("#IsEditingTree").val("1");
        //SetFormDirty(true);
    }

    if (!treeView.dataItem(e.node).checked) return;

    BlockUI();
    treeView.expand(treeView.dataItem(e.node));

    checkAllChildNodes(treeView, e.node);

    if (treeView.dataItem(e.node).id == "-1" && treeView.dataItem(e.node).checked) {
        treeView.dataItem(e.node).set("checked", false);
    }
    UnBlockUI();
}
function checkAllChildNodes(treeView, node) {
    treeView.dataItem(node).set("expanded", true);
    if (treeView.dataItem(node).id != "-1") {
        treeView.dataItem(node).set("checked", true);
        $("#IsEditingTree").val("1");
        //SetFormDirty(true);
    }
    var childNodes = node.childNodes;
    if (node.childNodes.length > 1) {
        for (var i = 0; i < childNodes[1].childNodes.length; i++) {
            var innerNode = childNodes[1].childNodes[i];
            checkAllChildNodes(treeView, innerNode)
        }
    }
}

function expandAllChildNodes(treeView, dNode) {
    treeView.expand(dNode);
    var childNodes = dNode.children();
    if (childNodes.length > 0) {
        //for (var i = 0; i < childNodes.length; i++) {
            expandAllChildNodes(treeView, childNodes);
        //}
    }
}

$(window).on('load', function () {
    var treeView = $("#UserGroupTreeView").data("kendoTreeView");
    treeView.items().each(function (i, el) {
        $(el).on("dblclick", function (e) {
            var dNode = treeView.findByUid(treeView.select().data().uid);
            expandAllChildNodes(treeView, dNode);
        });
    });
});

function getAllCheckedCodeRegion(treeView, node) {
    var checkedCodes = [];

    var childNodes = node.childNodes;
    if (node.childNodes.length > 1) {
        for (var i = 0; i < childNodes.length; i++) {
            var innerNode = childNodes[i];
            checkedCodes = checkedCodes.concat(getAllCheckedUsersCode(treeView, innerNode));
        }
    }
    return checkedCodes;
}

function getAllCheckedUsersCode(treeView, node) {
    var checkedCodes = [];
    if (treeView.dataItem(node).checked && treeView.dataItem(node).id != "-1") {
        checkedCodes.push(treeView.dataItem(node).id);
    }

    var childNodes = node.childNodes;
    if (node.childNodes.length > 1) {
        for (var i = 0; i < childNodes[1].childNodes.length; i++) {
            var innerNode = childNodes[1].childNodes[i];
            checkedCodes = checkedCodes.concat(getAllCheckedUsersCode(treeView, innerNode));
        }
    }
    return checkedCodes;
}


function clickSave() {
    if ($("#selectedId").val() == "") {
        swal({
            title: "",
            type: "warning",
            text: getMsgTypeID("SYSTEM") + sysMsg.text_PlsSelectItem,
            confirmButtonText: CommonResx.text_confirm,
        })
    } else {
        $.ajax({
            url: contextRoot + "/Main/UserGroup/CreateUserGroupUser",
            type: "post",
            data: {
                "userGroupID": $("#EditUserGroup_UserGroupId").val(),
                "selectedCodes": getAllCheckedCodeRegion($("#UserGroupTreeView").data("kendoTreeView"), $('.k-group')[0]).join(",")
            },
            async: false,
            success: function (dat) {
                if (dat.IsSuccess) {
                    if (dat.Message == "1") {
                        $("#IsEditingTree").val("0");
                        //UpdateDirty($("#EntityGroupRole .k-grid-edit-row"));
                        swal({
                            title: "",
                            type: "success",
                            text: CommonResx.SaveSucess,
                            confirmButtonText: CommonResx.btnOk_Text
                        });
                    }
                }
            },
            error: function (err) {
                swal("error", dat.Message, "error");
                UnBlockUI();
            }
        })
    }
}

function ClickReturn() {
    RedirectScreen(contextRoot + "/Main/UserGroup");
}






//Tab2
$(function () {
    $("#search").hide();
    $("#NewAdd").click(function () {
        var grid = $("#EntityGroupRole").data("kendoGrid");
        if ($("#EntityGroupRole").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        grid.addRow();
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );
        } else {
            grid.addRow();
            //SetFormDirty(true);
        }
    });

    $("#EntityGroupRole").on("click", ".GridRowEdit", function () {
        var row = $(this).closest("tr");
        var control = $(this);

        if ($("#EntityGroupRole").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#EntityGroupRole").data("kendoGrid").editRow(row);
                        //control.closest("tr").find(".GridRowEdit").addClass('displayNone');
                        //control.closest("tr").find(".GridRowSave").removeClass('displayNone');
                        //control.closest("tr").find(".GridRowDelete").addClass('displayNone');
                        //control.closest("tr").find(".GridRowCancel").removeClass('displayNone');
                        $("#EntityGroupRole").find(".k-grid-edit-row .GridRowEdit").addClass('displayNone');
                        $("#EntityGroupRole").find(".k-grid-edit-row .GridRowDelete").addClass('displayNone');
                        $("#EntityGroupRole").find(".k-grid-edit-row .GridRowSave").removeClass('displayNone');
                        $("#EntityGroupRole").find(".k-grid-edit-row .GridRowCancel").removeClass('displayNone');
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );

        } else {
            $("#EntityGroupRole").data("kendoGrid").editRow(row);
            $(this).closest("tr").find(".GridRowEdit").addClass('displayNone');
            $(this).closest("tr").find(".GridRowSave").removeClass('displayNone');
            $(this).closest("tr").find(".GridRowDelete").addClass('displayNone');
            $(this).closest("tr").find(".GridRowCancel").removeClass('displayNone');

            $(this).closest("tr").find("input[name='UserGroupId']").removeAttr('data-val-required');
            $(this).closest("tr").find("input[name='RoleId']").removeAttr('data-val-required');

            //SetFormDirty(true);
        }
    });

    $("#EntityGroupRole").on("click", ".GridRowSave", function () {
        var row = $(this).closest("tr");
        $('#EntityGroupRole').data('kendoGrid').dataSource.data()[
            $('#EntityGroupRole').data('kendoGrid').dataSource.data().length - 1].UserGroupId = $("#EditUserGroup_UserGroupId").val();
        var prompt = "";

        prompt = verifyUserGroup($(this));

        if (prompt == "") {   

            var result = $("#DuplicatePrivilege").val();
            if (result == 1) {
                swal({
                    title: "",
                    type: "warning",
                    text: getMsgTypeID("SYSTEM") + CommonResx.text_dapPrivilege + sysMsg.text_existCode,
                    confirmButtonText: CommonResx.text_confirm,
                });
            }
            else {
                BlockUI();
                $("#EntityGroupRole").data("kendoGrid").saveRow(row);
                //$("#editMode").val("0");
                //UpdateDirty($("#EntityGroupRole .k-grid-edit-row"));
                UnBlockUI();
            }
        } else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });

    $("#EntityGroupRole").on("click", ".GridRowDelete", function () {
        var row = $(this).closest("tr");
        var UserGroupId = $(this).closest("tr").find("input:eq(0)").val();
        var prompt = "";
        $.ajax({
            url: contextRoot + "/Main/UserGroup/ValidateUserGroup",
            type: "post",
            data: { "Id": UserGroupId },
            async: false,
            success: function (dat) {
                if (dat.IsSuccess) {
                    if (dat.Message == "0") {
                        prompt += getMsgTypeID("SYSTEM") + sysMsg.text_existUserGroup;
                    }
                }
            },
            error: function (err) {
                swal("error", err.Message, "error");
                UnBlockUI();
            }
        })
        if (prompt == "") {

            swal({
                title: "",
                text: sysMsg.text_confirmDelete,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#EntityGroupRole").data("kendoGrid").removeRow(row);
                        //UpdateDirty($("#EntityGroupRole .k-grid-edit-row"));
                        UnBlockUI();
                    }
                    else { }
                }
            );
        }
        else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });

    $("#EntityGroupRole").on("click", ".GridRowCancel", function () {
        swal({
            title: "",
            text: sysMsg.text_confirmCancel,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        },

            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    var row = $(this).closest("tr");
                    $("#EntityGroupRole").data("kendoGrid").cancelRow(row);
                    $(this).closest("tr").find(".GridRowEdit").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowSave").addClass('displayNone');
                    $(this).closest("tr").find(".GridRowDelete").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowCancel").addClass('displayNone');
                    //UpdateDirty($("#EntityGroupRole .k-grid-edit-row"));
                    UnBlockUI();
                }
                else { }
            }
        );
    });

})

function onEdit(e) {

    if (e.model.isNew()) {        
        e.container.find(".GridRowEdit").addClass('displayNone');
        e.container.find(".GridRowSave").removeClass('displayNone');
        e.container.find(".GridRowDelete").addClass('displayNone');
        e.container.find(".GridRowCancel").removeClass('displayNone');

        e.container.find("input[name='UserGroupId']").removeAttr('data-val-required');
        e.container.find("input[name='RoleId']").removeAttr('data-val-required');

    }
}

function verifyUserGroup(e) {
    //var countPromptMessages = 0;
    //var countRequiredValidate = 0;
    var msgs = [];
    var promptMessage = "";
    var isCodeAlreadyInput = false;


    if (e.closest("tr").find("input[name='RoleId']").val() == 0) {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CommonResx.text_dapRole);        
    }

    if (e.closest("tr").find("input[name='EntityGroupId']").val() == 0) {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CommonResx.text_dapEntityGroup);        
    }



    for (var j = 0; j < msgs.length; j++) {
        promptMessage += msgs[j] + '\n';
    }


    //Generate Validation Message to sentence //20180610
    //for (var j = 0; j < msgs.length; j++)
    //{
    //    //start to add and or , from the second item
    //    if (j > 0) {
    //        if (j < countPromptMessages - 1) {
    //            if (msgs[j].trim().length > 0)
    //                promptMessage += " , "
    //        }
    //        else
    //            promptMessage += " " + sysMsg.text_and + " ";
    //    }
    //    promptMessage += msgs[j];
    //}

    if (promptMessage == "") { checkDuplicatePrivilege(e); }

    return promptMessage;
}

function checkDuplicatePrivilege(e) {
    var userGroupId = $("#userGroupId").val();
    var entityGroupId = e.closest("tr").find("input[name='EntityGroupId']").val();
    var roleId = e.closest("tr").find("input[name='RoleId']").val();
    var id = e.closest("tr").find("input:eq(0)").val();
    $.ajax({
        url: contextRoot + "/Main/Privilege/ValidatePrivilegeDuplicate",
        type: "post",
        data: { "id": id, "entityGroupId": entityGroupId, "userGroupId": userGroupId, "roleId": roleId },
        async: false,
        success: function (dat) {
            if (dat.IsSuccess) {
                if (dat.Message == "1") {
                    $("#DuplicatePrivilege").val("1");
                }
                else {
                    $("#DuplicatePrivilege").val("0");
                }
            }
        },
        error: function (err) {
            swal("error", err.Message, "error");
            UnBlockUI();
        }
    })

}

function exportList() {
    var grid = $("#EntityGroupRole").data("kendoGrid");
    grid.saveAsExcel();
}


function checkDirtyPage() {
    return ($("#EntityGroupRole .k-grid-edit-row").length > 0 || $("#IsEditingTree").val() == "1");
}