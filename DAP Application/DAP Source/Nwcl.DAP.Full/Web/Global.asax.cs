﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using log4net;
using MohammadYounes.Owin.Security.MixedAuth;
using Nwcl.DAP.Application.Service;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace Nwcl.DAP.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public MvcApplication()
        {
            //register MixedAuth
            this.RegisterMixedAuth();
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log4net.Config.XmlConfigurator.Configure();

            // need to configure remoting if remote service is used (Separate App Server)
            if (ServiceProviderFactory.IsRemoteService)
            {
                RemotingConfiguration.Configure(this.Server.MapPath("~/Web.config"), false);
            }
        }
    }
}
