﻿using Nwcl.DAP.Application.Service;
using Nwcl.DAP.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Nwcl.DAP.Web.API.Controllers
{
    public class CitiesController : ApiController
    {
        private CommonServiceProvider _commonServiceProvider = null;

        //[Authorize]
        //[ResponseType(typeof(List<Project>))]
        //public IHttpActionResult GetProjectByDateRange(DateTime startDate, DateTime endDate)
        //{
        //    return Json(this.CommonServiceProvider.GetProjectByDateRange(startDate, endDate));
        //}

        ///// <summary>
        ///// POC function
        ///// </summary>
        ///// <returns></returns>
        //[Authorize]
        //[ResponseType(typeof(List<City>))]
        //public IHttpActionResult GetCities()
        //{
        //    return Json(this.CommonServiceProvider.GetCityList());// Ok(this.CommonServiceProvider.GetCityList());
        //}

        //[Authorize]
        //[ResponseType(typeof(City))]
        //public IHttpActionResult GetCities(int id)
        //{
        //    City city = this.CommonServiceProvider.GetCityList().FirstOrDefault(c => c.ID == id);
        //    if (city == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(city);
        //}

        //// POST: api/Cities
        //[Authorize]
        //[ResponseType(typeof(City))]
        //public IHttpActionResult PostProduct(City city)
        //{
        //    //if (!ModelState.IsValid)
        //    //{
        //    //    return BadRequest(ModelState);
        //    //}

        //    //// TODO: Call corresponding service provider to add to the system

        //    //return CreatedAtRoute("DefaultApi", new { id = city.ID }, city);

        //    throw new NotImplementedException();
        //}

        //// PUT: api/Cities/5
        //[Authorize]
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutCity(int id, City city)
        //{
        //    //if (!ModelState.IsValid)
        //    //{
        //    //    return BadRequest(ModelState);
        //    //}

        //    //if (id != city.ID)
        //    //{
        //    //    return BadRequest();
        //    //}

        //    //// TODO: call corresponding service provider to update city in the system

        //    //return StatusCode(HttpStatusCode.NoContent);

        //    throw new NotImplementedException();
        //}

        //// DELETE: api/Cities/5
        //[Authorize]
        //[ResponseType(typeof(City))]
        //public IHttpActionResult DeleteCity(int id)
        //{
        //    //City city = this.CommonServiceProvider.GetCityList().FirstOrDefault(c => c.ID == id);
        //    //if (city == null)
        //    //{
        //    //    return NotFound();
        //    //}

        //    //// TODO: Call the corresponding service provider to delete city from the system

        //    throw new NotImplementedException();
        //}

        #region Property

        /// <summary>
        /// Gets the CommonServiceProvider for the controller
        /// </summary>
        protected CommonServiceProvider CommonServiceProvider
        {
            get
            {
                if (this._commonServiceProvider == null)
                {
                    this._commonServiceProvider = (CommonServiceProvider)ServiceProviderFactory.GetServiceProvider(typeof(CommonServiceProvider));
                }
                return this._commonServiceProvider;
            }
        }

        

        #endregion
    }
}
