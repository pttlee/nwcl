﻿using Nwcl.DAP.Application.Service;
using Nwcl.DAP.DAL.Model;
using Nwcl.DAP.Web.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using Newtonsoft.Json;

namespace Nwcl.DAP.Web.API.Controllers
{
    public class PackageController : ApiController
    {
        private CommonServiceProvider _commonServiceProvider = null;

        //[Authorize]
        //[ResponseType(typeof(GetTenderPackageResultModel))]
        //[ActionName("GetPackage")]
        //public IHttpActionResult GetPackage(int tenderPackageID)
        //{
        //    List<TenderPackage>  tenderPackage = null;
        //    IEnumerable<TenderPackageModel> tenderPackageModel = null;
        //    IEnumerable<Package> packageModel = null;
        //    GetTenderPackageResultModel getTenderPackageResultModel = new GetTenderPackageResultModel();
        //    getTenderPackageResultModel.Status = true;


        //    try
        //    {
        //        tenderPackage = this.CommonServiceProvider.GetTenderPackageByID(tenderPackageID);
        //        if (tenderPackage != null && tenderPackage.Count() > 0)
        //        {
        //            Project proj = this.CommonServiceProvider.GetProjectByID(tenderPackage.First().ProjectID);

        //            tenderPackageModel = tenderPackage.Select(o => new TenderPackageModel
        //            {
        //                ID = o.ID,
        //                ProjectCode = proj.ProjectCode,
        //                PhaseCode = proj.PhaseCode,
        //                Type = o.Type,
        //                TenderPackageType = this.CommonServiceProvider.GetGeneralTypeList().Where(e => e.ID == o.TenderPackageTypeID).First().Code,
        //                Name = o.Name,
        //                Description = o.Description,
        //                ReleasedBudget = o.ReleasedBudget,
        //                CreatedBy = o.CreatedBy,
        //                Created = o.Created,
        //                LastUpdatedBy = o.LastUpdatedBy,
        //                LastUpdated = o.LastUpdated,

        //                TenderProgress = o.TenderProgresses.Select(a => new TenderProgressModel
        //                {
        //                    ID = a.ID,
        //                    TenderDate = a.GeneralType.Code,
        //                    TargetDate = a.TargetDate

        //                }).ToList(),

        //                Package = o.Packages.Select(a => new PackageModel
        //                {
        //                    ID = a.ID,
        //                    Code = a.Code,
        //                    Description = a.Description,
        //                    ReleasedBudget = a.ReleaseBudget,
        //                    PackageItem = a.PackageItems.Select(b => new PackageItemModel
        //                    {
        //                        ID = b.ID,
        //                        ItemNo = b.ItemNo,
        //                        Location = this.CommonServiceProvider.GetLocationByID(b.LocationID) != null ?
        //                            this.CommonServiceProvider.GetLocationByID(b.LocationID).Code : null,
        //                        CostCodeElement = this.CommonServiceProvider.GetCostCodeElement(b.CostCodeID) != null ?
        //                            this.CommonServiceProvider.GetCostCodeElement(b.CostCodeID).Code : null,
        //                        CostCodeTrade = this.CommonServiceProvider.GetCostCodeTrade(b.CostCodeID) != null ?
        //                            this.CommonServiceProvider.GetCostCodeTrade(b.CostCodeID).Code : null,
        //                        CostCodeSubTrade = this.CommonServiceProvider.GetCostCodeSubTrade(b.CostCodeID) != null ?
        //                            this.CommonServiceProvider.GetCostCodeSubTrade(b.CostCodeID).Code : null,
        //                        Description = b.Description,
        //                        Qty = b.Qty,
        //                        Unit = this.CommonServiceProvider.GetUnitByID(b.UnitID) != null ?
        //                            this.CommonServiceProvider.GetUnitByID(b.UnitID).Code : null,
        //                        ParentPackageItem = b.ParentID != null ?
        //                        (this.CommonServiceProvider.GetPackageItemByID(b.ParentID) != null ?
        //                        this.CommonServiceProvider.GetPackageItemByID(b.ParentID).ItemNo : null) : null,
        //                        EstimatedRate = b.EstimatedRate,
        //                        EstimatedSum = b.EstimatedSum
        //                    }).ToList()

        //                }).ToList()

        //            });
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        getTenderPackageResultModel.Status = false;
        //        getTenderPackageResultModel.Message = e.Message;
        //    }

        //    getTenderPackageResultModel.Content = tenderPackageModel;

        //    //string currentUserID = HttpContext.Current.User.Identity.GetUserId();


        //    SCMInterface scmInterface = new SCMInterface();
        //    scmInterface.APICall = "GetPackage";
        //    scmInterface.Created = DateTime.Now;
        //    scmInterface.LastUpdated = DateTime.Now;
        //    scmInterface.JSON = JsonConvert.SerializeObject(tenderPackageModel);
        //    scmInterface.CreatedBy = "Web API: Get Package";
        //    scmInterface.LastUpdatedBy = "Web API: Get Package";
        //    scmInterface.ToSCM = true;
        //    scmInterface.MessageBody = getTenderPackageResultModel.Message;
        //    this.CommonServiceProvider.InsertSCMInterfaceLog(scmInterface);

        //    return Json(getTenderPackageResultModel);

        //}


        #region Property

        /// <summary>
        /// Gets the CommonServiceProvider for the controller
        /// </summary>
        protected CommonServiceProvider CommonServiceProvider
        {
            get
            {
                if (this._commonServiceProvider == null)
                {
                    this._commonServiceProvider = (CommonServiceProvider)ServiceProviderFactory.GetServiceProvider(typeof(CommonServiceProvider));
                }
                return this._commonServiceProvider;
            }
        }



        #endregion
    }
}
