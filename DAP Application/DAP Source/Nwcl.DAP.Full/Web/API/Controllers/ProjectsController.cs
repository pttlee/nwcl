﻿using Nwcl.DAP.Application.Service;
using Nwcl.DAP.DAL.Model;
using Nwcl.DAP.Web.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using Newtonsoft.Json;

namespace Nwcl.DAP.Web.API.Controllers
{
    public class ProjectsController : ApiController
    {
        private CommonServiceProvider _commonServiceProvider = null;

        //[Authorize]
        //[ResponseType(typeof(GetProjectResultModel))]
        //[ActionName("GetProjects")]
        //public IHttpActionResult GetProjects(DateTime startDate, DateTime endDate)
        //{
        //    List<Project> prjs = null;
        //    IEnumerable<ProjectModel> projectModel = null;
        //    GetProjectResultModel getProjectResultModel = new GetProjectResultModel();
        //    getProjectResultModel.Status = true;
            
        //    try
        //    {
        //        prjs = this.CommonServiceProvider.GetProjectByDateRange(startDate, endDate);

        //        projectModel = prjs.Select(o => new ProjectModel
        //        {
        //            ID = o.ID,
        //            CityName = o.City.NameCN,
        //            RegionName = o.RegionalOffice.NameCN,
        //            StatusName = o.Status1.NameCN,
        //            ProjectCompany = o.ProjectCompanies.Select(a => new ProjectCompanyModel
        //            {
        //                ID = a.ID,
        //                Company = a.Company.NameCN,
        //                Type = a.GeneralType.NameCN,
        //                Contact1 = a.Contact1,
        //                Contact2 = a.Contact2,
        //                ContactTel1 = a.ContactTel1,
        //                ContactTel2 = a.ContactTel2,
        //                Created = a.Created,
        //                CreatedBy = a.CreatedBy,
        //                LastUpdated = a.LastUpdated,
        //                LastUpdatedBy = a.LastUpdatedBy
        //            }
        //            ).ToList(),
        //            ProjectCostCenter = o.ProjectCostCenters.Select(a => new ProjectCostCenterModel
        //            {
        //                ID = a.ID,
        //                ActualDate = a.ActualDate,
        //                CarPark = a.CarPark,
        //                CFADown = a.CFADown,
        //                CFAUp = a.CFAUp,
        //                CommercialHouseSalesPermit = a.CommercialHouseSalesPermit,
        //                CompletionDate = a.CompletionDate,
        //                ConstructionLandPlanningPermit = a.ConstructionLandPlanningPermit,
        //                ConstructionPermit = a.ConstructionPermit,
        //                ConstructionProjectPlanningPermit = a.ConstructionProjectPlanningPermit,
        //                CostCenterCode = a.CostCenterCode,
        //                CostCenterName = a.CostCenterName,
        //                Created = a.Created,
        //                CreatedBy = a.CreatedBy,
        //                CreateProjectDate = a.CreateProjectDate,
        //                EstimatedDate = a.EstimatedDate,
        //                GFADown = a.GFADown,
        //                GFAUp = a.GFAUp,
        //                HandoverOfLand = a.HandoverOfLand,
        //                isSecCostAllocate = a.isSecCostAllocate,
        //                LastUpdated = a.LastUpdated,
        //                LastUpdatedBy = a.LastUpdatedBy,
        //                OrderSeq = a.OrderSeq,
        //                PaymentDate = a.PaymentDate,
        //                RADown = a.RADown,
        //                RAUp = a.RAUp,
        //                StateOwnedLandUseCert = a.StateOwnedLandUseCert,
        //                PropertyType = a.PropertyType.Code + " " + a.PropertyType.NameCN
        //            }
        //            ).ToList(),
        //            Address = o.Address,
        //            ApprovalStep = o.ApprovalStep,
        //            BuildingTotalArea = o.BuildingTotalArea,
        //            CFAExcludeCarpark = o.CFAExcludeCarpark,
        //            CompletionDate = o.CompletionDate,
        //            ConstructionPermit = o.ConstructionPermit,
        //            Created = o.Created,
        //            DeliverDate = o.DeliverDate,
        //            CreatedBy = o.CreatedBy,
        //            Description = o.Description,
        //            IsCreatedBudget = o.IsCreatedBudget,
        //            IsCreatedCostPlan = o.IsCreatedCostPlan,
        //            LandHandover = o.LandHandover,
        //            LandPlanningPermit = o.LandPlanningPermit,
        //            LandUsageLicense = o.LandUsageLicense,
        //            LandUsagePermit = o.LandUsagePermit,
        //            LastUpdated = o.LastUpdated,
        //            LastUpdatedBy = o.LastUpdatedBy,
        //            NameCN = o.NameCN,
        //            NameEN = o.NameEN,
        //            NonAccountableGFA = o.NonAccountableGFA,
        //            //ParentProject
        //            PhaseCode = o.PhaseCode,
        //            PhaseCodeLv1 = o.PhaseCodeLv1,
        //            PhaseCodeLv2 = o.PhaseCodeLv2,
        //            PhaseCodeLv3 = o.PhaseCodeLv3,
        //            PresaleLicense = o.PresaleLicense,
        //            ProjectCode = o.ProjectCode,
        //            ProjectDate = o.ProjectDate,
        //            Enable = o.Status,
        //            TotalArea = o.TotalArea,
        //            TotalCFA = o.TotalCFA,
        //            TotalGFA = o.TotalGFA,
        //            TotalSaleableLeaseableArea = o.TotalSaleableLeaseableArea,
        //            VolumeRate = o.VolumeRate,

        //            AreaResp = o.SystemUser == null ? "" : o.SystemUser.DisplayNameEN,
        //            Chairman = o.SystemUser1 == null ? "" : o.SystemUser1.DisplayNameEN,
        //            ContractResp = o.SystemUser2 == null ? "" : o.SystemUser2.DisplayNameEN,
        //            ProjectDesigner = o.SystemUser3 == null ? "" : o.SystemUser3.DisplayNameEN,
        //            ProjectResp = o.SystemUser4 == null ? "" : o.SystemUser4.DisplayNameEN,
        //            ProjectSec = o.SystemUser5 == null ? "" : o.SystemUser5.DisplayNameEN,
        //            ProjectViceResp = o.SystemUser6 == null ? "" : o.SystemUser6.DisplayNameEN

        //        });
        //    }
        //    catch(Exception e)
        //    {
        //        getProjectResultModel.Status = false;
        //        getProjectResultModel.Message = e.Message;
        //    }

        //    getProjectResultModel.Content = projectModel;

        //    //string currentUserID = HttpContext.Current.User.Identity.GetUserId();
            
            
        //    SCMInterface scmInterface = new SCMInterface();
        //    scmInterface.APICall = "GetProject";
        //    scmInterface.Created = DateTime.Now;
        //    scmInterface.LastUpdated = DateTime.Now;
        //    scmInterface.JSON = JsonConvert.SerializeObject(projectModel);
        //    scmInterface.CreatedBy = "Web API: Get Project";
        //    scmInterface.LastUpdatedBy = "Web API: Get Project";
        //    scmInterface.ToSCM = true;
        //    scmInterface.MessageBody = getProjectResultModel.Message;
        //    this.CommonServiceProvider.InsertSCMInterfaceLog(scmInterface);

        //    return Json(getProjectResultModel);

        //}
        

        #region Property

        /// <summary>
        /// Gets the CommonServiceProvider for the controller
        /// </summary>
        protected CommonServiceProvider CommonServiceProvider
        {
            get
            {
                if (this._commonServiceProvider == null)
                {
                    this._commonServiceProvider = (CommonServiceProvider)ServiceProviderFactory.GetServiceProvider(typeof(CommonServiceProvider));
                }
                return this._commonServiceProvider;
            }
        }

        

        #endregion
    }
}
