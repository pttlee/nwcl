﻿using Microsoft.Owin.Security.OAuth;
using Nwcl.DAP.Common.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;

namespace Nwcl.DAP.Web.API.Providers
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private ApplicationUserManager _userManager;

        //public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        //{
        //    string clientId;
        //    string clientSecret;

        //    if (context.TryGetBasicCredentials(out clientId, out clientSecret))
        //    {
        //        IdentityUser user = this.UserManager.Users.FirstOrDefault(u => u.ClientID == clientId && u.ClientSecret == clientSecret);
        //        if (user == null)
        //        {
        //            context.SetError("invalid_client", "The client id or client secret are incorrect.");
        //            context.Rejected();
        //        }
        //        else
        //        {
        //            context.Validated();
        //        }
        //    }
        //    else
        //    {
        //        context.SetError("invalid_client", "Client credentials could not be retrieved from the Authorization header");
        //        context.Rejected();
        //    }
        //    return Task.FromResult<object>(null);
        //}

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId;
            string clientSecret;
            if (context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                // try to get the impersonated user from the client id and client secret
                IdentityUser user = this.UserManager.Users.FirstOrDefault(u => u.ClientID == clientId && u.ClientSecret == clientSecret);
                if (user == null)
                {
                    context.SetError("invalid_client", "The client id or client secret are incorrect.");
                    context.Rejected();
                }
                else
                {
                    context.Validated(clientId);
                }
            }
            else
            {
                context.SetError("invalid_client", "Client credentials could not be retrieved from the Authorization header");
                context.Rejected();
            }
            return base.ValidateClientAuthentication(context);
        }

        public override Task GrantClientCredentials(OAuthGrantClientCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("ClientID", context.ClientId));
            var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
            context.Validated(ticket);

            return base.GrantClientCredentials(context);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}