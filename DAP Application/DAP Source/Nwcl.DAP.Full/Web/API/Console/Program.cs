﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO;
using System.Data;

namespace Nwcl.DAP.Web.API.Console
{
    public class Program
    {
        static void Main(string[] args)
        {
            //// please specify the correct parameters here
            //string webApiServerUri = "http://localhost:59906";
            //string clientId = "atos";
            //string clientSecret = "atos@nwcl";
            //System.Console.OutputEncoding = Encoding.Unicode;
            //OAuthClientTest oAuthClientTest = new OAuthClientTest(webApiServerUri);
            //string getAccessTokenJsonResult = oAuthClientTest.GetAccessTokenByClientCredentials(clientId, clientSecret);
            //JObject accessTokenJObj = JObject.Parse(getAccessTokenJsonResult);
            //if (accessTokenJObj["error"] != null)
            //{
            //    System.Console.Out.WriteLine("Error: " + accessTokenJObj["error"]);
            //}
            //else
            //{
            //    System.Console.Out.WriteLine("AccessToken: " + accessTokenJObj["access_token"]);
            //    //System.Console.Out.WriteLine(oAuthClientTest.GetCities((string)accessTokenJObj["access_token"]));
            //    //System.Console.Out.WriteLine(oAuthClientTest.GetProjects((string)accessTokenJObj["access_token"], new DateTime(2018,1,1), new DateTime(2019,1,1)));
            //    System.Console.Out.WriteLine(oAuthClientTest.GetPackage((string)accessTokenJObj["access_token"], 3));

            //}
            ExcelProcess();
        }

        public static void ExcelProcess()
        {
            XSSFWorkbook wb;
            //HSSFWorkbook wb;
            using (FileStream file = new FileStream(@"c:\temp\-房产项目匯總_180709.xlsx", FileMode.Open, FileAccess.Read))
            {
                wb = new XSSFWorkbook(file);
                ISheet sheet = wb.GetSheet("地區及城市");
                DataTable dt = new DataTable();
                dt.Columns.Add("Region");
                dt.Columns.Add("CityCode");
                dt.Columns.Add("CityName");
                DataRow dr;
                for (int row = 1; row <= sheet.LastRowNum; row++) //first line no need.
                {
                    dr = dt.NewRow();
                    string region = sheet.GetRow(row).GetCell(0).StringCellValue;
                    string cityCode = sheet.GetRow(row).GetCell(1).StringCellValue;
                    string cityName = sheet.GetRow(row).GetCell(2).StringCellValue;
                    dr["Region"] = region;
                    dr["CityCode"] = cityCode;
                    dr["CityName"] = cityName;
                    dt.Rows.Add(dr);
                }

                ISheet citySheet;
                DataTable dtProject = new DataTable();
                dtProject.Columns.Add("CityCode");
                dtProject.Columns.Add("ProjectCode");
                dtProject.Columns.Add("ProjectPhase1");
                dtProject.Columns.Add("ProjectPhase2");
                dtProject.Columns.Add("ProjectPhase3");
                dtProject.Columns.Add("ProjectFullCode");
                dtProject.Columns.Add("ProjectNameEN");
                dtProject.Columns.Add("ProjectNameCN");
                dtProject.Columns.Add("IsFinancial");
                dtProject.Columns.Add("IsSale");
                dtProject.Columns.Add("IsPM");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string cityCode = dt.Rows[i]["CityCode"].ToString();
                    citySheet = wb.GetSheet(cityCode);

                    dr = dtProject.NewRow();
                    for (int j = 1; j <= citySheet.LastRowNum; j++)
                    {
                        if (ExcelValue(citySheet, j, 2).Length > 0)
                        {
                            dr = dtProject.NewRow();
                            dr["CityCode"] = cityCode;
                            dr["ProjectCode"] = ExcelValue(citySheet, j, 2);
                            dr["ProjectPhase1"] = ExcelValue(citySheet, j, 3);
                            dr["ProjectPhase2"] = ExcelValue(citySheet, j, 4);
                            dr["ProjectPhase3"] = ExcelValue(citySheet, j, 5);
                            dr["ProjectFullCode"] = ProjectFullCode(cityCode, ExcelValue(citySheet, j, 2), ExcelValue(citySheet, j, 3), ExcelValue(citySheet, j, 4), ExcelValue(citySheet, j, 5));
                            dr["ProjectNameEN"] = citySheet.GetRow(j).GetCell(7);
                            dr["ProjectNameCN"] = citySheet.GetRow(j).GetCell(7);
                            dr["IsFinancial"] = citySheet.GetRow(j).GetCell(10) != null ? "Y" : "N";
                            dr["IsSale"] = citySheet.GetRow(j).GetCell(11) != null ? "Y" : "N";
                            dr["IsPM"] = citySheet.GetRow(j).GetCell(12) != null ? "Y" : "N";
                            dtProject.Rows.Add(dr);
                        }
                    }
                }

                int totalProjectCount = dtProject.Rows.Count;
            }
        }

        private static string ExcelValue(ISheet sheet, int row, int cell)
        {
            return sheet.GetRow(row).GetCell(cell) == null ? "" : sheet.GetRow(row).GetCell(cell).StringCellValue;
        }
        private static string ProjectFullCode(string c, string p, string p1, string p2, string p3)
        {
            string result = "";
            result = c;
            if (p.Length > 0)
            {
                result += "-" + p;
                if(p1.Length >0)
                {
                    result += p1;
                    if (p2.Length > 0)
                    {
                        result += p2;
                        if (p3.Length > 0)
                            result += p3;
                    }
                }
            }
            return result;
        }
    }

    public class OAuthClientTest
    {
        private string _webApiServerUri = null;

        public OAuthClientTest(string webApiServerUri)
        {
            this._webApiServerUri = webApiServerUri;
        }

        public string GetAccessTokenByClientCredentials(string clientId, string clientSecret)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(this._webApiServerUri);
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Basic",
                Convert.ToBase64String(Encoding.ASCII.GetBytes(clientId + ":" + clientSecret)));

            var parameters = new Dictionary<string, string>();
            parameters.Add("grant_type", "client_credentials");

            return httpClient.PostAsync("/oauth/token",
                new FormUrlEncodedContent(parameters)).Result.Content.ReadAsStringAsync().Result;
        }

        public string GetCities(string accessToken)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(this._webApiServerUri);
            if (!string.IsNullOrEmpty(accessToken))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            }
            return httpClient.GetAsync("/api/cities").Result.Content.ReadAsStringAsync().Result;
        }

        public string GetProjects(string accessToken, DateTime startDate, DateTime endDate)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(this._webApiServerUri);


            if (!string.IsNullOrEmpty(accessToken))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            }
            string json = httpClient.GetAsync("/api/projects/GetProjects?startdate=" + startDate.ToString("yyyy-MM-dd") + "&endDate=" + endDate.ToString("yyyy-MM-dd")).Result.Content.ReadAsStringAsync().Result;

            return json;
        }

        public string GetPackage(string accessToken, int id)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(this._webApiServerUri);


            if (!string.IsNullOrEmpty(accessToken))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            }
            string json = httpClient.GetAsync("/api/package/GetPackage?tenderPackageID=" + id).Result.Content.ReadAsStringAsync().Result;

            return json;
        }
    }


}
