﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.API.Models
{
    public class PackageModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> ReleasedBudget { get; set; }
        public List<PackageItemModel> PackageItem { get; set; }
        //public string CreatedBy { get; set; }
        //public System.DateTime Created { get; set; }
        //public string LastUpdatedBy { get; set; }
        //public System.DateTime LastUpdated { get; set; }
    }
}