﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.API.Models
{
    public class GetProjectResultModel
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public IEnumerable<ProjectModel> Content { get; set; }
        
    }
}