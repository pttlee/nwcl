﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.API.Models
{
    public class ProjectModel
    {
        public int ID { get; set; }
        //public int RegionalOfficeID { get; set; }
        //public int CityID { get; set; }

        public string RegionName { get; set; }
        public string CityName { get; set; }
        //public string ProjectNameCode { get; set; }
        public string NameCN { get; set; }
        public string NameEN { get; set; }
        public string ProjectCode { get; set; }
        public string PhaseCode { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string PhaseCodeLv1 { get; set; }
        public string PhaseCodeLv2 { get; set; }
        public string PhaseCodeLv3 { get; set; }
        public string ApprovalStep { get; set; }
        public string CostPlanPath { get; set; }
        public bool IsCreatedCostPlan { get; set; }
        public bool IsCreatedBudget { get; set; }
        public int ProjectStatusID { get; set; }
        public string StatusName { get; set; }
        public bool Enable { get; set; }
        //public Nullable<int> ParentID { get; set; }
        public string ParentProject { get; set; }
        public string Chairman { get; set; }
        public string ProjectResp { get; set; }
        public string ProjectViceResp { get; set; }
        public string ProjectDesigner { get; set; }
        public string ContractResp { get; set; }
        public string AreaResp { get; set; }
        public string ProjectSec { get; set; }
        public Nullable<decimal> TotalArea { get; set; }
        public Nullable<decimal> BuildingTotalArea { get; set; }
        public Nullable<decimal> VolumeRate { get; set; }
        public Nullable<System.DateTime> LandHandover { get; set; }
        public Nullable<System.DateTime> LandUsageLicense { get; set; }
        public Nullable<System.DateTime> LandPlanningPermit { get; set; }
        public Nullable<System.DateTime> ConstructionPermit { get; set; }
        public Nullable<System.DateTime> CompletionDate { get; set; }
        public Nullable<System.DateTime> ProjectDate { get; set; }
        public Nullable<System.DateTime> LandUsagePermit { get; set; }
        public Nullable<System.DateTime> PresaleLicense { get; set; }
        public Nullable<System.DateTime> DeliverDate { get; set; }
        public Nullable<decimal> TotalGFA { get; set; }
        public Nullable<decimal> NonAccountableGFA { get; set; }
        public Nullable<decimal> TotalSaleableLeaseableArea { get; set; }
        public Nullable<decimal> TotalCFA { get; set; }
        public Nullable<decimal> CFAExcludeCarpark { get; set; }
        //public Nullable<int> RoleTemplateID { get; set; }
        public List<ProjectCostCenterModel> ProjectCostCenter { get; set; }
        public List<ProjectCompanyModel> ProjectCompany { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime Created { get; set; }
        public string LastUpdatedBy { get; set; }
        public System.DateTime LastUpdated { get; set; }

        
    }
}