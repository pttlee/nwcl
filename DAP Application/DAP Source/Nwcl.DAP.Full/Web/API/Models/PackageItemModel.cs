﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.API.Models
{
    public class PackageItemModel
    {
        public int ID { get; set; }
        public string ItemNo { get; set; }
        public string Location { get; set; }
        public string CostCodeElement { get; set; }
        public string CostCodeTrade { get; set; }
        public string CostCodeSubTrade { get; set; }
        public string Description { get; set; }
        public decimal Qty { get; set; }
        public string Unit { get; set; }
        public string ParentPackageItem { get; set; }
        public Nullable<decimal> EstimatedRate { get; set; }
        public Nullable<decimal> EstimatedSum { get; set; }
        //public string CreatedBy { get; set; }
        //public System.DateTime Created { get; set; }
        //public string LastUpdatedBy { get; set; }
       // public System.DateTime LastUpdated { get; set; }
    }
}