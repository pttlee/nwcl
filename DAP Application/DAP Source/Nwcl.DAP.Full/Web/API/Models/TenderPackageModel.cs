﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.API.Models
{
    public class TenderPackageModel
    {
        public int ID { get; set; }
        public string ProjectCode { get; set; }
        public string PhaseCode { get; set; }
        public string Type { get; set; }
        public string TenderPackageType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> ReleasedBudget { get; set; }
        public List<PackageModel> Package { get; set; }
        public List<TenderProgressModel> TenderProgress { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public string LastUpdatedBy { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
    }
}