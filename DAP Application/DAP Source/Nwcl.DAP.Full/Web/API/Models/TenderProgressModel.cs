﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.API.Models
{
    public class TenderProgressModel
    {
        public int ID { get; set; }
        public string TenderDate { get; set; }
        public System.DateTime TargetDate { get; set; }
        //public string CreatedBy { get; set; }
        //public System.DateTime Created { get; set; }
        //public string LastUpdatedBy { get; set; }
        //public System.DateTime LastUpdated { get; set; }
    }
}