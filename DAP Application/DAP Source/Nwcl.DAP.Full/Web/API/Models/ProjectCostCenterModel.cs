﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.API.Models
{
    public class ProjectCostCenterModel
    {
        public int ID { get; set; }
        //public Nullable<int> PropertyTypeID { get; set; }
        public string PropertyType { get; set; }
        public int ProjectID { get; set; }
        public string CostCenterCode { get; set; }
        public int OrderSeq { get; set; }
        public int CarPark { get; set; }
        //public Nullable<int> DevelopmentPurposeID { get; set; }
        public string DevelopmentPurpose { get; set; }
        public Nullable<bool> isSecCostAllocate { get; set; }
        public string CostCenterName { get; set; }
        public Nullable<decimal> CFAUp { get; set; }
        public Nullable<decimal> CFADown { get; set; }
        public Nullable<decimal> GFAUp { get; set; }
        public Nullable<decimal> GFADown { get; set; }
        public Nullable<decimal> RAUp { get; set; }
        public Nullable<decimal> RADown { get; set; }
        public Nullable<System.DateTime> EstimatedDate { get; set; }
        public Nullable<System.DateTime> ActualDate { get; set; }
        public Nullable<System.DateTime> HandoverOfLand { get; set; }
        public Nullable<System.DateTime> CreateProjectDate { get; set; }
        public Nullable<System.DateTime> StateOwnedLandUseCert { get; set; }
        public Nullable<System.DateTime> ConstructionLandPlanningPermit { get; set; }
        public Nullable<System.DateTime> ConstructionPermit { get; set; }
        public Nullable<System.DateTime> ConstructionProjectPlanningPermit { get; set; }
        public Nullable<System.DateTime> CommercialHouseSalesPermit { get; set; }
        public Nullable<System.DateTime> CompletionDate { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime Created { get; set; }
        public string LastUpdatedBy { get; set; }
        public System.DateTime LastUpdated { get; set; }
    }
}