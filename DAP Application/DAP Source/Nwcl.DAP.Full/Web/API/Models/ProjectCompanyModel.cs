﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.API.Models
{
    public class ProjectCompanyModel
    {
        public int ID { get; set; }
        //public int CompanyID { get; set; }
        public string Company { get; set; }
        public string Type { get; set; }
        public string Contact1 { get; set; }
        public string ContactTel1 { get; set; }
        public string Contact2 { get; set; }
        public string ContactTel2 { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime Created { get; set; }
        public string LastUpdatedBy { get; set; }
        public System.DateTime LastUpdated { get; set; }
        

    }
}