﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.API.Models
{
    public class GetTenderPackageResultModel
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public IEnumerable<TenderPackageModel> Content { get; set; }
        
    }
}