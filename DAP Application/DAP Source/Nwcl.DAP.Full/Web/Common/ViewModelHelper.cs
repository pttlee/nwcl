﻿using Nwcl.DAP.DAL.Model;
using Nwcl.DAP.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Web.Common
{
    /// <summary>
    /// Helper class for ViewModel and Entity Model conversion
    /// </summary>
    public class ViewModelHelper
    {
        /*
        /// <summary>
        /// Converts Regional Office Entity Model to Region Office View Model
        /// </summary>
        /// <param name="regionalOffice"></param>
        /// <returns></returns>
        public RegionalOfficeViewModel ConvertRegionalOfficeToRegionalOfficeViewModel(RegionalOffice regionalOffice)
        {
            RegionalOfficeViewModel regionalOfficeViewModel = new RegionalOfficeViewModel();
            regionalOfficeViewModel.ID = regionalOffice.ID;
            regionalOfficeViewModel.NameCN = regionalOffice.NameCN + " ( " + regionalOffice.Code + " ) ";
            regionalOfficeViewModel.NameEN = regionalOffice.NameEN + " ( " + regionalOffice.Code + " ) "; ;
            return regionalOfficeViewModel;
        }

        /// <summary>
        /// Converts Regional Office Entity Model List to Regional Office Veiw Model List
        /// </summary>
        /// <param name="regionalOfficeList"></param>
        /// <returns></returns>
        public List<RegionalOfficeViewModel> ConvertRegionalOfficeListToRegionalOfficeViewModelList(List<RegionalOffice> regionalOfficeList)
        {
            List<RegionalOfficeViewModel> regionalOfficeViewModelList = new List<RegionalOfficeViewModel>();
            if (regionalOfficeList != null)
            {
                foreach (RegionalOffice regionalOffice in regionalOfficeList)
                {
                    regionalOfficeViewModelList.Add(ConvertRegionalOfficeToRegionalOfficeViewModel(regionalOffice));
                }
            }
            return regionalOfficeViewModelList;
        }

        /// <summary>
        /// Converts EntityGroup Entity Model to EntityGroup View Model
        /// </summary>
        /// <param name="EntityGroup"></param>
        /// <returns></returns>
        public EntityGroupViewModel ConvertEntityGroupToEntityGroupViewModel(EntityGroup EntityGroup)
        {
            EntityGroupViewModel EntityGroupViewModel = new EntityGroupViewModel();
            EntityGroupViewModel.ID = EntityGroup.ID;
            EntityGroupViewModel.NameCN = EntityGroup.NameCN + " ( " + EntityGroup.Code + " ) ";
            EntityGroupViewModel.NameEN = EntityGroup.NameEN + " ( " + EntityGroup.Code + " ) ";
            EntityGroupViewModel.RegionalOfficeID = EntityGroup.RegionalOfficeID;
            return EntityGroupViewModel;
        }

        /// <summary>
        /// Converts EntityGroup Entity Model List to EntityGroup View Model List
        /// </summary>
        /// <param name="EntityGroupList"></param>
        /// <returns></returns>
        public List<EntityGroupViewModel> ConvertEntityGroupListToEntityGroupViewModelList(List<EntityGroup> EntityGroupList)
        {
            List<EntityGroupViewModel> EntityGroupViewModelList = new List<EntityGroupViewModel>();
            if (EntityGroupList != null)
            {
                foreach (EntityGroup EntityGroup in EntityGroupList)
                {
                    EntityGroupViewModelList.Add(ConvertEntityGroupToEntityGroupViewModel(EntityGroup));
                }
            }
            return EntityGroupViewModelList;
        }

        /// <summary>
        /// Converts Status Entity Model to Status View Model
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public StatusViewModel ConvertStatusToStatusViewModel(Status status)
        {
            StatusViewModel statusViewModel = new StatusViewModel();
            statusViewModel.ID = status.ID;
            statusViewModel.NameCN = status.NameCN;
            statusViewModel.NameEN = status.NameEN;
            return statusViewModel;
        }

        /// <summary>
        /// Converts Status Entity Model List to Status View Model List
        /// </summary>
        /// <param name="statusList"></param>
        /// <returns></returns>
        public List<StatusViewModel> ConvertStatusListToStatusViewModelList(List<Status> statusList)
        {
            List<StatusViewModel> statusViewModelList = new List<StatusViewModel>();
            if (statusList != null)
            {
                foreach (Status status in statusList)
                {
                    statusViewModelList.Add(ConvertStatusToStatusViewModel(status));
                }
            }
            return statusViewModelList;
        }

        /// <summary>
        /// Converts Company Entity Model to Company View Model
        /// </summary>
        /// <param name="EntityGroup"></param>
        /// <returns></returns>
        public CompanyViewModel ConvertCompanyToCompanyViewModel(Company company)
        {
            CompanyViewModel companyViewModel = new CompanyViewModel();
            companyViewModel.ID = company.ID;
            companyViewModel.NameCN = company.NameCN;
            companyViewModel.NameEN = company.NameEN;
            companyViewModel.RegionalOfficeID = company.RegionalOfficeID;
            companyViewModel.EntityGroupID = company.EntityGroupID;
            return companyViewModel;
        }






        /// <summary>
        /// Converts Company Entity Model to Company View Model
        /// </summary>
        /// <param name="Company"></param>
        /// <returns></returns>
        public List<CompanyViewModel> ConvertCompanyListToCompanyViewModelList(List<Company> companyList)
        {
            List<CompanyViewModel> companyViewModelList = new List<CompanyViewModel>();
            if (companyList != null)
            {
                foreach (Company company in companyList)
                {
                    companyViewModelList.Add(ConvertCompanyToCompanyViewModel(company));
                }
            }
            return companyViewModelList;
        }
        /// <summary>
        /// Converts Department Entity Model to Department View Model
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        public DepartmentViewModel ConvertDepartmentToDepartmentViewModel(Department department)
        {
            DepartmentViewModel departmentViewModel = new DepartmentViewModel();
            departmentViewModel.ID = department.ID;
            departmentViewModel.NameCN = department.NameCN;
            departmentViewModel.NameEN = department.NameEN;
            departmentViewModel.RegionalOfficeID = department.RegionalOfficeID;
            departmentViewModel.EntityGroupID = department.EntityGroupID;
            departmentViewModel.CompanyID = department.CompanyID;
            return departmentViewModel;
        }

        /// <summary>
        /// Converts Department Entity Model to Department View Model
        /// </summary>
        /// <param name="Department"></param>
        /// <returns></returns>
        public List<DepartmentViewModel> ConvertDepartmentListToDepartmentViewModelList(List<Department> departmentList)
        {
            List<DepartmentViewModel> departmentViewModelList = new List<DepartmentViewModel>();
            if (departmentList != null)
            {
                foreach (Department department in departmentList)
                {
                    departmentViewModelList.Add(ConvertDepartmentToDepartmentViewModel(department));
                }
            }
            return departmentViewModelList;
        }

        /// <summary>
        /// Converts Section Entity Model to Section View Model
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public SectionViewModel ConvertSectionToSectionViewModel(Section section)
        {
            SectionViewModel sectionViewModel = new SectionViewModel();
            sectionViewModel.ID = section.ID;
            sectionViewModel.NameCN = section.NameCN;
            sectionViewModel.NameEN = section.NameEN;
            sectionViewModel.RegionalOfficeID = section.RegionalOfficeID;
            sectionViewModel.EntityGroupID = section.EntityGroupID;
            sectionViewModel.CompanyID = section.CompanyID;
            sectionViewModel.DepartmentID = section.DepartmentID;
     ///       sectionViewModel.CreatedBy = section.CreatedBy;
     ///       sectionViewModel.Created = section.Created;
     ///       sectionViewModel.LastUpdatedBy = section.LastUpdatedBy;
     ///       sectionViewModel.LastUpdated = section.LastUpdated;
            return sectionViewModel;
        }

        /// <summary>
        /// Converts Section Entity Model to Section View Model
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public List<SectionViewModel> ConvertSectionListToSectionViewModelList(List<Section> sectionList)
        {
            List<SectionViewModel> sectionViewModelList = new List<SectionViewModel>();
            if (sectionList != null)
            {
                foreach (Section section in sectionList)
                {
                    sectionViewModelList.Add(ConvertSectionToSectionViewModel(section));
                }
            }
            return sectionViewModelList;
        }

        /// <summary>
        /// Converts Team Entity Model to Team View Model
        /// </summary>
        /// <param name="team"></param>
        /// <returns></returns>
        public TeamViewModel ConvertTeamToTeamViewModel(Team team)
        {
            TeamViewModel teamViewModel = new TeamViewModel();
            teamViewModel.ID = team.ID;
            teamViewModel.NameCN = team.NameCN;
            teamViewModel.NameEN = team.NameEN;
            teamViewModel.RegionalOfficeID = team.RegionalOfficeID;
            teamViewModel.EntityGroupID = team.EntityGroupID;
            teamViewModel.CompanyID = team.CompanyID;
            teamViewModel.DepartmentID = team.DepartmentID;
            teamViewModel.SectionID = team.SectionID;
            return teamViewModel;
        }

        /// <summary>
        /// Converts Team Entity Model list to Team View Model list
        /// </summary>
        /// <param name="team"></param>
        /// <returns></returns>
        public List<TeamViewModel> ConvertTeamListToTeamViewModelList(List<Team> teamList)
        {
            List<TeamViewModel> teamViewModelList = new List<TeamViewModel>();
            if (teamList != null)
            {
                foreach (Team team in teamList)
                {
                    teamViewModelList.Add(ConvertTeamToTeamViewModel(team));
                }
            }
            return teamViewModelList;
        }
        */

        /// <summary>
        /// Converts EntityGroup Entity Model to EntityGroup View Model
        /// </summary>
        /// <param name="EntityGroup"></param>
        /// <returns></returns>
        public EntityGroupViewModel ConvertEntityGroupToEntityGroupViewModel(PRV_ENT_ENTGROUP entityGroup)
        {
            EntityGroupViewModel entityGroupViewModel = new EntityGroupViewModel();
            entityGroupViewModel.Id = entityGroup.Id;
            entityGroupViewModel.EntityGroupName = entityGroup.EntityGroupName;            
            return entityGroupViewModel;
        }

        public UserGroupViewModel ConvertUserGroupToViewModel(PRV_URG_USRGROUP model)
        {
            UserGroupViewModel viewModel = new UserGroupViewModel();
            viewModel.Id = model.Id;
            viewModel.UserGroupName = model.UserGroupName;
            return viewModel;
        }

        public RoleViewModel ConvertRoleToViewModel(PRV_ROL_ROLE model)
        {
            RoleViewModel viewModel = new RoleViewModel();
            viewModel.Id = model.Id;
            viewModel.RoleName = model.RoleName;
            return viewModel;
        }

        /// <summary>
        /// Converts EntityGroup Entity Model List to EntityGroup View Model List
        /// </summary>
        /// <param name="EntityGroupList"></param>
        /// <returns></returns>
        public List<EntityGroupViewModel> ConvertEntityGroupListToEntityGroupViewModelList(List<PRV_ENT_ENTGROUP> entityGroupList)
        {
            List<EntityGroupViewModel> entityGroupViewModelList = new List<EntityGroupViewModel>();
            if (entityGroupList != null)
            {
                foreach (PRV_ENT_ENTGROUP entityGroup in entityGroupList)
                {
                    entityGroupViewModelList.Add(ConvertEntityGroupToEntityGroupViewModel(entityGroup));
                }
            }
            return entityGroupViewModelList;
        }

        public List<UserGroupViewModel> ConvertUserGroupListToViewModel(List<PRV_URG_USRGROUP> dalModel)
        {
            List<UserGroupViewModel> viewModelList = new List<UserGroupViewModel>();
            if (dalModel != null)
            {
                foreach (var item in dalModel)
                {
                    viewModelList.Add(ConvertUserGroupToViewModel(item));
                }
            }
            return viewModelList;
        }

        public List<RoleViewModel> ConvertRoleListToViewModel(List<PRV_ROL_ROLE> dalModel)
        {
            List<RoleViewModel> viewModelList = new List<RoleViewModel>();
            if (dalModel != null)
            {
                foreach (var item in dalModel)
                {
                    viewModelList.Add(ConvertRoleToViewModel(item));
                }
            }
            return viewModelList;
        }
    }
}
