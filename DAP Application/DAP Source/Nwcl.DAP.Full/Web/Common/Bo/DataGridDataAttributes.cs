﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Web.Common.Bo
{
    public enum DataType
    {
        Text,
        Integer,
        Decimal,
        Boolean,
        Date,
        DateTime
    }

    public class DataGridDataAttributes
    {
        public int RowNo { get; set; }
        public string ColumnName { get; set; }
        public ValueAttributes Value { get; set; }

        //public string Data { get; set; }
        //public DataType DataType { get; set; }
    }

    public class ValueAttributes
    {
        public object Data { get; set; }
        public DataType DataType { get; set; }
    }
}
