﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Web.Common.Bo
{
    public class GridAttributes
    {
        public string style { get; set; }
        public string Class { get; set; }
    }
}
