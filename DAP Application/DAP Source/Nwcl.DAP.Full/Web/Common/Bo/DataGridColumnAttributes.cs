﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Web.Common.Bo
{
    public class DataGridColumnAttributes
    {
        [DefaultValue(null)]
        public bool? hidden { get; set; }
        [DefaultValue("")]
        public string field { get; set; }
        [DefaultValue("")]
        public string title { get; set; }
        [DefaultValue("")]
        public string format { get; set; }
        [DefaultValue(null)]
        public GridTemplate template { get; set; }
        [DefaultValue("")]
        public string width { get; set; }
        [DefaultValue(null)]
        public GridHeaderAttributes headerAttributes { get; set; }
        [DefaultValue(null)]
        public GridHeaderTemplate headerTemplate { get; set; }
        public GridFooterTemplate footerTemplate { get; set; }
        public GridAttributes attributes { get; set; }
    }
}
