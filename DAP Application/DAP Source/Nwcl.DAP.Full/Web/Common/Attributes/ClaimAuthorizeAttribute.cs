﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nwcl.DAP.Web.Common.Attributes
{
    /// <summary>
    /// Custom attribute to check authorization based on claims
    /// </summary>
    public class ClaimsAuthorizeAttribute : AuthorizeAttribute
    {
        private string _claimType;
        private string _claimValue;

        public ClaimsAuthorizeAttribute(string claimType, string claimValue)
        {
            this._claimType = claimType;
            this._claimValue = claimValue;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true)
                || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true);

            if (skipAuthorization)
            {
                return;
            }

            var user = filterContext.HttpContext.User as ClaimsPrincipal;
            if (user.HasClaim(this._claimType, this._claimValue))
            {
                base.OnAuthorization(filterContext);
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}
