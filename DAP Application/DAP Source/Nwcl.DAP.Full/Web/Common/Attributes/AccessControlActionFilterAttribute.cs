﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nwcl.DAP.Web.Common.Attributes
{
    /// <summary>
    /// Action Filter Attribute to check whether the user is authorized to access the resource
    /// </summary>
    public class CheckAccessActionFilterAttribute : ActionFilterAttribute
    {
        private string _targetName = null;
        private string _actionName = null;
        private string _state = null;

        public CheckAccessActionFilterAttribute(string targetName, string actionName, string state = null)
        {
            this._targetName = targetName;
            this._actionName = actionName;
            this._state = state;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            // TODO: Check access control here
            // If functionName is not enough to check the access and action paramenters specified in runtime are required
            // then need to invoke BaseController.PassArgumentsToCheckAccessActionFilter method to pass the necessary arguments to the ViewBag
            // the arguments can be obtained from filterContext.Controller.ViewBag.ActionArguments dynamic property
            if(!filterContext.HttpContext.User.IsTargetActionAllowed(this._targetName, this._actionName, this._state))
            {
                // return to AccessDenied view if the checking is failed
                filterContext.Result = new RedirectResult("~/Error/AcessDenied");
            }
            else
            {
                base.OnActionExecuted(filterContext);
            }
        }
    }

    public static class IPrincipalExtensions
    {
        public static bool IsTargetActionAllowed(this IPrincipal principal, string targetName, string actionName, string state = null)
        {
            ClaimsPrincipal claimsPrincipal = principal as ClaimsPrincipal;
            // TODO: Add the actual implementation to check whether the principal is allowed to perform action on the target
            return true;
        }
    }
}
