﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Web.Common.Models
{
    public class EntityGroupUserGroupRoleViewModel
    {
        public int Id { get; set; }
        public int EntityGroupId { get; set; }
        public int UserGroupId { get; set; }
        public int RoleId { get; set; }
        public DateTime ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
    }
}
