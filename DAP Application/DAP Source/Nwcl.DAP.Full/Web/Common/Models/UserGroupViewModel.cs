﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Web.Common.Models
{
    public  class UserGroupViewModel
    {
        public int Id { get; set; }
        public string UserGroupName { get; set; }
        public DateTime ModifiedDt { get; set; }
        public string ModifiedBy { get; set; }
    }
}
