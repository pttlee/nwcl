﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Web.Common.Models
{
    public  class DepartmentUserViewModel
    {
        public string ADLogonName { get; set; }
        public string ADDisplayName { get; set; }
        public string ADDomain { get; set; }
        public string ADRegion { get; set; }
        public string ADCompany { get; set; }
        public string ADDepartment { get; set; }
        public string ADEmail { get; set; }
        public string ADTitle { get; set; }
        public string ADLogonNameDisplay { get; set; }
        public string Id { get; set; }
    }
}
