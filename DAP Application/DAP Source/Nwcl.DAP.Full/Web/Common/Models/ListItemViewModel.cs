﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Web.Common.Models
{
    public  class ListItemViewModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
