﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.Models
{
    public class BreadcrumbViewModels
    {
        public int Seq { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string BeforeTitleIcon { get; set; }
        public string AfterTitleIcon { get; set; }
    }
}