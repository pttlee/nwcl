﻿using System.Web;
using System.Web.Optimization;

namespace Nwcl.DAP.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryVal").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/jsJson").Include(
                "~/Scripts/json2.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                 "~/Scripts/kendo/jszip.min.js",
                 "~/Scripts/kendo/kendo.all.min.js",
                 "~/Scripts/kendo/kendo.aspnetmvc.min.js",
                 "~/Scripts/kendo.timezones.min.js",
                 "~/Scripts/kendo/console.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/misc").Include(
                "~/Scripts/numeral.js",
                 "~/Scripts/jquery.blockui.min.js",
                 "~/Scripts/scripts/app.min.js",
                 "~/Scripts/ui-blockui.min.js",
                 "~/Scripts/JS/include.js"
            ));

            bundles.Add(new StyleBundle("~/Content/kendo/css").Include(
                "~/Content/kendo/kendo.common-bootstrap.min.css",
                "~/Content/kendo/kendo.bootstrap.min.css"
            ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/CSS/Account/Login.css",
                "~/Content/CSS/bootstrap.min.css"
            ));
        }
    }
}

