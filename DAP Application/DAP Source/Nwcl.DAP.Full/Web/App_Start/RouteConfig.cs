﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Nwcl.DAP.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Breadcrumb",
                url: "Home/{action}/{pageid}/{title}/{path}",
                defaults: new { controller = "Home" },
                namespaces: new string[] { "Nwcl.DAP.Web.Controllers" }
            );
            routes.MapRoute(
                name: "SysMsg",
                url: "Home/{action}/{function}",
                defaults: new { controller = "Home" },
                namespaces: new string[] { "Nwcl.DAP.Web.Controllers" }
            );
            routes.MapRoute(
                "CostPlanDraftCreate",
                "CostPlan/CreateCostPlan/{projectid}/{templateId}",
                new { controller = "CostPlan", action = "CreateDraft" },
                new { projectid = @"\d+", templateId = @"\d+" },
                new string[] { "Nwcl.DAP.Web.Controllers" }
            );
            routes.MapRoute(
                "BudgetPlan",
                "BudgetPlan/{action}/{id}",
                //new { controller = "BudgetPlan", action = "CreateBudgetPlan" },
                defaults: new { controller = "BudgetPlan", action = "index", id = UrlParameter.Optional },
                namespaces: new string[] { "Nwcl.DAP.Web.Controllers" }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "account", action = "login", id = UrlParameter.Optional },
                namespaces: new string[] { "Nwcl.DAP.Web.Controllers" }
            );

        }
    }
}
