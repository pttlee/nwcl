﻿function OnBindCostCenter(e) {
    if (e.item) {
        var dataItem = this.dataItem(e.item);
        var Code = dataItem.Value
        var YaerID = $("#Year").val();

        BindCat(Code, YaerID);
    }
}
function OnBindYear(e) {
    if (e.item) {
        var dataItem = this.dataItem(e.item);
        var YaerID = dataItem.Value
        var Code = $("#CostCenter").val();

        BindCat(Code, YaerID);
    }
}
function BindCat(Code, YaerID) {

    var dl = $("#BudgetNature").data("kendoDropDownList");
  
    dl.setDataSource(
        {
            transport: {
                read: {
                    dataType: "Json",
                    url: "/Base/GetSearchNature",
                    data: function () {
                        return {
                            CostCenterCode: Code,
                            FinancialYearID: YaerID,
                            text: $("#BudgetNature").data("kendoDropDownList").filterInput.val()
                        };
                    }
                }
            },
            serverFiltering: true,
        }
        )

}

function filterSearchAccountCode() {

    return {
        YearID: $("#Year").val(),
        //text: $('#AccountCode').data('kendoDropDownList').input.value()
        text: $("#AccountCode").data("kendoDropDownList").filterInput.val()
    };
}

function filterSearchBudgetNature() {
    return {
        CostcenterCode: $("#CostCenter").val(),
        FinancialYearID: $("#Year").val(),
        CatID: $("#BudgetNature").val(),
        text: $("#CategoryCode").data("kendoDropDownList").filterInput.val()
    };
}

function filterReportHolder()
{
    return {
        CostcenterCode: $("#ReportCostCenter").val(),
        //FinancialYearID: $("Report#Year").val(),
        //CatID: $("#BudgetNature").val(),
        //text: $("#CategoryCode").data("kendoDropDownList").filterInput.val()
    };
}


