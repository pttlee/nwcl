﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nwcl.DAP.Application.Service;
using Nwcl.DAP.DAL.Model;

namespace Nwcl.DAP.Batch.SyncUser
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static CommonServiceProvider sp = new CommonServiceProvider();

        static void Main(string[] args)
        {
            try { 
                log.Info("--------------------Batch job starting--------------------");
                List<SYS_CFG_PARAM> pList = sp.GetParameter("ADDomainInfo", "ADDomain", "ADDomainList");
                string passPhase = sp.GetParameter("Cryptography", "Config", "PassPhase").First().Value;

                log.Info(string.Format("Getting domain list count: {0}", pList == null ? 0 : pList.Count));
                foreach(SYS_CFG_PARAM domain in pList)
                {
                    ProcessDomain(domain.Value, passPhase);
                }
                log.Info("--------------------Batch job Completed--------------------");
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
        }

        private static void ProcessDomain(string domainName, string passPhase)
        {
            try { 
                log.Info("Processing domain: " + domainName);
                string ldapQueryString = sp.GetParameter("ADDomainInfo", "LDAPQueryString", domainName).First().Value;
                log.Debug(String.Format("{0}: {1}", "LDAPQueryString", ldapQueryString));
                string domainBindUsername = sp.GetParameter("ADDomainInfo", "BindUsername", domainName).First().Value;
                log.Debug(String.Format("{0}: {1}", "BindUsername", domainBindUsername));
                string domainBindPasswordHash = sp.GetParameter("ADDomainInfo", "BindPassword", domainName).First().Value;
                log.Debug(String.Format("{0}: {1}", "BindPassword", domainBindPasswordHash));
                string domainBaseDn = "LDAP://" + sp.GetParameter("ADDomainInfo", "BaseDN", domainName).First().Value;
                log.Debug(String.Format("{0}: {1}", "BaseDN", domainBaseDn));

                string domainBindPassword = CryptographyServiceProvider.DecryptString(domainBindPasswordHash, passPhase);
            
                LDAPServiceProvider lsp = new LDAPServiceProvider();
                SearchResultCollection rc = lsp.SearchObjectsFromBaseDN(domainBindUsername
                    , domainBindPassword, domainBaseDn, ldapQueryString);

                List<MST_URG_USR> userList = lsp.ConvertResultToDALUsers(rc, domainName, "LdapBatch");

                sp.UpdateMasterUserByDomain(userList, domainName);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
        }
    }
}
