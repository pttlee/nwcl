﻿$(function () {
    $("#search").hide();

})


function openSearchGrid() {
    $("#search").show();
    $("#openSearch").hide();

}

function hideOpenSearchGrid() {
    $("#search").hide();
    $("#openSearch").show();
    $("#PrivilegeGrid").data("kendoGrid").dataSource.filter({});
}

function searchList() {
    var searchStr = $("#SearchText").val();
    if (searchStr != null && searchStr.length > 0) {
        var filters = $("#PrivilegeGrid").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
                {
                    field: "EntityGroupName",
                    operator: "contains",
                    value: searchStr
                },
                {
                    field: "UserGroupName",
                    operator: "contains",
                    value: searchStr
                },
                {
                    field: "RoleName",
                    operator: "contains",
                    value: searchStr
                },
                {
                    field: "ProjectName",
                    operator: "contains",
                    value: searchStr
                },
                {
                    field: "UserName",
                    operator: "contains",
                    value: searchStr
                },
                {
                    field: "DashboardName",
                    operator: "contains",
                    value: searchStr
                }
            ]
        });
        $("#PrivilegeGrid").data("kendoGrid").refresh();
        $("#PrivilegeGrid").data("kendoGrid").dataSource.filter(filters);
    }
}

function exportList() {
    var grid = $("#PrivilegeGrid").data("kendoGrid");
    grid.setOptions({
        excel: {
            allPages: true
        }
    });
    grid.saveAsExcel();
}
