﻿$(function () {
    $("#search").hide();

    $("#NewAdd").click(function () {
        var grid = $("#PrivilegeGrid").data("kendoGrid");
        if ($("#PrivilegeGrid").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        grid.addRow();
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );
        } else {
            grid.addRow();
            //SetFormDirty(true);
        }
    });

    $("#PrivilegeGrid").on("click", ".GridRowEdit", function () {
        var row = $(this).closest("tr");
        var control = $(this);

        if ($("#PrivilegeGrid").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#PrivilegeGrid").data("kendoGrid").editRow(row);

                        //control.closest("tr").find(".GridRowEdit").addClass('displayNone');
                        //control.closest("tr").find(".GridRowSave").removeClass('displayNone');
                        //control.closest("tr").find(".GridRowDelete").addClass('displayNone');
                        //control.closest("tr").find(".GridRowCancel").removeClass('displayNone');
                        //$("#PrivilegeGrid").find(".k-grid-edit-row .GridRowView").addClass('displayNone');
                        $("#PrivilegeGrid").find(".k-grid-edit-row .GridRowEdit").addClass('displayNone');
                        $("#PrivilegeGrid").find(".k-grid-edit-row .GridRowDelete").addClass('displayNone');
                        $("#PrivilegeGrid").find(".k-grid-edit-row .GridRowSave").removeClass('displayNone');
                        $("#PrivilegeGrid").find(".k-grid-edit-row .GridRowCancel").removeClass('displayNone');
                        //SetFormDirty(true);
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );

        } else {
            $("#PrivilegeGrid").data("kendoGrid").editRow(row);

            $(this).closest("tr").find(".GridRowEdit").addClass('displayNone');
            $(this).closest("tr").find(".GridRowSave").removeClass('displayNone');
            $(this).closest("tr").find(".GridRowDelete").addClass('displayNone');
            $(this).closest("tr").find(".GridRowCancel").removeClass('displayNone');
            //SetFormDirty(true);
        }
    });

    $("#PrivilegeGrid").on("click", ".GridRowSave", function () {
        var row = $(this).closest("tr");
        var prompt = "";

        prompt = verifyPrivilege($(this));

        if (prompt == "") {
            var result = $("#DuplicatePrivilege").val();
            if (result == 1) {
                swal({
                    title: "",
                    type: "warning",
                    text: getMsgTypeID("SYSTEM") + CommonResx.text_dapPrivilege + sysMsg.text_existCode,
                    confirmButtonText: CommonResx.text_confirm,
                });
            }
            else {
                BlockUI();
                $("#PrivilegeGrid").data("kendoGrid").saveRow(row);
                //SetFormDirty(false);
                UnBlockUI();
            }
        } else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });
    
    $("#PrivilegeGrid").on("click", ".GridRowDelete", function () {
        var row = $(this).closest("tr");

        swal({
            title: "",
            text: sysMsg.text_confirmDelete,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    $("#PrivilegeGrid").data("kendoGrid").removeRow(row);
                    //SetFormDirty(false);
                    UnBlockUI();
                }
                else { }
            }
        );
    });

    $("#PrivilegeGrid").on("click", ".GridRowCancel", function () {
        swal({
            title: "",
            text: sysMsg.text_confirmCancel,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        },

            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    var row = $(this).closest("tr");
                    $("#PrivilegeGrid").data("kendoGrid").cancelRow(row);
                    $(this).closest("tr").find(".GridRowEdit").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowSave").addClass('displayNone');
                    $(this).closest("tr").find(".GridRowDelete").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowCancel").addClass('displayNone');
                    //SetFormDirty(false);
                    UnBlockUI();
                }
                else { }
            }
        );
    });
})

function verifyPrivilege(e) {
    var msgs = [];
    var promptMessage = "";

    if (e.closest("tr").find("input[name='EntityGroupId']").val() == 0) {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CommonResx.text_dapEntityGroup);
    }

    if (e.closest("tr").find("input[name='UserGroupId']").val() == 0) {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CommonResx.text_dapUserGroup);
    }

    if (e.closest("tr").find("input[name='RoleId']").val() == 0) {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CommonResx.text_dapRole);
    }


    for (var j = 0; j < msgs.length; j++) {
        promptMessage += msgs[j] + '\n';
    }

    if (promptMessage == "") { checkDuplicatePrivilege(e); }

    return promptMessage;
}

function checkDuplicatePrivilege(e) {
    var roleId = e.closest("tr").find("input[name='RoleId']").val();
    var entityGroupId = e.closest("tr").find("input[name='EntityGroupId']").val();
    var userGroupId = e.closest("tr").find("input[name='UserGroupId']").val();
    var id = e.closest("tr").find("input:eq(0)").val();
    $.ajax({
        url: contextRoot +"/Main/Privilege/ValidatePrivilegeDuplicate",
        type: "post",
        data: { "id": id, "entityGroupId": entityGroupId, "userGroupId": userGroupId, "roleId": roleId },
        async: false,
        success: function (dat) {
            if (dat.IsSuccess) {
                if (dat.Message == "1") {
                    $("#DuplicatePrivilege").val("1");
                }
                else {
                    $("#DuplicatePrivilege").val("0");
                }
            }
        },
        error: function (err) {
            swal("error", err.Message, "error");
            UnBlockUI();
        }
    })

}

function onEdit(e) {
    
    if (e.model.isNew()) {
        //e.container.find(".GridRowEdit").hide();
        e.container.find(".GridRowEdit").addClass('displayNone');
        //e.container.find(".GridRowSave").show();
        e.container.find(".GridRowSave").removeClass('displayNone');
        //e.container.find(".GridRowDelete").hide();
        e.container.find(".GridRowDelete").addClass('displayNone');
        //e.container.find(".GridRowCancel").show();
        e.container.find(".GridRowCancel").removeClass('displayNone');
        
        e.container.find("input[name='RoleId']").removeAttr('data-val-required');
        e.container.find("input[name='UserGroupId']").removeAttr('data-val-required');
        e.container.find("input[name='EntityGroupId']").removeAttr('data-val-required');

    }
}



function openSearchGrid() {
    $("#search").show();
    $("#openSearch").hide();

}

function hideOpenSearchGrid() {
    $("#search").hide();
    $("#openSearch").show();
    $("#PrivilegeGrid").data("kendoGrid").dataSource.filter({});
}

function searchList() {
    var searchStr = $("#SearchText").val();
    if (searchStr != null && searchStr.length > 0) {
        var filters = $("#PrivilegeGrid").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
                {
                    field: "EntityGroupName",
                    operator: "contains",
                    value: searchStr
                },
                {
                    field: "UserGroupName",
                    operator: "contains",
                    value: searchStr
                },
                {
                    field: "RoleName",
                    operator: "contains",
                    value: searchStr
                }
            ]
        });
        $("#PrivilegeGrid").data("kendoGrid").refresh();
        $("#PrivilegeGrid").data("kendoGrid").dataSource.filter(filters);
    }
}

function exportList() {
    var grid = $("#PrivilegeGrid").data("kendoGrid");
    grid.setOptions({
        excel: {
            allPages: true
        }
    });
    grid.saveAsExcel();
}

function checkDirtyPage() {
    if ($("#PrivilegeGrid .k-grid-edit-row").length > 0) {
        return true;
    }
    else {
        return false;
    }
}