﻿$(function () {
    $("#search").hide();
    $("#NewAdd").click(function () {
        var grid = $("#CompanyGrid").data("kendoGrid");
        if ($("#CompanyGrid").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        grid.addRow();
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );
        } else {
            grid.addRow();
            //SetFormDirty(true);
        }
    });

    $("#CompanyGrid").on("click", ".GridRowEdit", function () {
        var row = $(this).closest("tr");
        var control = $(this);

        if ($("#CompanyGrid").find(".k-grid-edit-row").length > 0) {
            swal({
                title: "",
                text: sysMsg.text_confirmLeave,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#CompanyGrid").data("kendoGrid").editRow(row);
                        //control.closest("tr").find(".GridRowEdit").addClass('displayNone');
                        //control.closest("tr").find(".GridRowSave").removeClass('displayNone');
                        //control.closest("tr").find(".GridRowDelete").addClass('displayNone');
                        //control.closest("tr").find(".GridRowCancel").removeClass('displayNone');
                        //$("#CompanyGrid").find(".k-grid-edit-row .GridRowView").addClass('displayNone');
                        $("#CompanyGrid").find(".k-grid-edit-row .GridRowEdit").addClass('displayNone');
                        $("#CompanyGrid").find(".k-grid-edit-row .GridRowDelete").addClass('displayNone');
                        $("#CompanyGrid").find(".k-grid-edit-row .GridRowSave").removeClass('displayNone');
                        $("#CompanyGrid").find(".k-grid-edit-row .GridRowCancel").removeClass('displayNone');
                        //SetFormDirty(true);
                        UnBlockUI();
                    }
                    else { UnBlockUI(); }
                }
            );

        } else {
            $("#CompanyGrid").data("kendoGrid").editRow(row);
            $(this).closest("tr").find(".GridRowEdit").addClass('displayNone');
            $(this).closest("tr").find(".GridRowSave").removeClass('displayNone');
            $(this).closest("tr").find(".GridRowDelete").addClass('displayNone');
            $(this).closest("tr").find(".GridRowCancel").removeClass('displayNone');
            //SetFormDirty(true);
        }
    });

    $("#CompanyGrid").on("click", ".GridRowSave", function () {
        var row = $(this).closest("tr");
        var prompt = "";

        prompt = verifyCompany($(this));

        if (prompt == "") {
            checkName($(this));
            var result = $("#DuplicateName").val();
            if (result == 1) {
                swal({
                    title: "",
                    type: "warning",
                    text: getMsgTypeID("SYSTEM") + sysMsg.text_existName,
                    confirmButtonText: CommonResx.text_confirm,
                });
            }
            else {
                BlockUI();
                $("#CompanyGrid").data("kendoGrid").saveRow(row);
                //SetFormDirty(false);
                UnBlockUI();
            }
        } else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });

    $("#CompanyGrid").on("click", ".GridRowDelete", function () {
        var row = $(this).closest("tr");
        var CompanyId = $(this).closest("tr").find("input:eq(0)").val();
        var prompt = "";
        $.ajax({
            url: contextRoot + "/Main/Company/ValidateCompanyDelete",
            type: "post",
            data: { "Id": CompanyId },
            async: false,
            success: function (dat) {
                if (dat.IsSuccess) {
                    if (dat.Message == "1") {
                        prompt += getMsgTypeID("SYSTEM") + sysMsg.text_companyInUse;
                    }
                }
            },
            error: function (err) {
                swal("error", err.Message, "error");
                UnBlockUI();
            }
        })
        if (prompt == "") {

            swal({
                title: "",
                text: sysMsg.text_confirmDelete,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: CommonResx.text_confirm,
                cancelButtonText: CommonResx.text_cancel,
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    swal.close();
                    if (isConfirm) {
                        BlockUI();
                        $("#CompanyGrid").data("kendoGrid").removeRow(row);
                        //SetFormDirty(false);
                        UnBlockUI();
                    }
                    else { }
                }
            );
        }
        else {
            swal({
                title: "",
                type: "warning",
                text: prompt,
                confirmButtonText: CommonResx.text_confirm,
            })
        }
    });

    $("#CompanyGrid").on("click", ".GridRowCancel", function () {
        swal({
            title: "",
            text: sysMsg.text_confirmCancel,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: CommonResx.text_confirm,
            cancelButtonText: CommonResx.text_cancel,
            closeOnConfirm: false,
            closeOnCancel: false
        },

            function (isConfirm) {
                swal.close();
                if (isConfirm) {
                    BlockUI();
                    var row = $(this).closest("tr");
                    $("#CompanyGrid").data("kendoGrid").cancelRow(row);
                    $(this).closest("tr").find(".GridRowEdit").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowSave").addClass('displayNone');
                    $(this).closest("tr").find(".GridRowDelete").removeClass('displayNone');
                    $(this).closest("tr").find(".GridRowCancel").addClass('displayNone');
                    //SetFormDirty(false);
                    UnBlockUI();
                }
                else { }
            }
        );
    });

})

function onEdit(e) {
    
    if (e.model.isNew()) {
        //e.container.find(".GridRowEdit").hide();
        e.container.find(".GridRowEdit").addClass('displayNone');
        //e.container.find(".GridRowSave").show();
        e.container.find(".GridRowSave").removeClass('displayNone');
        //e.container.find(".GridRowDelete").hide();
        e.container.find(".GridRowDelete").addClass('displayNone');
        //e.container.find(".GridRowCancel").show();
        e.container.find(".GridRowCancel").removeClass('displayNone');
        
        e.container.find("input[name='CompanyName']").removeAttr('data-val-required');

    }
}

function verifyCompany(e) {
    //var countPromptMessages = 0;
    //var countRequiredValidate = 0;
    var msgs = [];
    var promptMessage = "";
    var isCodeAlreadyInput = false;
    

    if (e.closest("tr").find("input[name='CompanyName']").val() == "") {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CompanyResx.lblCompanyName_Text);
        //countPromptMessages++;
        //countRequiredValidate++;
    }

    if (e.closest("tr").find("input[name='CityCode']").val() == "") {
        msgs.push(getMsgTypeID("REQUIRED") + sysMsg.text_pleaseEnter + CompanyResx.lblCityName_Text);
        //countPromptMessages++;
        //countRequiredValidate++;
    }

    

    for (var j = 0; j < msgs.length; j++) {
        promptMessage += msgs[j] + '\n';
    }

    return promptMessage;
}


function openSearchGrid() {
    $("#search").show();
    $("#openSearch").hide();

}

function hideOpenSearchGrid() {
    $("#search").hide();
    $("#openSearch").show();
    $("#CompanyGrid").data("kendoGrid").dataSource.filter({});
}

function searchList() {
    var searchStr = $("#SearchText").val();
    if (searchStr != null && searchStr.length > 0) {
        var filters = $("#CompanyGrid").data("kendoGrid").dataSource.filter({
            logic: "or",
            filters: [
                {
                    field: "CompanyName",
                    operator: "contains",
                    value: searchStr
                },
                {
                    field: "CityName",
                    operator: "contains",
                    value: searchStr
                }
            ]
        });
        $("#CompanyGrid").data("kendoGrid").refresh();
        $("#CompanyGrid").data("kendoGrid").dataSource.filter(filters);
    }
}

function exportList() {
    var grid = $("#CompanyGrid").data("kendoGrid");
    grid.setOptions({
        excel: {
            allPages: true
        }
    });
    grid.saveAsExcel();
}

function EditCompanyDetail(CompanyId) {
    RedirectScreen(contextRoot + "/Main/Company/EditCompany?CompanyID=" + CompanyId);
}

function checkName(e) {
    var name = e.closest("tr").find("input[name='CompanyName']").val();
    var id = e.closest("tr").find("input:eq(0)").val();
    $.ajax({
        url: contextRoot + "/Main/Company/ValidateCompanyDuplicateName",
        type: "post",
        data: { "Name": name, "Id": id },
        async: false,
        success: function (dat) {
            if (dat.IsSuccess) {
                if (dat.Message == "1") {
                    $("#DuplicateName").val("1");
                }
                else {
                    $("#DuplicateName").val("0");
                }
            }
        },
        error: function (err) {
            swal("error", err.Message, "error");
            UnBlockUI();
        }
    })

}

function checkDirtyPage() {
    if ($("#CompanyGrid .k-grid-edit-row").length > 0) {
        return true;
    }
    else {
        return false;
    }
}
