﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Nwcl.DAP.Application.Service;
using Nwcl.DAP.DAL.Model;
using Nwcl.DAP.Web.Areas.Main.Models;
using Nwcl.DAP.Web.Common;
using Nwcl.DAP.Web.Common.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nwcl.DAP.Web.Common.Attributes;
using Nwcl.DAP.Common;

namespace Nwcl.DAP.Web.Areas.Main.Controllers
{
    public class RoleController : BaseController
    {
        private CommonServiceProvider _commonServiceProvider = null;
        private CommonServiceProvider ServiceProvider
        {
            get
            {
                if (this._commonServiceProvider == null)
                {
                    this._commonServiceProvider = (CommonServiceProvider)ServiceProviderFactory.GetServiceProvider(typeof(CommonServiceProvider));
                }
                return this._commonServiceProvider;
            }
        }
        // GET: Role
        public ActionResult Index()
        {
            ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.RoleMaintenance);
            ViewData["SystemMessageCode"] = "Role";
            GetDashboardList();
            return View();
        }

        // GET: Role
        public ActionResult EditRole(int roleId)
        {

            ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.EditRole);

            PRV_ROL_ROLE role = this.ServiceProvider.GetRoleByRoleId(roleId, false);

            RoleExtendedViewModel roleViewModel = new RoleExtendedViewModel()
            {
                RoleName = role.RoleName,
                Id = role.Id
            };

            GetDashboardList();
            GetUserGroup();
            GetEntityGroup();
            //Session["roledashboard_roleid"] = roleId;
            return View(roleViewModel);
        }

        [CheckAccessActionFilter("Role", "Viewer")]
        public ActionResult GetRoleList([DataSourceRequest] DataSourceRequest request, RoleExtendedViewModel model)
        {
            List<RoleExtendedViewModel> roleViewModel = new List<RoleExtendedViewModel>();
            List<PRV_ROL_ROLE> roleDashboardList = this.ServiceProvider.GetRoleList();


            roleViewModel = (from c in roleDashboardList.ToList()
                                      orderby c.Id
                                      select new RoleExtendedViewModel
                                      {
                                          Id = c.Id,
                                          RoleName = c.RoleName,
                                          DashboardName = DAP.Common.CultureHelper.GetCurrentCultureFieldLanguage().ToUpper().Equals(Constants.FIELD_LANGUAGE_EN) ?
                                                        string.Join(",", c.PRV_ROL_ROLE_DASHBOARD_RELN
                                                        .Select(db => db.SYS_CFG_DASHBOARD)
                                                        .Select(name => name.DashboardNameEN == null
                                                              ? name.DashboardNameCN
                                                              : name.DashboardNameEN
                                                        ).ToList()) :
                                                        string.Join(",", c.PRV_ROL_ROLE_DASHBOARD_RELN
                                                        .Select(db => db.SYS_CFG_DASHBOARD)
                                                        .Select(name => name.DashboardNameCN).ToList()),
                                          ModifiedBy = c.ModifiedBy,
                                          ModifiedDt = c.ModifiedDt != null ? (DateTime)c.ModifiedDt : new DateTime()
                                      }).ToList();

            return Json(roleViewModel.ToDataSourceResult(request));
        }

        [CheckAccessActionFilter("Role", "Viewer")]
        public JsonResult GetDashboardList()
        {
            List<SYS_CFG_DASHBOARD> dashboardList = this.ServiceProvider.GetDashboardList(false);

            ViewData["dashboards"] = dashboardList;
            return Json(dashboardList, JsonRequestBehavior.AllowGet);
        }

        [CheckAccessActionFilter("Role", "Viewer")]
        public JsonResult GetRoleDashboardListByRoleId([DataSourceRequest] DataSourceRequest request, int roleId)
        {
            List<PRV_ROL_ROLE_DASHBOARD_RELN> roleDashboardList = this.ServiceProvider.GetRoleDashboardListByRoleId(roleId, false);
            List<RoleDashboardExtendedViewModel> roleDashboardViewModel = new List<RoleDashboardExtendedViewModel>();

            roleDashboardViewModel = (from c in roleDashboardList.ToList()
                             orderby c.Id
                             select new RoleDashboardExtendedViewModel
                             {
                                 Id = c.Id,
                                 RoleName = c.PRV_ROL_ROLE.RoleName,
                                 RoleId = c.RoleId.Value,
                                 DashboardId = c.DashboardId.Value,
                                 DashboardName = DAP.Common.CultureHelper.GetCurrentCultureFieldLanguage().ToUpper().Equals(Constants.FIELD_LANGUAGE_EN) ?
                                               c.SYS_CFG_DASHBOARD.DashboardNameEN :
                                               c.SYS_CFG_DASHBOARD.DashboardNameCN,
                                 ModifiedBy = c.ModifiedBy,
                                 ModifiedDt = c.ModifiedDt != null ? (DateTime)c.ModifiedDt : new DateTime()
                             }).ToList();

            return Json(roleDashboardViewModel.ToDataSourceResult(request));
        }

        [CheckAccessActionFilter("Role", "Viewer")]
        public JsonResult GetUserGroup()
        {
            List<PRV_URG_USRGROUP> list = ServiceProvider.GetUserGroupList();
            List<PRV_URG_USRGROUP> resultList = list.Select(c => new PRV_URG_USRGROUP
            {
                Id = c.Id,
                UserGroupName = c.UserGroupName
            }).ToList();
            ViewData["usergroups"] = resultList;
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }

        [CheckAccessActionFilter("Role", "Viewer")]
        public JsonResult GetEntityGroup()
        {
            List<PRV_ENT_ENTGROUP> list = ServiceProvider.GetEntityGroupList();
            List<PRV_ENT_ENTGROUP> resultList = list.Select(c => new PRV_ENT_ENTGROUP
            {
                Id = c.Id,
                EntityGroupName = c.EntityGroupName
            }).ToList();
            ViewData["entitygroups"] = resultList;
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }

        [CheckAccessActionFilter("Role", "Viewer")]
        public ActionResult GetEntityGroupUserGroupRoleListByRoleId([DataSourceRequest] DataSourceRequest request, int roleId)
        {
            List<RoleEntityGroupUserGroupExtendedViewModel> vmList = new List<RoleEntityGroupUserGroupExtendedViewModel>();
            List<PRV_ENT_URG_ROL_RELN> mList = this.ServiceProvider.GetEntityGroupUserGroupRoleListByRoleId(roleId, false);
            //string lang = Nwcl.DAP.Common.CultureHelper.GetCurrentCultureFieldLanguage();
            vmList = (
                        from vm in mList.ToList()
                        orderby vm.Id descending
                        select new RoleEntityGroupUserGroupExtendedViewModel
                        {
                            Id = vm.Id,
                            UserGroupId = vm.UserGroupId.Value,
                            UserGroupName = vm.PRV_URG_USRGROUP.UserGroupName,
                            RoleId = roleId,
                            RoleName = vm.PRV_ROL_ROLE.RoleName,
                            EntityGroupId = vm.EntityGroupId.Value,
                            EntityGroupName = vm.PRV_ENT_ENTGROUP.EntityGroupName,
                            CurrentUserGroupId = vm.PRV_URG_USRGROUP.Id,
                            CurrentEntityGroupId = vm.PRV_ENT_ENTGROUP.Id,
                            CurrentUserGroupName = vm.PRV_URG_USRGROUP.UserGroupName,
                            CurrentEntityGroupName = vm.PRV_ENT_ENTGROUP.EntityGroupName
                        }
                     ).ToList();
            return Json(vmList.ToDataSourceResult(request));
        }

        [HttpPost]
        public JsonResult AddRoleName([DataSourceRequest] DataSourceRequest request, RoleExtendedViewModel model)
        {
            PRV_ROL_ROLE Role = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                Role = ViewModelHelper.ConvertRoleViewModelToRole(model);
                //Role.RoleId = Convert.ToInt32(Session["roledashboard_roleid"]);
                Role = this.ServiceProvider.AddRole(Role);
                model.Id = Role.Id;
            });
            //return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            return Json(new { Data = model });
        }

        [HttpPost]
        public JsonResult EditRoleName(RoleExtendedViewModel model)
        {
            PRV_ROL_ROLE Role = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.RoleName = model.RoleName.Trim();
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                Role = ViewModelHelper.ConvertRoleViewModelToRole(model);
                //Role.RoleId = Convert.ToInt32(Session["roledashboard_roleid"]);
                this.ServiceProvider.EditRole(Role);
            });
            //return Json(executionResult, JsonRequestBehavior.AllowGet);
            return Json(new { Data = model });
        }

        [HttpPost]
        public JsonResult DeleteRole(RoleExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                this.ServiceProvider.RemoveRole(ViewModelHelper.ConvertRoleViewModelToRole(model));
            });
            return Json(executionResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddRoleDashboard([DataSourceRequest] DataSourceRequest request , RoleDashboardExtendedViewModel model)
        {
            PRV_ROL_ROLE_DASHBOARD_RELN Role = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                Role = ViewModelHelper.ConvertRoleDashboardViewModelToRoleDashboard(model);
                //Role.RoleId = Convert.ToInt32(Session["roledashboard_roleid"]);
                Role = this.ServiceProvider.AddRoleDashboard(Role);
                model.Id = Role.Id;
            });
            //return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            return Json(new { Data = model });
        }

        [HttpPost]
        public JsonResult EditRoleDashboard(RoleDashboardExtendedViewModel model)
        {
            PRV_ROL_ROLE_DASHBOARD_RELN Role = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.RoleName = model.RoleName.Trim();
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                Role = ViewModelHelper.ConvertRoleDashboardViewModelToRoleDashboard(model);
                //Role.RoleId = Convert.ToInt32(Session["roledashboard_roleid"]);
                this.ServiceProvider.EditRoleDashboard(Role);
            });
            //return Json(executionResult, JsonRequestBehavior.AllowGet);
            return Json(new { Data = model });
        }

        public JsonResult DeleteRoleDashboard(RoleDashboardExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                this.ServiceProvider.RemoveRoleDashboard(ViewModelHelper.ConvertRoleDashboardViewModelToRoleDashboard(model));
            });
            return Json(executionResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddEntityGroupUserGroupRole([DataSourceRequest] DataSourceRequest request, RoleEntityGroupUserGroupExtendedViewModel model)
        {
            PRV_ENT_URG_ROL_RELN prv = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                prv = ViewModelHelper.ConvertRoleEntityGroupUserGroupViewModelToDALModel(model);
                //Role.RoleId = Convert.ToInt32(Session["roledashboard_roleid"]);
                prv = this.ServiceProvider.AddEntityGroupUserGroupRole(prv);
                model.Id = prv.Id;
            });
            //return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            return Json(new { Data = model });
        }

        [HttpPost]
        public JsonResult EditEntityGroupUserGroupRole(RoleEntityGroupUserGroupExtendedViewModel model)
        {
            PRV_ENT_URG_ROL_RELN prv = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.RoleName = model.RoleName.Trim();
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                prv = ViewModelHelper.ConvertRoleEntityGroupUserGroupViewModelToDALModel(model);
                //Role.RoleId = Convert.ToInt32(Session["roledashboard_roleid"]);
                this.ServiceProvider.EditEntityGroupUserGroupRole(prv);
            });
            //return Json(executionResult, JsonRequestBehavior.AllowGet);
            return Json(new { Data = model });
        }

        public JsonResult DeleteEntityGroupUserGroupRole(RoleEntityGroupUserGroupExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                this.ServiceProvider.RemoveEntityGroupUserGroupRole(
                    ViewModelHelper.ConvertRoleEntityGroupUserGroupViewModelToDALModel(model));
            });
            return Json(executionResult, JsonRequestBehavior.AllowGet);
        }

        private const int existPrivilege = 1;
        private const int notExistPrivilege = 0;
        [HttpPost]
        [CheckAccessActionFilter("Role", "Viewer")]
        public JsonResult ValidateEntityGroupUserGroupRoleDuplicate(int id, int entityGroupId, int userGroupId, int roleId)
        {
            int result = existPrivilege;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.ValidatePrivilegeDuplicate(id, entityGroupId, userGroupId, roleId);
            });
            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckAccessActionFilter("Role", "Viewer")]
        public JsonResult ValidateRoleDashboardDuplicate(int dashboardId, int roleId)
        {
            int result = existPrivilege;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.ValidateRoleDashboardDuplicate(dashboardId, roleId);
            });
            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckAccessActionFilter("Role", "Viewer")]
        public JsonResult ValidateRoleNameDuplicate(int roleId, string roleName)
        {
            int result = existPrivilege;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.ValidateRoleNameDuplicate(roleId, roleName);
            });
            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckAccessActionFilter("Role", "Viewer")]
        public JsonResult ValidateRoleEmpty(int roleId)
        {
            int result = existPrivilege;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.ValidateRoleEmpty(roleId);
            });
            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);
        }

    }
}