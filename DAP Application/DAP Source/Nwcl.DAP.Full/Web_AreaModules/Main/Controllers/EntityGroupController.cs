﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Nwcl.DAP.Application.Service;
using Nwcl.DAP.DAL.Model;
using Nwcl.DAP.Web.Areas.Main.Models;
using Nwcl.DAP.Web.Common;
using Nwcl.DAP.Web.Common.Controllers;
using Nwcl.DAP.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Nwcl.DAP.Web.Common.Attributes;
using Nwcl.DAP.Web.Areas.Main.Resources.EntityGroup;

namespace Nwcl.DAP.Web.Areas.Main.Controllers
{
    public class EntityGroupController : BaseController
    {
        // GET: EntityGroup
        [CheckAccessActionFilter("EntityGroup", "Viewer")]
        public ActionResult Index()
        {
            ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.EntityGroupMaintenance);
            ViewData["SystemMessageCode"] = "Entity Group";            
            return View();
        }

        //[CheckAccessActionFilter("EditEntityGroup", "Viewer")]
        //public ActionResult EditEntityGroup(int entityGroupID)
        //{
        //    ViewData["id"] = entityGroupID;
        //    Session["id"] = entityGroupID;
        //    //List<sp_GetEntityGroupTreeView_Result> entityGroupTreeList = this.ServiceProvider.GetEntityGroupTreeView().ToList();
        //    //entityGroupTreeList.Insert(0, GetTreeViewParentNodeMstr());
        //    //Session["entityGroupTreeList"] = entityGroupTreeList;
        //    ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.EditEntityGroup);
        //    ViewData["SystemMessageCode"] = "Edit Entity Group";
        //    ViewData["Name"] = ServiceProvider.GetEntityGroupById(entityGroupID).EntityGroupName;
        //    GetUserGroup("");
        //    GetRole("");

        //    List<PRV_ENT_ENTGROUP_PROJ_RELN> entityGroupProjectList = this.ServiceProvider.GetEntityGroupProject().Where(e => e.EntityGroupId == entityGroupID).ToList();
        //    string checkedIds = string.Empty;
        //    foreach (var item in entityGroupProjectList)
        //    {
        //        checkedIds += item.ProjectFullCode + ",";
        //    }

        //    if (checkedIds.Length > 1 && checkedIds.Contains(","))
        //    {
        //        checkedIds = checkedIds.Remove(checkedIds.Length - 1, 1);
        //    }

        //    ViewData["checkedIds"] = checkedIds;
        //    return View();
        //}

        [CheckAccessActionFilter("EditEntityGroup", "Viewer")]
        public ActionResult EditEntityGroup(int entityGroupID)
        {            
            ViewData["id"] = entityGroupID;
            Session["id"] = entityGroupID;
            //List<sp_GetEntityGroupTreeView_Result> entityGroupTreeList = this.ServiceProvider.GetEntityGroupTreeView().ToList();
            //entityGroupTreeList.Insert(0, GetTreeViewParentNodeMstr());
            //Session["entityGroupTreeList"] = entityGroupTreeList;
            ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.EditEntityGroup);
            ViewData["SystemMessageCode"] = "Edit Entity Group";
            GetUserGroup("");
            GetRole("");

            PRV_ENT_ENTGROUP ent = this.ServiceProvider.GetEntityGroupByEntityGroupId(entityGroupID, false);
            EntityGroupEditExtendedViewModel vm = new EntityGroupEditExtendedViewModel()
            {
                Id = ent.Id,
                EntityGroupName = ent.EntityGroupName,
                entityTreeModel = PrepareEntityTree(ent)
            };

            return View(vm);
        }

        public List<EntityUserGroupTreeExtendedViewModel> PrepareEntityTree(PRV_ENT_ENTGROUP ent)
        {
            List<sp_GetEntityGroupTreeView_Result> entityGroupTreeList = null;

            entityGroupTreeList = this.ServiceProvider.GetEntityGroupTreeView().ToList();

            Dictionary<string, string> dicEntityGroupTreeList = new Dictionary<string, string>();

            if (ent.PRV_ENT_ENTGROUP_PROJ_RELN != null && ent.PRV_ENT_ENTGROUP_PROJ_RELN.Count() > 0)
            {
                foreach (var item in ent.PRV_ENT_ENTGROUP_PROJ_RELN)
                {
                    dicEntityGroupTreeList.Add(item.ProjectFullCode, item.ProjectFullCode);
                }
            }

            List<sp_GetEntityGroupTreeView_Result> regions = entityGroupTreeList
                .Where(o => o.ParentCode.IsCaseInsensitiveEqual("Root")).ToList();
            entityGroupTreeList.RemoveAll(o => o.ParentCode.IsCaseInsensitiveEqual("Root"));

            List<EntityUserGroupTreeExtendedViewModel> tree = new List<EntityUserGroupTreeExtendedViewModel>();

            if (regions != null && regions.Count() > 0)
            {
                foreach (sp_GetEntityGroupTreeView_Result r in regions)
                {
                    bool childChecked = false;
                    EntityUserGroupTreeExtendedViewModel vm = new EntityUserGroupTreeExtendedViewModel()
                    {
                        NodeCode = r.Code,
                        NodeName = CultureHelper.GetCurrentCultureFieldLanguage().IsCaseInsensitiveEqual(
                                  DAP.Common.Constants.FIELD_LANGUAGE_CN)
                                  ? r.NameCN : r.NameEN == null ? r.NameCN : r.NameEN,
                        level = 1,
                        isChecked = false
                    };
                    childChecked = PrepareEntitySubTree(vm, entityGroupTreeList, dicEntityGroupTreeList);
                    if (childChecked)
                    {
                        vm.isExpanded = true;
                    }
                    tree.Add(vm);
                }

                if (entityGroupTreeList.Count() > 0)
                {
                    EntityUserGroupTreeExtendedViewModel errNode = new EntityUserGroupTreeExtendedViewModel()
                    {
                        NodeCode = "ERROR",
                        NodeName = Nwcl.DAP.Web.Areas.Main.Resources.UploadExcel.IndexResx.lblException,
                        child = new List<EntityUserGroupTreeExtendedViewModel>()
                    };
                    errNode.child.AddRange((from p in entityGroupTreeList
                                            select ViewModelHelper.ConvertProjectToEntityTreeModel(p)).ToList());
                    tree.Add(errNode);
                }
            }

            return tree;
        }

        public bool PrepareEntitySubTree(EntityUserGroupTreeExtendedViewModel parentNode
            , List<sp_GetEntityGroupTreeView_Result> entityGroupTreeList, Dictionary<string, string> selectList)
        {
            bool isReturnExpanded = false;
            if (entityGroupTreeList == null || entityGroupTreeList.Count() == 0)
                return isReturnExpanded;
            List<sp_GetEntityGroupTreeView_Result> nodes = entityGroupTreeList
                .Where(o => o.ParentCode.IsCaseInsensitiveEqual(parentNode.NodeCode)).ToList();
            entityGroupTreeList.RemoveAll(o => o.ParentCode.IsCaseInsensitiveEqual(parentNode.NodeCode));
            if (nodes != null && nodes.Count() > 0)
            {
                foreach (sp_GetEntityGroupTreeView_Result r in nodes)
                {
                    bool childChecked = false;
                    bool selfChecked = selectList.ContainsKey(r.Code);
                    EntityUserGroupTreeExtendedViewModel vm = new EntityUserGroupTreeExtendedViewModel()
                    {
                        NodeCode = r.Code,
                        NodeName = string.Format("{0} {1}", r.Code
                                    , CultureHelper.GetCurrentCultureFieldLanguage().IsCaseInsensitiveEqual(
                                    DAP.Common.Constants.FIELD_LANGUAGE_CN)
                                    ? r.NameCN : r.NameEN == null ? r.NameCN : r.NameEN),
                        isChecked = selfChecked,
                        isExpanded = selfChecked
                    };
                    childChecked = PrepareEntitySubTree(vm, entityGroupTreeList, selectList);
                    if (childChecked || selfChecked)
                    {
                        isReturnExpanded = true;
                        vm.isExpanded = true;
                    }
                    if (parentNode.child == null)
                        parentNode.child = new List<EntityUserGroupTreeExtendedViewModel>();
                    parentNode.child.Add(vm);

                }
            }
            return isReturnExpanded;
        }

        private List<TreeViewItemModel> GenModelListForTreeView(List<sp_GetEntityGroupTreeView_Result> list, string code, Dictionary<string, string> dicEntityGroupTreeList)
        {
            
            var returnList = new List<TreeViewItemModel>();
            foreach (var item in list.Where(o =>  o.ParentCode == code))
            {
                var newItem = new TreeViewItemModel();
                newItem.Id = item.Code;
                if (IsChinese)
                    newItem.Text = item.NameCN;
                else
                    newItem.Text = item.NameEN;
                newItem.Expanded = true;

                newItem.Checked = true;//dicEntityGroupTreeList.ContainsKey(item.Code);                
                newItem.Items.AddRange(GenModelListForTreeView(list, newItem.Id, dicEntityGroupTreeList));
                newItem.HasChildren = newItem.Items.Count > 0;
                returnList.Add(newItem);
            }
            return returnList;
        }

        public sp_GetEntityGroupTreeView_Result GetTreeViewParentNodeMstr()
        {
            sp_GetEntityGroupTreeView_Result item = new sp_GetEntityGroupTreeView_Result();
            item.NameCN = IndexResx.lblRootNodeNameCN_Text;
            item.NameEN = IndexResx.lblRootNodeNameEN_Text;
            item.Code = "Root";
            item.ParentCode = "";
            return item;
        }

        private CommonServiceProvider _commonServiceProvider = null;
        private CommonServiceProvider ServiceProvider
        {
            get
            {
                if (this._commonServiceProvider == null)
                {
                    this._commonServiceProvider = (CommonServiceProvider)ServiceProviderFactory.GetServiceProvider(typeof(CommonServiceProvider));
                }
                return this._commonServiceProvider;
            }
        }

        [CheckAccessActionFilter("EntityGroup", "Viewer")]
        public ActionResult GetEntityGroupList([DataSourceRequest] DataSourceRequest request, EntityGroupExtendedViewModel model)
        {
            List<EntityGroupExtendedViewModel> EntityGroupViewModel = new List<EntityGroupExtendedViewModel>();
            List<PRV_ENT_ENTGROUP> EntityGroupList = this.ServiceProvider.GetEntityGroupList();
            

            EntityGroupViewModel = (from c in EntityGroupList.ToList()
                             //orderby c.Id descending
                             select new EntityGroupExtendedViewModel
                             {
                                 Id = c.Id,
                                 EntityGroupName = c.EntityGroupName,
                                 ModifiedBy = c.ModifiedBy,
                                 ModifiedDt = c.ModifiedDt != null ? (DateTime)c.ModifiedDt : new DateTime()
                             }).ToList();

            return Json(EntityGroupViewModel.ToDataSourceResult(request));
        }

        [HttpPost]
        public JsonResult AddEntityGroup([DataSourceRequest] DataSourceRequest request, EntityGroupExtendedViewModel model)
        {
            PRV_ENT_ENTGROUP entityGroup = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;                
                entityGroup = this.ServiceProvider.AddEntityGroup(ViewModelHelper.ConvertEntityGroupViewModelToEntityGroup(model));
                model.Id = entityGroup.Id;
            });
            //return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            return Json(new { Data = model });
        }

        [HttpPost]
        public JsonResult EditEntityGroup(EntityGroupExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.EntityGroupName = model.EntityGroupName.Trim();
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                this.ServiceProvider.EditEntityGroup(ViewModelHelper.ConvertEntityGroupViewModelToEntityGroup(model));
            });
            //return Json(executionResult, JsonRequestBehavior.AllowGet);
            return Json(new { Data = model });
        }

        public JsonResult DeleteEntityGroup(EntityGroupExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                this.ServiceProvider.RemoveEntityGroup(ViewModelHelper.ConvertEntityGroupViewModelToEntityGroup(model));
            });
            return Json(executionResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckAccessActionFilter("EntityGroup", "Editor")]
        public JsonResult CreateEntityGroupProject(int entityGroupID, string selectedCodes)
        {
            int result = 0;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.CreateEntityGroupProject(entityGroupID, selectedCodes, this.CurrentUserName);
            });

            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);
        }

        [CheckAccessActionFilter("EntityGroupUserGroupRole", "Viewer")]
        public JsonResult GetUserGroup(string text)
        {
            List<PRV_URG_USRGROUP> list = ServiceProvider.GetUserGroupList();
            List<PRV_URG_USRGROUP> resultList = list.Select(c => new PRV_URG_USRGROUP
            {
                Id = c.Id,
                UserGroupName = c.UserGroupName
            }).ToList();
            ViewData["usergroups"] = resultList;            
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }

        [CheckAccessActionFilter("EntityGroupUserGroupRole", "Viewer")]
        public JsonResult GetRole(string text)
        {
            List<PRV_ROL_ROLE> list = ServiceProvider.GetRoleList();
            List<PRV_ROL_ROLE> resultList = list.Select(c => new PRV_ROL_ROLE
            {
                Id = c.Id,
                RoleName = c.RoleName
            }).ToList();
            ViewData["roles"] = resultList;
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }

        [CheckAccessActionFilter("EntityGroupUserGroupRole", "Viewer")]
        public ActionResult GetEntityGroupUserGroupRoleListByEntityGroupId(
            [DataSourceRequest] DataSourceRequest request, int entityGroupId)
        {
            List<EntityGroupUserGroupRoleExtendedViewModel> vmList = new List<EntityGroupUserGroupRoleExtendedViewModel>();
            List<PRV_ENT_URG_ROL_RELN> mList = this.ServiceProvider.GetEntityGroupUserGroupRoleListByEntityGroupId(entityGroupId, false);
            //string lang = Nwcl.DAP.Common.CultureHelper.GetCurrentCultureFieldLanguage();
            vmList = (
                        from vm in mList.ToList()
                        orderby vm.Id 
                        select new EntityGroupUserGroupRoleExtendedViewModel
                        {
                            Id = vm.Id,
                            UserGroupId = vm.UserGroupId.Value,
                            UserGroupName = vm.PRV_URG_USRGROUP.UserGroupName,
                            RoleId = vm.PRV_ROL_ROLE.Id,
                            RoleName = vm.PRV_ROL_ROLE.RoleName,
                            EntityGroupId = entityGroupId
                        }
                     ).ToList();
            return Json(vmList.ToDataSourceResult(request));
        }

        [HttpPost]
        [CheckAccessActionFilter("EntityGroupUserGroupRole", "Editor")]
        public JsonResult AddEntityGroupUserGroupRole([DataSourceRequest] DataSourceRequest request, EntityGroupUserGroupRoleExtendedViewModel model)
        {
            PRV_ENT_URG_ROL_RELN item = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                item = this.ServiceProvider.AddEntityGroupUserGroupRole(ViewModelHelper.ConvertEntityGroupUserGroupRoleViewModelToDALModel(model));
                model.Id = item.Id;
                model.EntityGroupId = Convert.ToInt32(item.EntityGroupId);
                model.UserGroupId = Convert.ToInt32(item.UserGroupId);
                model.RoleId = Convert.ToInt32(item.RoleId);
                model.CurrentRoleId = Convert.ToInt32(item.RoleId);
                model.CurrentUserGroupId = Convert.ToInt32(item.UserGroupId);
            });
            //return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            return Json(new { Data = model });
        }

        [HttpPost]
        [CheckAccessActionFilter("EntityGroupUserGroupRole", "Editor")]
        public JsonResult EditEntityGroupUserGroupRole(EntityGroupUserGroupRoleExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                this.ServiceProvider.EditEntityGroupUserGroupRole(ViewModelHelper.ConvertEntityGroupUserGroupRoleViewModelToDALModel(model));
            });
            //return Json(executionResult, JsonRequestBehavior.AllowGet);
            return Json(new { Data = model });
        }
        [CheckAccessActionFilter("EntityGroupUserGroupRole", "Editor")]
        public JsonResult DeleteEntityGroupUserGroupRole(EntityGroupUserGroupRoleExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                this.ServiceProvider.RemoveEntityGroupUserGroupRole(ViewModelHelper.ConvertEntityGroupUserGroupRoleViewModelToDALModel(model));
            });
            return Json(executionResult, JsonRequestBehavior.AllowGet);
        }


        private const int existEntityGroup = 1;
        private const int notExistEntityGroup = 0;
        [HttpPost]
        [CheckAccessActionFilter("EntityGroup", "Viewer")]
        public JsonResult ValidateEntityGroupDuplicateName(string name, int id)
        {
            int result = existEntityGroup;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.ValidateEntityGroupDuplicateName(name.ToUpper().Trim(), id);
            });
            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckAccessActionFilter("EntityGroup", "Viewer")]
        public JsonResult ValidateEntityGroupDelete(int id)
        {
            int result = existEntityGroup;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.ValidateEntityGroupDelete(id);
            });
            
            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);

        }
    }
}