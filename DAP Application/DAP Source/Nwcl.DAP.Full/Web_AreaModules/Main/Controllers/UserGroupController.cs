﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Nwcl.DAP.Application.Service;
using Nwcl.DAP.DAL.Model;
using Nwcl.DAP.Web.Areas.Main.Models;
using Nwcl.DAP.Web.Common;
using Nwcl.DAP.Web.Common.Controllers;
using Nwcl.DAP.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Nwcl.DAP.Web.Common.Attributes;
using Nwcl.DAP.Web.Areas.Main.Resources.UserGroup;
using System.DirectoryServices;

namespace Nwcl.DAP.Web.Areas.Main.Controllers
{
    public class UserGroupController : BaseController
    {
        private CommonServiceProvider _commonServiceProvider = null;
        private Dictionary<string, string> _availableDomains = null;
        private Dictionary<string, string> AvailableDomains
        {
            get
            {
                if (this._availableDomains == null)
                {
                    this._availableDomains = ServiceProvider.GetParameter("ADDomainInfo", "ADDomain", "ADDomainList").ToDictionary(d => d.Value.ToUpper(), d => d.Value);
                }
                return this._availableDomains;
            }
        }
        private CommonServiceProvider ServiceProvider
        {
            get
            {
                if (this._commonServiceProvider == null)
                {
                    this._commonServiceProvider = (CommonServiceProvider)ServiceProviderFactory.GetServiceProvider(typeof(CommonServiceProvider));
                }
                return this._commonServiceProvider;
            }
        }
        // GET: UserGroup
        public ActionResult Index()
        {
            ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.UserGroupMaintenance);
            ViewData["SystemMessageCode"] = "User Group";
            return View();
        }

        [CheckAccessActionFilter("UserGroup", "Viewer")]
        public ActionResult GetUserGroupList([DataSourceRequest] DataSourceRequest request, UserGroupExtendedViewModel model)
        {
            List<UserGroupExtendedViewModel> UserGroupViewModel = new List<UserGroupExtendedViewModel>();
            List<PRV_URG_USRGROUP> UserGroupList = this.ServiceProvider.GetUserGroupList();


            UserGroupViewModel = (from c in UserGroupList.ToList()
                                    orderby c.Id 
                                    select new UserGroupExtendedViewModel
                                    {
                                        Id = c.Id,
                                        UserGroupName = c.UserGroupName,
                                        ModifiedBy = c.ModifiedBy,
                                        ModifiedDt = c.ModifiedDt != null ? (DateTime)c.ModifiedDt : new DateTime()
                                    }).ToList();

            return Json(UserGroupViewModel.ToDataSourceResult(request));
        }

        [HttpPost]
        public JsonResult AddUserGroup([DataSourceRequest] DataSourceRequest request, UserGroupExtendedViewModel model)
        {
            PRV_URG_USRGROUP UserGroup = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                UserGroup = this.ServiceProvider.AddUserGroup(ViewModelHelper.ConvertUserGroupViewModelToUserGroup(model));
                model.Id = UserGroup.Id;
            });
            //return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            return Json(new { Data = model });
        }

        [HttpPost]
        public JsonResult EditUserGroup(UserGroupExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.UserGroupName = model.UserGroupName.Trim();
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                this.ServiceProvider.EditUserGroup(ViewModelHelper.ConvertUserGroupViewModelToUserGroup(model));
            });
            //return Json(executionResult, JsonRequestBehavior.AllowGet);
            return Json(new { Data = model });
        }

        public JsonResult DeleteUserGroup(UserGroupExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                this.ServiceProvider.RemoveUserGroup(ViewModelHelper.ConvertUserGroupViewModelToUserGroup(model));
            });
            return Json(executionResult, JsonRequestBehavior.AllowGet);
        }

        [CheckAccessActionFilter("EditUserGroup", "Viewer")]
        public ActionResult EditUserGroup(int userGroupId)
        {
            ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.EditUserGroup);
            ViewData["SystemMessageCode"] = "Edit User Group";
            //ViewData["Name"] = ServiceProvider.GetUserGroupList().Where(o => o.Id == userGroupId).FirstOrDefault().UserGroupName;
            GetEntityGroup("");
            GetRole("");

            PRV_URG_USRGROUP ug = this.ServiceProvider.GetUserGroupByUserGroupId(userGroupId, false);
            List<EntityUserGroupTreeExtendedViewModel> ugTree = PrepareTreeModel(ug);

            UserGroupEditExtendedViewModel ugvm = new UserGroupEditExtendedViewModel()
            {
                Id = ug.Id,
                UserGroupName = ug.UserGroupName,
                userGroupTreeModel = ugTree
            };

            return View(ugvm);
        }

        private List<EntityUserGroupTreeExtendedViewModel> PrepareTreeModel(PRV_URG_USRGROUP ug)
        {
            List<EntityUserGroupTreeExtendedViewModel> tree = new List<EntityUserGroupTreeExtendedViewModel>();
            List<PRV_URG_USER_GROUP_RELN> checkedList = ug.PRV_URG_USER_GROUP_RELN.ToList();
            Dictionary<string, string> checkedDic = new Dictionary<string, string>();
            Dictionary<string, string> checkedAddedDic = new Dictionary<string, string>();

            for (int i = 0; i < checkedList.Count(); i++)
            {
                if (!checkedDic.ContainsKey(checkedList.ElementAt(i).ADLogonName))
                    checkedDic.Add(checkedList.ElementAt(i).ADLogonName, checkedList.ElementAt(i).ADLogonName);
            }

            List<MST_PROJ_REGION> regions = this.ServiceProvider.GetRegionTreeViewForUserGroup();

            for (int r = 0; r < regions.Count(); r++) //Region Level
            {
                MST_PROJ_REGION reg = regions.ElementAt(r);
                EntityUserGroupTreeExtendedViewModel vmRegion = ViewModelHelper.ConvertRegionDALToTreeViewModel(reg);
                List<MST_PROJ_CITY> cities = reg.MST_PROJ_CITY.ToList();

                //Prepare child - City
                for (int c = 0; c < cities.Count(); c++) //City Level
                {
                    MST_PROJ_CITY city = cities.ElementAt(c);
                    EntityUserGroupTreeExtendedViewModel vmCity = ViewModelHelper.ConvertCityDALToTreeViewModel(city);
                    List<PRV_URG_COMPANY> companies = city.PRV_URG_COMPANY.ToList();

                    //Prepare child - Company
                    for (int com = 0; com < companies.Count(); com++) //Company Level
                    {
                        PRV_URG_COMPANY company = companies.ElementAt(com);
                        EntityUserGroupTreeExtendedViewModel vmCompany = ViewModelHelper.ConvertCompanyDALToTreeViewModel(company);
                        List<PRV_URG_DEPARTMENT> departments = company.PRV_URG_DEPARTMENT.ToList();

                        //Prepare child - Department
                        for (int d = 0; d < departments.Count(); d++) //Department Level
                        {
                            PRV_URG_DEPARTMENT dept = departments.ElementAt(d);
                            EntityUserGroupTreeExtendedViewModel vmDept = ViewModelHelper.ConvertDepartmentDALToTreeViewModel(dept);
                            List<PRV_URG_DEPARTMENT_USER_RELN> departmentUsers = dept.PRV_URG_DEPARTMENT_USER_RELN.ToList();

                            //Prepare child - User
                            for (int u = 0; u < departmentUsers.Count(); u++) //User Level
                            {
                                PRV_URG_DEPARTMENT_USER_RELN du = departmentUsers.ElementAt(u);
                                MST_URG_USR theUser = GetUserADInfoByLogonNameAndDomain(du.ADDomain, du.ADLogonName);
                                if (theUser != null && theUser.ADLogonName != null)
                                {
                                    if (vmDept.child == null)
                                        vmDept.child = new List<EntityUserGroupTreeExtendedViewModel>();

                                    EntityUserGroupTreeExtendedViewModel vmUser = ViewModelHelper.ConvertUserDALToTreeViewModel(theUser);
                                    if (vmUser == null)
                                    {
                                        continue;
                                    }
                                    bool isUserChecked = checkedDic.ContainsKey(theUser.ADLogonName);

                                    if (isUserChecked) { 
                                        vmUser.isChecked = isUserChecked;
                                        if (!checkedAddedDic.ContainsKey(theUser.ADLogonName))
                                        {
                                            checkedAddedDic.Add(theUser.ADLogonName, theUser.ADLogonName);
                                        }

                                        vmDept.isExpanded = isUserChecked;
                                        vmCompany.isExpanded = isUserChecked;
                                        vmCity.isExpanded = isUserChecked;
                                        vmRegion.isExpanded = isUserChecked;
                                    }

                                    vmDept.child.Add(vmUser);
                                }
                            }

                            if (vmCompany.child == null)
                                vmCompany.child = new List<EntityUserGroupTreeExtendedViewModel>();
                            vmCompany.child.Add(vmDept);
                        }

                        if (vmCity.child == null)
                            vmCity.child = new List<EntityUserGroupTreeExtendedViewModel>();
                        vmCity.child.Add(vmCompany);
                    }
                    if (vmRegion.child == null)
                        vmRegion.child = new List<EntityUserGroupTreeExtendedViewModel>();
                    vmRegion.child.Add(vmCity);
                }

                tree.Add(vmRegion);
            }

            //Process users which are checked previously, but currently not in tree hiearachy
            if (checkedList.Count() > checkedAddedDic.Count())
            {
                EntityUserGroupTreeExtendedViewModel exNode = new EntityUserGroupTreeExtendedViewModel()
                {
                    NodeCode = "-1",
                    NodeName = IndexResx.lblTreeOrphanUserRoot,
                    level = 1,
                };
                exNode.isExpanded = true;
                exNode.child = new List<EntityUserGroupTreeExtendedViewModel>();

                for (int i = 0; i < checkedList.Count(); i++)
                {
                    PRV_URG_USER_GROUP_RELN ugr = checkedList.ElementAt(i);
                    if (!checkedAddedDic.ContainsKey(ugr.ADLogonName))
                    {
                        MST_URG_USR exUser = GetUserADInfoByLogonNameAndDomain(ugr.ADLogonName.Split('\\')[0], ugr.ADLogonName);
                        EntityUserGroupTreeExtendedViewModel vmUser = ViewModelHelper.ConvertUserDALToTreeViewModel(
                            exUser);
                        vmUser.isChecked = true;
                        exNode.child.Add(vmUser);
                    }
                }
                tree.Add(exNode);
            }

            return tree;
        }

        public MST_URG_USR GetUserADInfoByLogonNameAndDomain(string ADDomainName, string ADLogonName)
        {
            MST_URG_USR userExtendedVM = new MST_URG_USR();
            userExtendedVM.ADLogonName = ADLogonName;
            userExtendedVM.ADDisplayName = ADLogonName;
            //deptUserExtendedVM = null;

            string domainName = ADDomainName.ToUpper();

            if (!AvailableDomains.ContainsKey(domainName))
            {
                //Probabily domain disabled, skip querying LDAP for attributes

                userExtendedVM.ADDomain = domainName;
                //userExtendedVM.ADLogonName = ADLogonName;
                //userExtendedVM.ADDisplayName = ADLogonName;
                return userExtendedVM;
            }

            string ADLogonNamePart = ADLogonName.Split('\\')[1];

            string passPhase = ServiceProvider.GetParameter("Cryptography", "Config", "PassPhase").First().Value;
            string ldapQueryString = ServiceProvider.GetParameter("ADDomainInfo", "LDAPQueryString", domainName).First().Value;
            ldapQueryString = String.Format(ldapQueryString, ADLogonNamePart);

            string domainBindUsername = ServiceProvider.GetParameter("ADDomainInfo", "BindUsername", domainName).First().Value;
            string domainBindPasswordHash = ServiceProvider.GetParameter("ADDomainInfo", "BindPassword", domainName).First().Value;
            string domainBindPassword = CryptographyServiceProvider.DecryptString(domainBindPasswordHash, passPhase);

            // Search multiple times, base on no. of BaseDN
            //string domainBaseDn = "LDAP://" + ServiceProvider.GetParameter("ADDomainInfo", "BaseDN", domainName).First().Value;
            List<SYS_CFG_PARAM> baseDNList = ServiceProvider.GetParameter("ADDomainInfo", "BaseDN", domainName);

            foreach (SYS_CFG_PARAM domainBaseDn in baseDNList)
            {
                LDAPServiceProvider lsp = new LDAPServiceProvider();
                SearchResultCollection rc = lsp.SearchObjectsFromBaseDN(domainBindUsername, domainBindPassword, "LDAP://" + domainBaseDn.Value, ldapQueryString);
                List<DepartmentUserExtendedViewModel> userList = ViewModelHelper.ConvertResultToViewModelDepartmentUsers(rc, domainName, "LdapBatch");

                if (userList.Count() > 0)
                {
                    DepartmentUserExtendedViewModel deptVm = userList[0];
                    userExtendedVM.ADDomain = domainName;
                    userExtendedVM.ADLogonName = ADLogonName;
                    userExtendedVM.ADDisplayName = deptVm.ADDisplayName;
                    userExtendedVM.ADEmail = deptVm.ADEmail;
                    break;
                }

            }
            return userExtendedVM;
        }

        public JsonResult CreateUserGroupUser(int userGroupID, string selectedCodes)
        {
            int result = 0;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.CreateUserGroupUser(userGroupID, selectedCodes, this.CurrentUserName);
            });

            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);
        }

        [CheckAccessActionFilter("UserGroupRole", "Viewer")]
        public JsonResult GetEntityGroup(string text)
        {
            List<PRV_ENT_ENTGROUP> list = ServiceProvider.GetEntityGroupList();
            List<PRV_ENT_ENTGROUP> resultList = list.Select(c => new PRV_ENT_ENTGROUP
            {
                Id = c.Id,
                EntityGroupName = c.EntityGroupName
            }).ToList();
            ViewData["entitygroups"] = resultList;
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }

        [CheckAccessActionFilter("UserGroupRole", "Viewer")]
        public JsonResult GetRole(string text)
        {
            List<PRV_ROL_ROLE> list = ServiceProvider.GetRoleList();
            List<PRV_ROL_ROLE> resultList = list.Select(c => new PRV_ROL_ROLE
            {
                Id = c.Id,
                RoleName = c.RoleName
            }).ToList();
            ViewData["roles"] = resultList;
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }


        [CheckAccessActionFilter("EntityGroupUserGroupRole", "Viewer")]
        public ActionResult GetEntityGroupUserGroupRoleListByEntityGroupId([DataSourceRequest] DataSourceRequest request, int userGroupId)
        {
            List<UserGroupEntityGroupRoleExtendedViewModel> vmList = new List<UserGroupEntityGroupRoleExtendedViewModel>();
            List<PRV_ENT_URG_ROL_RELN> mList = this.ServiceProvider.GetEntityGroupUserGroupRoleListByUserGroupId(userGroupId, false);
            string lang = Nwcl.DAP.Common.CultureHelper.GetCurrentCultureFieldLanguage();
            vmList = (
                        from vm in mList.ToList()
                        orderby vm.Id 
                        select new UserGroupEntityGroupRoleExtendedViewModel
                        {
                            Id = vm.Id,
                            UserGroupId = vm.UserGroupId.Value,
                            RoleId = vm.PRV_ROL_ROLE.Id,
                            RoleName = vm.PRV_ROL_ROLE.RoleName,
                            EntityGroupId = vm.EntityGroupId.Value,
                            EntityGroupName = vm.PRV_ENT_ENTGROUP.EntityGroupName
                        }
                     ).ToList();
            return Json(vmList.ToDataSourceResult(request));
        }

        [HttpPost]
        [CheckAccessActionFilter("EntityGroupUserGroupRole", "Editor")]
        public JsonResult AddEntityGroupUserGroupRole([DataSourceRequest] DataSourceRequest request, UserGroupEntityGroupRoleExtendedViewModel model)
        {
            PRV_ENT_URG_ROL_RELN item = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                item = this.ServiceProvider.AddEntityGroupUserGroupRole(ViewModelHelper.ConvertUserGroupEntityGroupRoleViewModelToDALModel(model));
                model.Id = item.Id;
                model.EntityGroupId = Convert.ToInt32(item.EntityGroupId);
                model.UserGroupId = Convert.ToInt32(item.UserGroupId);
                model.RoleId = Convert.ToInt32(item.RoleId);
                model.CurrentRoleId = Convert.ToInt32(item.RoleId);
                model.CurrentEntityGroupId = Convert.ToInt32(item.EntityGroupId);
            });
            //return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            return Json(new { Data = model });
        }

        [HttpPost]
        [CheckAccessActionFilter("EntityGroupUserGroupRole", "Editor")]
        public JsonResult EditEntityGroupUserGroupRole(UserGroupEntityGroupRoleExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                this.ServiceProvider.EditEntityGroupUserGroupRole(ViewModelHelper.ConvertUserGroupEntityGroupRoleViewModelToDALModel(model));
            });
            //return Json(executionResult, JsonRequestBehavior.AllowGet);
            return Json(new { Data = model });
        }
        [CheckAccessActionFilter("EntityGroupUserGroupRole", "Editor")]
        public JsonResult DeleteEntityGroupUserGroupRole(UserGroupEntityGroupRoleExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                this.ServiceProvider.RemoveEntityGroupUserGroupRole(ViewModelHelper.ConvertUserGroupEntityGroupRoleViewModelToDALModel(model));
            });
            return Json(executionResult, JsonRequestBehavior.AllowGet);
        }


        private const int existUserGroup = 1;
        [HttpPost]
        [CheckAccessActionFilter("UserGroup", "Viewer")]
        public JsonResult ValidateUserGroupDuplicateName(string name, int id)
        {
            int result = existUserGroup;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.ValidateUserGroupDuplicateName(name.ToUpper().Trim(), id);
            });
            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckAccessActionFilter("UserGroup", "Viewer")]
        public JsonResult ValidateUserGroupDelete(int id)
        {
            int result = existUserGroup;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.ValidateUserGroupDelete(id);
            });

            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);

        }
    }
}