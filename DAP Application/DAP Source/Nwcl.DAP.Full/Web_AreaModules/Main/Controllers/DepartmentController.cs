﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Nwcl.DAP.Application.Service;
using Nwcl.DAP.DAL.Model;
using Nwcl.DAP.Web.Areas.Main.Models;
using Nwcl.DAP.Web.Common;
using Nwcl.DAP.Web.Common.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nwcl.DAP.Web.Common.Attributes;
using System.DirectoryServices;
using Nwcl.DAP.Web.Common.Models;

namespace Nwcl.DAP.Web.Areas.Main.Controllers
{
    public class DepartmentController : BaseController
    {
        private CommonServiceProvider _commonServiceProvider = null;
        private Dictionary<string, string> _availableDomains = null;
        private Dictionary<string, string> AvailableDomains
        {
            get
            {
                if (this._availableDomains == null)
                {
                    this._availableDomains = ServiceProvider.GetParameter("ADDomainInfo", "ADDomain", "ADDomainList").ToDictionary(d => d.Value.ToUpper(), d=> d.Value);
                }
                return this._availableDomains;
            }
        }
        private CommonServiceProvider ServiceProvider
        {
            get
            {
                if (this._commonServiceProvider == null)
                {
                    this._commonServiceProvider = (CommonServiceProvider)ServiceProviderFactory.GetServiceProvider(typeof(CommonServiceProvider));
                }
                return this._commonServiceProvider;
            }
        }
        // GET: Department
        public ActionResult Index()
        {
            ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.DepartmentMaintenance);
            ViewData["SystemMessageCode"] = "Department";
            GetCompanyList();
            return View();
        }

        // GET: Department
        public ActionResult EditDepartment(int departmentId)
        {
            ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.EditDepartment);
            ViewData["SystemMessageCode"] = "Edit Department";
            PRV_URG_DEPARTMENT obj = ServiceProvider.GetDepartmentById(departmentId, false);
            DepartmentExtendedViewModel viewModel = new DepartmentExtendedViewModel()
            {
                DepartmentName = obj.DepartmentName,
                Id = obj.Id
            };            
            ViewData["Name"] = obj.DepartmentName;
            ViewData["CompanyName"] = obj.PRV_URG_COMPANY.CompanyName;
            return View(viewModel);
        }

        public DepartmentUserExtendedViewModel GetUserADInfoByLogonNameAndDomain(string domainName, string ADLogonName)
        {
            DepartmentUserExtendedViewModel deptUserExtendedVM = null;
            //deptUserExtendedVM = null;
            domainName = domainName.ToUpper();

            if (!AvailableDomains.ContainsKey(domainName))
            {
                //Probabily domain disabled, skip querying LDAP for attributes
                deptUserExtendedVM = new DepartmentUserExtendedViewModel();
                deptUserExtendedVM.ADDomain = domainName;
                deptUserExtendedVM.ADLogonName = domainName + "\\" + ADLogonName;
                deptUserExtendedVM.ADLogonNameDisplay = domainName + "\\" + ADLogonName;
                //deptUserExtendedVM.ADDisplayName = domainName + "\\" + ADLogonName;
                return deptUserExtendedVM;
            }

            string passPhase = ServiceProvider.GetParameter("Cryptography", "Config", "PassPhase").First().Value;
            string ldapQueryString = ServiceProvider.GetParameter("ADDomainInfo", "LDAPQueryString", domainName).First().Value;
            ldapQueryString = String.Format(ldapQueryString, ADLogonName);

            string domainBindUsername = ServiceProvider.GetParameter("ADDomainInfo", "BindUsername", domainName).First().Value;
            string domainBindPasswordHash = ServiceProvider.GetParameter("ADDomainInfo", "BindPassword", domainName).First().Value;
            string domainBindPassword = CryptographyServiceProvider.DecryptString(domainBindPasswordHash, passPhase);

            // Search multiple times, base on no. of BaseDN
            //string domainBaseDn = "LDAP://" + ServiceProvider.GetParameter("ADDomainInfo", "BaseDN", domainName).First().Value;
            List<SYS_CFG_PARAM> baseDNList = ServiceProvider.GetParameter("ADDomainInfo", "BaseDN", domainName);

            foreach (SYS_CFG_PARAM domainBaseDn in baseDNList)
            {
                LDAPServiceProvider lsp = new LDAPServiceProvider();
                SearchResultCollection rc = lsp.SearchObjectsFromBaseDN(domainBindUsername, domainBindPassword, "LDAP://" + domainBaseDn.Value, ldapQueryString);
                List<DepartmentUserExtendedViewModel> userList = ViewModelHelper.ConvertResultToViewModelDepartmentUsers(rc, domainName, "LdapBatch");

                if (userList.Count() > 0)
                {
                    deptUserExtendedVM = userList[0];
                    break;
                }

            }

            if (deptUserExtendedVM == null)
            {
                //Probabily domain disabled, skip querying LDAP for attributes
                deptUserExtendedVM = new DepartmentUserExtendedViewModel();
                deptUserExtendedVM.ADDomain = domainName;
                deptUserExtendedVM.ADLogonName = domainName+"\\"+ADLogonName;
                deptUserExtendedVM.ADLogonNameDisplay = domainName + "\\" + ADLogonName;
                //deptUserExtendedVM.ADDisplayName = domainName + "\\" + ADLogonName;
                return deptUserExtendedVM;
            }
            return deptUserExtendedVM;
        }

        #region Department Index...
        [CheckAccessActionFilter("Company", "Viewer")]
        public ActionResult GetDepartmentList([DataSourceRequest] DataSourceRequest request, DepartmentExtendedViewModel model)
        {
            List<DepartmentExtendedViewModel> vmList = new List<DepartmentExtendedViewModel>();
            List<PRV_URG_DEPARTMENT> deptList = this.ServiceProvider.GetDepartmentList();


            vmList = (from c in deptList.ToList()
                                    orderby c.Id 
                                    select new DepartmentExtendedViewModel
                                    {
                                        Id = c.Id,
                                        CompanyId = Convert.ToInt32(c.CompanyId),
                                        DepartmentName = c.DepartmentName,
                                        CompanyName = c.PRV_URG_COMPANY.CompanyName,
                                        CurrentCompanyName = c.PRV_URG_COMPANY.CompanyName,
                                        ModifiedBy = c.ModifiedBy,
                                        ModifiedDt = c.ModifiedDt != null ? (DateTime)c.ModifiedDt : new DateTime()
                                    }).ToList();

            return Json(vmList.ToDataSourceResult(request));
        }

        [CheckAccessActionFilter("Company", "Viewer")]
        public JsonResult GetCompanyList()
        {
            List<PRV_URG_COMPANY> list = this.ServiceProvider.GetCompanyList();
            List<PRV_URG_COMPANY> resultList = list.Select(c => new PRV_URG_COMPANY
            {
                Id = c.Id,
                CompanyName = c.CompanyName
            }).ToList();
            ViewData["companies"] = resultList;
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddDepartment([DataSourceRequest] DataSourceRequest request, DepartmentExtendedViewModel model)
        {
            PRV_URG_DEPARTMENT Department = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                Department = this.ServiceProvider.AddDepartment(ViewModelHelper.ConvertDepartmentViewModelToDepartment(model));
                model.Id = Department.Id;
            });
            //return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            return Json(new { Data = model });
        }

        [HttpPost]
        public JsonResult EditDepartment(DepartmentExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.DepartmentName = model.DepartmentName.Trim();
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                this.ServiceProvider.EditDepartment(ViewModelHelper.ConvertDepartmentViewModelToDepartment(model));
            });
            //return Json(executionResult, JsonRequestBehavior.AllowGet);
            return Json(new { Data = model });
        }

        public JsonResult DeleteDepartment(DepartmentExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                this.ServiceProvider.RemoveDepartment(ViewModelHelper.ConvertDepartmentViewModelToDepartment(model));
            });
            return Json(executionResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Edit Department...
        public JsonResult GetDepartmentUserListById([DataSourceRequest] DataSourceRequest request, int DepartmentId)
        {
            List<sp_GetUserByDepartmentId_Result> departmentUserList = this.ServiceProvider.GetDepartmentUserByDepartmentId(DepartmentId);

            List<DepartmentUserExtendedViewModel> departmentUserViewModel = new List<DepartmentUserExtendedViewModel>();

            // Append search result to ViewModel
            foreach (sp_GetUserByDepartmentId_Result departmentUser in departmentUserList)
            {
                string ADLogonName = departmentUser.ADLogonName;
                DepartmentUserExtendedViewModel tmpVM = GetUserADInfoByLogonNameAndDomain(departmentUser.ADDomain, ADLogonName.Split('\\')[1]);

                if (tmpVM != null) {
                    tmpVM.deptID = DepartmentId;
                    departmentUserViewModel.Add(tmpVM);
                }
            }

            return Json(departmentUserViewModel.ToDataSourceResult(request));
        }

        public JsonResult DeleteDepartmentUser(DepartmentUserExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                string adLogonName = model.ADLogonName.Replace(@"\\", @"\");
                //string adLogonName = model.ADLogonName;
                this.ServiceProvider.DeleteDepartmentUserByAdLogonNameAndDepartmentId(adLogonName, model.deptID);
            });
            return Json(executionResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDomains()
        {
            List<ListItemExtendedViewModel> vm = new List<ListItemExtendedViewModel>();
            List<SYS_CFG_PARAM> pList = ServiceProvider.GetParameter("ADDomainInfo", "ADDomain", "ADDomainList");
            vm = (from x in pList
                 orderby x.DisplayOrder
                 select new ListItemExtendedViewModel
                 {
                     Text = x.Value,
                     Value = x.Value
                 }).ToList();
            return Json(vm);
        }

        
  

        [HttpPost]
        public JsonResult SearchUser([DataSourceRequest]DataSourceRequest request, string domains, string criteria)
        {
            

            List<DepartmentUserExtendedViewModel> vm = new List<DepartmentUserExtendedViewModel>();

            string passPhase = ServiceProvider.GetParameter("Cryptography", "Config", "PassPhase").First().Value;
            List<string> pList = domains.Substring(0,domains.Length-1).Split(';').ToList();

            foreach (string domain in pList)
            {
                    string domainName = domain;
                    List<DepartmentUserExtendedViewModel> tmpList = new List<DepartmentUserExtendedViewModel>();

                    string decodeCriteria = System.Web.HttpUtility.UrlDecode(criteria);
                    string ldapQueryString = ServiceProvider.GetParameter("ADDomainInfo", "LDAPQueryString", domainName).First().Value;
                    if (criteria.Length > 0)
                    {
                    //ldapQueryString = String.Format(ldapQueryString, "(displayName=*" + decodeCriteria + "*)");
                    ldapQueryString = String.Format(ldapQueryString, "*"+decodeCriteria+"*");
                } else
                    {
                        // P.S. Fail on query by (diaplayName=**)
                        ldapQueryString = String.Format(ldapQueryString, "");
                    }
                    string domainBindUsername = ServiceProvider.GetParameter("ADDomainInfo", "BindUsername", domainName).First().Value;
                    string domainBindPasswordHash = ServiceProvider.GetParameter("ADDomainInfo", "BindPassword", domainName).First().Value;
                    string domainBindPassword = CryptographyServiceProvider.DecryptString(domainBindPasswordHash, passPhase);

                // Search multiple times, base on no. of BaseDN
                //string domainBaseDn = "LDAP://" + ServiceProvider.GetParameter("ADDomainInfo", "BaseDN", domainName).First().Value;
                List<SYS_CFG_PARAM> baseDNList = ServiceProvider.GetParameter("ADDomainInfo", "BaseDN", domainName);

                foreach (SYS_CFG_PARAM domainBaseDn in baseDNList)
                {
                    LDAPServiceProvider lsp = new LDAPServiceProvider();
                    SearchResultCollection rc = lsp.SearchObjectsFromBaseDN(domainBindUsername, domainBindPassword, "LDAP://" + domainBaseDn.Value, ldapQueryString);
                    List<DepartmentUserExtendedViewModel> userList = ViewModelHelper.ConvertResultToViewModelDepartmentUsers(rc, domainName, "LdapBatch");

                    if (userList != null && userList.Count() >= 1)
                    {
                        if(userList[0].ADLogonName != null) { 
                            // Append the result set to view model 
                            tmpList = (from x in userList
                                       select new DepartmentUserExtendedViewModel
                                       {
                                           ADDisplayName = x.ADDisplayName,
                                           ADLogonName = x.ADLogonName,
                                           ADLogonNameDisplay = x.ADLogonNameDisplay,
                                           ADCompany = x.ADCompany,
                                           ADDepartment = x.ADDepartment,
                                           ADDomain = x.ADDomain,
                                           ADEmail = x.ADEmail,
                                           ADRegion = x.ADRegion,
                                           ADTitle = x.ADTitle
                                       }).ToList();
                            vm.AddRange(tmpList);
                        }
                    }
                }                    
            }
            return Json(vm.ToDataSourceResult(request));
        }

        public JsonResult GetUser([DataSourceRequest] DataSourceRequest request)
        {
            //List<DepartmentUserExtendedViewModel> vm = new List<DepartmentUserExtendedViewModel>();

            //string passPhase = ServiceProvider.GetParameter("Cryptography", "Config", "PassPhase").First().Value;
            //List<SYS_CFG_PARAM> pList = ServiceProvider.GetParameter("ADDomainInfo", "ADDomain", "ADDomainList");

            //foreach (SYS_CFG_PARAM domain in pList)
            //{
            //    string domainName = domain.Value;
            //    List<DepartmentUserExtendedViewModel> tmpList = new List<DepartmentUserExtendedViewModel>();

            //    string ldapQueryString = ServiceProvider.GetParameter("ADDomainInfo", "LDAPQueryString", domainName).First().Value;
            //    ldapQueryString = String.Format(ldapQueryString, "");
            //    string domainBindUsername = ServiceProvider.GetParameter("ADDomainInfo", "BindUsername", domainName).First().Value;
            //    string domainBindPasswordHash = ServiceProvider.GetParameter("ADDomainInfo", "BindPassword", domainName).First().Value;
            //    string domainBaseDn = "LDAP://" + ServiceProvider.GetParameter("ADDomainInfo", "BaseDN", domainName).First().Value;

            //    string domainBindPassword = CryptographyServiceProvider.DecryptString(domainBindPasswordHash, passPhase);

            //    LDAPServiceProvider lsp = new LDAPServiceProvider();

            //    SearchResultCollection rc = lsp.SearchObjectsFromBaseDN(domainBindUsername, domainBindPassword, domainBaseDn, ldapQueryString);

            //    List<DepartmentUserViewModel> userList = ViewModelHelper.ConvertResultToViewModelDepartmentUsers(rc, domainName, "LdapBatch");


            //    tmpList = (from x in userList
            //               select new DepartmentUserExtendedViewModel
            //               {
            //                   ADDisplayName = x.ADDisplayName,
            //                   ADLogonName = x.ADLogonName,
            //                   ADCompany = x.ADCompany,
            //                   ADDepartment = x.ADDepartment,
            //                   ADDomain = x.ADDomain,
            //                   ADEmail = x.ADEmail,
            //                   ADRegion = x.ADRegion,
            //                   ADTitle = x.ADTitle

            //               }).ToList();
            //    vm.AddRange(tmpList);
            //}
            //return Json(vm.ToDataSourceResult(request));
            return null;
        }

        [HttpPost]
        public JsonResult AddUserToGroup(string departmentId, string adLogonName)
        {
            string result = "";
            ExecutionResult executionResult = this.Execute(() =>
            {
                int deptID = Convert.ToInt32(departmentId);
                result = ServiceProvider.AddUserToDepartment(adLogonName, deptID, this.CurrentUserName);
            });
            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        private const int existDepartment = 1;
        [HttpPost]
        [CheckAccessActionFilter("Department", "Viewer")]
        public JsonResult ValidateDepartmentDuplicateName(string name, int id)
        {
            int result = existDepartment;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.ValidateDepartmentDuplicateName(name.ToUpper().Trim(), id);
            });
            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //[CheckAccessActionFilter("Department", "Viewer")]
        //public JsonResult ValidateDepartmentDelete(int id)
        //{
        //    int result = existDepartment;
        //    ExecutionResult executionResult = this.Execute(() =>
        //    {
        //        result = this.ServiceProvider.ValidateDepartmentDelete(id);
        //    });

        //    return Json(new
        //    {
        //        IsSuccess = executionResult.IsSuccess,
        //        Message = result,
        //    }, JsonRequestBehavior.AllowGet);

        //}
    }
}