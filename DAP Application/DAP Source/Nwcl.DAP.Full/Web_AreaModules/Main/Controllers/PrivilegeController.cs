﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Nwcl.DAP.Application.Service;
using Nwcl.DAP.DAL.Model;
using Nwcl.DAP.Web.Areas.Main.Models;
using Nwcl.DAP.Web.Common;
using Nwcl.DAP.Web.Common.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nwcl.DAP.Web.Common.Attributes;
using Nwcl.DAP.Common;
using Nwcl.DAP.Common.Identity;
using log4net;

namespace Nwcl.DAP.Web.Areas.Main.Controllers
{
    public class PrivilegeController : BaseController
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private CommonServiceProvider _commonServiceProvider = null;
        private CommonServiceProvider ServiceProvider
        {
            get
            {
                if (this._commonServiceProvider == null)
                {
                    this._commonServiceProvider = (CommonServiceProvider)ServiceProviderFactory.GetServiceProvider(typeof(CommonServiceProvider));
                }
                return this._commonServiceProvider;
            }
        }
        // GET: Privilege
        public ActionResult Index()
        {
            ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.PrivilegeMaintenance);
            ViewData["SystemMessageCode"] = "Privilege";
            GetUserGroup();
            GetEntityGroup();
            GetRole();
            return View();
        }

        public ActionResult PrivilegeList()
        {
            ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.PrivilegeList);
            ViewData["SystemMessageCode"] = "PrivilegeList";
            return View();
        }


        [CheckAccessActionFilter("Privilege", "Viewer")]
        public JsonResult GetUserGroup()
        {
            List<PRV_URG_USRGROUP> list = ServiceProvider.GetUserGroupList();
            List<PRV_URG_USRGROUP> resultList = list.Select(c => new PRV_URG_USRGROUP
            {
                Id = c.Id,
                UserGroupName = c.UserGroupName
            }).ToList();
            ViewData["usergroups"] = resultList;
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }

        [CheckAccessActionFilter("Privilege", "Viewer")]
        public JsonResult GetEntityGroup()
        {
            List<PRV_ENT_ENTGROUP> list = ServiceProvider.GetEntityGroupList();
            List<PRV_ENT_ENTGROUP> resultList = list.Select(c => new PRV_ENT_ENTGROUP
            {
                Id = c.Id,
                EntityGroupName = c.EntityGroupName
            }).ToList();
            ViewData["entitygroups"] = resultList;
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }

        [CheckAccessActionFilter("Privilege", "Viewer")]
        public JsonResult GetRole()
        {
            List<PRV_ROL_ROLE> list = ServiceProvider.GetRoleList();
            List<PRV_ROL_ROLE> resultList = list.Select(c => new PRV_ROL_ROLE
            {
                Id = c.Id,
                RoleName = c.RoleName
            }).ToList();
            ViewData["roles"] = resultList;
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }


        [CheckAccessActionFilter("Privilege", "Viewer")]
        public ActionResult GetPrivilegeList([DataSourceRequest] DataSourceRequest request, PrivilegeExtendedViewModel model)
        {
            List<PrivilegeExtendedViewModel> privViewModel = new List<PrivilegeExtendedViewModel>();
            List<PRV_ENT_URG_ROL_RELN> privList = this.ServiceProvider.GetEntityGroupUserGroupRoleList();


            privViewModel = (from c in privList.ToList()
                             orderby c.Id
                             select new PrivilegeExtendedViewModel
                             {
                                 Id = c.Id,
                                 EntityGroupId = c.EntityGroupId.Value,
                                 UserGroupId = c.UserGroupId.Value,
                                 RoleId = c.RoleId.Value,
                                 EntityGroupName = c.PRV_ENT_ENTGROUP.EntityGroupName,
                                 UserGroupName = c.PRV_URG_USRGROUP.UserGroupName,
                                 RoleName = c.PRV_ROL_ROLE.RoleName,
                                 ModifiedBy = c.ModifiedBy,
                                 ModifiedDt = c.ModifiedDt != null ? (DateTime)c.ModifiedDt : new DateTime()
                             }).ToList();

            return Json(privViewModel.ToDataSourceResult(request));
        }

        [CheckAccessActionFilter("Privilege", "Viewer")]
        public ActionResult GetPrivilegeDetail([DataSourceRequest] DataSourceRequest request, PrivilegeExtendedViewModel model)
        {
            try
            {
                UserGroupController ugc = new UserGroupController();
                List<PrivilegeDetailExtendedViewModel> privViewModel = new List<PrivilegeDetailExtendedViewModel>();
                List<PRV_ENT_URG_ROL_RELN> privList = this.ServiceProvider.GetPrivilegeDetailList();
                Dictionary<string, string> userNameDic = new Dictionary<string, string>();

                foreach (PRV_ENT_URG_ROL_RELN r in privList)
                {

                    foreach (PRV_URG_USER_GROUP_RELN usr in r.PRV_URG_USRGROUP.PRV_URG_USER_GROUP_RELN.DefaultIfEmpty())
                    {
                        string udisplayName = "";
                        if (usr == null || usr.ADLogonName == null || usr.ADLogonName.Trim().Length == 0)
                        {
                            udisplayName = "";
                        }
                        else { 
                            log.Debug("usr.ADLogonName: " + usr.ADLogonName);
                            if (userNameDic.ContainsKey(usr.ADLogonName))
                            {
                                udisplayName = userNameDic[usr.ADLogonName];
                            }
                            else { 
                                MST_URG_USR theusr = ugc.GetUserADInfoByLogonNameAndDomain(usr.ADLogonName.Split('\\')[0], usr.ADLogonName);
                                udisplayName = theusr == null ? "" : theusr.ADDisplayName + " (" + theusr.ADEmail + ")";
                                userNameDic.Add(usr.ADLogonName, udisplayName);
                            }
                        }
                        foreach (PRV_ENT_ENTGROUP_PROJ_RELN ent in r.PRV_ENT_ENTGROUP.PRV_ENT_ENTGROUP_PROJ_RELN.DefaultIfEmpty())
                        {
                            foreach (PRV_ROL_ROLE_DASHBOARD_RELN rol in r.PRV_ROL_ROLE.PRV_ROL_ROLE_DASHBOARD_RELN.DefaultIfEmpty())
                            {
                                PrivilegeDetailExtendedViewModel vm = new PrivilegeDetailExtendedViewModel()
                                {
                                    Id = r.Id,
                                    EntityGroupId = r.EntityGroupId.Value,
                                    UserGroupId = r.UserGroupId.Value,
                                    RoleId = r.RoleId.Value,
                                    EntityGroupName = r.PRV_ENT_ENTGROUP.EntityGroupName,
                                    UserGroupName = r.PRV_URG_USRGROUP.UserGroupName,
                                    RoleName = r.PRV_ROL_ROLE.RoleName,
                                    ProjectName = ent == null || ent.MST_PROJ_PROJECT == null ? "" : DAP.Common.CultureHelper.GetCurrentCultureFieldLanguage().ToUpper().Equals(Constants.FIELD_LANGUAGE_EN)
                                        ? ent.MST_PROJ_PROJECT.ProjectNameEN
                                        : ent.MST_PROJ_PROJECT.ProjectNameCN,
                                    //UserName = usr == null || usr.MST_URG_USR == null? "" : usr.MST_URG_USR.ADDisplayName + " (" + usr.MST_URG_USR.ADEmail + ")",
                                    UserName = udisplayName,
                                    DashboardName = rol == null || rol.SYS_CFG_DASHBOARD == null ? "" : DAP.Common.CultureHelper.GetCurrentCultureFieldLanguage().ToUpper().Equals(Constants.FIELD_LANGUAGE_EN)
                                        ? rol.SYS_CFG_DASHBOARD.DashboardNameEN
                                        : rol.SYS_CFG_DASHBOARD.DashboardNameCN,
                                    ModifiedBy = r.ModifiedBy,
                                    ModifiedDt = r.ModifiedDt != null ? (DateTime)r.ModifiedDt : new DateTime()
                                };
                                privViewModel.Add(vm);
                            }
                        }
                    }
                }

                return Json(privViewModel.ToDataSourceResult(request));
            }
            catch(Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
        }

        private const int existPrivilege = 1;
        private const int notExistPrivilege = 0;
        [HttpPost]
        [CheckAccessActionFilter("Privilege", "Viewer")]
        public JsonResult ValidatePrivilegeDuplicate(int id, int entityGroupId, int userGroupId, int roleId)
        {
            int result = existPrivilege;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.ValidatePrivilegeDuplicate(id, entityGroupId, userGroupId, roleId);
            });
            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddPrivilege([DataSourceRequest] DataSourceRequest request, PrivilegeExtendedViewModel model)
        {
            PRV_ENT_URG_ROL_RELN privilege = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                privilege = ViewModelHelper.ConvertPrivilegeViewModelToDALModel(model);
                privilege = this.ServiceProvider.AddEntityGroupUserGroupRole(privilege);
                model.Id = privilege.Id;
            });
            //return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            return Json(new { Data = model });
        }

        [HttpPost]
        public JsonResult EditPrivilege(PrivilegeExtendedViewModel model)
        {
            PRV_ENT_URG_ROL_RELN privilege = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.RoleName = model.RoleName.Trim();
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                privilege = ViewModelHelper.ConvertPrivilegeViewModelToDALModel(model);
                //Role.RoleId = Convert.ToInt32(Session["roledashboard_roleid"]);
                this.ServiceProvider.EditEntityGroupUserGroupRole(privilege);
            });
            //return Json(executionResult, JsonRequestBehavior.AllowGet);
            return Json(new { Data = model });
        }

        [HttpPost]
        public JsonResult DeletePrivilege(PrivilegeExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                this.ServiceProvider.RemoveEntityGroupUserGroupRole(ViewModelHelper.ConvertPrivilegeViewModelToDALModel(model));
            });
            return Json(executionResult, JsonRequestBehavior.AllowGet);
        }
    }

}