﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Nwcl.DAP.Application.Service;
using Nwcl.DAP.DAL.Model;
using Nwcl.DAP.Web.Areas.Main.Models;
using Nwcl.DAP.Web.Common;
using Nwcl.DAP.Web.Common.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nwcl.DAP.Web.Common.Attributes;


namespace Nwcl.DAP.Web.Areas.Main.Controllers
{
    public class CompanyController : BaseController
    {
        private CommonServiceProvider _commonServiceProvider = null;
        private CommonServiceProvider ServiceProvider
        {
            get
            {
                if (this._commonServiceProvider == null)
                {
                    this._commonServiceProvider = (CommonServiceProvider)ServiceProviderFactory.GetServiceProvider(typeof(CommonServiceProvider));
                }
                return this._commonServiceProvider;
            }
        }
        // GET: Company
        public ActionResult Index()
        {
            ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.CompanyMaintenance);
            ViewData["SystemMessageCode"] = "Company";
            GetCityList();
            return View();
        }

        [CheckAccessActionFilter("Company", "Viewer")]
        public ActionResult GetCompanyList([DataSourceRequest] DataSourceRequest request, CompanyExtendedViewModel model)
        {
            List<CompanyExtendedViewModel> CompanyViewModel = new List<CompanyExtendedViewModel>();
            List<PRV_URG_COMPANY> companyList = this.ServiceProvider.GetCompanyList();


            CompanyViewModel = (from c in companyList.ToList()
                                    orderby c.Id 
                                    select new CompanyExtendedViewModel
                                    {
                                        Id = c.Id,
                                        CityCode = c.CityCode,
                                        CompanyName = c.CompanyName,
                                        CityName = DAP.Common.CultureHelper.GetCurrentCultureFieldLanguage().IsCaseInsensitiveEqual(
                                  DAP.Common.Constants.FIELD_LANGUAGE_CN)
                                  ? c.MST_PROJ_CITY.CityNameCN : c.MST_PROJ_CITY.CityNameEN == null ? c.MST_PROJ_CITY.CityNameCN : c.MST_PROJ_CITY.CityNameEN,
                                        CurrentCityNameEn = c.MST_PROJ_CITY.CityNameEN == null ? c.MST_PROJ_CITY.CityNameCN : c.MST_PROJ_CITY.CityNameEN,
                                        CurrentCityNameCn = c.MST_PROJ_CITY.CityNameCN,
                                        ModifiedBy = c.ModifiedBy,
                                        ModifiedDt = c.ModifiedDt != null ? (DateTime)c.ModifiedDt : new DateTime()
                                    }).ToList();

            return Json(CompanyViewModel.ToDataSourceResult(request));
        }

        [CheckAccessActionFilter("Company", "Viewer")]
        public JsonResult GetCityList()
        {
            List<MST_PROJ_CITY> cityList = this.ServiceProvider.GetCityList(false);

            ViewData["cities"] = cityList;
            return Json(cityList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddCompany([DataSourceRequest] DataSourceRequest request, CompanyExtendedViewModel model)
        {
            PRV_URG_COMPANY Company = null;
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                Company = this.ServiceProvider.AddCompany(ViewModelHelper.ConvertCompanyViewModelToCompany(model));
                model.Id = Company.Id;
            });
            //return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            return Json(new { Data = model });
        }

        [HttpPost]
        public JsonResult EditCompany(CompanyExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                model.CompanyName = model.CompanyName.Trim();
                model.ModifiedBy = this.CurrentUserName;
                model.ModifiedDt = DateTime.Now;
                this.ServiceProvider.EditCompany(ViewModelHelper.ConvertCompanyViewModelToCompany(model));
            });
            //return Json(executionResult, JsonRequestBehavior.AllowGet);
            return Json(new { Data = model });
        }

        public JsonResult DeleteCompany(CompanyExtendedViewModel model)
        {
            ExecutionResult executionResult = this.Execute(() =>
            {
                this.ServiceProvider.RemoveCompany(ViewModelHelper.ConvertCompanyViewModelToCompany(model));
            });
            return Json(executionResult, JsonRequestBehavior.AllowGet);
        }

        private const int existCompany = 1;
        [HttpPost]
        [CheckAccessActionFilter("Company", "Viewer")]
        public JsonResult ValidateCompanyDuplicateName(string name, int id)
        {
            int result = existCompany;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.ValidateCompanyDuplicateName(name.ToUpper().Trim(), id);
            });
            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CheckAccessActionFilter("Company", "Viewer")]
        public JsonResult ValidateCompanyDelete(int id)
        {
            int result = existCompany;
            ExecutionResult executionResult = this.Execute(() =>
            {
                result = this.ServiceProvider.ValidateCompanyDelete(id);
            });

            return Json(new
            {
                IsSuccess = executionResult.IsSuccess,
                Message = result,
            }, JsonRequestBehavior.AllowGet);

        }
    }
}