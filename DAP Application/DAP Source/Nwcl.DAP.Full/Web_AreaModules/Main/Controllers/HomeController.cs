﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nwcl.DAP.Web.Areas.Main.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            // Default page is Main/EntityGroup index Page
            return RedirectToAction("Index", "EntityGroup", new { Area = "Main" });
        }
        public ActionResult HomePage()
        {
            return View();
        }
    }
}