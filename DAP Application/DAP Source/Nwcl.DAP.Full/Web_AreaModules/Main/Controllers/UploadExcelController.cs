﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Nwcl.DAP.Application.Service;
using Nwcl.DAP.DAL.Model;
using Nwcl.DAP.Web.Areas.Main.Models;
using Nwcl.DAP.Web.Common;
using Nwcl.DAP.Web.Common.Controllers;
using Nwcl.DAP.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Nwcl.DAP.Web.Common.Attributes;
using Nwcl.DAP.Web.Areas.Main.Resources.UploadExcel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO;
using System.Data;
using System.Data.Entity;
using NPOI.SS.Formula.Functions;
using log4net;

namespace Nwcl.DAP.Web.Areas.Main.Controllers
{
    public class UploadExcelController : BaseController
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private CommonServiceProvider _commonServiceProvider = null;
        private CommonServiceProvider ServiceProvider
        {
            get
            {
                if (this._commonServiceProvider == null)
                {
                    this._commonServiceProvider = (CommonServiceProvider)ServiceProviderFactory.GetServiceProvider(typeof(CommonServiceProvider));
                }
                return this._commonServiceProvider;
            }
        }
        // GET: UserGroup
        public ActionResult Index()
        {
            ViewData["FormID"] = GetDescription(Nwcl.DAP.Common.Constants.ScreenID.UploadExcel);
            ViewData["SystemMessageCode"] = "Upload Excel";

            ViewBag.inlineTree = PrepareEntityTree("");
            return View();
        }
        public ActionResult Events_Save(IEnumerable<HttpPostedFileBase> files)
        {
            try
            {
                // The Name of the Upload component is "files"
                string errM = "";
                string gid = "";
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        // Some browsers send file names with full path.
                        // We are only interested in the file name.
                        var fileName = Path.GetFileName(file.FileName);
                        Guid g = Guid.NewGuid();
                        gid = g.ToString();
                        if (!Directory.Exists(Server.MapPath("~/App_Data/" + g.ToString())))
                            Directory.CreateDirectory(Server.MapPath("~/App_Data/" + g.ToString()));
                        var physicalPath = Path.Combine(Server.MapPath("~/App_Data/" + g.ToString()), fileName);

                        // The files are not actually saved in this demo
                        file.SaveAs(physicalPath);
                        errM = ExcelProcess(physicalPath, g);
                        ViewData["ErrorMessage"] = errM;
                    }
                }

                // Return an empty string to signify success
                return Json(new { data = gid }, "text/plain");
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
        }
        public ActionResult Events_Remove(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"

            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

                    // TODO: Verify user permissions

                    if (System.IO.File.Exists(physicalPath))
                    {
                        // The files are not actually removed in this demo
                        System.IO.File.Delete(physicalPath);
                    }
                }
            }

            // Return an empty string to signify success
            return Content("");
        }

        public string ExcelProcess(string filePath, Guid g)
        {
            try
            {
                string resultMessage = "";
                XSSFWorkbook wb;
                DateTime mdt = DateTime.Now;
                string guid = g.ToString();
                //HSSFWorkbook wb;
                //using (FileStream file = new FileStream(@"c:\temp\-房产项目匯總_180709.xlsx", FileMode.Open, FileAccess.Read))
                using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    wb = new XSSFWorkbook(file);
                    string sheetName = CommonServiceProvider.GetParameterSingle("MasterUpload", "Project", "RegionCitySheetName").Value;
                    ISheet sheet = wb.GetSheet(sheetName);
                    ISheet citySheet;

                    List<MST_PROJ_REGION_UPLOAD> regionList = new List<MST_PROJ_REGION_UPLOAD>();
                    List<MST_PROJ_CITY_UPLOAD> cityList = new List<MST_PROJ_CITY_UPLOAD>();
                    List<MST_PROJ_PROJECT_UPLOAD> projList = new List<MST_PROJ_PROJECT_UPLOAD>();
                    for (int row = 1; row <= sheet.LastRowNum; row++) //first line no need.
                    {
                        if (regionList.Where(o => o.RegionCode == sheet.GetRow(row).GetCell(0).StringCellValue).ToList().Count == 0)
                        {
                            MST_PROJ_REGION_UPLOAD region = new MST_PROJ_REGION_UPLOAD();
                            region.RegionCode = sheet.GetRow(row).GetCell(0).StringCellValue;
                            region.RegionNameCN = sheet.GetRow(row).GetCell(0).StringCellValue;
                            region.RegionNameEN = sheet.GetRow(row).GetCell(0).StringCellValue;
                            region.ModifieBy = CurrentUserName;
                            region.ModifiedDt = mdt;
                            region.GUID = guid;
                            regionList.Add(region);
                        }
                        MST_PROJ_CITY_UPLOAD city = new MST_PROJ_CITY_UPLOAD();
                        city.RegionCode = sheet.GetRow(row).GetCell(0).StringCellValue;
                        city.CityCode = sheet.GetRow(row).GetCell(1).StringCellValue;
                        city.CityNameCN = sheet.GetRow(row).GetCell(2).StringCellValue;
                        city.CityNameEN = sheet.GetRow(row).GetCell(2).StringCellValue;
                        city.ModifiedBy = CurrentUserName;
                        city.ModifiedDt = mdt;
                        city.GUID = guid;
                        cityList.Add(city);
                    }

                    for (int i = 0; i < cityList.Count; i++)
                    {
                        string cityCode = cityList[i].CityCode;
                        citySheet = wb.GetSheet(cityCode);
                        if (citySheet != null)
                        {
                            //dr = dtProject.NewRow();
                            for (int j = 1; j <= citySheet.LastRowNum; j++)
                            {
                                if (ExcelStringValue(citySheet, j, 2).Length > 0 && ExcelStringValue(citySheet, j, 7).Length > 0) //Check Name and Project Code exists.
                                {
                                    MST_PROJ_PROJECT_UPLOAD proj = new MST_PROJ_PROJECT_UPLOAD();
                                    proj.CityCode = cityCode;
                                    proj.ProjectCode = ExcelStringValue(citySheet, j, 2);
                                    proj.ProjectPhase1 = ExcelStringValue(citySheet, j, 3);
                                    proj.ProjectPhase2 = ExcelStringValue(citySheet, j, 4);
                                    proj.ProjectPhase3 = ExcelStringValue(citySheet, j, 5);
                                    proj.ProjectFullCode = ProjectFullCode(cityCode, ExcelStringValue(citySheet, j, 2), ExcelStringValue(citySheet, j, 3), ExcelStringValue(citySheet, j, 4), ExcelStringValue(citySheet, j, 5));
                                    proj.ProjectNameEN = ExcelStringValue(citySheet, j, 7);
                                    proj.ProjectNameCN = ExcelStringValue(citySheet, j, 7);
                                    proj.Remark = ExcelStringValue(citySheet, j, 9);
                                    proj.IsFinancial = (citySheet.GetRow(j).GetCell(10) != null);
                                    proj.IsSale = (citySheet.GetRow(j).GetCell(11) != null);
                                    proj.IsPM = (citySheet.GetRow(j).GetCell(12) != null);
                                    proj.Keywords = ExcelStringValue(citySheet, j, 13);
                                    proj.ModifiedBy = CurrentUserName;
                                    proj.ModifiedDt = mdt;
                                    proj.GUID = guid;

                                    //Construct Parent Full Code
                                    if (!string.IsNullOrWhiteSpace(proj.ProjectPhase3))
                                    {
                                        proj.ParentProjectFullCode = proj.CityCode + "-" + proj.ProjectCode + proj.ProjectPhase1 + proj.ProjectPhase2;
                                    }
                                    else if (!string.IsNullOrWhiteSpace(proj.ProjectPhase2))
                                    {
                                        proj.ParentProjectFullCode = proj.CityCode + "-" + proj.ProjectCode + proj.ProjectPhase1;
                                    }
                                    else if (!string.IsNullOrWhiteSpace(proj.ProjectPhase1))
                                    {
                                        proj.ParentProjectFullCode = proj.CityCode + "-" + proj.ProjectCode;
                                    }
                                    projList.Add(proj);
                                }
                            }
                        }
                    }// close loop city
                    resultMessage = ServiceProvider.UploadExcelProcessToTemp(regionList, cityList, projList, guid);
                    //ViewData["MST_PROJ_GUID"] = guid;
                }// close using excel file

                try
                {
                    Directory.Delete(filePath.Substring(0, filePath.LastIndexOf("\\")), true);
                }
                catch (Exception)
                {
                    //if exception throw on delete action, it won't affect the upload excel process.
                }

                return resultMessage;
                //return guid;
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
        }

        private string ExcelStringValue(ISheet sheet, int row, int cell)
        {
            return sheet.GetRow(row).GetCell(cell) == null ? "" : sheet.GetRow(row).GetCell(cell).StringCellValue;
        }
        private string ProjectFullCode(string c, string p, string p1, string p2, string p3)
        {
            string result = "";
            result = c;
            if (p.Length > 0)
            {
                result += "-" + p;
                if (!string.IsNullOrEmpty(p1))
                {
                    result += p1;
                    if (!string.IsNullOrEmpty(p2))
                    {
                        result += p2;
                        if (!string.IsNullOrEmpty(p3))
                            result += p3;
                    }
                }
            }
            return result;
        }

        public ActionResult _ProjectTreePartial(string guid)
        {
            EntityUserGroupTreeExtendedViewModel model = new EntityUserGroupTreeExtendedViewModel();

            model.NodeName = "ROOT";
            model.NodeCode = "Root";
            model.child = PrepareEntityTree(guid);

            return PartialView("_ProjectTreePartial", model);
        }

        //Prepare Entity Tree
        public List<EntityUserGroupTreeExtendedViewModel> PrepareEntityTree(string guid)
        {
            List<EntityUserGroupTreeExtendedViewModel> exceptionList = new List<EntityUserGroupTreeExtendedViewModel>();
            List<EntityUserGroupTreeExtendedViewModel> tree = new List<EntityUserGroupTreeExtendedViewModel>();
            List<MST_PROJ_REGION> regions = this.CommonServiceProvider.GetAllRegion(false, guid);

            foreach (MST_PROJ_REGION region in regions)
            {
                EntityUserGroupTreeExtendedViewModel node = new EntityUserGroupTreeExtendedViewModel()
                {
                    NodeCode = region.RegionCode,
                    NodeName = CultureHelper.GetCurrentCultureFieldLanguage().IsCaseInsensitiveEqual(
                                  DAP.Common.Constants.FIELD_LANGUAGE_CN)
                                  ? region.RegionNameCN : region.RegionNameEN == null ? region.RegionNameCN : region.RegionNameEN,
                    level = 0
                };

                //if (region.MST_PROJ_CITY != null && region.MST_PROJ_CITY.Count() > 0)
                //{
                List<MST_PROJ_CITY> cities = this.CommonServiceProvider.GetAllCityByRegion(false, guid, region.RegionCode);
                foreach (MST_PROJ_CITY c in cities/*region.MST_PROJ_CITY*/)
                    {
                        EntityUserGroupTreeExtendedViewModel citynode = new EntityUserGroupTreeExtendedViewModel()
                        {
                            NodeCode = c.CityCode,
                            NodeName = string.Format("{0} {1}", c.CityCode, CultureHelper.GetCurrentCultureFieldLanguage().IsCaseInsensitiveEqual(
                                  DAP.Common.Constants.FIELD_LANGUAGE_CN)
                                  ? c.CityNameCN : c.CityNameEN == null ? c.CityNameCN : c.CityNameEN),
                            parent = node,
                            level = 1
                        };
                        if (node.child == null)
                            node.child = new List<EntityUserGroupTreeExtendedViewModel>();
                        node.child.Add(citynode);

                        exceptionList.AddRange(PrepareEntityNodeByCity(citynode, c, guid));
                    }

                //}
                tree.Add(node);
            }

            if (exceptionList.Count() > 0)
            {
                EntityUserGroupTreeExtendedViewModel errNode = new EntityUserGroupTreeExtendedViewModel()
                {
                    NodeCode = "ERROR",
                    NodeName = IndexResx.lblException,
                    child = new List<EntityUserGroupTreeExtendedViewModel>()
                };
                errNode.child.AddRange(exceptionList);
                tree.Add(errNode);
            }
            return tree;
        }

        public List<EntityUserGroupTreeExtendedViewModel> PrepareEntityNodeByCity(EntityUserGroupTreeExtendedViewModel parentNode, MST_PROJ_CITY city, string guid)
        {
            List<EntityUserGroupTreeExtendedViewModel> exceptionList = new List<EntityUserGroupTreeExtendedViewModel>();
            List<MST_PROJ_PROJECT> projects = this.CommonServiceProvider.GetProjectsByCity(false, guid, city.CityCode);

            PrepareEntityNodeByProject(parentNode, projects);
            exceptionList = (from p in projects
                             select ViewModelHelper.ConvertProjectToEntityTreeModel(p)).ToList();
            return exceptionList;
        }

        public List<EntityUserGroupTreeExtendedViewModel> PrepareEntityNodeByProject(EntityUserGroupTreeExtendedViewModel parentNode, List<MST_PROJ_PROJECT> projects)
        {
            List<EntityUserGroupTreeExtendedViewModel> exceptionList = new List<EntityUserGroupTreeExtendedViewModel>();
            if (projects.Count() > 0)
            {
                parentNode.child = (from p in projects
                                    where (p.ParentProjectFullCode == null ? p.CityCode.IsCaseInsensitiveEqual(parentNode.NodeCode)
                                       : p.ParentProjectFullCode.IsCaseInsensitiveEqual(parentNode.NodeCode))
                                    select ViewModelHelper.ConvertProjectToEntityTreeModel(p)).ToList();

                projects.RemoveAll(p => p.ParentProjectFullCode == null ? p.CityCode.IsCaseInsensitiveEqual(parentNode.NodeCode)
                    : p.ParentProjectFullCode.IsCaseInsensitiveEqual(parentNode.NodeCode));

                if (parentNode.child.Count() > 0)
                {
                    foreach (EntityUserGroupTreeExtendedViewModel c in parentNode.child)
                    {
                        PrepareEntityNodeByProject(c, projects);
                    }
                }

            }
            return exceptionList;
        }

        public ActionResult UpdateTempToMaster(string guid)
        {
            try { 
                this.CommonServiceProvider.UpdateProjectMasterFromTemp(false, guid);
                return Content("");
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return Content(ex.Message);
            }

        }

    }
}