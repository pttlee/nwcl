﻿using Nwcl.DAP.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.Areas.Main.Models
{
    public class PrivilegeDetailExtendedViewModel : EntityGroupUserGroupRoleViewModel
    {
        public string EntityGroupName { get; set; }

        public string UserGroupName { get; set; }

        public string RoleName { get; set; }

        public string ProjectName { get; set; }

        public string UserName { get; set; }

        public string DashboardName { get; set; }

        public List<DAL.Model.SystemMessage> SystemMessage { get; set; }
    }
}