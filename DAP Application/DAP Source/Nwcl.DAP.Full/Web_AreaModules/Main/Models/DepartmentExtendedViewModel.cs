﻿using Nwcl.DAP.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.Areas.Main.Models
{
    public class DepartmentExtendedViewModel : DepartmentViewModel
    {
        [UIHint("TextBoxGrid")]
        public new string DepartmentName { get; set; }        
        [UIHint("CompanyDropDownGrid")]
        public new int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int CurrentCompanyId { get; set; }
        public string CurrentCompanyName { get; set; }

        public List<DAL.Model.SystemMessage> SystemMessage { get; set; }
    }
}