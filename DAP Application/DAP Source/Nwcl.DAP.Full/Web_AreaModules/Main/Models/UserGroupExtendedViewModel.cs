﻿using Nwcl.DAP.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.Areas.Main.Models
{
    public class UserGroupExtendedViewModel : UserGroupViewModel
    {
        [UIHint("TextBoxGrid")]
        public new string UserGroupName { get; set; }        
        public List<DAL.Model.SystemMessage> SystemMessage { get; set; }
    }
}