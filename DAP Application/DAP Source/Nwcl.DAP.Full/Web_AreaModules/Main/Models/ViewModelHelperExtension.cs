﻿using Nwcl.DAP.DAL.Model;
using Nwcl.DAP.Web.Common;
using Nwcl.DAP.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nwcl.DAP.Application.Service;
using System.DirectoryServices;

namespace Nwcl.DAP.Web.Areas.Main.Models
{
    /// <summary>
    /// Extensions class to extend the ViewModelHelper to provide Entity Model and View Model Conversions for MasterMaintenance Area Module
    /// </summary>
    public static class ViewModelHelperExtensions
    {
        #region Master 
        public static EntityUserGroupTreeExtendedViewModel ConvertProjectToEntityTreeModel(this ViewModelHelper helper, MST_PROJ_PROJECT project)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project", "project is not specified!");
            }

            EntityUserGroupTreeExtendedViewModel vm = new EntityUserGroupTreeExtendedViewModel()
            {
                NodeCode = project.ProjectFullCode,
                NodeName = string.Format("{0} {1} {2}", project.ProjectFullCode, (CultureHelper.GetCurrentCultureFieldLanguage().Equals(
                                  DAP.Common.Constants.FIELD_LANGUAGE_CN)
                                  ? project.ProjectNameCN :
                                  project.ProjectNameEN == null ? project.ProjectNameCN : project.ProjectNameEN).Replace(System.Environment.NewLine, "")
                                  ,((project.Keywords == null || project.Keywords.Replace(System.Environment.NewLine, "").Trim().Length == 0) ? "" 
                                    : ("["+project.Keywords.Replace(System.Environment.NewLine, "")+"]")))
                                  ,
                level = 2
            };

            return vm;
        }

        public static EntityUserGroupTreeExtendedViewModel ConvertProjectToEntityTreeModel(this ViewModelHelper helper, sp_GetEntityGroupTreeView_Result node)
        {
            if (node == null)
            {
                throw new ArgumentNullException("Tree Node", "Tree Node is not specified!");
            }

            EntityUserGroupTreeExtendedViewModel vm = new EntityUserGroupTreeExtendedViewModel()
            {
                NodeCode = node.Code,
                NodeName = CultureHelper.GetCurrentCultureFieldLanguage().Equals(
                                  DAP.Common.Constants.FIELD_LANGUAGE_CN)
                                  ? node.NameCN :
                                  node.NameEN == null ? node.NameCN : node.NameEN,
                level = 2
            };

            return vm;
        }
        #endregion

        #region EntityGroup
        public static PRV_ENT_ENTGROUP ConvertEntityGroupViewModelToEntityGroup(this ViewModelHelper helper, EntityGroupExtendedViewModel entityGroupViewModel)
        {
            if (entityGroupViewModel == null)
            {
                throw new ArgumentNullException("entityGroupViewModel", "Entity Group view model is not specified!");
            }

            PRV_ENT_ENTGROUP entityGroup = new PRV_ENT_ENTGROUP()
            {
                Id = (int)entityGroupViewModel.Id,
                
                EntityGroupName = entityGroupViewModel.EntityGroupName,
                ModifiedBy = entityGroupViewModel.ModifiedBy,
                ModifiedDt = entityGroupViewModel.ModifiedDt
            };

            return entityGroup;
        }
        #endregion

        #region EntityGroupUserGroupRole
        public static PRV_ENT_URG_ROL_RELN ConvertEntityGroupUserGroupRoleViewModelToDALModel(this ViewModelHelper helper, EntityGroupUserGroupRoleExtendedViewModel viewModel)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException("entityGroupUserGroupRoleViewModel", "Entity Group user group role view model is not specified!");
            }

            PRV_ENT_URG_ROL_RELN returnObj = new PRV_ENT_URG_ROL_RELN()
            {
                Id = (int)viewModel.Id,
                EntityGroupId = viewModel.EntityGroupId,
                UserGroupId = viewModel.UserGroupId,
                RoleId = viewModel.RoleId,
                ModifiedBy = viewModel.ModifiedBy,
                ModifiedDt = viewModel.ModifiedDt
            };

            return returnObj;
        }

        public static PRV_ENT_URG_ROL_RELN ConvertUserGroupEntityGroupRoleViewModelToDALModel(this ViewModelHelper helper, UserGroupEntityGroupRoleExtendedViewModel viewModel)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException("UserGroupEntityGroupRoleExtendedViewModel", "Entity Group user group role view model is not specified!");
            }

            PRV_ENT_URG_ROL_RELN returnObj = new PRV_ENT_URG_ROL_RELN()
            {
                Id = (int)viewModel.Id,
                EntityGroupId = viewModel.EntityGroupId,
                UserGroupId = viewModel.UserGroupId,
                RoleId = viewModel.RoleId,
                ModifiedBy = viewModel.ModifiedBy,
                ModifiedDt = viewModel.ModifiedDt
            };

            return returnObj;
        }

        public static PRV_ENT_URG_ROL_RELN ConvertRoleEntityGroupUserGroupViewModelToDALModel(this ViewModelHelper helper, RoleEntityGroupUserGroupExtendedViewModel viewModel)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException("entityGroupUserGroupRoleViewModel", "Entity Group user group role view model is not specified!");
            }

            PRV_ENT_URG_ROL_RELN returnObj = new PRV_ENT_URG_ROL_RELN()
            {
                Id = (int)viewModel.Id,
                EntityGroupId = viewModel.EntityGroupId,
                UserGroupId = viewModel.UserGroupId,
                RoleId = viewModel.RoleId,
                ModifiedBy = viewModel.ModifiedBy,
                ModifiedDt = viewModel.ModifiedDt
            };

            return returnObj;
        }

        public static PRV_ENT_URG_ROL_RELN ConvertPrivilegeViewModelToDALModel(this ViewModelHelper helper, PrivilegeExtendedViewModel viewModel)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException("privilegeViewModel", "Privilege view model is not specified!");
            }

            PRV_ENT_URG_ROL_RELN returnObj = new PRV_ENT_URG_ROL_RELN()
            {
                Id = (int)viewModel.Id,
                EntityGroupId = viewModel.EntityGroupId,
                UserGroupId = viewModel.UserGroupId,
                RoleId = viewModel.RoleId,
                ModifiedBy = viewModel.ModifiedBy,
                ModifiedDt = viewModel.ModifiedDt
            };

            return returnObj;
        }
        #endregion

        #region UserGroup
        public static EntityUserGroupTreeExtendedViewModel ConvertRegionDALToTreeViewModel(this ViewModelHelper helper, MST_PROJ_REGION dal)
        {
            if (dal == null)
            {
                throw new ArgumentNullException("region", "Region DAL is not specified!");
            }

            EntityUserGroupTreeExtendedViewModel vm = new EntityUserGroupTreeExtendedViewModel()
            {
                NodeCode = dal.RegionCode,
                NodeName = CultureHelper.GetCurrentCultureFieldLanguage().Equals(
                                  DAP.Common.Constants.FIELD_LANGUAGE_CN)
                                  ? dal.RegionNameCN :
                                  dal.RegionNameEN == null ? dal.RegionNameCN : dal.RegionNameEN,
                level = 1,
            };

            return vm;
        }

        public static EntityUserGroupTreeExtendedViewModel ConvertCityDALToTreeViewModel(this ViewModelHelper helper, MST_PROJ_CITY dal)
        {
            if (dal == null)
            {
                throw new ArgumentNullException("city", "City DAL is not specified!");
            }

            EntityUserGroupTreeExtendedViewModel vm = new EntityUserGroupTreeExtendedViewModel()
            {
                NodeCode = dal.RegionCode,
                NodeName = CultureHelper.GetCurrentCultureFieldLanguage().Equals(
                                  DAP.Common.Constants.FIELD_LANGUAGE_CN)
                                  ? dal.CityNameCN :
                                  dal.CityNameEN == null ? dal.CityNameCN : dal.CityNameEN,
                level = 2,
            };

            return vm;
        }

        public static EntityUserGroupTreeExtendedViewModel ConvertCompanyDALToTreeViewModel(this ViewModelHelper helper, PRV_URG_COMPANY dal)
        {
            if (dal == null)
            {
                throw new ArgumentNullException("company", "Company DAL is not specified!");
            }

            EntityUserGroupTreeExtendedViewModel vm = new EntityUserGroupTreeExtendedViewModel()
            {
                NodeCode = dal.Id.ToString(),
                NodeName = dal.CompanyName,
                level = 3,
            };

            return vm;
        }

        public static EntityUserGroupTreeExtendedViewModel ConvertDepartmentDALToTreeViewModel(this ViewModelHelper helper, PRV_URG_DEPARTMENT dal)
        {
            if (dal == null)
            {
                //throw new ArgumentNullException("department", "Department DAL is not specified!");
                return null;
            }

            EntityUserGroupTreeExtendedViewModel vm = new EntityUserGroupTreeExtendedViewModel()
            {
                NodeCode = dal.Id.ToString(),
                NodeName = dal.DepartmentName,
                level = 4,
            };

            return vm;
        }

        public static EntityUserGroupTreeExtendedViewModel ConvertUserDALToTreeViewModel(this ViewModelHelper helper, MST_URG_USR dal)
        {
            if (dal == null)
            {
                //throw new ArgumentNullException("user", "User DAL is not specified!");

            }

            EntityUserGroupTreeExtendedViewModel vm = new EntityUserGroupTreeExtendedViewModel()
            {
                NodeCode = dal.ADLogonName,
                NodeName = String.Format("{0} ({1})", dal.ADDisplayName, dal.ADEmail),
                level = 5,
            };

            return vm;
        }

        public static PRV_URG_USRGROUP ConvertUserGroupViewModelToUserGroup(this ViewModelHelper helper, UserGroupExtendedViewModel userGroupViewModel)
        {
            if (userGroupViewModel == null)
            {
                throw new ArgumentNullException("userGroupViewModel", "User Group view model is not specified!");
            }

            PRV_URG_USRGROUP userGroup = new PRV_URG_USRGROUP()
            {
                Id = (int)userGroupViewModel.Id,

                UserGroupName = userGroupViewModel.UserGroupName,
                ModifiedBy = userGroupViewModel.ModifiedBy,
                ModifiedDt = userGroupViewModel.ModifiedDt
            };

            return userGroup;
        }


        #endregion

        
        

        #region Role
        public static PRV_ROL_ROLE_DASHBOARD_RELN ConvertRoleDashboardViewModelToRoleDashboard(this ViewModelHelper helper, RoleDashboardExtendedViewModel roleDashboardViewModel)
        {
            if (roleDashboardViewModel == null)
            {
                throw new ArgumentNullException("roleDashboardViewModel", "Role Dashboard view model is not specified!");
            }

            PRV_ROL_ROLE_DASHBOARD_RELN roleDashboardReln = new PRV_ROL_ROLE_DASHBOARD_RELN()
            {
                Id = (int)roleDashboardViewModel.Id,
                RoleId = roleDashboardViewModel.RoleId,
                DashboardId = roleDashboardViewModel.DashboardId,
                ModifiedBy = roleDashboardViewModel.ModifiedBy,
                ModifiedDt = roleDashboardViewModel.ModifiedDt
            };

            return roleDashboardReln;
        }

        public static PRV_ROL_ROLE ConvertRoleDashboardViewModelToRole(this ViewModelHelper helper, RoleDashboardExtendedViewModel roleDashboardViewModel)
        {
            if (roleDashboardViewModel == null)
            {
                throw new ArgumentNullException("roleDashboardViewModel", "Role Dashboard view model is not specified!");
            }

            PRV_ROL_ROLE role = new PRV_ROL_ROLE()
            {
                Id = (int)roleDashboardViewModel.Id,
                RoleName = roleDashboardViewModel.RoleName,
                ModifiedBy = roleDashboardViewModel.ModifiedBy,
                ModifiedDt = roleDashboardViewModel.ModifiedDt
            };

            return role;
        }

        public static PRV_ROL_ROLE ConvertRoleViewModelToRole(this ViewModelHelper helper, RoleExtendedViewModel viewModel)
        {
            if (viewModel == null)
            {
                throw new ArgumentNullException("RoleExtendedViewModel", "Role view model is not specified!");
            }

            PRV_ROL_ROLE returnObj = new PRV_ROL_ROLE()
            {
                Id = (int)viewModel.Id,
                RoleName = viewModel.RoleName,
                ModifiedBy = viewModel.ModifiedBy,
                ModifiedDt = viewModel.ModifiedDt
            };

            return returnObj;
        }
        #endregion

        public static PRV_URG_DEPARTMENT ConvertDepartmentViewModelToDepartment(this ViewModelHelper helper, DepartmentExtendedViewModel departmentViewModel)
        {
            if (departmentViewModel == null)
            {
                throw new ArgumentNullException("companyViewModel", "Company view model is not specified!");
            }

            PRV_URG_DEPARTMENT department = new PRV_URG_DEPARTMENT()
            {
                Id = (int)departmentViewModel.Id,
                CompanyId = departmentViewModel.CompanyId,
                DepartmentName = departmentViewModel.DepartmentName,
                ModifiedBy = departmentViewModel.ModifiedBy,
                ModifiedDt = departmentViewModel.ModifiedDt
            };

            return department;
        }

        public static PRV_URG_COMPANY ConvertCompanyViewModelToCompany(this ViewModelHelper helper, CompanyExtendedViewModel companyViewModel)
        {
            if (companyViewModel == null)
            {
                throw new ArgumentNullException("companyViewModel", "Company view model is not specified!");
            }

            PRV_URG_COMPANY company = new PRV_URG_COMPANY()
            {
                Id = (int)companyViewModel.Id,
                CityCode = companyViewModel.CityCode,
                CompanyName = companyViewModel.CompanyName,
                ModifiedBy = companyViewModel.ModifiedBy,
                ModifiedDt = companyViewModel.ModifiedDt
            };

            return company;
        }

        #region Department

        public static List<DepartmentUserExtendedViewModel> ConvertResultToViewModelDepartmentUsers(this ViewModelHelper helper, SearchResultCollection result, string domainCode, string modifier)
        {
            try
            {
                List<DepartmentUserExtendedViewModel> userList = new List<DepartmentUserExtendedViewModel>();
 
                if (result.Count > 0)
                {
                    int tmpID = 0;
                    foreach (SearchResult r in result)
                    {
                        tmpID += 1;
                        if (r.Properties["samaccountname"].Count == 0)
                        {
                            continue;
                        }
                        DepartmentUserExtendedViewModel user = new DepartmentUserExtendedViewModel()
                        {
                            ADLogonName = string.Format("{0}\\\\{1}", domainCode.ToLower(), r.Properties["samaccountname"][0]),
                            ADLogonNameDisplay = string.Format("{0}\\{1}", domainCode.ToLower(), r.Properties["samaccountname"][0]),
                            ADDisplayName = r.Properties["displayName"].Count > 0 ? (string)r.Properties["displayName"][0] : "",
                            ADDomain = domainCode.ToLower(),
                            ADRegion = r.Properties["co"].Count > 0 ? (string)r.Properties["co"][0] : "",
                            ADCompany = r.Properties["company"].Count > 0 ? (string)r.Properties["company"][0] : "",
                            ADDepartment = r.Properties["department"].Count > 0 ? (string)r.Properties["department"][0] : "",
                            ADEmail = r.Properties["mail"].Count > 0 ? (string)r.Properties["mail"][0] : "",
                            ADTitle = r.Properties["title"].Count > 0 ? (string)r.Properties["title"][0] : "",
                            Id = tmpID.ToString()
                        };
                        userList.Add(user);
                    }
                }
                return userList;
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException("department", ex);
            }
        }

        #endregion
    }
}