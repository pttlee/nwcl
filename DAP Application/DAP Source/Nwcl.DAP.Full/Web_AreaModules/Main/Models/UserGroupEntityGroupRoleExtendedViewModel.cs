﻿using Nwcl.DAP.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.Areas.Main.Models
{
    public class UserGroupEntityGroupRoleExtendedViewModel : EntityGroupUserGroupRoleViewModel
    {

        [UIHint("EntityGroupDropDownGrid")]
        public new int EntityGroupId { get; set; }
        public int CurrentEntityGroupId { get; set; }
        public string CurrentEntityGroupName { get; set; }

        public string EntityGroupName { get; set; }

        [UIHint("RoleDropDownGrid")]
        public new int RoleId { get; set; }
        public int CurrentRoleId { get; set; }
        public string CurrentRoleName { get; set; }

        public string RoleName { get; set; }

        public List<DAL.Model.SystemMessage> SystemMessage { get; set; }
    }
}