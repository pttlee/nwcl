﻿using Nwcl.DAP.Web.Common.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nwcl.DAP.Web.Areas.Main.Models
{
    public class RoleEntityGroupUserGroupExtendedViewModel : EntityGroupUserGroupRoleViewModel
    {

        [UIHint("UserGroupDropDownGrid")]
        public new int UserGroupId { get; set; }
        public int CurrentUserGroupId { get; set; }
        public string CurrentUserGroupName { get; set; }

        public string UserGroupName { get; set; }

        [UIHint("EntityGroupDropDownGrid")]
        public new int EntityGroupId { get; set; }
        public int CurrentEntityGroupId { get; set; }
        public string CurrentEntityGroupName { get; set; }

        public string EntityGroupName { get; set; }

        public string RoleName { get; set; }

        public List<DAL.Model.SystemMessage> SystemMessage { get; set; }
    }
}