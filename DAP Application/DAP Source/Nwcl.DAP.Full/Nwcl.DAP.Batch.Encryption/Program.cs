﻿using Nwcl.DAP.Application.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Batch.Encryption
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static CommonServiceProvider sp = new CommonServiceProvider();

        static void Main(string[] args)
        {
            log.Info("--------------------Batch job starting--------------------");

            try
            {

                string passPhase = sp.GetParameter("Cryptography", "Config", "PassPhase").First().Value;

                log.Debug("PassPhase: " + passPhase);

                string requestInputMsg = "Please input text to encrypt: ";
                string rawString = "";

                Console.Write(requestInputMsg);

                rawString = Console.ReadLine();

                log.Info("Text to encrypt: " + rawString);

                string encryptedString = CryptographyServiceProvider.EncryptString(rawString, passPhase);

                log.Info("Encrypted result: " + encryptedString);

                Console.WriteLine("");
                Console.WriteLine(string.Format("Encrypted result: {0}", encryptedString));

                #if DEBUG
                string decryptedString = CryptographyServiceProvider.DecryptString(encryptedString, passPhase);
                Console.WriteLine("Decrypted result: " + decryptedString);
                #endif

                Console.WriteLine("Press any key to quit");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
        }
    }
}
