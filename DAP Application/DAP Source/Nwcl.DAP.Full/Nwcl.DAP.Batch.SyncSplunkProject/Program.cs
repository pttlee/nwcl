﻿using Nwcl.DAP.Application.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nwcl.DAP.Batch.SyncSplunkProject
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static CommonServiceProvider sp = new CommonServiceProvider();

        static void Main(string[] args)
        {
            try
            {
                log.Info("--------------------Batch job starting--------------------");
                List<string> projectList = sp.GetProjectListFromSplunk();

                if (projectList.Count() == 0)
                {
                    throw new Exception("Empty project list returned from Splunk");
                }
                log.Info("Start converting project list to DAL Objects");
                List<DAL.Model.INT_SPLUNK_PROJECT> dalList = new List<DAL.Model.INT_SPLUNK_PROJECT>();
                foreach (string project in projectList)
                {
                    log.Debug("Processing project: " + project);
                    DAL.Model.INT_SPLUNK_PROJECT proj = new DAL.Model.INT_SPLUNK_PROJECT()
                    {
                        ProjectFullCode = project,
                        ModifiedBy = "SplunkBatch",
                        ModifiedDt = DateTime.Now
                    };
                    dalList.Add(proj);
                }
                log.Info("Start Updating Splunk Project List to DB");
                sp.UpdateIntSplunkProject(dalList);
                log.Info("--------------------Batch job Completed--------------------");
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
        }
    }
}
